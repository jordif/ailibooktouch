/****************************************************************************
**
** Copyright (C) 2006-2010 SoftTopia. All rights reserved.
**
** This file is part of SoftTopia Software.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** SoftTopia reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef BUILDSETTINGSPAGE_H
#define BUILDSETTINGSPAGE_H
#include "stiwizardpage.h"
#include <QDate>
#include "stappmodule.h"
#include "templateinfo.h"
#include "buildoptions.h"

namespace SPhotoBook
{
	class DesignInfoModel;
	class DesignInfo;
}

class QLabel;
class QToolButton;
class QGroupBox;
class QtSvgToggleSwitch;
class QFormLayout;
class QLineEdit;
class STMonthSelector;
class QtScrollDial;
class QListView;
class QTextBrowser;
class QModelIndex;
class BuildSettingsPage : public STIWizardPage
{
Q_OBJECT
	QLabel* HeaderLabel;
	QLabel* AutoAdjustLabel;
	QLabel* USeBackgroundsLabel;
	QLabel* TitleLabel;
	QLabel* UseTextsLabel;
	QLabel* BWBackgroundsLabel;
	QLabel* AutoImproveLabel;
	QLabel* NumPagesLabel;
	QLabel* FromDateLabel;
	QLabel* ToDateLabel;
	QToolButton* BackBut;
	QToolButton* NextBut;
	QGroupBox* GBMainSettings;
	QGroupBox* GBCalendarSettings;
	QGroupBox* GBPhotoBookSettings;
	QWidget* KeybWidget;
	QLineEdit* LETitle;

	QtSvgToggleSwitch* SAutoAdjust;
	QtSvgToggleSwitch* SUseBackgrounds;
	QtSvgToggleSwitch* SBWBackgrounds;
	QtSvgToggleSwitch* SUseTexts;
	QtSvgToggleSwitch* SAutoImprove;
	QtScrollDial* SDNumPages;

	STMonthSelector* SFromMonth;
	STMonthSelector* SToMonth;

	bool CalSettingsVisible;

	//Presesign
	SPhotoBook::DesignInfoModel* DesignModel;
	SPhotoBook::TemplateInfo MyTemplateInfo;
	QListView* LVDesigns;
	QTextBrowser* TBDescription;
	SPhotoBook::BuildOptions DefaultBuildOptions;


	void showKeyboard(bool _Show);
	void retranslateUi();
	QToolButton* newActionButton(const QString& _Icon);
	void addSwitchRow(QLabel* _Label, QtSvgToggleSwitch* _Switch, QFormLayout* _Layout);

public:
    explicit BuildSettingsPage(QWidget *parent = 0);
	void init();

	bool autoAdjust() const;
	void setAutoAdjust(bool _Value);
	bool useBackgrounds() const;
	void setUseBackgrounds(bool _Value);
	bool useTexts() const;
	void setUseTexts(bool _Value);
	bool bWBackgrounds() const;
	void setBWBackgrounds(bool _Value);
	bool autoImprove() const;
	void setAutoImprove(bool _Value);
	QDate fromDate() const;
	void setFromDate(const QDate& _Date);
	QDate toDate() const;
	void setToDate(const QDate& _Date);
	QString title() const;
	void setTitle(const QString& _Title);
	void setCalendarSettingsVisible(bool _Visible);
	bool calendarSettingsVisible() const;
	void setTemplateInfo(const SPhotoBook::TemplateInfo& _TemplateInfo);
	void setNumPages(int _Value);
	void setIsVariableCalendar(bool _Value);
	void setNumPagesRange(int _From, int _To);
	int numPages() const;
	void setBuildOptions(const SPhotoBook::BuildOptions& _Options);
	SPhotoBook::BuildOptions getBuildOptions() const;
	SPhotoBook::DesignInfo designInfo() const;

signals:

private slots:
	void slotFocusChanged(QWidget* _Old, QWidget* _Now);
	void slotToDateChanged(const QDate& _Date);
	void slotFromDateChanged(const QDate& _Date);
	void slotPredesignChanged(const QModelIndex& _Index);
	void slotHideKeyboard();
};

#endif // BUILDSETTINGSPAGE_H
