/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photobookoutputpage.h"

#include <QLayout> 
#include <QLabel> 
#include <QToolButton> 
#include <QListView>  
#include <QDebug>

#include "publisherdatabase.h"
#include "tpproductlistview.h" 
#include "addremovewidget.h" 
#include "appsettings.h"
#include "printjobmodel.h"
#include "ddocfactory.h"

// Rendering
#include "stutils.h"
#include "sprocessstatuswidget.h"
#include "smessagebox.h"
#include "stimagetools.h"

void PhotoBookOutputPage::retranslateUi()
{
	ButPrevious->setText(tr("Back")); 
	ButPrint->setText(tr("Print"));
	ButStore->setText(tr("Store"));
}

QToolButton* PhotoBookOutputPage::newActionButton(const QIcon& _Icon)
{
	QToolButton* ButRes = new QToolButton(this); 
	ButRes->setIcon(_Icon);
	ButRes->setIconSize(QSize(128, 128));
	ButRes->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
	ButRes->setObjectName("ActionButton"); 
	return ButRes; 
}


void PhotoBookOutputPage::renderAlbum()
{
	if (Model)
	{
		//Export to a temp directory.
		QDir TempDir(QDir::temp().absolutePath() + "/ttpopsphotobookrendertmp");
		if (TempDir.exists())
			STUtils::rmDir(TempDir);
		TempDir.mkdir(TempDir.absolutePath());

		STErrorStack ErrStack;

		AppSettings Settings;
		SPhotoBook::RenderSettings PBRSettings(Settings.photoBookRenderFormat());
		PBRSettings.setCoverRenderFormat(Settings.photoBookCoverRenderFormat());
		PBRSettings.setForPrint(!Settings.sendToPublisherMode());

		StatW->showProgressBar(tr("Building, please wait..."), 100, true);
		QFileInfoList Images = MPhotoBook->exportImages(TempDir.absolutePath(), PBRSettings, ErrStack, StatW);
		if (!ErrStack.isEmpty())
			SMessageBox::warning(this, tr("Error building PhotoBook"), tr("The following errors has occurred during photobook export:\n %1").arg(ErrStack.errorString().join("\n")));
		else
		{
			 //STImageTools::recursiveImagesEntryInfoList(TempDir, true);
			Model->clearAll();
			Model->sourceDocModel()->setDocs(Images);
			editAddProductToAll();
		}
	}
}


PhotoBookOutputPage::PhotoBookOutputPage(QWidget* _Parent): STIWizardPage(_Parent), Model(0), StoreEnabled(false), MPhotoBook(0)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	HeaderLabel = new QLabel(this); 
	HeaderLabel->setObjectName("SummaryHeaderLabel");
	HeaderLabel->setAlignment(Qt::AlignHCenter); 
	MLayout->addWidget(HeaderLabel);

	SummaryLabel = new QLabel(this);
	SummaryLabel->setObjectName("SummaryLabel");
	SummaryLabel->setAlignment(Qt::AlignTop | Qt::AlignHCenter); 
	SummaryLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::MinimumExpanding);
	MLayout->addWidget(SummaryLabel);
	MLayout->setAlignment(SummaryLabel, Qt::AlignCenter);

	StatW = new SProcessStatusWidget(this);
	StatW->setFixedWidth(600);
	MLayout->addWidget(StatW);
	MLayout->setAlignment(StatW, Qt::AlignCenter);

	FrameProdSelect = new QFrame(this);
	MLayout->addWidget(FrameProdSelect);
	MLayout->setAlignment(FrameProdSelect, Qt::AlignCenter);

	QHBoxLayout* PWidgetLayout = new QHBoxLayout(FrameProdSelect);

	LVProducts = new TPProductListView(this); 
	LVProducts->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	PWidgetLayout->addWidget(LVProducts); 

	AddRemoveWidget* ARWidget = new AddRemoveWidget(this); 
	connect(ARWidget, SIGNAL(addClicked()), this, SLOT(editAddProductToAll()));
	connect(ARWidget, SIGNAL(removeClicked()), this, SLOT(editDelProductToAll())); 
	PWidgetLayout->addWidget(ARWidget); 

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));
	
	QGridLayout* ButLayout = new QGridLayout;
	MLayout->addLayout(ButLayout); 

	ButPrevious = newActionButton(QIcon(":/st/tpopsl/previous.png"));
	connect(ButPrevious, SIGNAL(clicked( bool )), this, SIGNAL(previousPage())); 
	ButLayout->addWidget(ButPrevious, 1, 0); 
	
	ButLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred), 0, 1); 

	ButStore = newActionButton(QIcon(":/store.png"));
	connect(ButStore, SIGNAL(clicked( bool )), this, SIGNAL(storePhotoBook())); 
	ButLayout->addWidget(ButStore, 0, 2); 

	ButPrint = newActionButton(QIcon(":/printoutput.png"));
	connect(ButPrint, SIGNAL(clicked( bool )), this, SIGNAL(printPhotoBook())); 
	ButLayout->addWidget(ButPrint, 1, 2); 
	setStoreEnabled(false);
}

void PhotoBookOutputPage::init()
{
	retranslateUi(); 
	setStatus(StatusRendering);
	renderAlbum();
	calcBill();
	setStatus(StatusShowInfo);
}

void PhotoBookOutputPage::setModel(STDom::PrintJobModel* _Model)
{
	Model = _Model; 
	//connect(_Model, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )), this, SLOT(calcBill()));
}

void PhotoBookOutputPage::setProductsModel(QAbstractItemModel* _Model)
{
	LVProducts->setModel(_Model);
}

void PhotoBookOutputPage::calcBill()
{
	if (Model) //Defensive 
	{
		STDom::PublisherDatabase PublDB;
		QString StrBill = PublDB.billRitchText(Model->printJob(), ShippingMethod);
		SummaryLabel->setText(StrBill); 		
	}
}

void PhotoBookOutputPage::setStoreEnabled(bool _Enabled)
{
	StoreEnabled = _Enabled;
	ButStore->setVisible(_Enabled);
}

bool PhotoBookOutputPage::storeEnabled() const
{
	return StoreEnabled;
}

void PhotoBookOutputPage::setStatus(EnStatus _Status)
{
	FrameProdSelect->setVisible(_Status != StatusRendering);
	StatW->setVisible(_Status == StatusRendering);
	SummaryLabel->setVisible(_Status != StatusRendering);
	ButPrevious->setEnabled(_Status != StatusRendering);
	ButPrint->setEnabled(_Status != StatusRendering);
	ButStore->setEnabled(_Status != StatusRendering);

	switch (_Status)
	{
		case StatusRendering :
			HeaderLabel->setText(tr("<h1>Processing...</h1>"));
		break;
		case StatusShowInfo :
			HeaderLabel->setText(tr("<h1>This is your Order Summary.</h1>"));
		break;
	}
}


void PhotoBookOutputPage::editAddProductToAll()
{
	if (Model) //Defensive
	{
		Model->incProductCopiesAll(1, LVProducts->currentProduct());
		calcBill();
	}
}


void PhotoBookOutputPage::editDelProductToAll()
{
	if (Model) //Defensive
	{
		Model->incProductCopiesAll(-1, LVProducts->currentProduct());
		calcBill();
	}
}
