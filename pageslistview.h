/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef PAGESLISTVIEW_H
#define PAGESLISTVIEW_H

#include "sptoolbarlistview.h"

class STPhotoBookThumbnailModel;
class QModelIndex;
class STAlbumWidget;
namespace SPhotoBook
{
	class DocumentViewWidget;
	class Document;
	class PageThumbnailModel;
	class TemplateScene;
};

class PagesListView : public SPToolbarListView
{
Q_OBJECT

public:

private:

	SPhotoBook::PageThumbnailModel* Model;
	SPhotoBook::DocumentViewWidget* PhotoBookView;
	SPhotoBook::Document* PBDocument;

	QAction* InsertPageAction;
	QAction* RemovePageAction;
	QAction* MovePageLeftAction;
	QAction* MovePageRightAction;

	void checkStatus();
	void modifyPhotoBookAction(QAction* _Sender);


public:
	PagesListView(SPhotoBook::DocumentViewWidget* _PhotoBookView, QWidget* _Parent = 0);
	void setDocument(SPhotoBook::Document* _PBDocument);
	void addPage();

protected:
	QSize sizeHint () const;

private slots:
	void slotPageIndexClicked(const QModelIndex& _Index);
	void slotModifyPhotoBookAction();
	void slotPageChange(int _FromPage, int _ToPage);

signals:
	void currentPageRemoved();
};

#endif // PagesListView_H
