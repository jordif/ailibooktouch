/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "formatlistpage.h"
#include <QWidget>
#include <QLayout>
#include <QScrollArea>
#include "mactionlabel.h"

// _________________________________________________________________________*/
//
//   Class ModuleAction
// _________________________________________________________________________*/

FormatAction::FormatAction(const STDom::DDocFormat& _Format, QObject* _Parent )
	: QAction(QIcon(":/formaticon.png"), _Format.description(), _Parent ), Format ( _Format )
{
}


// _________________________________________________________________________*/
//
//   Class FormatListPage
// _________________________________________________________________________*/

FormatListPage::FormatListPage( QWidget* _Parent ) : STIWizardPage ( _Parent ), LabelListWidget ( 0 )
{
	QVBoxLayout* MLayout = new QVBoxLayout ( this );
	MLayout->setMargin ( 2 );

	// Scroll Menu
	MScrArea = new QScrollArea ( this );
	MScrArea->setSizePolicy ( QSizePolicy::Preferred, QSizePolicy::MinimumExpanding );
	MScrArea->setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
	MScrArea->setWidgetResizable ( true );

	MLayout->addWidget ( MScrArea );

	LabelListWidget = new QWidget ( this );
	LabelListWidget->setObjectName ( "LabelListWidget" );
	MScrArea->setWidget ( LabelListWidget );

	QHBoxLayout* LLMainLayout = new QHBoxLayout ( LabelListWidget );
	LLMainLayout->setSizeConstraint ( QLayout::SetMinAndMaxSize );

	FormatsFrame = new QWidget ( LabelListWidget );
	LLMainLayout->addWidget ( FormatsFrame );

	LLModTypeLayout = new QVBoxLayout ( FormatsFrame );
	LLModTypeLayout->setMargin ( 0 );

	//Return to Type Menu Action
	QAction* MainMenuAction = new QAction ( QIcon ( ":/go-previous.png" ), "", this );
	GoBackLabel = new MActionLabel ( MainMenuAction, "", QUrl(), this, false );
	GoBackLabel->setText("<h3>" + tr("Main Menu") + "</h3>");
	GoBackLabel->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
	connect ( MainMenuAction, SIGNAL ( triggered ( bool ) ), this, SIGNAL(previousPage()));
	GoBackLabel->setObjectName ( "MainMenuLabel" );
	GoBackLabel->setTextAlignment ( Qt::AlignCenter );
	MLayout->addWidget ( GoBackLabel );
	MLayout->setAlignment ( GoBackLabel, Qt::AlignHCenter );
}

void FormatListPage::setFormats(const STDom::DDocFormatList& _Formats)
{
	QList<QAction*> ObjList = FormatsFrame->findChildren<QAction*>();
	QList<QAction*>::iterator ait;
	for (ait = ObjList.begin(); ait != ObjList.end(); ++ait)
	{
		delete (*ait);
	}
	QList<MActionLabel*> LabList = FormatsFrame->findChildren<MActionLabel*>();
	QList<MActionLabel*>::iterator lit;
	for (lit = LabList.begin(); lit != LabList.end(); ++lit)
	{
		delete (*lit);
	}

	STDom::DDocFormatList::const_iterator it;
	FormatAction* FirstAction = 0;
	for (it = _Formats.begin(); it != _Formats.end(); ++it)
	{
		FormatAction* NewAction = new FormatAction ( *it, FormatsFrame  );
		if (!FirstAction)
			FirstAction = NewAction;
		connect ( NewAction, SIGNAL ( triggered() ), this, SLOT ( slotFormatSelected() ) );

		MActionLabel* NewLabel = new MActionLabel ( NewAction, "", QUrl(), FormatsFrame );
		LLModTypeLayout->addWidget ( NewLabel );
	}
	if (FirstAction && _Formats.size() == 1)
		emit formatSelected(FirstAction->format());
}

void FormatListPage::init()
{
	//It does nothing!.
}

void FormatListPage::setFromPrevious()
{
	emit previousPage();
}

void FormatListPage::slotFormatSelected()
{
	if (FormatAction* FAction = qobject_cast<FormatAction*>(sender()))
		emit formatSelected(FAction->format());
}
