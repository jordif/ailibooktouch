/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef GETMEDIAPAGE_H
#define GETMEDIAPAGE_H

#include <stiwizardpage.h>
#include <QToolButton>


class CustomPathButton : public QToolButton
{
Q_OBJECT

	QString Path;

public:
	CustomPathButton(const QString& _Label, const QString& _Path, QWidget* _Parent = 0);
	QString path() const;
};

/**
	@author
*/
class QLabel;
class QToolButton; 
class QDir; 
class GetMediaPage : public STIWizardPage
{
Q_OBJECT
	QLabel* MainLabel;
	QToolButton* TButtonMCard; 
	QToolButton* TButtonCD;
	QToolButton* TButtonFacebook;
	QToolButton* ButBack; 
	QToolButton* TBluetooth; 
	bool WritableMediaSelected;

	void clearBluetooth();
	void retranslateUi();
	QToolButton* newMediaToolButton(const QString& _Icon);

public:
	GetMediaPage(QWidget* _Parent);
	~GetMediaPage();
	void init();
	void setFromPrevious();
	void setCDSourceEnabled(bool _Value);
	void setBluetoothSourceEnabled(bool _Value);
	bool writableMediaSelected() const { return WritableMediaSelected; }

public slots: 
	void searchInUsbPen(); 
	void searchInUsbCdRom();
	void searchInBluetooth();
	void selectFacebookNode();
	void slotSelectCustomPathNode();


signals: 
	void mediaSelected(const QDir& _Dir); 

};

#endif
