/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef MAINWIZARD_H
#define MAINWIZARD_H

#include <QSqlRecord> 

#include "stiwizard.h"
#include "sterror.h" 
#include "stcollectionpublishermodel.h"
#include "stappmodule.h"
#include "publisherdatabase.h"
#include "ddoc.h"
#include "stftpordertransfer.h"
#include "producttypeselectionpage.h"
#include "metainfo.h"


/**
Main wizard for TTPOPS.

	@author
*/
class TPOrderSummaryPage;
class TPSendProgressPage;
class GetMediaPage;
class ModuleListPage;
class FormatListPage;
class DigiprintWizard;
class PhotoBookWizard;
class PhotoIndexWizard;
class CDRecordWizard;
class TemplateLoaderWizard;
class MainWizard : public STIWizard
{
Q_OBJECT

public:
	ST_DECLARE_ERRORCLASS();

private: 	
	QAbstractItemModel* ProdModel; 
	TPOrderSummaryPage* SummPage; 
	TPSendProgressPage* ProgPage;
	ProductTypeSelectionPage* PTypePage;
	GetMediaPage* MediaPage;

	DigiprintWizard* DPWizard;
	PhotoBookWizard* PBWizard;
	PhotoIndexWizard* PIWizard;
	CDRecordWizard* CDRWizard; 
	STDom::STFtpOrderTransfer* FtpTrans; //To sync remote publisher data.
	ProductTypeSelectionPage::EnProductType SelectedType;

	STDom::PublisherBill::PublisherShippingMethod ShippingMethod;

	bool commitAccess();
	void commitOrderOnAccess(const STDom::PrintJob& _Job);
	QString storeOrder(const STDom::PrintJob& _Job, const QString& _SenderComment = "", bool _PrintTicket = true);
	QString commitOrder(const STDom::PrintJob& _Job);
	void syncPublisherData();
	SPhotoBook::MetaInfo::EnTemplateType selectedTypeToTemplateType(ProductTypeSelectionPage::EnProductType _Type);

public:
	MainWizard(QWidget* _Parent = 0);
	~MainWizard();
	virtual void init();
	void initPublisher();

private slots: 
	void slotTypeSelected(ProductTypeSelectionPage::EnProductType _Type);
	void mediaSelected(const QDir& _Dir);
	void settingsSelected(); 
	void slotCommitOrder();
	void printTicket(const STDom::PrintJob& _Job, const STDom::PublisherBill::PublisherShippingMethod & _ShippingMethod,
					 const QString& _OrderRef, const QString& _Name = "");
};

#endif
