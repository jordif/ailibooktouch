/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOINDEXWIZARD_H
#define PHOTOINDEXWIZARD_H

#include "stiwizard.h"
#include "sterror.h" 
#include "iorderwizard.h"
#include "printjob.h"
#include "publisherdatabase.h"

/**
Wizars for photoindexs.

	@author 
*/
class STProductPrintsProxyModel; 
class OPhotoCollectionImageModel; 
class QAbstractItemModel; 
class QSqlRecord; 
class SinglePhotoSelPage;
class TPOrderSummaryPage; 
namespace STDom
{
	class PrintJobModel;
};

class PhotoIndexWizard : public STIWizard, public IOrderWizard
{
Q_OBJECT

public:
ST_DECLARE_ERRORCLASS();

private:
	SinglePhotoSelPage* PSPage;
	TPOrderSummaryPage* SummPage;
	STDom::PrintJob MPrintJob;
	void retranslateUi();

public:
	PhotoIndexWizard(QWidget* _Parent);
	void setModel(STProductPrintsProxyModel* _Model);
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value);
	//- IOrderWizard iface
	virtual STDom::PrintJob getPrintJob() const;
	virtual void setImages(const QFileInfoList& _Images, const QDir& _RootDir);

protected:
    virtual void init();

private slots: 
	void renderPhotoIndex();
	void wantsToFinish();

signals: 
	void commitOrder(); 

};

#endif
