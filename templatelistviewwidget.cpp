/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "templatelistviewwidget.h"
#include "pagethumbnailproxymodel.h"
#include "pagethumbnailmodel.h"
#include "stthumbnailview.h"

TemplateListViewWidget::TemplateListViewWidget(QWidget* _Parent) : SPToolbarListView(Qt::Horizontal, _Parent)
{
	TemplateModel = new SPhotoBook::PageThumbnailProxyModel(this);
	TemplateModel->setSourceModel(new SPhotoBook::PageThumbnailModel(this));
	listView()->setViewMode(QListView::IconMode); //In iconmode DropIndicator isn't an image.
	setModel(TemplateModel);
	connect(listView(), SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(slotApplyCurrentTemplate(const QModelIndex&)));
}

void TemplateListViewWidget::slotApplyCurrentTemplate(const QModelIndex& _Index)
{
	emit applyTemplate(TemplateModel->sourceModel()->page(TemplateModel->mapToSource( _Index)));
}

void TemplateListViewWidget::setPages(const SPhotoBook::PageList& _Pages)
{
	TemplateModel->sourceModel()->setPages(_Pages);
}
