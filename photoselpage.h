/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOSELPAGE_H
#define PHOTOSELPAGE_H

#include <stiwizardpage.h>

/**
Page to select photos.

	@author 
*/
class TPPhotoSelWidget; 
class STProductPrintsProxyModel; 
class QAbstractItemModel; 
class QSqlRecord; 
class QDir;
namespace STDom{
	class PrintJobModel;
}
class PhotoSelPage : public STIWizardPage
{
Q_OBJECT

	TPPhotoSelWidget* PSWidget; 

public:
	PhotoSelPage(QWidget *parent = 0);
	~PhotoSelPage();
	void init();
	void setModel(STDom::PrintJobModel* _Model);
	void setRootDir(const QDir& _Rootdir);
	void setProductsModel(QAbstractItemModel* _Model);
	void setImagesPerSheet(int _Value); 
	bool hasChanges() const;
	void setFilterEnabled(bool _Value);

signals: 
	void accepted();

};

#endif
