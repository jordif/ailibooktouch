
QWidget{
   font-size:14px;
}
QSplashScreen{
   font-size:10px;
}

#LabelListWidget{
	background-image: url(:/background_clean.jpg);
}


#STIWizardPage, QDialog{
   background-color: beige;
	background-image: url(:/background.jpg);
	background-position: center;
	background-repeat: repeat-xy;	
}


QScrollArea {
	background-image: url(:/background_clean.jpg);
}

#OWebInfoWidget {
	background-color: white;
}

QDialog#AppSettingsDialog {
   background-color: beige;
	background-image: url(:/background.jpg);
	background-position: center;
	background-repeat: repeat-xy;	
   font-size:16px;
}

/* Nice Windows-XP-style password character. */
QLineEdit[echoMode="2"] {
    lineedit-password-character: 9679;

}


QPushButton {
    background-color: #FFFFFF;
    border-width: 2px;
	border-color: #4c7f00;
    border-style: solid;
    border-radius: 5;
    padding: 0px;
    min-width: 4ex;
    min-height: 4ex;
    font-size:20px;
}

QPushButton:pressed {
    padding-left: 5px;
    padding-top: 5px;
    background-color: #d0d67c;
}


/* We provide a min-width and min-height for push buttons
   so that they look elegant regardless of the width of the text. */
/*QToolButton {
    background-color: palegoldenrod;
    border-width: 4px;
	border-color: #4c7f00;
    border-style: solid;
    border-radius: 27;
    padding: 2px;
    min-width: 5ex;
    min-height: 5ex;
    background: none;
}*/


QToolButton {
	border-width: 1px;
	border-color: black;
    border-style: solid;
    border-radius: 8;
    padding: 0px;
    min-width: 4ex;
    min-height: 4ex;
	background-color: rgba(255, 255, 255, 66%)
}


QToolButton#MediaButton {
	border-width: 2px;
    border-radius: 46;
    min-width: 8em;
    min-height: 15ex;
    font-size:25px;
	 font: bold;
}

QToolButton#ActionButton {
	border-width: 2px;
    border-radius: 26;
    min-width: 6em;
    min-height: 10ex;
    font-size: 18px;
	 font: bold;
}

QToolButton#SmallActionButton {
	border-width: 1px;
	border-radius: 18;
	min-width: 5em;
	min-height: 8ex;
	font-size: 14px;
}


QToolButton#ImageActionButton {
	border-width: 1px;
    border-radius: 8;
    min-width: 4em;
    font-size: 12px;
}


QToolButton#MiniActionButton {
	border-width: 1px;
    border-radius: 6;
    min-width: 1ex;
    min-height: 1ex;
    font-size: 10px;
	 font: bold;
}

QToolButton#ContextBarButton {
    background-color: rgba(255, 255, 255, 100);
    border-radius: 8;
	border-width: 1px;
    min-width: 2ex;
    min-height: 2ex;
    font-size: 10px;
}

QToolBar > QToolButton {
	border-width: 1px;
    border-radius: 20;
    min-width: 8ex;
    min-height: 5ex;
	font-size: 11px;
}

QToolBar#SceneItemToolBar > QToolButton {
    border-width: 1px;
    border-radius: 2;
    min-width: 8px;
    max-width: 16px;
    min-height: 8px;
    max-height: 16px;
    font-size: 8px;
}

QToolBar#PBEditorToolBar > QToolButton {
	border-width: 1px;
    border-radius: 8;
    min-width: 10px;
	/*max-width: 40px;*/
    min-height: 10px;
	max-height: 55px;
	font-size: 9px;
}


QToolButton:hover {
   background-color: #c9d98b;
}

/* Increase the padding, so the text is shifted when the button is
   pressed. */
QToolButton:pressed {
    padding-left: 5px;
    padding-top: 5px;
    background-color: #d0d67c;
}


/* Mark mandatory fields with a brownish color. */
.mandatory {
    color: brown;
}

/* Bold text on status bar looks awful. */
QStatusBar QLabel {
   font: normal;
}

QStatusBar::item {
    border-width: 1;
	border-color: #4c7f00;
    border-style: solid;
    border-radius: 2;
}

QComboBox, QLineEdit, QSpinBox, QTextEdit, QListView {
    background-color: cornsilk;
    selection-color: #0a214c; 
    selection-background-color: #FFB431;
}

QLineEdit#BuildSettingsTitle {
	font-size: 18px;
	min-height: 40px;
}

/* We reserve 1 pixel space in padding. When we get the focus,
   we kill the padding and enlarge the border. This makes the items
   glow. */
QLineEdit {
	border-radius: 2px;
}

QFrame {
	border-width: 0px;
    padding: 1px;
    border-style: solid;
    /*border-color: #F0D48B;*/
	border-color: #FFFFFF;
	border-radius: 2px;
}

QLabel#MainOptionLabel
{
	border-color: #acd900;
/*	background-color: #f8dea0;*/
	background-color: rgba(201, 217, 139, 50%);
	border-width: 2px;
	border-radius: 18px;
	border-style: solid;
	text-align: center;
}

QLabel#MainOptionLabel:hover
{
	background-color: #acd900;
	border-color: #FFFFFF;
	border-width: 2px;
}

QFrame#MainMenuLabel:hover
{
	background-color: #FFFFFF;
}

QFrame#MainMenuLabel
{
	background-color: wheat;
}


QLineEdit#RequiredSetting {
    border-color: #0000FF;
}

/* As mentioned above, eliminate the padding and increase the border. */
QLineEdit:focus, QFrame:focus {
    border-width: 3px;
    padding: 0px;
}

QListView {
	border-style: none;
	border-radius: 5;
	show-decoration-selected: 1;
	font-size: 14px;
}

QListView#ProductsListView {
	border-width: 2;
	border-color: #4c7f00;
	border-style: solid;
	border-radius: 8;
	font-size: 16px;
	font:bold;
	selection-color: white;
	selection-background-color: #4c7f00;
}

StringWheelWidget {
	font-size: 18px;
	font:bold;
}

QListView::item:hover {
	background-color: #4c7f00;
}

/* A QLabel is a QFrame ... */

QLabel {
    border: none;
    padding: 0;
    background: none;
}

QLabel, QToolButton {
    font: bold;
    font: large "Arial";
    font-size:20px;
}


QLabel#SMappedWidgetHeaderLabel {
    font-size:16px;
    font: bold;
}

QLabel#RequiredSetting {
    color: #0000FF;
    font: bold;
}

QLabel#ReceiptLabel {
    font-size: 12px;
	padding: 10px;
	background-image: url(:/pagefold.png);
	border: 1px inset;

	border-radius: 14px;
	background-position: top right;
	background-repeat: no-repeat;
	background-color: rgba(255, 255, 255, 46%)
}

QLabel#RevLabel {
    font-size: 10px;
    font: italic;
}

QLabel#SummaryLabel {
	padding: 10px;
	background-image: url(:/pagefold.png);
	border: 1px inset;
	border-radius: 9px;
	background-position: top right;
	background-repeat: no-repeat;
	background-color: rgba(255, 255, 255, 76%)
}

QLabel#SummaryHeaderLabel, QLabel#ProgPageHeader, QLabel#GetMediaMainLabel, QLabel#SinglePhotoSelHeader,
QLabel#BuildSettingsHeader{
	color: white;
	background-color: #4c7f00;
}

QLabel#ProgMessageLabel {
	padding: 10px;
	border: 2px solid;
	border-color: #4c7f00;
	border-radius: 7px;
	background-color: rgba(255, 255, 255, 76%)
}

QLabel#ConfigPageLabel {
	color: white;
	background-color: #4c7f00;
}

QLabel#WarningLabel {
	color: white;
	background-color: red;
	font-size: 14px;
	font: italic;
}

QProgressBar {
	 border: 3px solid #4c7f00;
	 border-radius: 15px;
	 min-height: 50px;
	 text-align: center;
	 color: black;
	 font: bold;
	 font-size: 14px;
 }

 QProgressBar::chunk {
	 background-color: #4c7f00;
	 width: 20px;
	 margin: 0.5px;
 }

/* A QToolTip is a QLabel ... */
QToolTip {
	border: 2px solid #4c7f00;
    padding: 5px;
    border-radius: 3px;
    opacity: 200;
}

/* Nice to have the background color change when hovered. */
QRadioButton:hover, QCheckBox:hover {
    background-color: wheat;
}

/* Force the dialog's buttons to follow the Windows guidelines. */
QDialogButtonBox {
    button-layout: 0;
}



QScrollBar:horizontal {
	 border: 2px solid #4c7f00;
     background: beige;
     height: 35px;
     margin: 0px 32px 0 32px;
 }
 QScrollBar::handle:horizontal {
	  background: #4c7f00;
		image: url(:/scrollhandle.png);
      min-height: 30px;

 }
 QScrollBar::add-line:horizontal {
	 border: 2px solid #4c7f00;
     background: beige;
     width: 30px;
     subcontrol-position: right;
     subcontrol-origin: margin;
 }

 QScrollBar::sub-line:horizontal {
	 border: 2px solid #4c7f00;
     background: beige;
     width: 30px;
     subcontrol-position: left;
     subcontrol-origin: margin;
 }

QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {
	 border: 2px solid #4c7f00;
     width: 31px;
     height: 28px;
	 background: #4c7f00;
 }

QScrollBar:left-arrow:horizontal:pressed, QScrollBar::right-arrow:horizontal:pressed {
     border: none;
     width: 20px;
     height: 18px;
	 background: #4c7f00;
 }

  QScrollBar::left-arrow:horizontal{
		image: url(:/st/tpopsl/1leftarrow.png);
  }

  QScrollBar::right-arrow:horizontal {
		image: url(:/st/tpopsl/1rightarrow.png);
  }

 QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {
     background: none;
 }



QScrollBar:vertical {
	  border: 2px solid #4c7f00;
      background: beige;
      width: 35px;
      margin: 32px 0 32px 0;
  }
  QScrollBar::handle:vertical {
	  background: #4c7f00;
		image: url(:/scrollhandle.png);
      min-height: 30px;
  }
  QScrollBar::add-line:vertical {
	  border: 2px solid #4c7f00;
      background: beige;
      height: 30px;
      subcontrol-position: bottom;
      subcontrol-origin: margin;
  }

  QScrollBar::sub-line:vertical {
	  border: 2px solid #4c7f00;
      background: beige;
      height: 30px;
      subcontrol-position: top;
      subcontrol-origin: margin;
  }

  QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {
	  border: 2px solid #4c7f00;
      width: 31px;
      height: 28px;
	  background: #4c7f00;
  }

  QScrollBar::up-arrow:vertical:pressed, QScrollBar::down-arrow:vertical:pressed {
      border: none;
	  background: #4c7f00;
      width: 20px;
      height: 18px;
  }


  QScrollBar::up-arrow:vertical{
		image: url(:/st/tpopsl/1uparrow.png);
  }

  QScrollBar::down-arrow:vertical {
		image: url(:/st/tpopsl/1downarrow.png);
  }


  QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
      background: none;
  }

/*  -------****** QSliders ******-------  */

QSlider::groove:horizontal {
	 border: 1px solid #999999;
	 height: 30px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */
	 background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
	 margin: 4px 0;
 }

 QSlider::handle:horizontal {
/*	 background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);*/
	 background: #4c7f00;
	 height: 25px;

	 image: url(:scrollhandle.png);
	 border: 2px solid #5c5c5c;
	 width: 35px;
	 margin: -6px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
	 border-radius: 5px;
 }

/*  -------****** QTabWidget ******-------  */

QTabWidget::pane { /* The tab widget frame */
     border-top: 2px solid #9F7833;
 }

 QTabWidget::tab-bar {
     left: 5px; /* move to the right by 5px */
 }

 /* Style the tab using the tab sub-control. Note that
     it reads QTabBar _not_ QTabWidget */
 QTabBar::tab {
     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
								 stop: 0 #4c7f00, stop: 0.15 #9F7833,
                                 stop: 0.3 #D19E45, stop: 1.0 #EBD896);
     border: 2px solid #EBD896;
     border-bottom-color: #9F7833; /* same as the pane color */
     border-top-left-radius: 4px;
     border-top-right-radius: 4px;
     min-width: 8ex;
     padding: 2px;
 }

 QTabBar::tab:selected, QTabBar::tab:hover {
     background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                 stop: 0 #fafafa, stop: 0.15 #D1D1D1,
                                 stop: 0.3 #e7e7e7, stop: 1.0 cornsilk);
 }

 QTabBar::tab:selected {
     border-color: #FFFFFF;
     border-bottom-color: #EBD896; /* same as pane color */
 }

 QTabBar::tab:!selected {
     margin-top: 2px; /* make non-selected tabs look smaller */
 }

 /* make use of negative margins for overlapping tabs */
 QTabBar::tab:selected {
     /* expand/overlap to the left and right by 4px */
     margin-left: -4px;
     margin-right: -4px;
 }

 QTabBar::tab:first:selected {
     margin-left: 0; /* the first selected tab has nothing to overlap with on the left */
 }

 QTabBar::tab:last:selected {
     margin-right: 0; /* the last selected tab has nothing to overlap with on the right */
 }

 QTabBar::tab:only-one {
     margin: 0; /* if there is only one tab, we don't want overlapping margins */
 }


 QTabBar::scroller { /* the width of the scroll buttons */
     width: 50px;
 }

 QTabBar QToolButton::right-arrow { /* the arrow mark in the tool buttons */
     image: url(:/st/tpopsl/1rightarrow.png);
 }

 QTabBar QToolButton::left-arrow {
     image: url(:/st/tpopsl/1leftarrow.png);
 }

QGroupBox#BuildSettingsGroupBox {
	 background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
									 stop: 0 #919191, stop: 0.15 #FFFFFF,
									 stop: 0.3 #D1D1D1, stop: 1.0 #e7e7e7);
	 border: 2px solid gray;
	 border-radius: 5px;
	 margin-top: 1ex; /* leave space at the top for the title */
	 font: bold;
	 font-size: 16px;
 }

 QGroupBox::title {
	color: white;
	subcontrol-origin: margin;
	subcontrol-position: top center; /* position at the top center */
	padding: 0 3px;
	background-color: #4c7f00;

	/*background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
								   stop: 0 #FFOECE, stop: 1 #FFFFFF);*/
 }

QTreeView#SelectFolderTreeView {
/*	alternate-background-color: yellow;*/
}

QTreeView#SelectFolderTreeView::item {
/*	border: 1px solid #d9d9d9;*/
	border-top-color: transparent;
	border-bottom-color: transparent;
 }

 QTreeView#SelectFolderTreeView::item:hover {
	 background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #e7effd, stop: 1 #cbdaf1);
/*	 border: 1px solid #bfcde4;*/
 }

 QTreeView#SelectFolderTreeView::item:selected {
/*	 border: 1px solid #567dbc;*/
 }

 QTreeView#SelectFolderTreeView::item:selected:active{
	 background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6ea1f1, stop: 1 #567dbc);
 }

 QTreeView#SelectFolderTreeView::item:selected:!active {
	 background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #6b9be8, stop: 1 #577fbf);
 }

QTreeView#SelectFolderTreeView::branch:has-siblings:!adjoins-item {
	 border-image: url(:/stylesheet-vline.png) 0;
 }

 QTreeView#SelectFolderTreeView::branch:has-siblings:adjoins-item {
	 border-image: url(:/stylesheet-branch-more.png) 0;
 }

 QTreeView#SelectFolderTreeView::branch:!has-children:!has-siblings:adjoins-item {
	 border-image: url(:/stylesheet-branch-end.png) 0;
 }

QTreeView#SelectFolderTreeView::branch:has-children:!has-siblings:closed,
 QTreeView#SelectFolderTreeView::branch:closed:has-children:has-siblings {
		 border-image: none;
		 image: url(:/branch-closed.png);
 }

QTreeView#SelectFolderTreeView::branch:open:has-children:!has-siblings,
 QTreeView#SelectFolderTreeView::branch:open:has-children:has-siblings  {
		 border-image: none;
		 image: url(:/branch-open.png);
 }
