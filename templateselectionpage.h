/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef TEMPLATESELECTIONPAGE_H
#define TEMPLATESELECTIONPAGE_H
#include "stiwizardpage.h"
#include <QFileInfo>

#include "templateinfomodel.h"

class QLabel;
class QFrame;
class QWebView;
class QListView;
class QComboBox;
class QActionGroup;
class QToolButton;
class TemplateSelectionPage : public STIWizardPage
{
	Q_OBJECT
	enum EnState
	{
		StateShowWebInfo,
		StateNoInfo,
		StateGettingInfo,
		StateTemplatesEmpty,
		StateUnselected
	};

	SPhotoBook::TemplateInfoModel* Model;
	QListView* View;
	QFileInfo TemplateFileInfo;
	QWebView* WebView;
	QFrame* NoInfoFrame;
	QComboBox* CBSize;
	QLabel* NoInfoLabel;
	QLabel* LabelInfoPixmap;
	QLabel* LabSize;
	QLabel* UnselectedLabel;
	bool InetgetPicture1;
	QTimer* InetgetTimer;
	QActionGroup* TypeActions;
	QFrame* BottomFrame;
	QLabel* NoTemplatesLabel;
	bool HasPreselection;
	SPhotoBook::MetaInfo::EnTemplateType  PreselectedType;
	EnState CurrentState;
	QToolButton* BackBut;
	QToolButton* NextBut;

	QToolButton* newActionButton(const QString& _Icon);
	void setCurrentState(EnState _State);

public:
    explicit TemplateSelectionPage(QWidget *parent = 0);
	void selectFirstIndex();
	void completeChanged();
	void init();
	void setTemplateList(const SPhotoBook::TemplateInfoList& _TemplateList);
	SPhotoBook::TemplateInfo templateInfo(const QModelIndex& _Index, const QSizeF& _Size) const;
	SPhotoBook::TemplateInfo selectedTemplateInfo() const;
	bool isComplete() const;

private slots:
	void slotTemplateIndexClicked(const QModelIndex& );
	void slotWebLoadStarted();
	void slotWebLoadFinished(bool _Error);
	void inetgetBlinkTimeout();

signals:
	void templateSelected();


};

#endif // TEMPLATESELECTIONPAGE_H
