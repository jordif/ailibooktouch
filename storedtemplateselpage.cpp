/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "storedtemplateselpage.h"
#include <QLayout>
#include <QToolButton>
#include <QLabel>
#include <QApplication>
#include "stthumbnailview.h"
#include "checkitemdelegate.h"
#include "smessagebox.h"
#include "stutils.h"
#include "stphotobookcollectionmodel.h"

void StoredTemplateSelPage::retranslateUi()
{
	BackBut->setText(tr("Back"));
	OkBut->setText(tr("Load"));
	LabHeader->setText(tr("<h1>Please select a PhotoBook to load.</h1>"));
}

QToolButton* StoredTemplateSelPage::newActionButton(const QString& _Icon)
{
	QToolButton* Res = new QToolButton(this);
	Res->setObjectName("ActionButton");
	Res->setIcon(QIcon(_Icon));
	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	Res->setIconSize(QSize(64,64));
	return Res;
}

StoredTemplateSelPage::StoredTemplateSelPage(QWidget* _Parent) : STIWizardPage(_Parent)
{

	QVBoxLayout* MLayout = new QVBoxLayout(this);
	MLayout->setMargin(2);
	MLayout->setSpacing(2);
	LabHeader = new QLabel(this);
	LabHeader->setAlignment(Qt::AlignCenter);
	MLayout->addWidget(LabHeader);

	LVImages = new STThumbnailView(this);
	LVImages->setSelectionMode(QAbstractItemView::SingleSelection);
	LVImages->setSelectionBehavior(QAbstractItemView::SelectRows);
	LVImages->setMovement(QListView::Static);
	LVImages->setGridSize(QSize(200, 200));
	CheckItemDelegate* PDelegate = new CheckItemDelegate(LVImages);
	LVImages->setItemDelegate(PDelegate);
	Model = new STPhotoBookCollectionModel(this, false);
	//Model->setThumbnailSize(QSize(128, 128));
	LVImages->setModel(Model);
	MLayout->addWidget(LVImages);

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::Preferred));

	QHBoxLayout* BottomLayout = new QHBoxLayout;
	MLayout->addLayout(BottomLayout);

	BackBut = newActionButton(":/st/tpopsl/previous.png");
	connect(BackBut, SIGNAL(clicked( bool )), this, SIGNAL(previousPage()));
	BottomLayout->addWidget(BackBut);

	OkBut = newActionButton(":/st/tpopsl/accept.png");
	connect(OkBut, SIGNAL(clicked( bool )), this, SIGNAL(nextPage()));
	BottomLayout->addWidget(OkBut);
}

void StoredTemplateSelPage::init()
{
	retranslateUi();

	int NumMediaAccessTries = 0;
	bool FilesFound = false;
	while (NumMediaAccessTries  < 5 && !FilesFound)
	{
		Model->load(true);
		QApplication::processEvents();
		FilesFound = Model->rowCount() > 0;
		if (!FilesFound)
			STUtils::sleep(2000);
		NumMediaAccessTries++;
	}

	if (Model->rowCount() == 0)
	{
		SMessageBox::critical(this, tr("Photobook selection"), tr("Sorry but there are no stored PhotoBooks in this media"));
		emit previousPage();
	}
	else
		LVImages->setCurrentIndex(Model->index(0, 0));
}

void StoredTemplateSelPage::setMediaRootDir(const QDir& _Dir)
{
	Model->setRootDir(_Dir);
}

QString StoredTemplateSelPage::currentPhotoBookPath() const
{
	QString Res;
	QModelIndex CIndex = LVImages->currentIndex();
	if (CIndex.isValid())
		Res = Model->photoBookPath(CIndex);
	return Res;
}
