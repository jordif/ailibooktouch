/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef LEGALPROTPAGE_H
#define LEGALPROTPAGE_H

#include <stiwizardpage.h>

/**
Legal protection page.

	@author
*/
class QToolButton; 
class QLabel; 
class LegalProtPage : public STIWizardPage
{
	Q_OBJECT

	QToolButton* ButBack;
	QToolButton* ButContinue; 
	QLabel* LegalProtLabel; 

	void retranslateUI();

public:
	LegalProtPage ( QWidget* _Parent = 0 );
	void init();
	void setFromPrevious();
};

#endif
