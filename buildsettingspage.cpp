/****************************************************************************
**
** Copyright (C) 2006-2010 SoftTopia. All rights reserved.
**
** This file is part of SoftTopia Software.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** SoftTopia reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "buildsettingspage.h"
#include "QtSvgToggleSwitch"
#include <QDebug>
#include <QLayout>
#include <QFormLayout>
#include <QLabel>
#include <QToolButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QApplication>
#include <QDate>
#include <QDesktopWidget>
#include <QListView>
#include <QTextBrowser>
#include <QDir>

#include "qtscrolldial.h"
#include "gkeyboard2.h"
#include "stmonthselector.h"
#include "designinfomodel.h"
#include "designinfo.h"

void BuildSettingsPage::showKeyboard(bool _Show)
{
	GBMainSettings->setVisible(!_Show);
	GBPhotoBookSettings->setVisible(!_Show);
	if (_Show)
	{
		if (!KeybWidget)
		{
			GKeyboard2* KeyBoard = new GKeyboard2(this);
			connect(KeyBoard, SIGNAL(enterPressed()), this, SLOT(slotHideKeyboard()));
			KeyBoard->setNumericKeyboardVisible(false);
			KeyBoard->setMinimumWidth(900);
			KeybWidget = KeyBoard;
		}
		//KeybWidget->move((width() - KeybWidget->width()) / 2, LETitle->mapToGlobal(LETitle->geometry().topLeft()).y() + LETitle->height() + 2 );
		//qDebug() << LETitle->mapToGlobal(LETitle->geometry().topLeft()).y();
		//qDebug() << LETitle->geometry().topLeft().y();
		KeybWidget->move((width() - KeybWidget->width()) / 2, LETitle->geometry().topLeft().y() + LETitle->height() + 2);
		KeybWidget->show();

	}
	else
	{
		if (KeybWidget)
			KeybWidget->hide();
	}
}

void BuildSettingsPage::retranslateUi()
{
	HeaderLabel->setText(tr("<h2>Please pay attention to build settings</h2>"));
	BackBut->setText(tr("Back"));
	NextBut->setText(tr("Next"));
	GBMainSettings->setTitle(tr("Main"));
	GBCalendarSettings->setTitle(tr("Calendar"));
	GBPhotoBookSettings->setTitle(tr("PhotoBook"));
	TitleLabel->setText(tr("<h2>Assembly title:</h2>"));
	UseTextsLabel->setText(tr("Include text frames"));
	AutoAdjustLabel->setText(tr("Auto adjust images to frames ?"));
	USeBackgroundsLabel->setText(tr("Use images as backgrounds ?"));
	BWBackgroundsLabel->setText(tr("Background images in Black and White"));
	AutoImproveLabel->setText(tr("Auto Improve images"));
	NumPagesLabel->setText(tr("Number of Pages:"));
	FromDateLabel->setText(tr("From Date"));
	ToDateLabel->setText(tr("To Date"));
}

QToolButton* BuildSettingsPage::newActionButton(const QString& _Icon)
{
	QToolButton* Res = new QToolButton(this);
	Res->setObjectName("ActionButton");
	Res->setIcon(QIcon(_Icon));
	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	Res->setIconSize(QSize(64,64));
	return Res;
}

void BuildSettingsPage::addSwitchRow(QLabel* _Label, QtSvgToggleSwitch* _Switch, QFormLayout* _Layout)
{
	_Label->setMinimumHeight(_Switch->sizeHint().height());
	_Label->setAlignment(Qt::AlignVCenter);
	_Layout->addRow(_Label, _Switch);
}

BuildSettingsPage::BuildSettingsPage(QWidget *parent) :
	STIWizardPage(parent), KeybWidget(0), CalSettingsVisible(false)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this);
	HeaderLabel = new QLabel(this);
	HeaderLabel->setObjectName("BuildSettingsHeader");
	HeaderLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
	HeaderLabel->setAlignment(Qt::AlignHCenter);
	MLayout->addWidget(HeaderLabel);

	MLayout->addItem(new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Fixed));

	// --- Design Settings ---

	QGroupBox* GBUsePredesign = new QGroupBox(tr("Select a design"), this);
	GBUsePredesign->setObjectName("BuildSettingsGroupBox");
	MLayout->addWidget(GBUsePredesign);
	GBUsePredesign->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);

	QHBoxLayout* GBLayout = new QHBoxLayout(GBUsePredesign);

	LVDesigns = new QListView(GBUsePredesign);
	LVDesigns->setIconSize(QSize(32, 32));
	LVDesigns->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);

	GBLayout->addWidget(LVDesigns);

	DesignModel = new SPhotoBook::DesignInfoModel(this);
	LVDesigns->setModel(DesignModel);
	connect(LVDesigns->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), this, SLOT(slotPredesignChanged(QModelIndex)));

	TBDescription = new QTextBrowser(this);
	GBLayout->addWidget(TBDescription);
	TBDescription->setHtml("<h1>This is a sample description</h1><p>Here comes the description for each selected item. It depends upon a description that user enters while predesign creation.</p><img src=./automatic.png/>");


	// --- Main Settings ---

	QVBoxLayout* TopLayout = new QVBoxLayout;
	MLayout->addLayout(TopLayout);
	MLayout->setAlignment(TopLayout, Qt::AlignHCenter);

	QHBoxLayout* TopTopLayout = new QHBoxLayout;
	TopLayout->addLayout(TopTopLayout);
	// --- Main Settings
	GBMainSettings = new QGroupBox(this);
	GBMainSettings->setObjectName("BuildSettingsGroupBox");
	GBMainSettings->setAlignment(Qt::AlignCenter);
	GBMainSettings->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	TopTopLayout->addWidget(GBMainSettings);

	QFormLayout* MSettingsLayout = new QFormLayout(GBMainSettings);
	MSettingsLayout->setFormAlignment(Qt::AlignCenter);

	SAutoAdjust = new QtSvgToggleSwitch(GBMainSettings);
	AutoAdjustLabel = new QLabel(this);
	addSwitchRow(AutoAdjustLabel, SAutoAdjust, MSettingsLayout);

	SUseBackgrounds = new QtSvgToggleSwitch(GBMainSettings);
	USeBackgroundsLabel = new QLabel(this);
	addSwitchRow(USeBackgroundsLabel, SUseBackgrounds, MSettingsLayout);

	SBWBackgrounds = new QtSvgToggleSwitch(GBMainSettings);
	BWBackgroundsLabel = new QLabel(this);
	addSwitchRow(BWBackgroundsLabel, SBWBackgrounds, MSettingsLayout);

	SUseTexts = new QtSvgToggleSwitch(GBMainSettings);
	UseTextsLabel = new QLabel(this);
	addSwitchRow(UseTextsLabel, SUseTexts, MSettingsLayout);

	//!TODO: Enable this feature
	SBWBackgrounds->setVisible(false);
	BWBackgroundsLabel->setVisible(false);


	SAutoImprove = new QtSvgToggleSwitch(GBMainSettings);
	AutoImproveLabel = new QLabel(this);
	addSwitchRow(AutoImproveLabel, SAutoImprove, MSettingsLayout);

	//!TODO: Enable this feature
	AutoImproveLabel->setVisible(false);
	SAutoImprove->setVisible(false);


	// --- Calendar Settings ---
	GBCalendarSettings = new QGroupBox(this);
	GBCalendarSettings->setObjectName("BuildSettingsGroupBox");
	//QDesktopWidget Desktop;
	//GBCalendarSettings->setGeometry(Desktop.screenGeometry().width() / 2 - 200, 300, 400, 300);
	GBCalendarSettings->setAlignment(Qt::AlignCenter);
	GBCalendarSettings->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	GBCalendarSettings->setVisible(false);
	//GBCalendarSettings->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	TopTopLayout->addWidget(GBCalendarSettings);

	QGridLayout* CalLayout = new QGridLayout(GBCalendarSettings);

	FromDateLabel = new QLabel(this);
	FromDateLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	CalLayout->addWidget(FromDateLabel, 0, 0);

	SFromMonth = new STMonthSelector(GBCalendarSettings);
	SFromMonth->setDateRange(QDate(1900,1,1), QDate(2100, 1, 1));
	SFromMonth->setDate(QDate(QDate::currentDate().year(), 1, 1));
	CalLayout->addWidget(SFromMonth, 1, 0);
	connect(SFromMonth, SIGNAL(dateChanged(QDate)), this, SLOT(slotFromDateChanged(QDate)));

	ToDateLabel = new QLabel(this);
	ToDateLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	CalLayout->addWidget(ToDateLabel, 0, 1);

	SToMonth = new STMonthSelector(GBCalendarSettings);
	SToMonth->setDateRange(QDate(1900,1,1), QDate(2100, 1, 1));
	SToMonth->setDate(QDate(QDate::currentDate().year(), 12, 1));
	CalLayout->addWidget(SToMonth, 1, 1);
	connect(SToMonth, SIGNAL(dateChanged(QDate)), this, SLOT(slotToDateChanged(QDate)));


	GBPhotoBookSettings = new QGroupBox(this);
	GBPhotoBookSettings->setObjectName("BuildSettingsGroupBox");
	GBPhotoBookSettings->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	GBPhotoBookSettings->setAlignment(Qt::AlignCenter);
	TopTopLayout->addWidget(GBPhotoBookSettings);
	SDNumPages = new QtScrollDial(GBPhotoBookSettings);
	SDNumPages->setRange(0, 100);
	SDNumPages->setValue(10);
	SDNumPages->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	QFormLayout* PBSettingsLayout = new QFormLayout(GBPhotoBookSettings);
	NumPagesLabel = new QLabel(this);
	PBSettingsLayout->addRow(NumPagesLabel, SDNumPages);



	//MLayout->addItem(new QSpacerItem(10, 40, QSizePolicy::Fixed, QSizePolicy::Fixed));

	// --- Title
	QHBoxLayout* TitleLayout = new QHBoxLayout;
	MLayout->addLayout(TitleLayout);
	TitleLabel = new QLabel(this);
	LETitle = new QLineEdit(this);
	LETitle->setObjectName("BuildSettingsTitle");
	TitleLayout->addWidget(TitleLabel, 0, Qt::AlignRight);
	LETitle->setMinimumWidth(400);
	connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)), this, SLOT(slotFocusChanged(QWidget*, QWidget*)));
	TitleLayout->addWidget(LETitle,0, Qt::AlignLeft);



	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));

	// --- Footer ---
	QHBoxLayout* BottomLayout = new QHBoxLayout;
	MLayout->addLayout(BottomLayout);

	BackBut = newActionButton(":/st/tpopsl/previous.png");
	connect(BackBut, SIGNAL(clicked()), this, SIGNAL(previousPage()));
	BottomLayout->addWidget(BackBut);

	BottomLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	NextBut = newActionButton(":/st/tpopsl/next.png");
	connect(NextBut, SIGNAL(clicked()), this, SIGNAL(nextPage()));
	BottomLayout->addWidget(NextBut);
}

void BuildSettingsPage::init()
{
	retranslateUi();
	setAutoImprove(false);
	setTitle("");
}

bool BuildSettingsPage::autoAdjust() const
{
	return SAutoAdjust->isChecked();
}

void BuildSettingsPage::setAutoAdjust(bool _Value)
{
	SAutoAdjust->setChecked(_Value);
}

bool BuildSettingsPage::useBackgrounds() const
{
	return SUseBackgrounds->isChecked();
}

void BuildSettingsPage::setUseBackgrounds(bool _Value)
{
	SUseBackgrounds->setChecked(_Value);
}

bool BuildSettingsPage::useTexts() const
{
	return SUseTexts->isChecked();
}

void BuildSettingsPage::setUseTexts(bool _Value)
{
	SUseTexts->setChecked(_Value);
}

bool BuildSettingsPage::bWBackgrounds() const
{
	return SBWBackgrounds->isChecked();
}

void BuildSettingsPage::setBWBackgrounds(bool _Value)
{
	SBWBackgrounds->setChecked(_Value);;
}

bool BuildSettingsPage::autoImprove() const
{
	return SAutoImprove->isChecked();
}

void BuildSettingsPage::setAutoImprove(bool _Value)
{
	SAutoImprove->setChecked(_Value);
}

QDate BuildSettingsPage::fromDate() const
{
	return SFromMonth->currentDate();
}

void BuildSettingsPage::setFromDate(const QDate& _Date)
{
	SFromMonth->setDate(_Date);
}

QDate BuildSettingsPage::toDate() const
{
	return SToMonth->currentDate();
}

void BuildSettingsPage::setToDate(const QDate& _Date)
{
	SToMonth->setDate(_Date);
}

QString BuildSettingsPage::title() const
{
	return LETitle->text();
}

void BuildSettingsPage::setTitle(const QString& _Title)
{
	LETitle->setText(_Title);
}

void BuildSettingsPage::setCalendarSettingsVisible(bool _Visible)
{
	CalSettingsVisible = _Visible;
	GBCalendarSettings->setVisible(_Visible);
}

bool BuildSettingsPage::calendarSettingsVisible() const
{
	return CalSettingsVisible;
}

void BuildSettingsPage::setTemplateInfo(const SPhotoBook::TemplateInfo& _TemplateInfo)
{
	MyTemplateInfo = _TemplateInfo;
	setCalendarSettingsVisible(false);
	GBPhotoBookSettings->setVisible(false);
	TitleLabel->setVisible(false);
	LETitle->setVisible(false);
	SUseBackgrounds->setVisible(false);
	USeBackgroundsLabel->setVisible(false);
	SUseTexts->setVisible(false);
	UseTextsLabel->setVisible(false);


	setCalendarSettingsVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypeCalendar);
	GBPhotoBookSettings->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	TitleLabel->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	LETitle->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	SUseBackgrounds->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	USeBackgroundsLabel->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	SUseTexts->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);
	UseTextsLabel->setVisible(_TemplateInfo.type() == SPhotoBook::MetaInfo::TypePhotoBook);

	SPhotoBook::BuildOptions InitOpts;
	InitOpts.setDefaults(_TemplateInfo.type(), 1);
	setBuildOptions(InitOpts);

	//Fill predesigns of template.
	//DesignModel->setDesignInfoList(_TemplateInfo.designs());
	DesignModel->setTemplateInfo(_TemplateInfo);
	if (DesignModel->rowCount() > 0)
	{
		LVDesigns->setCurrentIndex(DesignModel->index(0, 0));
	}
}

void BuildSettingsPage::setNumPages(int _Value)
{
	SDNumPages->setValue(_Value);
}

void BuildSettingsPage::setIsVariableCalendar(bool _Value)
{
	if (calendarSettingsVisible())
		setCalendarSettingsVisible(_Value);
}

void BuildSettingsPage::setNumPagesRange(int _From, int _To)
{
	SDNumPages->setRange(_From, _To);
}

int BuildSettingsPage::numPages() const
{
	return SDNumPages->value();
}

void BuildSettingsPage::setBuildOptions(const SPhotoBook::BuildOptions& _Options)
{
	DefaultBuildOptions = _Options;
	setAutoAdjust(_Options.autoadjustFrames());
	setAutoFillBackground(_Options.autoFillBackgrounds());
	//!_Options.ignoreExifRotation() ??;
	setUseTexts( _Options.useTexts());
	setTitle(_Options.title());
	//_Options.subTitle() ??
	//_Options.author() ??
	setNumPages(_Options.pagesToFill());
	setFromDate(_Options.fromDate());
	setToDate(_Options.toDate());
}


SPhotoBook::BuildOptions BuildSettingsPage::getBuildOptions() const
{
	SPhotoBook::BuildOptions Res = DefaultBuildOptions;
	Res.setAutoadjustFrames(autoAdjust());
	Res.setAutoFillBackgrounds(autoFillBackground());
	Res.setIgnoreExifRotation(true);
	Res.setUseTexts(useTexts());
	Res.setTitle(title());
	Res.setSubTitle("");
	Res.setAuthor("");
	Res.setPagesToFill(numPages());
	Res.setFromDate(fromDate());
	Res.setToDate(toDate());
	return Res;
}

SPhotoBook::DesignInfo BuildSettingsPage::designInfo() const
{
	SPhotoBook::DesignInfo Res;
	if (DesignModel->rowCount() > 0)
	{
		QModelIndex CIndex = LVDesigns->currentIndex();
		if (CIndex.isValid())
			Res = DesignModel->designInfo(CIndex);
	}
	return Res;
}


void BuildSettingsPage::slotFocusChanged(QWidget* _Old, QWidget* _Now)
{
	if (_Now == LETitle)
		showKeyboard(true);
	else
	{
		if (KeybWidget)
		{
			if (KeybWidget->isVisible())
				showKeyboard(false);
		}
	}
}

void BuildSettingsPage::slotToDateChanged(const QDate& _Date)
{
	if (_Date < SFromMonth->currentDate().addMonths(1))
		SToMonth->setDate(SFromMonth->currentDate().addMonths(1));
}

void BuildSettingsPage::slotFromDateChanged(const QDate& _Date)
{
	if (_Date >= SToMonth->currentDate())
		SFromMonth->setDate(SToMonth->currentDate().addMonths(-1));
}


void BuildSettingsPage::slotPredesignChanged(const QModelIndex& _Index)
{
	if (_Index.isValid()) //Defensive
	{
		//Lets load the photobook
		SPhotoBook::DesignInfo DInfo = designInfo();
		setNumPagesRange(DInfo.metaInfo().minPages(), DInfo.metaInfo().maxPages());

		//TODO: Move it to global class.
		QString DesignImageFile = QDir(MyTemplateInfo.absolutePath(DInfo)).absoluteFilePath(DInfo.imageFile());
		QFileInfo ImageFInfo(DesignImageFile);

		QString ImageFile;
		if (ImageFInfo.isFile())
			ImageFile = QString(QUrl::fromLocalFile(ImageFInfo.absoluteFilePath()).toEncoded());
		else
			ImageFile = ":/st/wizards/defaultdesign.png";
		TBDescription->setHtml(QString("<h1>%1</h1><p>%2</p><center><img src=""%3""/></center>").arg(DInfo.name()).arg(DInfo.description()).arg(
				ImageFile));
	}
}

void BuildSettingsPage::slotHideKeyboard()
{
	showKeyboard(false);
	GBMainSettings->setFocus();
	//qApp->notify(LVImages, new QKeyEvent(QEvent::KeyPress, Qt::Key_PageDown, Qt::NoModifier));
}
