/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "producttypeselectionpage.h"
#include "stclickablelabel.h"
#include <QSignalMapper>
#include <QToolButton>
#include <QFile>
#include <QDesktopWidget>
#include <QMouseEvent>
#include "flowlayout.h"
#include "sapplication.h"
#include "systemtemplates.h"
#include "ttpopsapp.h"
#include "stburningsettings.h"


MainOptionLabel* ProductTypeSelectionPage::createMainOption(ProductTypeSelectionPage::EnProductType _Type, const QString& _Title,
											 const QString& _Description, const QString& _Image)
{
	MainOptionLabel* NewLabel = new MainOptionLabel(_Type, _Title, _Description, _Image, this);
	connect(NewLabel, SIGNAL(typeSelected(ProductTypeSelectionPage::EnProductType)), this, SIGNAL(typeSelected(ProductTypeSelectionPage::EnProductType)));
	NewLabel->setFixedSize(OptionLabelWidth, 320);
	return NewLabel;

}

QToolButton* ProductTypeSelectionPage::newLocaleButton ( const QString& _Locale, QSignalMapper* _Mapper )
{
	QToolButton* NButton = new QToolButton ( this );
	NButton->setIcon ( QIcon ( ":/locale_"+ _Locale + ".png" ) );
	NButton->setIconSize ( QSize ( 64, 64 ) );
	connect ( NButton, SIGNAL ( clicked ( bool ) ), _Mapper, SLOT ( map() ) );
	_Mapper->setMapping ( NButton, _Locale );
	return NButton;
}

void ProductTypeSelectionPage::deleteMainOptionButtons()
{
	QList<MainOptionLabel*> ObjList = this->findChildren<MainOptionLabel*>();
	QList<MainOptionLabel*>::iterator it;
	for (it = ObjList.begin(); it != ObjList.end(); ++it)
	{
		delete (*it);
	}
}


void ProductTypeSelectionPage::createMainOptionButtons()
{
	STBurningSettings BurnSettings;
	SPhotoBook::TemplateInfoList TInfoList = TtpopsApp::loadTemplates();

	MainOptionsLayout->addWidget(createMainOption(TypeDigitalCopies, tr("Order Prints"),
												  tr("Print your photos in standard format sizes"), ":/digitalphoto-group.png"));

	if (!TInfoList.subList(SPhotoBook::MetaInfo::TypeIdPhoto).isEmpty())
		MainOptionsLayout->addWidget(createMainOption(TypePhotoId, tr("Photo Id"),
												  tr("Make your photo ids"), ":/photoid-subgroup.png"));

	MainOptionsLayout->addWidget(createMainOption(TypePhotoIndex, tr("Photo Index"),
												  tr("Build and index with your photos"), ":/photoindex.png"));
	if (BurnSettings.burningEnabled())
		MainOptionsLayout->addWidget(createMainOption(TypeCDRecord, tr("CD Burning"),
													  tr("Save your photos on CD"), ":/cdrecordmodule.png"));

	if (!TInfoList.subList(SPhotoBook::MetaInfo::TypePhotoBook).isEmpty())
		MainOptionsLayout->addWidget(createMainOption(TypePhotoBook, tr("PhotoBooks"),
												  tr("PhotoBooks, Photo Magazines, with photo quality"), ":/photobook.png"));

	if (!TInfoList.subList(SPhotoBook::MetaInfo::TypeCalendar).isEmpty())
		MainOptionsLayout->addWidget(createMainOption(TypeCalendar, tr("Calendars"),
												  tr("Build a calendar with your photos"), ":/calendars-subgroup.png"));

	if (!TInfoList.subList(SPhotoBook::MetaInfo::TypeCard).isEmpty())
		MainOptionsLayout->addWidget(createMainOption(TypeCard, tr("Decorations"),
												  tr("Use your photos to wish christmas, birthdays, ... "), ":/card.png"));

	if (!TInfoList.subList(SPhotoBook::MetaInfo::TypeMultiPhoto).isEmpty())
		MainOptionsLayout->addWidget(createMainOption(TypeOther, tr("Other"),
												  tr("Other designs with your photos"), ":/other.png"));
}



ProductTypeSelectionPage::ProductTypeSelectionPage(QWidget* _Parent) : STIWizardPage ( _Parent )
{
	QDesktopWidget DesktopWidget;

	QVBoxLayout* MLayout = new QVBoxLayout(this);
	MLayout->setSpacing(10);
	OptionLabelWidth = (DesktopWidget.screenGeometry(DesktopWidget.primaryScreen()).width() - (MLayout->spacing() * 3) - (MLayout->margin() * 2) - 5) / 4;
	MLayout->setMargin(10);
	MainOptionsLayout = new FlowLayout;
	MLayout->addLayout(MainOptionsLayout);

	createMainOptionButtons();

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));

	QHBoxLayout* BottomLayout = new QHBoxLayout;
	MLayout->addLayout(BottomLayout);

	QHBoxLayout* LangLayout = new QHBoxLayout;
	BottomLayout->addLayout ( LangLayout );
	BottomLayout->setAlignment ( LangLayout, Qt::AlignHCenter );
	QSignalMapper* LocaleMapper = new QSignalMapper ( this );

	SApplication::TAppLocaleList LocaleList = SApplication::locales();
	SApplication::TAppLocaleList::iterator it;
	for (it = LocaleList.begin(); it != LocaleList.end(); ++it)
	{
		LangLayout->addWidget ( newLocaleButton ( it->Id, LocaleMapper ) );
	}
	connect ( LocaleMapper, SIGNAL ( mapped ( const QString& ) ), this, SLOT ( slotLocaleSelected ( const QString& ) ) );

	QVBoxLayout* LogoLayout = new QVBoxLayout;
	BottomLayout->addLayout ( LogoLayout );

	QLabel* LogoLabel = new QLabel ( this );

	QString PublisherLogoFilePath = QCoreApplication::applicationDirPath() + "/publisherlogo.png";
	if (QFile::exists(PublisherLogoFilePath))
	{
		QLabel* PubLogoLabel = new QLabel(this);
		PubLogoLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
		PubLogoLabel->setPixmap(QPixmap(PublisherLogoFilePath));
		BottomLayout->addWidget(PubLogoLabel);
		BottomLayout->setAlignment(PubLogoLabel, Qt::AlignTop);
		LogoLabel->setPixmap(QPixmap( ":/logo_starblitz.png" ));
	}
	else
		LogoLabel->setPixmap(QPixmap( ":/logo_starblitz-km.png" ));

	LogoLabel->setAlignment ( Qt::AlignRight );
	LogoLayout->addWidget ( LogoLabel );

	QLabel* RevLabel = new QLabel ( SApplication::fullApplicationName(), this );
	RevLabel->setObjectName ( "RevLabel" );
	RevLabel->setAlignment ( Qt::AlignRight );
	LogoLayout->addWidget ( RevLabel );

}

void ProductTypeSelectionPage::init()
{
}

void ProductTypeSelectionPage::reloadMainOptions()
{
	deleteMainOptionButtons();
	createMainOptionButtons();

}

void ProductTypeSelectionPage::retranslateUi()
{
	reloadMainOptions();
}

void ProductTypeSelectionPage::mousePressEvent ( QMouseEvent* _Event )
{
	QDesktopWidget Desktop;
	if ( _Event->y() > Desktop.size().height() - 40 && _Event->x() < 40 )
	{
		emit settingsSelected();
	}
}


void ProductTypeSelectionPage::slotLocaleSelected ( const QString& _Locale )
{
	SApplication::setLocale ( _Locale );
	retranslateUi();
}


//-----------------------------------------------------------------------------
// Class MainOptionLabel
//-----------------------------------------------------------------------------

MainOptionLabel::MainOptionLabel(ProductTypeSelectionPage::EnProductType _Type, const QString& _Title,
								 const QString& _Description, const QString& _Image, QWidget* _Parent) : MyType(_Type), STClickableLabel(_Parent)
{
	setText(QString("<center><img src=%1 />"
						"<h2>%2</h2>"
						"<p>%3</p></center>"
						"").arg(_Image).arg(_Title).arg(_Description));
	setWordWrap(true);
	setObjectName("MainOptionLabel");
	connect(this, SIGNAL(clicked()), this, SLOT(slotClicked()));
}

void MainOptionLabel::slotClicked()
{
	emit typeSelected(MyType);
}
