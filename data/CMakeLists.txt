#Data files for ttpops

IF(WIN32 AND NOT UNIX)
	INSTALL(FILES 
                ${ailibooktouch_SOURCE_DIR}/images/appicon.ico
	DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir})
ELSE(WIN32 AND NOT UNIX)
	INSTALL(FILES 
                ${ailibooktouch_SOURCE_DIR}/images/appicon.png
	DESTINATION share/icons/hicolor/64x64/apps/
	RENAME ailibooktouch.png )
	INSTALL(DIRECTORY ${ailibooktouch_SOURCE_DIR}/kde4/applications DESTINATION share)
	INSTALL(DIRECTORY ${ailibooktouch_SOURCE_DIR}/kde4/menu DESTINATION share)
	INSTALL(FILES 
		${ailibooktouch_SOURCE_DIR}/scripts/runailibooktouch
	DESTINATION bin PERMISSIONS WORLD_EXECUTE WORLD_READ OWNER_EXECUTE OWNER_WRITE OWNER_READ
                      GROUP_EXECUTE GROUP_READ)

ENDIF(WIN32 AND NOT UNIX)


# ---------------- Publishers --------------------

SET(TTPOPS_PUBLISHER_LOGO "" CACHE FILEPATH
	"Publisher logo image.")

INSTALL(FILES 
	${TTPOPS_PUBLISHER_LOGO}
DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir}
RENAME publisherlogo.png)


SET(PUBLISHERSDEFAULTDB_FILE "" CACHE FILEPATH
                "Publishers default database file.")

IF(APPLE)
        INSTALL(FILES ${PUBLISHERSDEFAULTDB_FILE}  DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir}/share/st
                RENAME publisher.db )
ELSE(APPLE)
        INSTALL(FILES ${PUBLISHERSDEFAULTDB_FILE}  DESTINATION share/st
                RENAME publisher.db )
ENDIF(APPLE)


# ---------------- Docs --------------------

INSTALL(DIRECTORY ./docs/ DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir}/docs
	PATTERN ".git" EXCLUDE )

# ---------------- Reports --------------------

INSTALL(DIRECTORY ./reports/ DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir}/reports
	PATTERN ".git" EXCLUDE )

# ---------------- Templates --------------------

SET(TEMPLATES_PATH "./templates_base/" CACHE STRING
		"Templates directory.")

INSTALL(DIRECTORY ${TEMPLATES_PATH} DESTINATION share/st/templates/default
        PATTERN ".git" EXCLUDE )



# ---------------- CDRecord --------------------
IF(WIN32 AND NOT UNIX)
	SET ( CDRECORDBIN_PATH "../../installer/windows/files/depbin" CACHE STRING
	"CDRecord binaries path")
	FILE(GLOB depcdrecordfiles "${CDRECORDBIN_PATH}/*.*")
	INSTALL(FILES ${depcdrecordfiles} DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir})
ENDIF(WIN32 AND NOT UNIX)

# ---------------- Translations --------------------

install(FILES ${QM_FILES} DESTINATION ${${CMAKE_PROJECT_NAME}bin_dest_dir})

#In apple we need to install qt system translations
IF (APPLE)
		SET ( QTLIBS_PATH "/Developer/Applications/Qt/" )
		SET ( SYS_TRANSLATIONS_DESTPATH "${${CMAKE_PROJECT_NAME}bin_dest_dir}/../translations/" )
		INSTALL(FILES ${QTLIBS_PATH}/translations/qt_es.qm DESTINATION ${SYS_TRANSLATIONS_DESTPATH})
		INSTALL(FILES ${QTLIBS_PATH}/translations/qt_fr.qm DESTINATION ${SYS_TRANSLATIONS_DESTPATH})
ENDIF(APPLE)
