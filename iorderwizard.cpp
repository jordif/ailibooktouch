/****************************************************************************
**
** Copyright (C) 2006-2010 SoftTopia. All rights reserved.
**
** This file is part of SoftTopia Software.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** SoftTopia reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "iorderwizard.h"
#include "ttpopsapp.h"
#include "remotepublishermanager.h"

IOrderWizard::IOrderWizard() : ProductsModel(0)
{
}

QAbstractItemModel* IOrderWizard::newProductsModel(QObject* _Parent, STDom::PublisherDatabase::EnProductType _ProductType, const QString& _TemplateName)
{
	STDom::PublisherDatabase PubDatabase = pManager->getProducts();
	Assert(PubDatabase.open(), Error(QString(QObject::tr("Could not open publisher database file %1")).arg(pManager->publisherDatabaseFilePath())));
	if (ProductsModel)
		delete ProductsModel;
	if (_TemplateName.isEmpty())
		ProductsModel = PubDatabase.newProductsModel(_Parent, _ProductType);
	else
		ProductsModel = PubDatabase.newProductsTemplateModel(_Parent, _ProductType, _TemplateName);
	PubDatabase.close();
	return ProductsModel;
}
