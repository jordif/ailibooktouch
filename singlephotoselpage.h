/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef SINGLEPHOTOSELPAGE_H
#define SINGLEPHOTOSELPAGE_H

#include "stiwizardpage.h"
#include <QDir> 

/**
Single image selection page.

	@author
*/
namespace STDom
{
	class PrintJobModel;
}
class STThumbnailView; 
class QToolButton; 
class QVBoxLayout; 
class QLabel;
class TPPhotoEditor;
class TPProductListView;
class QAbstractItemModel;
class STNumQualityGauge;
namespace STDom{
	class DDocProduct;
}
class SinglePhotoSelPage : public STIWizardPage
{	
Q_OBJECT 

	STThumbnailView* LVImages;
	QToolButton* BackBut;
	QToolButton* ZoomBut;
	QToolButton* SelectAllBut;
	QToolButton* SelectNoneBut; 
	QToolButton* FilterBut; 
	QToolButton* NoFilterBut; 
	QToolButton* OkBut;
	STDom::PrintJobModel* Model;
	QDir RootDir;
	QLabel* HeaderLabel;
	TPPhotoEditor* PEditor;
	TPProductListView* LVProducts;
	STNumQualityGauge* NumImagesGauge;

	void retranslateUi();
	QToolButton* newActionButton(const QString& _Icon);
	QToolButton* newImageActionButton(const QString& _Icon);

protected: 
	QVBoxLayout* MLayout;

public:
	SinglePhotoSelPage(QWidget *parent = 0);
	void init();
	QFileInfoList selectedImages() const;
	void setImages(const QFileInfoList& _Images);

	//! if we set this model widget will show a list to select desired product.
	void setProductsModel(QAbstractItemModel* _Model);
	void setRootDir(const QDir& _Dir) { RootDir = _Dir; }
	STDom::DDocProduct currentProduct() const;
	void setHeaderText(const QString& _Text);	
	void setOptimalNumImagesRange(int _From, int _To);
	bool hasChanges();


private slots: 
	void getFolderFilter();
	void clearFolderFilter();
	void updateGauge();
	void slotOkClicked();
	void editCurrent();
	void slotNextPage();
	void slotPreviousPage();

signals: 
	void accepted();
	void backButtonClicked();
};

#endif
