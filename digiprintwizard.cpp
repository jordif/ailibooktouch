/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "digiprintwizard.h"
#include "photoselpage.h"
#include "tpordersummarypage.h" 

#include "smessagebox.h" 
#include "dimagedoc.h"
#include "printjobmodel.h"
#include "ddocmodel.h"
#include "ttpopsapp.h"
#include "remotepublishermanager.h"

DigiprintWizard::DigiprintWizard(QWidget* _Parent): STIWizard(_Parent)
{
	Model = new STDom::PrintJobModel(this);
	STDom::DDocModel* MDocModel = new STDom::DDocModel(this);
	MDocModel->setNoImagePixmap(QPixmap(":/hourglass.png"));
	Model->setSourceModel(MDocModel);
	Model->setRitchTextDisplay(true);
	Model->setThumbnailSize(STDom::DImageDoc::thumbnailSize());

	PSPage = new PhotoSelPage(this);
	PSPage->setModel(Model);
	addPage(PSPage); 
	connect(PSPage, SIGNAL(previousPage()), this, SLOT(wantsToFinish())); 
	connect(PSPage, SIGNAL(accepted()), this, SLOT(slotShowSummary()));

	SummPage = new TPOrderSummaryPage(this); 
	connect(SummPage, SIGNAL(orderAccepted()), this, SIGNAL(commitOrder())); 
	connect(SummPage, SIGNAL(orderCanceled()), this, SIGNAL(finished())); 
	addPage(SummPage); 

}

void DigiprintWizard::setProductsModel(QAbstractItemModel* _Model)
{
	PSPage->setProductsModel(_Model); 
}

void DigiprintWizard::setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod& _Value)
{
	SummPage->setShippingMethod(_Value); 
}

void DigiprintWizard::init()
{
	Model->clearAll();
	setProductsModel(newProductsModel(this, STDom::PublisherDatabase::DigiprintsProduct));
	firstPage();
}

void DigiprintWizard::wantsToFinish()
{
	bool Finish = true; 
	if (PSPage->hasChanges())
		Finish = SMessageBox::question(this, tr("Order cancel question"), tr("You'll loose all the changes. Cancel anyway ?")) == QMessageBox::Yes;

	if (Finish)
		emit previousWizard(); 
}

STDom::PrintJob DigiprintWizard::getPrintJob() const
{
	return Model->printJob();
}

void DigiprintWizard::setImages(const QFileInfoList& _Images, const QDir& _RootDir)
{
	Model->sourceDocModel()->setDocs(_Images);
	PSPage->setRootDir(_RootDir);
}

void DigiprintWizard::slotShowSummary()
{
	SummPage->setPrintJob(getPrintJob());
	nextPage();
}
