/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef CDRECORDPAGE_H
#define CDRECORDPAGE_H

#include "stiwizardpage.h"
#include <QFileInfoList>
#include "sterror.h" 
#include "sterrorstack.h"


/**
This page shows CD Burning progress.

	@author
*/
class STMkIsoFs; 
class STCdRecord;
class QLabel; 
class SProcessStatusWidget; 
class STProductPrintsProxyModel; 
class QToolButton; 
class QDir;
class QProgressBar;
class CDRecordPage : public STIWizardPage
{
	Q_OBJECT

public:

	ST_DECLARE_ERRORCLASS();

	enum EnState
	{
		StateInit, 
		StateMakeIso,
		StateBurning,
		StateBurnedOk,
		StateError, 
	};

private:
	STMkIsoFs* PIsoFs;
	STCdRecord* PCdRecord;
	QLabel* MainLabel;
	SProcessStatusWidget* PrepCommandStatus;
	SProcessStatusWidget* MKisoFsStatus;
	SProcessStatusWidget* CdRecordStatus;
	QToolButton* BackButton; 
	QToolButton* OKButton;
	STErrorStack ErrorStack;
	bool Cancelled;
	bool BurnOk; 
	
	QString volumeDirName(int _NumVolume) const;
	int storeImagesOnDisk(const QFileInfoList& _Files, const QDir& _DestDir, int _VolumeSize, QProgressBar* _ProgBar);
	void retranslateUi();
	QToolButton* newActionButton(const QIcon& _Icon);

public:
	CDRecordPage(QWidget* _Parent = 0);
	void setState(EnState _State);
	void setError(const QString& _Error);
	void finish();
	virtual void init();
	void burn(const QFileInfoList& _Files, int _NCopies);
	bool dataFitOnCD();
	void getCD(int _NumVolumes, int _CurrentVolume, int _NCopy = 1);

private slots: 
	void slotBack();
	void slotBurnOK();

signals: 
	void aborted(); 
	void burned(); 
};

#endif
