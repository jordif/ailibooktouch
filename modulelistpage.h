/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef MODULELISTPAGE_H
#define MODULELISTPAGE_H

#include "stiwizardpage.h"
#include <QAction> 
#include "mactionlabel.h"
#include "stappmodule.h"


/**
	@author jordif.starblitz@gmail.com
*/
class ModuleAction : public QAction
{
	Q_OBJECT

	STAppModule Module; 

public:
	ModuleAction(const STAppModule& _Module, QObject* _Parent); 
	STAppModule module() const { return Module; }
};

/**
	@author jordif.starblitz@gmail.com
*/
class ModuleGroupAction : public QAction
{
	Q_OBJECT
	QString Description; 
	STAppModule::EnModuleGroup ModuleGroup; 

	static QIcon moduleGroupIcon(STAppModule::EnModuleGroup _ModuleGroup);

public:
	ModuleGroupAction(STAppModule::EnModuleGroup _ModuleGroup, QObject* _Parent); 
	QString description() const { return Description; }
	STAppModule::EnModuleGroup moduleGroup() const { return ModuleGroup; }
};

/**
	@author jordif.starblitz@gmail.com
*/
class ModuleSubGroupAction : public QAction
{
	Q_OBJECT
	QString Description;
	STAppModule::EnModuleSubGroup ModuleSubGroup;

	static QIcon moduleSubGroupIcon(STAppModule::EnModuleSubGroup _ModuleGroup);

public:
	ModuleSubGroupAction(STAppModule::EnModuleSubGroup _ModuleSubGroup, QObject* _Parent);
	QString description() const { return Description; }
	STAppModule::EnModuleSubGroup moduleSubGroup() const { return ModuleSubGroup; }
};


/**
	@author jordif.starblitz@gmail.com
*/
class ModuleLabel : public MActionLabel
{
	Q_OBJECT
	STAppModule::EnModuleGroup ModuleGroup; 
	STAppModule::EnModuleSubGroup ModuleSubGroup;

public:
	ModuleLabel(ModuleAction* _Action, QWidget* _Parent);
	STAppModule::EnModuleGroup moduleGroup() const { return ModuleGroup; }
	STAppModule::EnModuleSubGroup moduleSubGroup() const { return ModuleSubGroup; }

};

/**
	@author jordif.starblitz@gmail.com
*/
class ModuleGroupLabel : public MActionLabel
{
	Q_OBJECT
	bool IsSubGroup;

public:
	ModuleGroupLabel(ModuleGroupAction* _Action, QWidget* _Parent);
	ModuleGroupLabel(ModuleSubGroupAction* _Action, QWidget* _Parent);
	bool isSubGroup() const { return IsSubGroup; }
};


/**
	@author jordif.starblitz@gmail.com
*/
class QVBoxLayout;
class QScrollArea;
class QToolButton; 
class QSignalMapper; 
class QHBoxLayout;
class ModuleListPage : public STIWizardPage
{
	Q_OBJECT

	STAppModuleList ModuleList; 
	QWidget* LabelListWidget;
	QVBoxLayout* LLWLayout;
	QVBoxLayout* LLModTypeLayout;
	QScrollArea* MScrArea; 
	QWidget* ModuleTypeFrame; 
	QWidget* ModuleFrame; 
	QWidget* BottomFrame;
	MActionLabel* MMenuLabel;
	MActionLabel* LoadPhotoBookLabel;
	bool MenuEffectEnabled;
	QToolButton* DAButton;
	

	void filterModules(STAppModule::EnModuleGroup _ModuleGroup);
	void filterModules ( STAppModule::EnModuleSubGroup _ModuleSubGroup );
	void showSubGroups(bool _Value);
	void createModuleButtons();
	QToolButton* newLocaleButton(const QString& _Locale, QSignalMapper* _Mapper);
	void deleteModuleButtons();
	void retranslateUi();
	void runEject(); 
	void showModulesMenu();
	bool loadModulesFromTemplates();

public:
	ModuleListPage ( QWidget* _Parent );
	~ModuleListPage();
	void addBasicModules();
	void init();
	void setFromPrevious();
	void setMenuEffectEnabled(bool _Enabled) { MenuEffectEnabled = _Enabled; }
	STAppModuleList moduleList() const { return ModuleList; }

private slots: 
	void slotModuleActionTriggered();
	void slotLoadPhotoBook();
	void slotShowUrlInfo(const QUrl& _Url);
	void localeSelected(const QString& _Locale);
	void slotShowMainMenu();
	void slotModuleGroupSelected();
	void slotModuleSubGroupSelected();
	void slotDirectAction();

protected:
	void mousePressEvent(QMouseEvent* _Event);
	
signals: 
	void moduleTriggered(const STAppModule& );
	void settingsSelected(); 
	void loadPhotoBook();

};

#endif
