/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef TEMPLATELISTVIEWWIDGET_H
#define TEMPLATELISTVIEWWIDGET_H

#include "sptoolbarlistview.h"

namespace SPhotoBook
{
	 class PageThumbnailProxyModel;
	 class PageList;
	 class TemplateScene;
}
class QModelIndex;
class TemplateListViewWidget : public SPToolbarListView
{
	Q_OBJECT

	SPhotoBook::PageThumbnailProxyModel* TemplateModel;

public:
	TemplateListViewWidget(QWidget* _Parent = 0);
	void setPages(const SPhotoBook::PageList& _Pages);

private slots:
	void slotApplyCurrentTemplate(const QModelIndex& _Index);

signals:
	void applyTemplate(SPhotoBook::TemplateScene* _Template);
};

#endif // TEMPLATELISTVIEWWIDGET_H
