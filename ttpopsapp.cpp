/****************************************************************************
**
** Copyright (C) 2010-2011 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2011
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "ttpopsapp.h"
#include <QPlastiqueStyle>

#ifdef Q_OS_MAC
#include <QMacStyle>
#endif

#include <QTextStream>
#include <QFile>

#include "remotepublishermanager.h"
#include "starlabmanager.h"
#include "publisher.h"
#include "appsettings.h"
#include "systemtemplates.h"

RemotePublisherManager* TtpopsApp::PubManager = 0;


void TtpopsApp::setStyleSheet()
{
	QFile StyleFile(":/ttpops_style.txt");
	if (!StyleFile.open(QIODevice::ReadOnly | QIODevice::Text))
		qWarning(QApplication::tr("Could not find ttpops_style.txt file").toLatin1());

	QTextStream In(&StyleFile);
	QString StyleStr = In.readAll();
	qApp->setStyleSheet(StyleStr);
}


void TtpopsApp::init(const QString& _StarlabPath)
{
#ifdef Q_OS_MAC
	setStyle(new QMacStyle);
#else
	setStyle(new QPlastiqueStyle);
#endif
	setWindowIcon(QIcon(":/appicon.png"));
	setStyleSheet();

	PubManager = new RemotePublisherManager(new StarlabManager(STARLAB_URL, _StarlabPath, this), publisherDatabaseLocalFile());
}

TtpopsApp::TtpopsApp(const QString& _StarlabPath, const QString& _StarlabUser, const QString& _StarlabPassword, int & argc, char ** argv)
	: SApplication(argc, argv, "AiliPhotobookTouch", PRJ_VERSION_MAJOR, PRJ_VERSION_MINOR, PRJ_VERSION_PATCH, QString(" - %1  Edition - (%2)").arg(CURRENT_EDITION_NAME).arg(RELEASE_CODENAME))
{
	STDom::Publisher::DefaultUserName = _StarlabUser;
	STDom::Publisher::DefaultUserPassword = _StarlabPassword;
	init(_StarlabPath);
}

TtpopsApp::TtpopsApp(int & argc, char ** argv)
	: SApplication(argc, argv, "AiliPhotobookTouch", PRJ_VERSION_MAJOR, PRJ_VERSION_MINOR, PRJ_VERSION_PATCH, QString(" - %1  Edition - (%2)").arg(CURRENT_EDITION_NAME).arg(RELEASE_CODENAME))
{
	PubManager = new RemotePublisherManager(new StarlabManager(STARLAB_URL, PUBLISHER_PATH, this), publisherDatabaseLocalFile());

	init(PUBLISHER_PATH);
}

SPhotoBook::TemplateInfoList TtpopsApp::loadTemplates()
{
	AppSettings Settings;
	return SPhotoBook::SystemTemplates::load(Settings.templatesManuallyInstall());
}
