/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photobooklistviewtabwidget.h"
#include <QListView> 
#include <QApplication> 
#include <QScrollArea>

#include "resourcelistview.h"
#include "stthumbnailview.h"
#include "pageslistview.h"
#include "templatepaths.h"
#include "document.h"
#include "templatelistviewwidget.h"


PhotoBookListViewTabWidget::PhotoBookListViewTabWidget(SPhotoBook::DocumentViewWidget* _AlbumWidget, QWidget *parent)
 : QTabWidget(parent), MPhotoBook(0)
{
	setIconSize(QSize(32,32)); 

	//TODO: Create a LayoutListView !!!
	// Layouts
	TemplatesView = new TemplateListViewWidget(this);
	connect(TemplatesView, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )),
			this, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )));
	setTabToolTip(addTab(TemplatesView, QIcon(":/layout.png"), tr("Layouts")), tr("Layouts"));

	CoversView = new TemplateListViewWidget(this);
	connect(CoversView, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )),
			this, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )));
	setTabToolTip(addTab(CoversView, QIcon(":/layout.png"), tr("Covers")), tr("Covers"));

	BackCoversView = new TemplateListViewWidget(this);
	connect(BackCoversView, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )),
			this, SIGNAL(applyTemplate(SPhotoBook::TemplateScene* )));
	setTabToolTip(addTab(BackCoversView, QIcon(":/layout.png"), tr("BackCovers")), tr("BackCovers"));


	// Cliparts
	ClipartListView = new ResourceListView(this);
	setTabToolTip(addTab(ClipartListView, QIcon(":/clipart.png"), tr("Clipart")), tr("Clipart"));
	connect(ClipartListView, SIGNAL(resourceSelected(SPhotoBook::Resource)), this, SIGNAL(resourceSelected(SPhotoBook::Resource)));

	// Masks
	MaskListView = new ResourceListView(this);
	setTabToolTip(addTab(MaskListView, QIcon(":/masks.png"), tr("Masks")), tr("Masks"));
	connect(MaskListView, SIGNAL(resourceSelected(SPhotoBook::Resource)), this, SIGNAL(resourceSelected(SPhotoBook::Resource)));

	// Frames
	FrameListView = new ResourceListView(this);
	setTabToolTip(addTab(FrameListView, QIcon(":/frames.png"), tr("Frames")), tr("Frames"));
	connect(FrameListView, SIGNAL(resourceSelected(SPhotoBook::Resource)), this, SIGNAL(resourceSelected(SPhotoBook::Resource)));

	// Backgrounds
	BackgroundListView = new ResourceListView(this);
	setTabToolTip(addTab(BackgroundListView, QIcon(":/background.png"), tr("Backgrounds")), tr("Backgrounds"));
	connect(BackgroundListView, SIGNAL(resourceSelected(SPhotoBook::Resource)), this, SIGNAL(resourceSelected(SPhotoBook::Resource)));

	// Pages 
	PhotoBookView = new PagesListView(_AlbumWidget, this);
	setTabToolTip(addTab(PhotoBookView, QIcon(":/pages.png"), tr("Pages")), tr("Pages"));
	connect(PhotoBookView, SIGNAL(currentPageRemoved()), this, SIGNAL(removePage()));
	//connect(PhotoBookView, SIGNAL(currentPageRemoved()), ImagesListView, SLOT(updateCheckedFiles()));
}


PhotoBookListViewTabWidget::~PhotoBookListViewTabWidget()
{
}

void PhotoBookListViewTabWidget::setDocument(SPhotoBook::Document* _PBDoc)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	MPhotoBook = _PBDoc;

	SPhotoBook::ResourceList AllResources;
	QStringList PathList = SPhotoBook::TemplatePaths::pathList();
	QStringList::iterator it;
	//TODO ! Add Fonts !
	for (it = PathList.begin(); it != PathList.end(); ++it)
	{
		AllResources.load(SPhotoBook::TemplatePaths::resourcesSubDir(QDir(*it)));
	}

	//QTime LastTime = QTime::currentTime();
	//AllResources += _PBDoc->resources();
	AllResources.addNonExist(_PBDoc->resources());
	TemplatesView->setPages(_PBDoc->layouts());
	CoversView->setPages(_PBDoc->covers());
    BackCoversView->setPages(_PBDoc->lastPageLayouts());

	MaskListView->setResourceList(AllResources, SPhotoBook::Resource::TypeMask);
	FrameListView->setResourceList(AllResources, SPhotoBook::Resource::TypeFrame);
	ClipartListView->setResourceList(AllResources, SPhotoBook::Resource::TypeClipart);
	BackgroundListView->setResourceList(AllResources, SPhotoBook::Resource::TypeBackground);

	PhotoBookView->setDocument(_PBDoc);
	QApplication::restoreOverrideCursor();
}

void PhotoBookListViewTabWidget::retranslateUi()
{
	setTabText(TabLayoutView, tr("Layouts"));
	setTabText(TabCoverView, tr("Covers"));
	setTabText(TabBackCoverView, tr("BackCovers"));
	setTabText(TabClipartView, tr("Clipart"));
	setTabText(TabMaskView, tr("Masks"));
	setTabText(TabFrameView, tr("Frames"));
	setTabText(TabBackgroundView, tr("Backgrounds"));
	setTabText(TabPhotoBookView, tr("Pages"));
	//PhotoBookView->retranslateUi();
}

void PhotoBookListViewTabWidget::setCurrentTab(EnTabs _Tab)
{
	setCurrentIndex(_Tab);
}

void PhotoBookListViewTabWidget::pageIndexClicked(const QModelIndex& _Index)
{
	emit pageClicked(_Index.row());
}

void PhotoBookListViewTabWidget::updatePhotoBookPreview()
{
	if (MPhotoBook)
		PhotoBookView->setDocument(MPhotoBook);

}

void PhotoBookListViewTabWidget::updatePhotoBookPreview(int _Page)
{
/*	if (MPhotoBook)
		PageModel->updateThumbnail(MPhotoBook, _Page);*/
}

