/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOBOOKWIZARD_H
#define PHOTOBOOKWIZARD_H

#include "stiwizard.h"
#include <QDir>
#include "iorderwizard.h"
#include "stappmodule.h"
#include "sterror.h" 
#include "printjob.h"
#include "publisherdatabase.h"
#include "document.h"
#include "metainfo.h"

/**
	@author
*/
namespace STDom
{
	class PrintJobModel;
	class DDocModel;
}

class PhotoBookEditor; 
class STAppModule; 
class STPhotoBook;
class PhotoBookOutputPage; 
class QAbstractItemModel; 
class QSqlRecord; 
class SinglePhotoSelPage; 
class SProcessStatusWidget;
class BuildSettingsPage;
class TemplateSelectionPage;
class PhotoBookWizard : public STIWizard, public IOrderWizard
{
Q_OBJECT 

	ST_DECLARE_ERRORCLASS(); 

	SProcessStatusWidget* StatW;
	PhotoBookEditor* PBEditor;
	PhotoBookOutputPage* PBOutPage;
	STAppModule CurrModule; 
	SPhotoBook::Document* MPhotoBook;
	TemplateSelectionPage* TemplateSelPage;
	SinglePhotoSelPage* SPPage; 
	BuildSettingsPage* SettingsPage;
	STDom::PrintJobModel* Model;
	STDom::DDocModel* PhotoBookImageModel;
	int NumImages;
	QDir PhotoBookStoreDir;
	QDir PhotoBookLoadedPath;
	SPhotoBook::MetaInfo::EnTemplateType CurrentTemplateType;

	bool PhotoBookLoaded;
	bool WritableMediaSelected;
	void configurePhotoBook();
	void createPhotoBook();

public:
	PhotoBookWizard ( QWidget* _Parent );
	~PhotoBookWizard();
	virtual void init();
	virtual STDom::PrintJob getPrintJob() const;
	void setTemplateType(SPhotoBook::MetaInfo::EnTemplateType _TemplateType );
	virtual void setImages(const QFileInfoList& _Images, const QDir& _RootDir);
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value);

private slots:
	void buildPhotoBook();
	void wantsToFinish();
	void showLastPageThumbnail();
	void slotSelectionPreviousPage();
	void slotTemplateSelected();

signals:
	void commitOrder();
};

#endif
