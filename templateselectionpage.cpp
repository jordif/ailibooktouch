/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/#include "templateselectionpage.h"
#include <QLayout>
#include <QFrame>
#include <QComboBox>
#include <QtWebKit/QWebView>
#include <QTimer>
#include <QListView>
#include <QLabel>
#include <QToolButton>
#include <QDebug>
#include "ttpopsapp.h"
#include "remotepublishermanager.h"

QToolButton* TemplateSelectionPage::newActionButton(const QString& _Icon)
{
	QToolButton* Res = new QToolButton(this);
	Res->setObjectName("ActionButton");
	Res->setIcon(QIcon(_Icon));
	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	Res->setIconSize(QSize(64,64));
	return Res;
}

void TemplateSelectionPage::setCurrentState(TemplateSelectionPage::EnState _State)
{
	CurrentState = _State;
	BottomFrame->setVisible(_State != StateTemplatesEmpty);

	bool TempInfoVisible = BottomFrame->isVisible() && _State != StateUnselected;
	LabSize->setVisible(TempInfoVisible);
	CBSize->setVisible(TempInfoVisible);

	NoInfoFrame->setVisible(_State != StateShowWebInfo && TempInfoVisible);
	WebView->setVisible(!NoInfoFrame->isVisible() && TempInfoVisible);

	InetgetTimer->stop();

	View->setVisible(_State != StateTemplatesEmpty);
	NoTemplatesLabel->setVisible(_State == StateTemplatesEmpty);
	UnselectedLabel->setVisible(_State == StateUnselected);

	switch (_State)
	{
		case StateGettingInfo:
			NoInfoLabel->setText(tr("<h1>Getting info from internet.</h1>"));
			LabelInfoPixmap->setPixmap(QPixmap(":/inetget.png"));
			InetgetPicture1 = false;
			InetgetTimer->start();
		break;
		case StateNoInfo:
			NoInfoLabel->setText(tr("<h1>There is no information for this item.</h1>"));
			LabelInfoPixmap->setPixmap(QPixmap(":/dialog-information.png"));
		break;
	}
	completeChanged();
}


TemplateSelectionPage::TemplateSelectionPage(QWidget *parent) :
    STIWizardPage(parent)
{
	//setTitle(tr("<h1>Template selection</h1>"));
	//setSubTitle(tr("The <em>Photo Book</em> template defines the photobook features like size, number of pages, layouts, etc... </br> Use the following list to choose the template that you want for your <em>Photo Book</em>"));
	QVBoxLayout* MLayout = new QVBoxLayout(this);

	UnselectedLabel = new QLabel(this);
	UnselectedLabel->setText(tr("<h2>Please, select a template from the list.</h2>"));
	UnselectedLabel->setWordWrap(true);
	MLayout->addWidget(UnselectedLabel);

	BottomFrame = new QFrame(this);
	MLayout->addWidget(BottomFrame);

	QHBoxLayout* BottomLayout = new QHBoxLayout(BottomFrame);

	QVBoxLayout* LeftLayout = new QVBoxLayout;
	BottomLayout->addLayout(LeftLayout);
	LeftLayout->addWidget(new QLabel(tr("Products:"), this));
	View = new QListView(this);
	Model = new SPhotoBook::TemplateInfoModel(this);
	View->setModel(Model);

	View->setIconSize(QSize(64, 64));
	View->setMinimumWidth(340);
	View->setVisible(false);
	connect(View, SIGNAL(clicked( const QModelIndex& )), this, SLOT(slotTemplateIndexClicked(const QModelIndex& )));
	connect(View, SIGNAL(clicked( const QModelIndex& )), this, SIGNAL(completeChanged()));
	LeftLayout->addWidget(View);

	NoTemplatesLabel = new QLabel(this);
	NoTemplatesLabel->setText(tr("<h2>There is no templates of this type.</h2>"));
	NoTemplatesLabel->setWordWrap(true);
	MLayout->addWidget(NoTemplatesLabel);

	//------------ Right info widgets -------------
	QVBoxLayout* RightLayout = new QVBoxLayout;
	BottomLayout->addLayout(RightLayout);

	QHBoxLayout* TopRightLayout = new QHBoxLayout;
	RightLayout->addLayout(TopRightLayout);
	TopRightLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::Fixed, QSizePolicy::Preferred));

	LabSize = new QLabel(tr("Size:"), this);
	LabSize->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
	TopRightLayout->addWidget(LabSize);
	CBSize = new QComboBox(this);
	CBSize->setMinimumWidth(250);
	CBSize->setObjectName("WizardSizeCombo");
	TopRightLayout->addWidget(CBSize);

	TopRightLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));


	WebView = new QWebView(this);
	RightLayout->addWidget(WebView, 1, 0);
	//WebView->load(QUrl("http://sites.google.com/site/orometemplates/km_magazinea4"));
	connect(WebView, SIGNAL(loadStarted()), this, SLOT(slotWebLoadStarted()));
	connect(WebView, SIGNAL(loadFinished(bool)), this, SLOT(slotWebLoadFinished(bool)));
	//WebView->setMinimumWidth(460);
	//WebView->setMinimumHeight(500);
	WebView->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);


	NoInfoFrame = new QFrame(this);
	NoInfoFrame->setMinimumHeight(460);
	NoInfoFrame->setMinimumWidth(500);
	RightLayout->addWidget(NoInfoFrame, 1, 0);

	QVBoxLayout* NoInfoFrameLayout = new QVBoxLayout(NoInfoFrame);
	NoInfoLabel = new QLabel(NoInfoFrame);
	NoInfoLabel->setWordWrap(true);
	NoInfoLabel->setAlignment(Qt::AlignCenter);
	NoInfoFrameLayout->addWidget(NoInfoLabel);
	LabelInfoPixmap = new QLabel(NoInfoFrame);
	LabelInfoPixmap->setAlignment(Qt::AlignCenter);
	NoInfoFrameLayout->addWidget(LabelInfoPixmap);
	InetgetTimer = new QTimer(this);
	InetgetTimer->setInterval(1000);
	connect(InetgetTimer, SIGNAL(timeout()), this, SLOT(inetgetBlinkTimeout()));

	// --- Footer ---
	QHBoxLayout* FooterLayout = new QHBoxLayout;
	MLayout->addLayout(FooterLayout);

	BackBut = newActionButton(":/st/tpopsl/previous.png");
	connect(BackBut, SIGNAL(clicked()), this, SIGNAL(previousPage()));
	FooterLayout->addWidget(BackBut);

	FooterLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	NextBut = newActionButton(":/st/tpopsl/next.png");
	connect(NextBut, SIGNAL(clicked()), this, SIGNAL(nextPage()));
	FooterLayout->addWidget(NextBut);
	setCurrentState(StateNoInfo);
}

void TemplateSelectionPage::selectFirstIndex()
{
	if (Model->rowCount() > 0)
	{
		View->setCurrentIndex(Model->index(0,0));
		slotTemplateIndexClicked(Model->index(0,0));
	}
	completeChanged();
}

void TemplateSelectionPage::completeChanged()
{
	//TODO: Update next button state. (Check isComplete)
	NextBut->setEnabled(isComplete());
}

void TemplateSelectionPage::init()
{
	setCurrentState(StateUnselected);
}

void TemplateSelectionPage::setTemplateList(const SPhotoBook::TemplateInfoList& _TemplateList )
{
	Model->setTemplateList(_TemplateList);
	//selectFirstIndex();
}

SPhotoBook::TemplateInfo TemplateSelectionPage::templateInfo(const QModelIndex& _Index, const QSizeF& _Size) const
{
	SPhotoBook::TemplateInfo Res;

	if (_Index.isValid())
		Res = Model->templateInfo(_Index, _Size);
	return Res;
}

SPhotoBook::TemplateInfo TemplateSelectionPage::selectedTemplateInfo() const
{
	SPhotoBook::TemplateInfo Res;

	if (CBSize->currentIndex() != -1)
	{
		QSizeF CurrentSize = CBSize->itemData(CBSize->currentIndex()).toSizeF();
		Res = templateInfo(View->currentIndex(), CurrentSize);
	}
	return Res;
}


bool TemplateSelectionPage::isComplete() const
{
	return Model->rowCount() > 0 && View->currentIndex().isValid() && CurrentState != StateUnselected;
}


void TemplateSelectionPage::slotTemplateIndexClicked(const QModelIndex& _Index)
{
	WebView->stop();
	completeChanged();
	//Load available sizes.
	CBSize->clear();
	QList<QSizeF> SizesList = Model->sizes(_Index);
	QList<QSizeF>::iterator it;
	for (it = SizesList.begin(); it != SizesList.end(); ++it)
	{
		QSizeF CSize = *it;
		CBSize->addItem(QString("%1x%2 mm").arg(CSize.width()).arg(CSize.height()), CSize);
	}
	if (!SizesList.isEmpty())
	{
		//Get info url from model and display it.
		QUrl InfoUrl =  pManager->starlabManager()->infoUrl(Model->templateInfo(_Index, SizesList.first()));
		WebView->setHtml("");
		setCurrentState(StateGettingInfo);
		WebView->load(InfoUrl);
	}
}

void TemplateSelectionPage::slotWebLoadStarted()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
}

void TemplateSelectionPage::slotWebLoadFinished(bool _Ok)
{
	QApplication::restoreOverrideCursor();
	if (_Ok)
		setCurrentState(StateShowWebInfo);
	else
		setCurrentState(StateNoInfo);
}

void TemplateSelectionPage::inetgetBlinkTimeout()
{
	if (InetgetPicture1)
		LabelInfoPixmap->setPixmap(QPixmap(":/inetget.png"));
	else
		LabelInfoPixmap->setPixmap(QPixmap(":/inetget_2.png"));

	InetgetPicture1 = !InetgetPicture1;
}
