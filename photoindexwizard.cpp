/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photoindexwizard.h"
#include <QApplication> 

#include "singlephotoselpage.h"
#include "tpordersummarypage.h" 
#include "appsettings.h" 
#include "dimagedoc.h"
#include "ddocmodel.h"
#include "printjobmodel.h"

//Builder
#include "stutils.h" 
#include "sprocessstatuswidget.h" 
#include "stphotoindexbuilder.h" 
#include "stimage.h" 
#include "smessagebox.h" 

void PhotoIndexWizard::retranslateUi()
{
	PSPage->setHeaderText(tr("<h2>Please, select images for index and the product.</h2>"));
}

PhotoIndexWizard::PhotoIndexWizard(QWidget* _Parent): STIWizard(_Parent)
{
	AppSettings Settings; 

	PSPage = new SinglePhotoSelPage(this);
	//PSPage->setImagesPerSheet(Settings.photoIndexImagesPerPage());
	addPage(PSPage); 
	connect(PSPage, SIGNAL(previousPage()), this, SLOT(wantsToFinish())); 
	connect(PSPage, SIGNAL(accepted()), this, SLOT(renderPhotoIndex())); 

	SummPage = new TPOrderSummaryPage(this); 
	connect(SummPage, SIGNAL(orderAccepted()), this, SIGNAL(commitOrder())); 
	connect(SummPage, SIGNAL(orderCanceled()), this, SIGNAL(finished())); 
	addPage(SummPage); 
}


void PhotoIndexWizard::setProductsModel(QAbstractItemModel* _Model)
{
	PSPage->setProductsModel(_Model); 
}

void PhotoIndexWizard::setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value)
{
	//PSPage->setShippingMethod(_Value);
	SummPage->setShippingMethod(_Value); 
}

STDom::PrintJob PhotoIndexWizard::getPrintJob() const
{
	return MPrintJob;
}

void PhotoIndexWizard::setImages(const QFileInfoList& _Images, const QDir& _RootDir)
{
	//Model->sourceDocModel()->setDocs(_Images);
	PSPage->setImages(_Images);
	PSPage->setRootDir(_RootDir);
}

void PhotoIndexWizard::init()
{
	retranslateUi();
	setProductsModel(newProductsModel(this, STDom::PublisherDatabase::PhotoIndexProduct));
	firstPage(); 
}

void PhotoIndexWizard::renderPhotoIndex()
{
	if (PSPage->selectedImages().size() > 0)
	{

		nextPage();

		SummPage->setStatus(TPOrderSummaryPage::StatusShowProgress);
		AppSettings Settings;
		MPrintJob.clear();

		QDir TempDir(QDir::temp().absolutePath() + "/ttpopsindextmp");
		if (TempDir.exists())
			STUtils::rmDir(TempDir);

		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		SProcessStatusWidget* SBStatusWidg = SummPage->processStatus();

		try
		{
			STDom::DDocProduct CurProduct = PSPage->currentProduct();
			QFileInfoList CImages = PSPage->selectedImages();

			STPhotoIndexBuilder IndBuilder(Settings.photoIndexImagesPerPage(), CurProduct.format().size(), Settings.photoIndexMargin(), 2, Settings.photoIndexDpis());
			//qDebug("From main Window Width %f", PhotoIndexWizard->field("width").toDouble());
			IndBuilder.setDrawFileNames(Settings.photoIndexDrawFileNames());
			IndBuilder.setDrawNumbers(Settings.photoIndexDrawFileNumbers());
			IndBuilder.setDrawFrames(Settings.photoIndexDrawFrames());
			IndBuilder.setBackgroundColor(QColor(Settings.photoIndexBackgroundColor()));
			IndBuilder.setImages(CImages);

			//SBStatusWidg->show();
			SBStatusWidg->showProgressBar(tr("Building photoindex..."), IndBuilder.numPages() * 2);
			QApplication::processEvents();

			//Export to a temp directory.
			QDir CurrTempDir = TempDir.absoluteFilePath(CurProduct.ref());
			CurrTempDir.mkpath(CurrTempDir.absolutePath());
			QString DestPath = CurrTempDir.absolutePath();

			for (int Vfor = 0; Vfor < IndBuilder.numPages(); Vfor++)
			{
				STImage CurrImage = IndBuilder.render(Vfor);
				SBStatusWidg->incrementProgress();
				QApplication::processEvents();

				QString CurrImageName = DestPath +
					QString("/indexpage_%1").arg(QString::number(Vfor), 4, QChar('0')) + ".JPG";
				Assert(CurrImage.save(CurrImageName), Error(tr("Error saving photoindex page %1").arg(CurrImageName)));

				//Fill printjob
				MPrintJob.addCopies(CurProduct, QFileInfo(CurrImageName), 1);

				SBStatusWidg->incrementProgress();
				QApplication::processEvents();
			}
			SummPage->setPrintJob(MPrintJob);
			SummPage->setStatus(TPOrderSummaryPage::StatusShowSummary);
		}
		catch (STError& _Error)
		{
			SMessageBox::critical(this, tr("Error creating photoindex"), _Error.description());
		}
		QApplication::restoreOverrideCursor();
	}
	else
		SMessageBox::warning(this, tr("Photo Index warning"), tr("You have to select at least one image."));
}


void PhotoIndexWizard::wantsToFinish()
{
	bool Finish = true;
	if (PSPage->hasChanges())
		Finish = SMessageBox::question(this, tr("Order cancel question"), tr("You'll loose all the changes. Cancel anyway ?")) == QMessageBox::Yes;

	if (Finish)
		emit previousWizard();
}
