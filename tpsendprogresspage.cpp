/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "tpsendprogresspage.h"
#include <QLayout> 
#include <QLabel> 
#include <QTimer>

#include "sprocessstatuswidget.h" 

void TPSendProgressPage::retranslateUi()
{
	LabHeader->setText(tr("<center><h1> Sending command ... </h1></center>"));
}

TPSendProgressPage::TPSendProgressPage(QWidget* _Parent): STIWizardPage(_Parent)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 

	LabHeader = new QLabel(this);
	LabHeader->setObjectName("ProgPageHeader");
	LabHeader->setAlignment(Qt::AlignCenter);
	LabHeader->setMinimumHeight(100);
	MLayout->addWidget(LabHeader); 

	StatusWidg = new SProcessStatusWidget(this);
	StatusWidg->showProgressBar(tr("Progress..."), 1);
	MLayout->addWidget(StatusWidg);
	MProgressBar = StatusWidg->progressBar();

	MLayout->addItem(new QSpacerItem(10, 100, QSizePolicy::Preferred, QSizePolicy::Minimum));

	MessageLabel = new QLabel(this);
	MessageLabel->setObjectName("ProgMessageLabel");
	MLayout->addWidget(MessageLabel);
	MLayout->setAlignment(MessageLabel, Qt::AlignCenter);

	QuitTimer = new QTimer(this);
	QuitTimer->setSingleShot(true);
	connect(QuitTimer, SIGNAL(timeout()), this, SIGNAL(finish()));

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding)); 
}


TPSendProgressPage::~TPSendProgressPage()
{
}

void TPSendProgressPage::init()
{
	retranslateUi();
	StatusWidg->setVisible(true);
	MessageLabel->setVisible(false);
	LabHeader->setVisible(true);
}

void TPSendProgressPage::showMessageAndQuit(const QString& _Message)
{
	StatusWidg->setVisible(false);
	MessageLabel->setVisible(true);
	LabHeader->setVisible(false);
	MessageLabel->setText(_Message);
	QuitTimer->start(5000);
}

void TPSendProgressPage::mouseReleaseEvent ( QMouseEvent * event )
{
	QuitTimer->stop();
	finish();
	QWidget::mouseReleaseEvent(event);
}
