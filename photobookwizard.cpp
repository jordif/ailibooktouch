/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photobookwizard.h"
#include <QDebug>

//#include "photobookeditor.h"
#include "stappmodule.h" 
#include "smessagebox.h" 
#include "stutils.h" 
#include "photobookoutputpage.h"
#include "sprocessstatuswidget.h"
#include "appsettings.h"
#include "stimagetools.h"
#include "templateselectionpage.h"
#include "singlephotoselpage.h" 
#include "buildsettingspage.h"
#include "stprogressindicator.h"
#include "photobookeditor.h"
//Download templates.
#include "templatedownloaddialog.h"
#include "stxmlpublishersettings.h"

#include "printjobmodel.h"
#include "dimagedoc.h"
#include "systemtemplates.h"
#include "ttpopsapp.h"
#include "remotepublishermanager.h"
#include "templategenerator.h"



void PhotoBookWizard::configurePhotoBook()
{
/*	STPhotoBookBuildOptions BuildOptions;
	STPhotoBookTemplate UsedTemplate = MPhotoBookTemplate;
	STPhotoLayout::EnLayoutType LayoutType;
	switch (CurrModule.type())
	{
		case STAppModule::TypeBatchTemplate :
			LayoutType = STPhotoLayout::TypeMultiPhoto;
		break;
		case STAppModule::TypePhotoId :
			LayoutType = STPhotoLayout::TypeIdPhoto;
		break;
		case STAppModule::TypeDecorations :
			LayoutType = STPhotoLayout::TypeCard;
		break;
		case STAppModule::TypePhotoBooks :
			LayoutType = STPhotoLayout::TypePhotoBook;
		break;
		case STAppModule::TypeCalendars :
			LayoutType = STPhotoLayout::TypeCalendar;
		break;
	}

	BuildOptions.setDefaults(LayoutType);
	switch (CurrModule.type())
	{
		case STAppModule::TypePhotoBooks :
			BuildOptions.setPagesToFill(SettingsPage->numPages());
			BuildOptions.setUseTexts(SettingsPage->useTexts());
			BuildOptions.setTitle(SettingsPage->title());
			BuildOptions.setAutoadjustFrames(SettingsPage->autoAdjust());
			BuildOptions.setAutoFillBackgrounds(SettingsPage->useBackgrounds());
		break;
		case STAppModule::TypeCalendars :
			BuildOptions.setAutoadjustFrames(SettingsPage->autoAdjust());
		break;
	}

	MPhotoBook->setTemplate(UsedTemplate, BuildOptions);*/
}

void PhotoBookWizard::createPhotoBook()
{
	SPhotoBook::DesignInfo DesInfo = SettingsPage->designInfo();
	SPhotoBook::TemplateInfo TInfo = TemplateSelPage->selectedTemplateInfo();
	SPhotoBook::BuildOptions BuildOptions = SettingsPage->getBuildOptions();

	//Load design
	MPhotoBook->clear();
	//MPhotoBook->setName(unsavedPhotoBookName()); //This is current photobook, not a template.
	MPhotoBook->setBuildOptions(BuildOptions);
	MPhotoBook->setMetaInfo(DesInfo.metaInfo());

	//Download templates if it's not on disk.
	if (!TInfo.designOnDisk(DesInfo))
	{
		StatW->show();
		StatW->showProgressBar(tr("Downloading template..."), -1);
		pManager->downloadTemplateDesign(TInfo, DesInfo, StatW);
		StatW->hide();
	}

	MPhotoBook->loadDesign(QDir(TInfo.absolutePath(DesInfo)));
	SPhotoBook::PageList Layouts = MPhotoBook->layouts();
    SPhotoBook::TemplateGenerator Generator(DesInfo.metaInfo());

	if (DesInfo.metaInfo().autogenerateLayouts())
	{
		Layouts += Generator.generate(MPhotoBook, 6);
		MPhotoBook->setLayouts(Layouts);
	}

	StatW->show();
	StatW->showProgressBar(tr("Building photobook..."), -1);
	if (TInfo.type() == SPhotoBook::MetaInfo::TypeCalendar)
		MPhotoBook->buildCalendar(PhotoBookImageModel, BuildOptions.fromDate(), BuildOptions.toDate(), StatW->progressBar());
	else
		MPhotoBook->autoBuild(PhotoBookImageModel, StatW->progressBar());

	PBEditor->setPhotoBook(MPhotoBook);
	StatW->hide();


	/*	MPhotoBook->clear();
	MPhotoBook->setTemplateFilePath(_TemplatePath);
	configurePhotoBook();

	STDom::STXmlPublisherSettings PXmlS = STDom::STXmlPublisherSettings::getDefaultSettings();
	if (!PXmlS.isEmpty()) //If there is no publishers we do nothing ! jeje...
	{
		TemplateDownloadDialog* TDDialog = new TemplateDownloadDialog(this);
		TDDialog->show();
		TDDialog->setMessage(tr("Downloading required files for this template, please wait..."));
		STErrorStack ErrorStack;
		MPhotoBookTemplate.downloadRemoteContents(PXmlS, TDDialog->progressIndicator(), TDDialog->ftpStatusWidget(), &ErrorStack);
		if (ErrorStack.hasErrors())
			SMessageBox::criticalDetailed(this, tr("Errors downloading templates"),
								  tr("There was errors downloading templates. Some of the layouts for this model may not work. Please contact to your software provider."),
								  ErrorStack.errorString().join(","));
		delete TDDialog;
	}

	PhotoBookImageModel->setDocs(STUtils::sortByCreatedDate(_Images));

	StatW->show();
	StatW->showProgressBar(tr("Building photobook..."), -1);

	if (CurrModule.type() == STAppModule::TypeCalendars)
		MPhotoBook->buildCalendar(PhotoBookImageModel, SettingsPage->fromDate(), SettingsPage->toDate(), StatW->progressBar());
	else
		MPhotoBook->autoBuild(PhotoBookImageModel, StatW->progressBar());
	PBEditor->setPhotoBook(MPhotoBook); 

	StatW->hide();*/
}

PhotoBookWizard::PhotoBookWizard(QWidget* _Parent): STIWizard(_Parent), Model(0), PhotoBookLoaded(false), WritableMediaSelected(false), NumImages(0)
{
	Model = new STDom::PrintJobModel(this);
	STDom::DDocModel* MDocModel = new STDom::DDocModel(this);
	MDocModel->setNoImagePixmap(QPixmap(":/hourglass.png"));
	Model->setSourceModel(MDocModel);
	//Model->setRitchTextDisplay(true);
	Model->setThumbnailSize(STDom::DImageDoc::thumbnailSize());

	TemplateSelPage = new TemplateSelectionPage(this);
	connect(TemplateSelPage, SIGNAL(previousPage()), this, SLOT(wantsToFinish()));
	connect(TemplateSelPage, SIGNAL(nextPage()), this, SLOT(slotTemplateSelected()));
	addPage(TemplateSelPage);

	StatW = new SProcessStatusWidget(this, Qt::Dialog);
	SettingsPage = new BuildSettingsPage(this);
	addPage(SettingsPage);

	SPPage = new SinglePhotoSelPage(this);
	//! TODO Get it from template !!!
	SPPage->setOptimalNumImagesRange(20, 25);
	addPage(SPPage); 
	connect(SPPage, SIGNAL(accepted()), this, SLOT(buildPhotoBook()));
	connect(SPPage, SIGNAL(backButtonClicked()), this, SLOT(slotSelectionPreviousPage()));

	PhotoBookImageModel = new STDom::DDocModel(this);

	PBEditor = new PhotoBookEditor(this);
	addPage(PBEditor);  
	PBEditor->setModel(PhotoBookImageModel); 

	MPhotoBook = new SPhotoBook::Document(this);
	//MPhotoBook->setAutoAdjustFrames(false);
	connect(MPhotoBook, SIGNAL(newPageCreated()), this, SLOT(showLastPageThumbnail()));

	PBOutPage = new PhotoBookOutputPage(this);
	PBOutPage->setModel(Model);
	PBOutPage->setPhotoBook(MPhotoBook);
	connect(PBOutPage, SIGNAL(printPhotoBook()), this, SIGNAL(commitOrder()));
	addPage(PBOutPage);

}

PhotoBookWizard::~PhotoBookWizard()
{
}

void PhotoBookWizard::init()
{
	STUtils::rmDir(PBEditor->tempPath());
	MPhotoBook->clear(); 
	PhotoBookLoaded = false;
	SPPage->setHeaderText(tr("<h2>Please select pictures for you photo assembly.</h2>"));
	firstPage();

/*	//Configure Settings Page
	switch (CurrModule.type())
	{
		case STAppModule::TypeBatchTemplate :
		case STAppModule::TypePhotoId :
		case STAppModule::TypeDecorations :
			nextPage(); //No settings page in this module types.
			slotSettingsConfirmed();
		break;
		case STAppModule::TypePhotoBooks :
		break;
		case STAppModule::TypeCalendars :
		break;
	}*/
}

STDom::PrintJob PhotoBookWizard::getPrintJob() const
{
	return Model->printJob();
}

void PhotoBookWizard::setTemplateType(SPhotoBook::MetaInfo::EnTemplateType _TemplateType )
{
	CurrentTemplateType = _TemplateType;
	SPhotoBook::TemplateInfoList TemplateList = TtpopsApp::loadTemplates();
	TemplateSelPage->setTemplateList(TemplateList.subList(_TemplateType));
}

void PhotoBookWizard::setImages(const QFileInfoList& _Images, const QDir& _RootDir)
{
	NumImages = _Images.size();
	SPPage->setImages(_Images);
	SPPage->setRootDir(_RootDir);
}

void PhotoBookWizard::setProductsModel(QAbstractItemModel* _Model)
{
	PBOutPage->setProductsModel(_Model); 
}

void PhotoBookWizard::setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value)
{
	PBOutPage->setShippingMethod(_Value); 
}

void PhotoBookWizard::buildPhotoBook()
{
	QFileInfoList SelectedImages = SPPage->selectedImages();

	try
	{
		QFileInfoList SelectedImages = SPPage->selectedImages();
		if (SelectedImages.size() > 0)
		{

			PhotoBookImageModel->setDocs(STUtils::sortByCreatedDate(SelectedImages));
			bool Create = true;
			if (!MPhotoBook->isEmpty())
				Create = SMessageBox::yesNo(this, tr("PhotoBook creation"), tr("Create a new PhotoBook ?")) == QMessageBox::Yes;
			if (Create)
			{
				SPPage->setEnabled(false);
				createPhotoBook();
				SPPage->setEnabled(true);
			}

			nextPage();
		}
		else
			SMessageBox::critical(this, tr("Too few images"), tr("You have to select more images to generate this photo book. "));
	}
	catch (STError& _Error)
	{
		SPPage->setEnabled(true);
		SMessageBox::critical(this, tr("Loading photobook"), tr("Error loading photobook template: %1").arg(_Error.description()));
	}

	//emit documentChanged(PBDocument);


	/*	if (!CurrModule.templatePath().isEmpty())
	{
		try
		{
			QFileInfoList SelectedImages = SPPage->selectedImages(); 
			if (SelectedImages.size() > 0) 
			{
				bool Create = !PhotoBookLoaded;
				if (!MPhotoBook->isEmpty() && Create)
					Create = SMessageBox::yesNo(this, tr("PhotoBook creation"), tr("Create a new PhotoBook ?")) == QMessageBox::Yes;
				if (Create)
				{
					SPPage->setEnabled(false);
					createPhotoBook(CurrModule.templatePath(), SelectedImages); 
					SPPage->setEnabled(true);
				}
				else 
					PhotoBookImageModel->setDocs(SelectedImages);

				nextPage(); 
			}
			else 
				SMessageBox::critical(this, tr("Too few images"), tr("You have to select more images to generate this photo book. "));
		}
		catch (STError& _Error)
		{
			SMessageBox::critical(this, tr("Loading photobook"), tr("Error loading photobook template: %1").arg(_Error.description()));
		}
	}
	else 
		SMessageBox::critical(this, tr("Loading photobook"), tr("Module %1 does not define any templatepath").arg(CurrModule.name()));*/
}


void PhotoBookWizard::wantsToFinish()
{
	bool Finish = true; 
	if (!MPhotoBook->isEmpty())
		Finish = SMessageBox::question(this, tr("Order cancel question"), tr("You'll loose all the changes. Cancel anyway ?")) == QMessageBox::Yes;

	if (Finish)
		emit previousWizard(); 
}

void PhotoBookWizard::showLastPageThumbnail()
{
	StatW->showPixmap(QPixmap::fromImage(MPhotoBook->getLastPageThumbnail(QSize(300, 200))));
	StatW->moveToScreenCenter();
}

void PhotoBookWizard::slotSelectionPreviousPage()
{
	if (CurrentTemplateType  == SPhotoBook::MetaInfo::TypeMultiPhoto ||
		CurrentTemplateType  == SPhotoBook::MetaInfo::TypeIdPhoto ||
		CurrentTemplateType  == SPhotoBook::MetaInfo::TypeCard )
	{
		wantsToFinish();
	}
	else
		emit previousPage();
}

void PhotoBookWizard::slotTemplateSelected()
{
	SPhotoBook::TemplateInfo TInfo = TemplateSelPage->selectedTemplateInfo();
	//Lets configure BuildOptions page
	SettingsPage->setTemplateInfo(TInfo);
	setProductsModel(newProductsModel(this, TInfo.pubDatabaseProductType(), TInfo.name()));
}

