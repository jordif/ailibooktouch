/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef STORAGEOUTPUTPAGE_H
#define STORAGEOUTPUTPAGE_H

#include "stiwizardpage.h"
#include <QSqlRecord>
#include "printjob.h"
#include "publisherdatabase.h"

/**
Page for storage modules output.

	@author
*/
class QLabel; 
class QToolButton; 
class TPProductListView; 
class QToolButton; 
class QAbstractItemModel; 
class StorageOutputPage : public STIWizardPage
{
	Q_OBJECT

	QLabel* HeaderLabel; 
	QLabel* SummaryLabel; 
	QToolButton* ButStore; 
	QToolButton* ButPrevious; 
	QToolButton* ButCancel;
	TPProductListView* LVProducts;
	STDom::PrintJob MPrintJob;
	STDom::PublisherBill::PublisherShippingMethod  ShippingMethod;
	int NCopies; 

	void retranslateUi();
	QToolButton* newActionButton(const QIcon& _Icon);

public:
	StorageOutputPage(QWidget* _Parent = 0);
	void setStoreIcon(const QIcon& _Icon);
	virtual void init();
	void setFiles(const QFileInfoList& _Files);
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value) { ShippingMethod = _Value; }

private slots: 
	void calcBill();
	void editAddProductToAll();
	void editDelProductToAll();
	void slotStore(); 

signals: 
	void store(int _Copies); 
	void orderCanceled();
};

#endif
