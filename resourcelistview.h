/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef RESOURCELISTVIEW_H
#define RESOURCELISTVIEW_H

#include "sptoolbarlistview.h"
#include "resourcelist.h"
#include "resource.h"

class QModelIndex;
namespace SPhotoBook
{
	class ResourceModel;
	class ResourceList;
	class Resource;
}

class ResourceListView : public SPToolbarListView
{
Q_OBJECT
	enum EnItemType{
		ItemTypeAll,
		ItemTypeFolder,
		ItemTypeMore
	};

	SPhotoBook::ResourceModel* ResModel;
	SPhotoBook::Resource::EnResourceType ResType;
	SPhotoBook::ResourceList MyResList;
	bool ResListChanged;

public:
	ResourceListView(QWidget* _Parent = 0);
	void setResourceList(const SPhotoBook::ResourceList& _List, SPhotoBook::Resource::EnResourceType _Type);
	void setNullItem(const SPhotoBook::Resource& _Resource);

protected:
	void updateResList();
	void showEvent(QShowEvent* _Event);

private slots:
	void slotApplyItem(const QModelIndex& _Index);

signals:
	void resourceSelected(const SPhotoBook::Resource& _Resource);
	void indexSelected(const QModelIndex& _Index);
};

#endif // ResourceListView_H
