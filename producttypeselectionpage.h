/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PRODUCTTYPESELECTIONPAGE_H
#define PRODUCTTYPESELECTIONPAGE_H

#include "stiwizardpage.h"
#include "stclickablelabel.h"

class MainOptionLabel;
class QToolButton;
class QSignalMapper;
class FlowLayout;
class ProductTypeSelectionPage : public STIWizardPage
{
	Q_OBJECT
public:
	enum EnProductType
	{
		TypeDigitalCopies,
		TypePhotoId,
		TypePhotoIndex,
		TypeCDRecord,
		TypePhotoBook,
		TypeCalendar,
		TypeCard,
		TypeOther
	};

private:
	int OptionLabelWidth;
	FlowLayout* MainOptionsLayout;

	MainOptionLabel* createMainOption(ProductTypeSelectionPage::EnProductType _Type, const QString& _Title,
									  const QString& _Description, const QString& _Image);
	QToolButton* newLocaleButton ( const QString& _Locale, QSignalMapper* _Mapper );
	void deleteMainOptionButtons();
	void createMainOptionButtons();

public:
	ProductTypeSelectionPage(QWidget* _Parent );
	void init();
	void reloadMainOptions();
	void retranslateUi();

protected:
	void mousePressEvent ( QMouseEvent* _Event );

public slots:
	void slotLocaleSelected ( const QString& _Locale );

signals:
	void typeSelected(ProductTypeSelectionPage::EnProductType _Type);
	void settingsSelected();

};


class MainOptionLabel : public STClickableLabel
{
	Q_OBJECT
	ProductTypeSelectionPage::EnProductType MyType;

public:
	MainOptionLabel(ProductTypeSelectionPage::EnProductType _Type, const QString& _Title,
					const QString& _Description, const QString& _Image, QWidget* _Parent = 0);
private slots:
	void slotClicked();

signals:
	void typeSelected(ProductTypeSelectionPage::EnProductType _Type);
};

#endif // PRODUCTTYPESELECTIONPAGE_H
