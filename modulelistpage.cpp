/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "modulelistpage.h"
#include <QLayout>
#include <QLabel>
#include <QScrollArea>
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QScrollBar>
#include <QSignalMapper>
#include <QToolButton>
#include <QDesktopWidget>
#include <QDebug>

#include "sactionbuttonsframe.h"
#include "smessagebox.h"
#include "sapplication.h"
#include "stclickablelabel.h"
#include "appsettings.h" 
#include "stburningsettings.h"

// _________________________________________________________________________*/
//
//   Class ModuleAction
// _________________________________________________________________________*/

ModuleAction::ModuleAction ( const STAppModule& _Module, QObject* _Parent )
		: QAction ( _Module.icon(), _Module.name(), _Parent ), Module ( _Module )
{
}

// _________________________________________________________________________*/
//
//   Class ModuleGroupAction
// _________________________________________________________________________*/

QIcon ModuleGroupAction::moduleGroupIcon ( STAppModule::EnModuleGroup _ModuleGroup )
{
	QIcon Res;
	switch ( _ModuleGroup )
	{
		case STAppModule::GroupDigitalPhoto :
			Res = QIcon ( ":/digitalphoto-group.png" );
			break;
		case STAppModule::GroupPhotoAssembly :
			Res = QIcon ( ":/photoassembly-group.png" );
			break;
		case STAppModule::GroupPhotoBook :
			Res = QIcon ( ":/photobook-group.png" );
			break;
		case STAppModule::GroupStorage :
			Res = QIcon ( ":/storage-group.png" );
			break;
		case STAppModule::GroupDigitalDocs :
			Res = QIcon ( ":/digitaldocs-group.png" );
			break;
		case STAppModule::GroupPhotoGift :
			Res = QIcon ( ":/photogift-group.png" );
			break;
		default:
			Res = QIcon ( ":/default-group.png" );
	};
	return Res;
}

ModuleGroupAction::ModuleGroupAction ( STAppModule::EnModuleGroup _ModuleGroup, QObject* _Parent )
		: QAction ( moduleGroupIcon ( _ModuleGroup ), STAppModule::groupName ( _ModuleGroup ), _Parent ), ModuleGroup ( _ModuleGroup )
{
	Description = STAppModule::groupDescription ( _ModuleGroup );
}

// _________________________________________________________________________*/
//
//   Class ModuleSubGroupAction
// _________________________________________________________________________*/

QIcon ModuleSubGroupAction::moduleSubGroupIcon ( STAppModule::EnModuleSubGroup _ModuleSubGroup )
{
	QIcon Res;
	switch ( _ModuleSubGroup )
	{
		case STAppModule::SubGroupCalendars :
			Res = QIcon ( ":/calendars-subgroup.png" );
			break;
		case STAppModule::SubGroupPhotoId :
			Res = QIcon ( ":/photoid-subgroup.png" );
			break;
		case STAppModule::SubGroupCards :
			Res = QIcon ( ":/cards-subgroup.png" );
			break;
		case STAppModule::SubGroupBatchTemplates :
			Res = QIcon ( ":/batchtemplates-subgroup.png" );
			break;
	};
	return Res;
}

ModuleSubGroupAction::ModuleSubGroupAction ( STAppModule::EnModuleSubGroup _ModuleSubGroup, QObject* _Parent )
		: QAction ( moduleSubGroupIcon ( _ModuleSubGroup ), STAppModule::subGroupName ( _ModuleSubGroup ), _Parent ), ModuleSubGroup ( _ModuleSubGroup )
{
	Description = STAppModule::subGroupDescription ( _ModuleSubGroup );
}



// _________________________________________________________________________*/
//
//   Class ModuleLabel
// _________________________________________________________________________*/

ModuleLabel::ModuleLabel ( ModuleAction* _Action, QWidget* _Parent ) : MActionLabel ( _Action, _Action->module().description(), _Action->module().templateInfo().remoteInfo() , _Parent ),
ModuleGroup ( _Action->module().group() ),
ModuleSubGroup ( _Action->module().subGroup())

{
}


// _________________________________________________________________________*/
//
//   Class ModuleGroupLabel
// _________________________________________________________________________*/


ModuleGroupLabel::ModuleGroupLabel ( ModuleGroupAction* _Action, QWidget* _Parent ) : MActionLabel ( _Action, _Action->description(), QUrl(), _Parent ),
IsSubGroup(false)
{
}

ModuleGroupLabel::ModuleGroupLabel ( ModuleSubGroupAction* _Action, QWidget* _Parent ) : MActionLabel ( _Action, _Action->description(), QUrl(), _Parent ),
IsSubGroup(true)
{
}


// _________________________________________________________________________*/
//
//   Class ModuleListPage
// _________________________________________________________________________*/

void ModuleListPage::filterModules ( STAppModule::EnModuleGroup _ModuleGroup )
{
	QList<ModuleLabel*> ModLabels =  ModuleFrame->findChildren<ModuleLabel*>();
	QList<ModuleLabel*>::iterator it;

	for ( it = ModLabels.begin(); it != ModLabels.end(); ++it )
	{
		( *it )->setVisible ( ( *it )->moduleGroup() == _ModuleGroup );
	}
}

void ModuleListPage::filterModules ( STAppModule::EnModuleSubGroup _ModuleSubGroup )
{
	QList<ModuleLabel*> ModLabels =  ModuleFrame->findChildren<ModuleLabel*>();
	QList<ModuleLabel*>::iterator it;

	for ( it = ModLabels.begin(); it != ModLabels.end(); ++it )
	{
		( *it )->setVisible ( ( *it )->moduleSubGroup() == _ModuleSubGroup );
	}
}

void ModuleListPage::showSubGroups(bool _Value)
{
	QList<ModuleGroupLabel*> ModLabels =  ModuleTypeFrame->findChildren<ModuleGroupLabel*>();
	QList<ModuleGroupLabel*>::iterator it;

	for ( it = ModLabels.begin(); it != ModLabels.end(); ++it )
	{
		( *it )->setVisible ( ( *it )->isSubGroup() == _Value );
	}
	MMenuLabel->setVisible(_Value);
	BottomFrame->setVisible(!_Value);
}


void ModuleListPage::createModuleButtons()
{
	STAppModuleList::iterator it;
	QList<int> InsertedGroups;
	QList<int> InsertedSubGroups;
	for ( it = ModuleList.begin(); it != ModuleList.end(); ++it )
	{
		ModuleAction* NewAction = new ModuleAction ( *it, ModuleFrame);
		connect ( NewAction, SIGNAL ( triggered() ), this, SLOT ( slotModuleActionTriggered()));
		ModuleLabel* NewLabel = new ModuleLabel ( NewAction, ModuleFrame );
		connect(NewLabel, SIGNAL(showUrlInfoClicked(QUrl)), this, SLOT(slotShowUrlInfo(QUrl)));
		LLWLayout->addWidget ( NewLabel );

		if ( !InsertedGroups.contains ( it->group() ) ) //Add Module type button.
		{
			InsertedGroups.push_back ( it->group() );
			ModuleGroupAction* NewAction = new ModuleGroupAction ( it->group(), ModuleTypeFrame  );
			connect ( NewAction, SIGNAL ( triggered() ), this, SLOT ( slotModuleGroupSelected() ) );
			ModuleGroupLabel* NewLabel = new ModuleGroupLabel ( NewAction, ModuleTypeFrame );
			LLModTypeLayout->addWidget ( NewLabel );
		}
		if ( !InsertedSubGroups.contains ( it->subGroup() ) && it->subGroup() != STAppModule::SubGroupNoSubGroup ) //Add Module type button.
		{
			InsertedSubGroups.push_back ( it->subGroup() );
			ModuleSubGroupAction* NewAction = new ModuleSubGroupAction ( it->subGroup(), ModuleTypeFrame  );
			connect ( NewAction, SIGNAL ( triggered() ), this, SLOT ( slotModuleSubGroupSelected() ) );
			ModuleGroupLabel* NewLabel = new ModuleGroupLabel ( NewAction, ModuleTypeFrame );
			LLModTypeLayout->addWidget ( NewLabel );
		}
	}
	//LLModTypeLayout->addItem ( new QSpacerItem ( 0, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding ) );
}


QToolButton* ModuleListPage::newLocaleButton ( const QString& _Locale, QSignalMapper* _Mapper )
{
	QToolButton* NButton = new QToolButton ( this );
	NButton->setIcon ( QIcon ( ":/locale_"+ _Locale + ".png" ) );
	NButton->setIconSize ( QSize ( 64, 64 ) );
	connect ( NButton, SIGNAL ( clicked ( bool ) ), _Mapper, SLOT ( map() ) );
	_Mapper->setMapping ( NButton, _Locale );
	return NButton;
}

void ModuleListPage::deleteModuleButtons()
{
	QList<QAction*> ObjList = ModuleTypeFrame->findChildren<QAction*>();
	ObjList += ModuleFrame->findChildren<QAction*>();
	QList<QAction*>::iterator it; 
	for (it = ObjList.begin(); it != ObjList.end(); ++it)
	{
		delete (*it); 
	}

	QList<MActionLabel*> LabList = ModuleTypeFrame->findChildren<MActionLabel*>();
	LabList += ModuleFrame->findChildren<MActionLabel*>();
	QList<MActionLabel*>::iterator lit; 
	for (lit = LabList.begin(); lit != LabList.end(); ++lit)
	{
		delete (*lit);
	}
}

void ModuleListPage::retranslateUi()
{
	deleteModuleButtons();
	createModuleButtons();
	MMenuLabel->setText("<h1>" + tr("Main Menu") + "</h1>"); 
	showSubGroups(false);
	LoadPhotoBookLabel->setText("<h3>" + tr("Load previously stored PhotoBook") + "</h3>");
}

void ModuleListPage::runEject()
{
#ifndef Q_OS_WIN32
	AppSettings Settings; 
	if (Settings.mediaRunEjectEO())
		system ( "/usr/bin/eject" );
#endif
}

void ModuleListPage::showModulesMenu()
{
	if (MenuEffectEnabled)
	{
		for ( int Vfor = 5; Vfor < 10; Vfor++ )
		{
			ModuleTypeFrame->scroll ( - Vfor * 16, 0 );
			QApplication::processEvents();
		}
	}
	else
		ModuleFrame->scroll ( - 2000, 0 );

	ModuleFrame->show();
	ModuleTypeFrame->hide();
	MMenuLabel->show();
	BottomFrame->hide(); 
}

ModuleListPage::ModuleListPage ( QWidget* _Parent ) : STIWizardPage ( _Parent ), LabelListWidget ( 0 ), MenuEffectEnabled(true)
{
	QVBoxLayout* MLayout = new QVBoxLayout ( this );
	MLayout->setMargin ( 2 );

	// Scroll Menu
	MScrArea = new QScrollArea ( this );
	MScrArea->setSizePolicy ( QSizePolicy::Preferred, QSizePolicy::MinimumExpanding );
	MScrArea->setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
	MScrArea->setWidgetResizable ( true );

	MLayout->addWidget ( MScrArea );

	LabelListWidget = new QWidget ( this );
	LabelListWidget->setObjectName ( "LabelListWidget" );
	MScrArea->setWidget ( LabelListWidget );

	QHBoxLayout* LLMainLayout = new QHBoxLayout ( LabelListWidget );
	LLMainLayout->setSizeConstraint ( QLayout::SetMinAndMaxSize );

	ModuleTypeFrame = new QWidget ( LabelListWidget );
	LLMainLayout->addWidget ( ModuleTypeFrame );

	LLModTypeLayout = new QVBoxLayout ( ModuleTypeFrame );
	LLModTypeLayout->setMargin ( 0 );

	ModuleFrame = new QWidget ( LabelListWidget );
	LLMainLayout->addWidget ( ModuleFrame );

	LLWLayout = new QVBoxLayout ( ModuleFrame );
	LLWLayout->setMargin ( 0 );

	//Return to Type Menu Action
	QAction* MainMenuAction = new QAction ( QIcon ( ":/go-previous.png" ), "", this ); 	
	MMenuLabel = new MActionLabel ( MainMenuAction, "", QUrl(), this, false );
	MMenuLabel->setText("<h3>" + tr("Main Menu") + "</h3>");
	connect ( MainMenuAction, SIGNAL ( triggered ( bool ) ), this, SLOT ( slotShowMainMenu() ) );
	MMenuLabel->setObjectName ( "MainMenuLabel" );
	MMenuLabel->setTextAlignment ( Qt::AlignCenter );
	MMenuLabel->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
	MLayout->addWidget ( MMenuLabel );
	MLayout->setAlignment ( MMenuLabel, Qt::AlignHCenter );
	MMenuLabel->hide();

	QAction* LoadPhotoBookAction = new QAction(QIcon(":/load-photobook.png"), "", this);
	connect(LoadPhotoBookAction, SIGNAL(triggered()), this, SLOT(slotLoadPhotoBook()));
	LoadPhotoBookLabel = new MActionLabel(LoadPhotoBookAction, "", QUrl(), this, false);
	LoadPhotoBookLabel->setText("<h3>" + tr("Load previously stored PhotoBook") + "</h3>");
	LoadPhotoBookLabel->setTextAlignment(Qt::AlignCenter);
	LoadPhotoBookLabel->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred));
	MLayout->addWidget(LoadPhotoBookLabel);
	MLayout->setAlignment(LoadPhotoBookLabel, Qt::AlignCenter);
	LoadPhotoBookLabel->hide();

	BottomFrame = new QWidget(this); 
	QHBoxLayout* BottomLayout = new QHBoxLayout(BottomFrame);
	MLayout->addWidget(BottomFrame);

	QHBoxLayout* LangLayout = new QHBoxLayout;
	BottomLayout->addLayout ( LangLayout );
	BottomLayout->setAlignment ( LangLayout, Qt::AlignHCenter );
	QSignalMapper* LocaleMapper = new QSignalMapper ( this );

	SApplication::TAppLocaleList LocaleList = SApplication::locales(); 
	SApplication::TAppLocaleList::iterator it; 
	for (it = LocaleList.begin(); it != LocaleList.end(); ++it)
	{
		LangLayout->addWidget ( newLocaleButton ( it->Id, LocaleMapper ) );
	}
	connect ( LocaleMapper, SIGNAL ( mapped ( const QString& ) ), this, SLOT ( localeSelected ( const QString& ) ) );

	DAButton = new QToolButton ( this );
	DAButton->setIcon ( QIcon ( ":/directaccess.png" ) );
	DAButton->setText(tr("Direct Action"));
	DAButton->setIconSize ( QSize ( 64, 64 ) );
	DAButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	connect ( DAButton, SIGNAL(clicked( bool)), this, SLOT(slotDirectAction()));
	BottomLayout->addWidget(DAButton);

	QVBoxLayout* LogoLayout = new QVBoxLayout;
	BottomLayout->addLayout ( LogoLayout );

	QLabel* LogoLabel = new QLabel ( this );

	QString PublisherLogoFilePath = QCoreApplication::applicationDirPath() + "/publisherlogo.png";
	if (QFile::exists(PublisherLogoFilePath))
	{
		QLabel* PubLogoLabel = new QLabel(this);
		PubLogoLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
		PubLogoLabel->setPixmap(QPixmap(PublisherLogoFilePath));
		BottomLayout->addWidget(PubLogoLabel);
		BottomLayout->setAlignment(PubLogoLabel, Qt::AlignTop);
		LogoLabel->setPixmap(QPixmap( ":/logo_starblitz.png" ));
	}
	else
		LogoLabel->setPixmap(QPixmap( ":/logo_starblitz-km.png" ));

	LogoLabel->setAlignment ( Qt::AlignRight );
	LogoLayout->addWidget ( LogoLabel );

	QLabel* RevLabel = new QLabel ( SApplication::fullApplicationName(), this );
	RevLabel->setObjectName ( "RevLabel" );
	RevLabel->setAlignment ( Qt::AlignRight );
	LogoLayout->addWidget ( RevLabel );



	ModuleFrame->setVisible ( false );
}


ModuleListPage::~ModuleListPage()
{
}

void ModuleListPage::addBasicModules()
{
	STAppModule DigiPrintsModule(STAppModule::TypeDigiprints, STAppModule::GroupDigitalPhoto);
	STLocaleString DPName("Ditital Prints");
	DPName.addValue("en", "Digital Prints");
	DPName.addValue("es", "Impresiones Digitales");
	DPName.addValue("ca", "Impressions Digitals");
	DPName.addValue("fr", "");
	DPName.addValue("eu", "");
	DigiPrintsModule.setName(DPName);
	STLocaleString DPDescription("Printing of digital pictures");
	DPDescription.addValue("en", "Printing of digital pictures");
	DPDescription.addValue("es", "Impresi�n de fotos digitales");
	DPDescription.addValue("ca", "Impressi� de fotos digitals");
	DPDescription.addValue("fr", "");
	DPDescription.addValue("eu", "");
	DigiPrintsModule.setDescription(DPDescription);
	DigiPrintsModule.setIcon(QIcon(":/digiprintsmodule.png"));
	ModuleList.push_back(DigiPrintsModule);

	STAppModule PhotoIndexModule(STAppModule::TypeIndex, STAppModule::GroupDigitalPhoto);
	STLocaleString PIName("Photo Index");
	PIName.addValue("en", "Photo Index");
	PIName.addValue("es", "Foto �ndice");
	PIName.addValue("ca", "Foto �ndex");
	PIName.addValue("fr", "");
	PIName.addValue("eu", "");
	PhotoIndexModule.setName(PIName);
	STLocaleString PIDescription("Build your photo indexs");
	PIDescription.addValue("en", "Build your photo indexs");
	PIDescription.addValue("es", "Construya sus foto �ndices");
	PIDescription.addValue("ca", "Construeixi els seus foto �ndexs");
	PIDescription.addValue("fr", "");
	PIDescription.addValue("eu", "");
	PhotoIndexModule.setDescription(PIDescription);
	PhotoIndexModule.setIcon(QIcon(":/photoindexmodule.png"));
	ModuleList.push_back(PhotoIndexModule);

	STBurningSettings Settings;
	if (Settings.burningEnabled())
	{
		STAppModule CDRecordModule(STAppModule::TypeCDRecord, STAppModule::GroupStorage);
		STLocaleString CDRName("CD Burn");
		CDRName.addValue("en", "CD Burn");
		CDRName.addValue("es", "Grabaci�n de CDs");
		CDRName.addValue("ca", "Gravaci� de CDs");
		CDRName.addValue("fr", "");
		CDRName.addValue("eu", "");
		CDRecordModule.setName(CDRName);
		STLocaleString CDRDescription("Backup your digital images on CD");
		CDRDescription.addValue("en", "Backup your digital images on CD");
		CDRDescription.addValue("es", "Haz copia de tus im�genes digitales en un CD");
		CDRDescription.addValue("ca", "Fes c�pia de les teves imatges digitals en un CD");
		CDRDescription.addValue("fr", "");
		CDRDescription.addValue("eu", "");
		CDRecordModule.setDescription(PIDescription);
		CDRecordModule.setIcon(QIcon(":/cdrecordmodule.png"));
		ModuleList.push_back(CDRecordModule);
	}
}

void ModuleListPage::init()
{
	if ( ModuleList.isEmpty() )
	{
		addBasicModules();		
		//ModuleList.load();
		ModuleList.loadFromTemplates();
		qSort ( ModuleList );
		if ( !ModuleList.errorStack().isEmpty() )
			SMessageBox::critical ( this, tr ( "Errors loading ttpops modules" ), ModuleList.errorStack().errorString().join ( "," ) );
		createModuleButtons();
		AppSettings Settings;
		DAButton->setVisible(Settings.mainDirectModuleEnabled());
	}
	setFromPrevious();
}

void ModuleListPage::setFromPrevious()
{
	//slotShowMainMenu();
	QList<ModuleLabel*> ModLabels =  ModuleFrame->findChildren<ModuleLabel*>();
	QList<ModuleLabel*>::iterator it;
	for ( it = ModLabels.begin(); it != ModLabels.end(); ++it )
	{
		( *it )->hideInfo();
	}

	showSubGroups(false);
	ModuleTypeFrame->show();
	ModuleFrame->hide();
	MMenuLabel->hide();
	LoadPhotoBookLabel->hide();
	BottomFrame->show(); 

	MScrArea->ensureVisible ( 0,0 );
	runEject();
}

void ModuleListPage::slotModuleActionTriggered()
{
	if ( ModuleAction* CurrAction = qobject_cast<ModuleAction*> ( sender() ) )
		emit moduleTriggered ( CurrAction->module() );
}

void ModuleListPage::slotLoadPhotoBook()
{
	emit loadPhotoBook();
//	STAppModule LoadModule(STAppModule::TypePhotoBooks, STAppModule::GroupPhotoBook);
//	LoadModule.setLoadTemplateFromDisk(true);
//	emit moduleTriggered(LoadModule);
}

void ModuleListPage::slotShowUrlInfo(const QUrl& _Url)
{
	ModuleLabel* CurrModLabel = qobject_cast<ModuleLabel*>(sender());

	if (CurrModLabel)
	{
		MScrArea->verticalScrollBar()->setValue(CurrModLabel->y());
		CurrModLabel->showUrlInfo(_Url);
		MMenuLabel->hide();
		//MScrArea->ensureWidgetVisible(CurrModLabel);
	}
}

void ModuleListPage::localeSelected ( const QString& _Locale )
{
	SApplication::setLocale ( _Locale );
	retranslateUi();
}

void ModuleListPage::slotShowMainMenu()
{
	if (MenuEffectEnabled)
	{
		for ( int Vfor = 10; Vfor < 20; Vfor++ )
		{
			ModuleFrame->scroll ( Vfor * 8, 0 );
			QApplication::processEvents();
		}
	}
	else
		ModuleFrame->scroll ( 2000, 0 );
	ModuleFrame->hide();
	ModuleTypeFrame->show();
	BottomFrame->show(); 
	showSubGroups(false);
}


void ModuleListPage::slotModuleGroupSelected()
{
	if ( ModuleGroupAction* CurrAction = qobject_cast<ModuleGroupAction*> ( sender() ) )
	{
		if (STAppModule::hasSubGroups(CurrAction->moduleGroup()))
		{
			showSubGroups(true);
			//filterSubGroups(CurrAction->moduleGroup());
		}
		else
		{
			filterModules ( CurrAction->moduleGroup() );
			showModulesMenu();
		}
		if (CurrAction->moduleGroup() == STAppModule::TypePhotoBooks)
		{
			LoadPhotoBookLabel->show();
			MMenuLabel->hide();
		}

	}
}

void ModuleListPage::slotModuleSubGroupSelected()
{
	if ( ModuleSubGroupAction* CurrAction = qobject_cast<ModuleSubGroupAction*> ( sender() ) )
	{
		filterModules(CurrAction->moduleSubGroup());
		showModulesMenu();
	}
}

void ModuleListPage::slotDirectAction()
{
	AppSettings Settings;
	if (Settings.mainDirectModuleEnabled())
	{
		int ModuleIndex = Settings.mainDirectModuleIndex();
		if (ModuleIndex >= 0 && ModuleIndex < ModuleList.size())
			emit moduleTriggered(ModuleList[ModuleIndex]);
	}
}

void ModuleListPage::mousePressEvent ( QMouseEvent* _Event )
{
	QDesktopWidget Desktop;
	if ( _Event->y() > Desktop.size().height() - 40 && _Event->x() < 40 )
	{
		emit settingsSelected();
	}
}


