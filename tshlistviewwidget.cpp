/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "tshlistviewwidget.h"
#include <QLayout> 
#include <QToolButton> 
#include <QListView>
#include <QApplication> 
#include <QKeyEvent> 
#include <QScrollBar> 


TSHListViewWidget::TSHListViewWidget(QWidget *parent)
 : QWidget(parent)
{
	QHBoxLayout* MLayout = new QHBoxLayout(this); 
	MLayout->setMargin(0);
	MLayout->setSpacing(0);
	ListView = new QListView(this); 
	connect(ListView, SIGNAL(clicked( const QModelIndex& )), this, SIGNAL(applyItem(const QModelIndex& )));
	ListView->setViewMode(QListView::IconMode); 
	ListView->setFlow(QListView::TopToBottom); 
	ListView->setSpacing(8);
	MLayout->addWidget(ListView); 
}


TSHListViewWidget::~TSHListViewWidget()
{
}

void TSHListViewWidget::setModel(QAbstractItemModel* _Model)
{
	ListView->setModel(_Model); 
}


