/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef TPORDERSUMMARYPAGE_H
#define TPORDERSUMMARYPAGE_H

#include "stiwizardpage.h"
#include <QSqlRecord>
#include "printjob.h"
#include "publisherdatabase.h"

/**
Order summary page.

	@author
*/
class QLabel; 
class QToolButton;
class SProcessStatusWidget;
class TPOrderSummaryPage : public STIWizardPage
{
	Q_OBJECT

public:
	enum EnStatus
	{
		StatusShowProgress,
		StatusShowSummary
	};

private:
	QLabel* HeaderLabel; 
	QLabel* SummaryLabel; 
	QToolButton* ButAccept; 
	QToolButton* ButPrevious; 
	QToolButton* ButCancel; 
	STDom::PrintJob MPrintJob;
	STDom::PublisherBill::PublisherShippingMethod ShippingMethod;
	SProcessStatusWidget* StatW;

	void retranslateUi();
	QToolButton* newActionButton(const QIcon& _Icon);

public:
	TPOrderSummaryPage(QWidget* _Parent = 0);
	void init();
	void setPrintJob(const STDom::PrintJob& _PrintJob);
	void calcBill();
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod& _Value) { ShippingMethod = _Value; }
	void setStatus(EnStatus _Status);
	SProcessStatusWidget* processStatus() const { return StatW; }

signals:
	void orderAccepted(); 
	void orderCanceled(); 
};

#endif
