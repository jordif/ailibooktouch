/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "pbpagesview.h"
#include <QToolBar>
#include <QLayout>
#include <QAction>
#include "stphotobook.h"


PBPagesView::PBPagesView(QWidget* _Parent) : TSHListViewWidget(_Parent)
{
	QToolBar* ToolBar = new QToolBar(this);
	ToolBar->setOrientation(Qt::Vertical);
	ToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	layout()->addWidget(ToolBar);

	InsertPageAction = new QAction(QIcon(":/phototemplates/list-add.png"), tr("Insert Page"), this);
	connect(InsertPageAction, SIGNAL(triggered()), this, SIGNAL(addPage()));
	ToolBar->addAction(InsertPageAction);

	RemovePageAction = new QAction(QIcon(":/phototemplates/list-remove.png"), tr("Remove Page"), this);
	connect(RemovePageAction, SIGNAL(triggered()), this, SIGNAL(removePage()));
	ToolBar->addAction(RemovePageAction);
	retranslateUi();
}

void PBPagesView::retranslateUi()
{
	RemovePageAction->setText(tr("Remove Page"));
	InsertPageAction->setText(tr("Insert Page"));
}
