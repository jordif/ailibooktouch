/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef FORMATLISTPAGE_H
#define FORMATLISTPAGE_H
#include <QAction>
#include "stiwizardpage.h"
#include "ddoc.h"

/**
	@author jordif.starblitz@gmail.com
*/
class FormatAction : public QAction
{
	Q_OBJECT

	STDom::DDocFormat Format;

public:
	FormatAction(const STDom::DDocFormat& _Format, QObject* _Parent);
	STDom::DDocFormat  format() const { return Format; }
};

class QWidget;
class QScrollArea;
class MActionLabel;
class QVBoxLayout;
class FormatListPage : public STIWizardPage
{
Q_OBJECT
	QWidget* LabelListWidget;
	QWidget* FormatsFrame;
	QScrollArea* MScrArea;
	MActionLabel* GoBackLabel;
	QVBoxLayout* LLModTypeLayout;

public:
	FormatListPage(QWidget* _Parent);
	void setFormats(const STDom::DDocFormatList& _Formats);
	void init();
	void setFromPrevious();

private slots:
	void slotFormatSelected();

signals:
	void formatSelected(const STDom::DDocFormat& _Format);
};

#endif // FORMATLISTPAGE_H
