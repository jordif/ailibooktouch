/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "getmediapage.h"
#include <QLabel>
#include <QDialog>
#include <QLayout>
#include <QDir>
#include <QApplication>
 
#include "appsettings.h" 
#include "stimagetools.h" 
#include "sapplication.h" 

#include "stutils.h" 


//_____________________________________________________________________________
// Class CustomPathButton
//_____________________________________________________________________________

CustomPathButton::CustomPathButton(const QString& _Label, const QString& _Path, QWidget* _Parent) :
		QToolButton(_Parent), Path(_Path)
{
	setObjectName("MediaButton");

	setIcon(QIcon(":/picturefolder.png"));
	setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	setIconSize(QSize(128,128));
	setText(_Label);
}

QString CustomPathButton::path() const
{
	return Path;
}


//_____________________________________________________________________________
// Class GetMediaPage
//_____________________________________________________________________________

void GetMediaPage::clearBluetooth()
{
	AppSettings Settings; 
	QDir BluetoothDir(Settings.mediaBluetoothInputPath().absoluteFilePath());
	
	if (BluetoothDir.exists())
	{
		STUtils::rmDir(BluetoothDir); 
		BluetoothDir.mkpath(BluetoothDir.absolutePath());
	}
}


void GetMediaPage::retranslateUi()
{
	MainLabel->setText(tr("<h1>Introduce the card or CD in the right socket.</h1> <h2>Then press the selected media button.</h2>")); 
	MainLabel->setWordWrap(true); 
	TButtonMCard->setText(tr("Mem.C./Pendrive"));
	TButtonCD->setText(tr("CD or DVD")); 
	TButtonFacebook->setText(tr("Facebook"));
	TBluetooth->setText(tr("Bluetooth")); 
	ButBack->setText(tr("Cancel")); 
} 

QToolButton* GetMediaPage::newMediaToolButton(const QString& _Icon)
{
	QToolButton* Res = new QToolButton(this); 
	Res->setObjectName("MediaButton"); 
 	Res->setIcon(QIcon(_Icon));
 	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
 	Res->setIconSize(QSize(128,128)); 
	return Res; 
}

GetMediaPage::GetMediaPage(QWidget* _Parent): STIWizardPage(_Parent), WritableMediaSelected(false)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	MainLabel = new QLabel(this); 
	MainLabel->setAlignment(Qt::AlignHCenter);
	MainLabel->setObjectName("GetMediaMainLabel");
	MLayout->addWidget(MainLabel); 

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding)); 
	
	QHBoxLayout* MCustomButLayout = new QHBoxLayout;
	MLayout->addLayout(MCustomButLayout);

	AppSettings Settings;
	for (int Vfor = 0; Vfor < AppSettings::NumCustomPaths; Vfor++ )
	{
		if (Settings.mediaCustomPathEnabled(Vfor))
		{
			CustomPathButton* NewButton = new CustomPathButton(Settings.mediaCustomPathLabel(Vfor),
														  Settings.mediaCustomPath(Vfor));
			connect(NewButton, SIGNAL(clicked()), this, SLOT(slotSelectCustomPathNode()));
			MCustomButLayout->addWidget(NewButton);
		}
	}

	QHBoxLayout* MButtLayout = new QHBoxLayout; 
	MLayout->addLayout(MButtLayout); 

	TButtonMCard = newMediaToolButton(":/sd_mmc.png"); 
	connect(TButtonMCard, SIGNAL(clicked( bool )), this, SLOT(searchInUsbPen())); 
	MButtLayout->addWidget(TButtonMCard); 

	TButtonCD = newMediaToolButton(":/cdrom.png"); 
	connect(TButtonCD, SIGNAL(clicked( bool )), this, SLOT(searchInUsbCdRom())); 
	MButtLayout->addWidget(TButtonCD); 

	TBluetooth = newMediaToolButton(":/bluetooth.png"); 
	MButtLayout->addWidget(TBluetooth); 
	connect(TBluetooth, SIGNAL(clicked( bool )), this, SLOT(searchInBluetooth())); 

	//If collection view is enabled:
	TButtonFacebook = newMediaToolButton(":/facebook.png");
	MButtLayout->addWidget(TButtonFacebook);
	connect(TButtonFacebook, SIGNAL(clicked( bool )), this, SLOT(selectFacebookNode()));
	TButtonFacebook->setVisible(false);
	//TButtonCollection->setVisible(false);

	MLayout->addItem(new QSpacerItem(0, 40, QSizePolicy::Preferred, QSizePolicy::Fixed)); 

	ButBack = new QToolButton(this);
	ButBack->setObjectName("ActionButton");
	ButBack->setIcon(QIcon(":/st/tpopsl/cancel.png")); 
	ButBack->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ButBack->setIconSize(QSize(64, 64)); 
	MLayout->addWidget(ButBack);
	MLayout->setAlignment(ButBack, Qt::AlignHCenter); 
	connect ( ButBack, SIGNAL(clicked()), this, SIGNAL(previousPage()));

	MLayout->addItem(new QSpacerItem(0, 40, QSizePolicy::Preferred, QSizePolicy::Fixed)); 
	
// 	QLabel* LogoLabel = new QLabel(this); 
// 	LogoLabel->setPixmap(QPixmap(":/st/logo_starblitz.png")); 
// 	LogoLabel->setAlignment(Qt::AlignHCenter); 
// 	MLayout->addWidget(LogoLabel); 
// 
// 	QLabel* RevLabel = new QLabel(SApplication::fullApplicationName(), this); 
// 	RevLabel->setObjectName("RevLabel");
// 	RevLabel->setAlignment(Qt::AlignHCenter); 
// 	MLayout->addWidget(RevLabel); 
	retranslateUi(); 
}

GetMediaPage::~GetMediaPage()
{
}

void GetMediaPage::init()
{
	WritableMediaSelected = false;
	retranslateUi(); 
	clearBluetooth(); 
}

void GetMediaPage::setFromPrevious()
{
	//emit previousPage();
}

void GetMediaPage::setCDSourceEnabled(bool _Value)
{
	TButtonCD->setVisible(_Value);
}

void GetMediaPage::setBluetoothSourceEnabled(bool _Value)
{
	TBluetooth->setVisible(_Value);
}


void GetMediaPage::searchInUsbPen()
{
	AppSettings Settings;
	WritableMediaSelected = true;
 #ifdef Q_OS_WIN32
	QFileInfoList ExcludedDrives = Settings.mediaExcludedCardDrives();
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QFileInfo SourcePath = STImageTools::searchDriveWithFiles(ExcludedDrives);
	QApplication::restoreOverrideCursor();
	if (SourcePath.exists())
	{
		mediaSelected(QDir(SourcePath.absolutePath()));
	}

 #else
	mediaSelected(QDir(Settings.mediaPath().absoluteFilePath())); 
 #endif 
}

void GetMediaPage::searchInUsbCdRom()
{
	AppSettings Settings; 
 #ifdef Q_OS_WIN32
	mediaSelected(QDir(Settings.mediaCdDrive().absoluteFilePath()));
 #else 
	mediaSelected(QDir(Settings.mediaPath().absoluteFilePath()));
 #endif 
}

void GetMediaPage::searchInBluetooth()
{
	AppSettings Settings; 
	mediaSelected(QDir(Settings.mediaBluetoothInputPath().absoluteFilePath()));
}

void GetMediaPage::selectFacebookNode()
{
}

void GetMediaPage::slotSelectCustomPathNode()
{
	if (CustomPathButton* Button = static_cast<CustomPathButton*>(sender()))
	{
		WritableMediaSelected = true;
		emit mediaSelected(QDir(Button->path()));
	}
}
