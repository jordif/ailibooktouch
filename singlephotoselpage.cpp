/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "singlephotoselpage.h"
#include <QLayout> 
#include <QToolButton> 
#include <QFileDialog> 
#include <QFileInfoList> 
#include <QLabel>
#include <QApplication>
#include <QKeyEvent>

#include "smessagebox.h"
#include "stfolderselector.h" 
#include "checkitemdelegate.h"
#include "stthumbnailview.h" 
#include "tpphotoeditor.h"
#include "tpproductlistview.h"

#include "printjobmodel.h"
#include "dimagedoc.h"
#include "ddocmodel.h"
#include "stnumqualitygauge.h"
#ifdef TTPOPS_KINETICSCROLL
#include "qscrollareakineticscroller.h"
#endif

void SinglePhotoSelPage::retranslateUi()
{
	BackBut->setText(tr("Back")); 
	SelectAllBut->setText(tr("All")); 
	SelectNoneBut->setText(tr("None")); 
	FilterBut->setText(tr("Filter")); 
	NoFilterBut->setText(tr("NO Filter")); 
	OkBut->setText(tr("Accept")); 
	ZoomBut->setText(tr("Edit"));
}

QToolButton* SinglePhotoSelPage::newActionButton(const QString& _Icon)
{
	QToolButton* Res = new QToolButton(this); 
	Res->setObjectName("SmallActionButton");
 	Res->setIcon(QIcon(_Icon));
 	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
 	Res->setIconSize(QSize(64,64)); 
	return Res; 
}

QToolButton* SinglePhotoSelPage::newImageActionButton(const QString& _Icon)
{
	QToolButton* Res = newActionButton(_Icon);
	Res->setObjectName("ImageActionButton");
	return Res;
}

SinglePhotoSelPage::SinglePhotoSelPage(QWidget* _Parent)
 : STIWizardPage(_Parent)
{
	MLayout = new QVBoxLayout(this); 
	MLayout->setMargin(2); 
	MLayout->setSpacing(2); 

	HeaderLabel = new QLabel(this);
	HeaderLabel->setObjectName("SinglePhotoSelHeader");
	HeaderLabel->setAlignment(Qt::AlignCenter);
	MLayout->addWidget(HeaderLabel);

	QHBoxLayout* TopLayout = new QHBoxLayout;
	MLayout->addLayout(TopLayout);

	QVBoxLayout* RButLayout = new QVBoxLayout;
	TopLayout->addLayout(RButLayout);

	LVImages = new STThumbnailView(this); 
	LVImages->setSelectionMode(QAbstractItemView::MultiSelection); 
	LVImages->setSelectionBehavior(QAbstractItemView::SelectRows); 
	LVImages->setMovement(QListView::Static);
	LVImages->setFlow(QListView::LeftToRight);
	CheckItemDelegate* PDelegate = new CheckItemDelegate(LVImages); 
	LVImages->setItemDelegate(PDelegate); 
	//LVImages->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	Model = new STDom::PrintJobModel(this);
	STDom::DDocModel* MDocModel = new STDom::DDocModel(this);
	MDocModel->setNoImagePixmap(QPixmap(":/hourglass.png"));
	Model->setSourceModel(MDocModel);
	Model->setRitchTextDisplay(false);
	Model->setThumbnailSize(STDom::DImageDoc::thumbnailSize());
	LVImages->setModel(Model);

	TopLayout->addWidget(LVImages);

	QToolButton* PrevPageBut = newImageActionButton(":/st/tpopsl/1uparrow.png");
	connect(PrevPageBut, SIGNAL(clicked( bool )), this, SLOT(slotPreviousPage()));
	RButLayout->addWidget(PrevPageBut);

	RButLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));

	QToolButton* NextPageBut = newImageActionButton(":/st/tpopsl/1downarrow.png");
	connect(NextPageBut, SIGNAL(clicked( bool )), this, SLOT(slotNextPage()));
	RButLayout->addWidget(NextPageBut);

	RButLayout->addItem(new QSpacerItem(10, 50, QSizePolicy::Preferred, QSizePolicy::Preferred));

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::Preferred));

	QHBoxLayout* BottomLayout = new QHBoxLayout;
	MLayout->addLayout(BottomLayout); 

	BackBut = newActionButton(":/st/tpopsl/previous.png");
	BackBut->setObjectName("ActionButton");
	connect(BackBut, SIGNAL(clicked( bool )), this, SIGNAL(previousPage()));
	BottomLayout->addWidget(BackBut); 

	BottomLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	ZoomBut = newActionButton(":/st/tpopsl/zoom.png");
	connect(ZoomBut, SIGNAL(clicked( bool )), this, SLOT(editCurrent()));
	BottomLayout->addWidget(ZoomBut);

	SelectAllBut = newActionButton(":/st/edit_add.png");
	connect(SelectAllBut, SIGNAL(clicked( bool )), LVImages, SLOT(selectAll())); 
	BottomLayout->addWidget(SelectAllBut); 

	SelectNoneBut = newActionButton(":/st/edit_remove.png");
	connect(SelectNoneBut, SIGNAL(clicked( bool )), LVImages, SLOT(clearSelection())); 
	BottomLayout->addWidget(SelectNoneBut); 

	FilterBut = newActionButton(":/st/tpopsl/filter.png");
	connect(FilterBut, SIGNAL(clicked( bool)), this, SLOT(getFolderFilter())); 
	BottomLayout->addWidget(FilterBut); 
	
	NoFilterBut = newActionButton(":/st/tpopsl/nofilter.png");
	connect(NoFilterBut, SIGNAL(clicked( bool)), this, SLOT(clearFolderFilter())); 
	//NoFilterBut->setVisible(false); 
	BottomLayout->addWidget(NoFilterBut); 

	BottomLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	//InfoLabel->setAlignment(Qt::AlignHCenter);
	//BottomLayout->addWidget(InfoLabel);

	NumImagesGauge = new STNumQualityGauge(this);
	BottomLayout->addWidget(NumImagesGauge);

	connect(LVImages->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(updateGauge()));

	LVProducts = new TPProductListView(this);
	LVProducts->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
	LVProducts->setVisible(false);
	BottomLayout->addWidget(LVProducts);

	BottomLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	OkBut = newActionButton(":/st/tpopsl/accept.png");
	OkBut->setObjectName("ActionButton");

	connect(OkBut, SIGNAL(clicked( bool )), this, SLOT(slotOkClicked()));
	BottomLayout->addWidget(OkBut); 

	PEditor = new TPPhotoEditor(this);
	PEditor->setModel(Model);
#ifdef TTPOPS_KINETICSCROLL
	QScrollAreaKineticScroller* NewScroller = new QScrollAreaKineticScroller();
	NewScroller->setWidget(LVImages);
#endif

}

void SinglePhotoSelPage::init()
{
	retranslateUi(); 
	if (Model)
	{
		if (Model->rowCount() > 0)
		{
			updateGauge();
		}
	}
	clearFolderFilter();
}

void SinglePhotoSelPage::setImages(const QFileInfoList& _Images)
{
	Model->sourceDocModel()->setDocs(_Images);
}

void SinglePhotoSelPage::setProductsModel(QAbstractItemModel* _Model)
{
	LVProducts->setVisible(true);
	NumImagesGauge->setVisible(false);
	LVProducts->setModel(_Model);
}

STDom::DDocProduct SinglePhotoSelPage::currentProduct() const
{
	return LVProducts->currentProduct();
}

void SinglePhotoSelPage::setHeaderText(const QString& _Text)
{
	HeaderLabel->setText(_Text);
}

void SinglePhotoSelPage::setOptimalNumImagesRange(int _From, int _To)
{
	NumImagesGauge->setOptimalQualityRange(_From, _To);
	updateGauge();
}

bool SinglePhotoSelPage::hasChanges()
{
	return LVImages->selectionModel()->selectedRows().count() >0;
}


QFileInfoList SinglePhotoSelPage::selectedImages() const
{
	QFileInfoList Res; 
	if (LVImages->selectionModel())
	{
		QModelIndexList SelectedIndexs = LVImages->selectionModel()->selectedRows(); 
		QModelIndexList::iterator it;
		for (it = SelectedIndexs.begin(); it != SelectedIndexs.end(); ++it)
		{
			Res.push_back(Model->doc(*it)->fileInfo());
		}
		
	}
	return Res; 
}


void SinglePhotoSelPage::getFolderFilter()
{
	if (Model)
	{
		QString FFilter = STFolderSelector::getExistingDirectory(this, tr("Please select directory to filter"), RootDir.absolutePath());
		if (!FFilter.isEmpty())
		{
			Model->setFolderFilter(FFilter); 
			NoFilterBut->setVisible(true); 
			updateGauge();
		}
	}
}

void SinglePhotoSelPage::clearFolderFilter()
{
	if (Model)
	{
		Model->clearFolderFilter(); 
		NoFilterBut->setVisible(false); 
		updateGauge();
	}
}

void SinglePhotoSelPage::updateGauge()
{
	if (Model)
		NumImagesGauge->setValue(LVImages->selectionModel()->selectedRows().size());
	//	InfoLabel->setText(tr("<b style=\"font-size:24pt;\">%1</b> of %2 <em>images selected.</em>").arg(LVImages->selectionModel()->selectedRows().size()).arg(Model->rowCount()));
}

void SinglePhotoSelPage::slotOkClicked()
{
	if (Model)
		Model->clearFolderFilter();
	emit accepted();
}

void SinglePhotoSelPage::editCurrent()
{
	//PEditor->setModel(static_cast<STProductPrintsProxyModel*>(LVImages->model()));
	try
	{
		PEditor->exec(LVImages->currentIndex());
	}
	catch(const STError& _Error)
	{
		QApplication::restoreOverrideCursor();
		SMessageBox::critical(this, tr("Error opening image"), _Error.description());
	}
}

void SinglePhotoSelPage::slotNextPage()
{
	qApp->notify(LVImages, new QKeyEvent(QEvent::KeyPress, Qt::Key_PageDown, Qt::NoModifier));
}

void SinglePhotoSelPage::slotPreviousPage()
{
	qApp->notify(LVImages, new QKeyEvent(QEvent::KeyPress, Qt::Key_PageUp, Qt::NoModifier));
}

