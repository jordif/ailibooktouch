/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef STAPPMODULE_H
#define STAPPMODULE_H

#include <QDomElement>
#include <QIcon>
#include <QList> 
#include <QCoreApplication> 

#include "sterrorstack.h" 
#include "sterror.h" 
#include "stlocalestring.h" 
#include "ddoc.h"
#include "templateinfo.h"
#include "templateinfolist.h"

/**
Abstraction of an Application Module

	@author
*/
class QDir; 
class STAppModule
{

	Q_DECLARE_TR_FUNCTIONS(STAppModule);

public:
	typedef enum EnModuleType
	{
		TypeUnknown = -1,
		TypeDigiprints = 0,
		TypeDecorations,
		TypePhotoBooks,
		TypeCDRecord,
		TypeVideo,
		TypeGifts,
		TypeIndex,
		TypeBatchTemplate,
		TypePDFDoc,
		TypePhotoId,
		TypeCalendars
	} TModuleType;

	typedef enum EnModuleGroup
	{
		GroupDigitalPhoto = 0, 
		GroupPhotoAssembly, 
		GroupPhotoBook, 
		GroupStorage, 
		GroupDigitalDocs, 
		GroupPhotoGift
	} TModuleGroup;

	typedef enum EnModuleSubGroup
	{
		SubGroupNoSubGroup = -1,
		SubGroupPhotoId = 0,
		SubGroupCalendars,
		SubGroupCards,
		SubGroupBatchTemplates
	} TModuleSubGroup;
	
private: 
	TModuleType Type; 
	TModuleGroup Group;
	TModuleSubGroup SubGroup;
	STDom::DDocProductList Products;
	STLocaleString Name, Description;
	QIcon Icon; 
	double Price;
	SPhotoBook::TemplateInfo TemplateInfo;
	bool LoadTemplateFromDisk;

	bool IsNull; 

public:
	void clear(); 
	//! Constructs a Null Module.
	STAppModule();
	STAppModule(TModuleType _Type, TModuleGroup _Group, TModuleSubGroup _SubGroup = SubGroupNoSubGroup);
	void setTemplateInfo(const SPhotoBook::TemplateInfo& _TemplateInfo);
	SPhotoBook::TemplateInfo templateInfo() const { return TemplateInfo; }
	bool loadXml(const QString& _FileName);
	bool load(const QDir& _ModuleDir);
	static bool hasSubGroups(TModuleGroup _Group);
	bool isNull() const { return IsNull; }
	QString name() const { return Name.value(); }
	void setName( const STLocaleString& _Value) { Name = _Value; }
	QString description() const { return Description.value(); }
	void setDescription(const STLocaleString& _Value) { Description = _Value; }
	void setIcon(const QIcon& _Icon) { Icon = _Icon; }
	QIcon icon() const { return Icon; }
	TModuleType type() const { return Type; }
	TModuleGroup  group() const { return Group; }
	TModuleSubGroup subGroup() const { return SubGroup; }
	double price() const { return Price; }
	void setLoadTemplateFromDisk(bool _Value) { LoadTemplateFromDisk = _Value; }
	bool loadTemplateFromDisk() const { return LoadTemplateFromDisk; }
	bool operator<(const STAppModule& ) const;
	static int firstModuleType() { return TypeDigiprints; }
	static int lastModuleType() { return TypePDFDoc; }
	static QString typeName(STAppModule::EnModuleType _Type);
	static QString groupName(STAppModule::EnModuleGroup _Group); 
	static QString groupDescription(STAppModule::EnModuleGroup _Group); 
	static QString subGroupName(STAppModule::EnModuleSubGroup _Group);
	static QString subGroupDescription(STAppModule::EnModuleSubGroup _Group);

};

/**
Abstraction of an Application Module

	@author
*/
class STAppModuleList : public QList<STAppModule>
{
public: 
	ST_DECLARE_ERRORCLASS(); 

private:
	STErrorStack ErrStack; 
	void addTemplates(STAppModule::EnModuleType _Type, STAppModule::EnModuleGroup _Group,
					  STAppModule::EnModuleSubGroup _SubGroup, const SPhotoBook::TemplateInfoList& _Model);


public: 
	STAppModuleList() {}
	bool loadFromTemplates();
	STErrorStack errorStack() const { return ErrStack; } 
};


#endif
