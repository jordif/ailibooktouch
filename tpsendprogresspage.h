/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef TPSENDPROGRESSPAGE_H
#define TPSENDPROGRESSPAGE_H

#include "stiwizardpage.h"


/**
	@author
*/
class QProgressBar; 
class QLabel; 
class SProcessStatusWidget;
class TPSendProgressPage : public STIWizardPage
{
	Q_OBJECT
	QLabel* LabHeader; 
	QLabel* MessageLabel;
	QProgressBar* MProgressBar; 
	SProcessStatusWidget* StatusWidg;
	QTimer*	QuitTimer;

	void retranslateUi();

public:
	TPSendProgressPage(QWidget* _Parent = 0);
	~TPSendProgressPage();
	QProgressBar* progressBar() const { return MProgressBar; }
	void init();
	void showMessageAndQuit(const QString& _Message);
	void mouseReleaseEvent ( QMouseEvent * event );


signals:
	void finish();
};

#endif
