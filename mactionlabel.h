/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef MACTIONLABEL_H
#define MACTIONLABEL_H

#include <QFrame>
#include <QtWebKit/QWebView>

/**
	@author jordif.starblitz@gmail.com
*/

class ClickableWebView : public QWebView
{
	Q_OBJECT

public:
	ClickableWebView(QWidget* _Parent);
protected:
	void mouseReleaseEvent(QMouseEvent* );
signals:
	void clicked();
};

/**
	@author jordif.starblitz@gmail.com
*/
class STClickableLabel;
class MActionLabel : public QFrame
{
	Q_OBJECT

	QAction* Action;
	STClickableLabel* DescLabel;
	QUrl RemoteInfo;
	ClickableWebView* WebView;

	bool showingInfo() const;

public:
	MActionLabel(QAction* _Action, const QString& _Description, const QUrl& _RemoteInfo, QWidget* _Parent = 0,  bool _Wide = true);
	void showUrlInfo(const QUrl& _UrlInfo);
	void hideInfo();
	void setTextAlignment(Qt::Alignment _Alignment);
	void setText(const QString& _Value);

protected:

protected slots:
	void slotLabelClicked();
	void slotDoAction();
signals:
	void showUrlInfoClicked(const QUrl& _UrlInfo);
};


#endif // MACTIONLABEL_H
