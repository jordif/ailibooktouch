/****************************************************************************
**
** Copyright (C) 2010-2011 Softtopia. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "templatemanagerwidget.h"
#include <QToolBar>
#include <QLayout>
#include <QComboBox>
#include <QtWebKit/QWebView>
#include <QAction>
#include <QLabel>
#include <QListView>
#include <QTableView>
#include <QToolButton>
#include <QHeaderView>
#include <QFileDialog>
#include <QInputDialog>
#include <QDebug>

#include "siconfactory.h"
#include "ttpopsapp.h"
#include "remotepublishermanager.h"
#include "smessagebox.h"
#include "sterror.h"
#include "defaultdbselectiondialog.h"
#include "fsqldatabasemanager.h"
#include "wproductmanager.h"
#include "sacceptcanceldialog.h"

#include "designinfotablemodel.h"
#include "collectioninfo.h"
#include "metainfowidget.h"
#include "systemtemplates.h"
#include "smultiprocessstatuswidget.h"

//.---------------------------------------------------------------------------.
//   Class TemplateInfoWidget
//.___________________________________________________________________________.


TemplateInfoWidget::TemplateInfoWidget(QWidget* _Parent) : QWidget(_Parent)
{
	QVBoxLayout* MainLayout = new QVBoxLayout(this);
	MainLayout->addWidget(new QLabel(tr("<h2>Templates</h2>"), this));

	QHBoxLayout* BottomLayout = new QHBoxLayout;
	MainLayout->addLayout(BottomLayout);

	QVBoxLayout* BotLeftLayout = new QVBoxLayout;
	BottomLayout->addLayout(BotLeftLayout);

	QHBoxLayout* CBSizeLayout = new QHBoxLayout;
	BotLeftLayout->addLayout(CBSizeLayout);
	CBSizeLayout->addWidget(new QLabel(tr("Size"), this));
	CBSize = new QComboBox(this);
	connect(CBSize, SIGNAL(currentIndexChanged(int)), this, SLOT(slotTemplateSizeSelected(int)));
	CBSizeLayout->addWidget(CBSize);

	LVTemplates = new QListView(this);
	connect(LVTemplates, SIGNAL(clicked(QModelIndex)), this, SLOT(slotTemplateIndexClicked(QModelIndex)));
	BotLeftLayout->addWidget(LVTemplates);

	QVBoxLayout* BotRightLayout = new QVBoxLayout;
	BottomLayout->addLayout(BotRightLayout);

	WVInfoTemplates = new QWebView(this);
	BotRightLayout->addWidget(WVInfoTemplates);

	Model = new SPhotoBook::TemplateInfoModel(this);
	LVTemplates->setModel(Model);

}


void TemplateInfoWidget::setTemplateList(const SPhotoBook::TemplateInfoList& _Templates)
{
	Model->setTemplateList(_Templates);
	TemplateList = _Templates;
}

void TemplateInfoWidget::updateTemplateInfo(const SPhotoBook::TemplateInfo& _Old, const SPhotoBook::TemplateInfo& _New)
{
	TemplateList.updateTemplateInfo(_Old, _New);
	Model->setTemplateList(TemplateList);
}


void TemplateInfoWidget::slotTemplateIndexClicked(const QModelIndex& _Index)
{
	WVInfoTemplates->stop();

	//Load available sizes.
	CBSize->clear();
	QList<QSizeF> SizesList = Model->sizes(_Index);
	QList<QSizeF>::iterator it;
	for (it = SizesList.begin(); it != SizesList.end(); ++it)
	{
		QSizeF CSize = *it;
		CBSize->addItem(QString("%1x%2 mm").arg(CSize.width()).arg(CSize.height()), CSize);
	}
	if (!SizesList.isEmpty())
	{
		//Get info url from model and display it.
		QUrl InfoUrl = pManager->starlabManager()->infoUrl(Model->templateInfo(_Index, SizesList.first()));
		WVInfoTemplates->setHtml("");
		//setCurrentState(StateGettingInfo);
		WVInfoTemplates->load(InfoUrl);
	}
}

void TemplateInfoWidget::slotTemplateSizeSelected(int _Index)
{
	QSizeF SelectedSize = CBSize->itemData(_Index).toSizeF();
	emit templateSelected(Model->templateInfo(LVTemplates->currentIndex(), SelectedSize));
}



//.---------------------------------------------------------------------------.
//   Class DesignInfoWidget
//.___________________________________________________________________________.

void DesignInfoWidget::setupActions()
{
	AGDesignActions = new QActionGroup(this);

	QAction* DeleteDesignAct = new QAction(stIcon(SIconFactory::EditDelete), tr("Uninstall"), this);
	connect(DeleteDesignAct, SIGNAL(triggered()), this, SLOT(slotDeleteCurrentDesign()));
	AGDesignActions->addAction(DeleteDesignAct);

	QAction* DownloadDesignAct = new QAction(QIcon(":/download.png"), tr("Install"), this);
	connect(DownloadDesignAct, SIGNAL(triggered()), this, SIGNAL(downloadCurrentDesign()));
	AGDesignActions->addAction(DownloadDesignAct);
}

void DesignInfoWidget::setupToolBars()
{
	TBDesignActions->addActions(AGDesignActions->actions());
	TBDesignActions->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}

DesignInfoWidget::DesignInfoWidget(QWidget* _Parent) : QWidget(_Parent)
{
	QVBoxLayout* MainLayout = new QVBoxLayout(this);
	MainLayout->addWidget(new QLabel(tr("<h2>Designs</h2>"), this));

	TBDesignActions = new QToolBar(this);
	MainLayout->addWidget(TBDesignActions);

	TVDesigns = new QTableView(this);
	TVDesigns->setSelectionBehavior(QAbstractItemView::SelectRows);
	TVDesigns->verticalHeader()->setVisible(false);
	MainLayout->addWidget(TVDesigns);

	Model = new SPhotoBook::DesignInfoTableModel(this);
	TVDesigns->setModel(Model);

	setupActions();
	setupToolBars();
}

bool DesignInfoWidget::hasChanges() const
{
	//TODO
	//There is changes if not all design infos are up to date Or if I delete some designInfos
	return false;
}

SPhotoBook::TemplateInfo DesignInfoWidget::templateInfo() const
{
	return Model->templateInfo();
}


void DesignInfoWidget::selectTemplate(const SPhotoBook::TemplateInfo& _Template )
{
	Model->setTemplate(_Template);
}

SPhotoBook::DesignInfo DesignInfoWidget::currentDesignInfo()
{
	SPhotoBook::DesignInfo Res;
	QModelIndex CIndex =  TVDesigns->currentIndex();
	if (CIndex.isValid())
		Res = Model->designInfo(CIndex);
	return Res;
}

void DesignInfoWidget::setCurrentDesignInfo(const SPhotoBook::DesignInfo& _Info)
{
	SPhotoBook::DesignInfo Res;
	QModelIndex CIndex =  TVDesigns->currentIndex();
	if (CIndex.isValid())
		Model->setDesignInfo(CIndex, _Info);
}

void DesignInfoWidget::slotDeleteCurrentDesign()
{
	SPhotoBook::DesignInfo DInfo;
	QModelIndex CIndex =  TVDesigns->currentIndex();
	if (CIndex.isValid())
		DInfo = Model->designInfo(CIndex);

	SPhotoBook::TemplateInfo TInfo = Model->templateInfo();
	if (!DInfo.isNull() && !TInfo.isNull())
	{
		if (SMessageBox::question(this, tr("Delete design"), tr("Are you sure you want to delete current Design?")) ==
				QMessageBox::Yes)
		{
			SPhotoBook::SystemTemplates::deleteTemplateDesign(TInfo, DInfo);
			Model->removeRow(CIndex.row());
			emit templateDesignsModified();
		}
	}
}

//.---------------------------------------------------------------------------.
//   Class TemplateManagerWidget
//.___________________________________________________________________________.

void TemplateManagerWidget::setupActions()
{
	TypeAG = new QActionGroup(this);
	TypeAG->setExclusive(true);

	QAction* TypePhotoBookAct = new QAction(QIcon(":/photobook.png"), tr("PhotoBook"), this);
	TypePhotoBookAct->setData(SPhotoBook::MetaInfo::TypePhotoBook);
	TypePhotoBookAct->setCheckable(true);
	TypeAG->addAction(TypePhotoBookAct);

	QAction* TypeCalendarAct = new QAction(QIcon(":/calendars-subgroup.png"), tr("Calendar"), this);
	TypeCalendarAct->setData(SPhotoBook::MetaInfo::TypeCalendar);
	TypeCalendarAct->setCheckable(true);
	TypeAG->addAction(TypeCalendarAct);

	QAction* TypeCardAct = new QAction(QIcon(":/card.png"), tr("Decorations"), this);
	TypeCardAct->setData(SPhotoBook::MetaInfo::TypeCard);
	TypeCardAct->setCheckable(true);
	TypeAG->addAction(TypeCardAct);

	QAction* TypePhotoIdAct = new QAction(QIcon(":/photoid-subgroup.png"), tr("Photo id"), this);
	TypePhotoIdAct->setData(SPhotoBook::MetaInfo::TypeIdPhoto);
	TypePhotoIdAct->setCheckable(true);
	TypeAG->addAction(TypePhotoIdAct);

	QAction* TypeMultiPhotoAct = new QAction(QIcon(":/other.png"), tr("Other"), this);
	TypeMultiPhotoAct->setData(SPhotoBook::MetaInfo::TypeMultiPhoto);
	TypeMultiPhotoAct->setCheckable(true);
	TypeAG->addAction(TypeMultiPhotoAct);

	connect(TypeAG, SIGNAL(triggered(QAction*)), this, SLOT(slotFilterActionTriggered(QAction* )));
}

void TemplateManagerWidget::setupToolBars()
{
	TBTemplateTypes->addActions(TypeAG->actions());
	TBTemplateTypes->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	TBTemplateTypes->setIconSize(QSize(64, 64));
}



TemplateManagerWidget::TemplateManagerWidget(QWidget *parent) :
	QWidget(parent)
{

	QVBoxLayout* MainLayout = new QVBoxLayout(this);
	TBTemplateTypes = new QToolBar(this);
	MainLayout->addWidget(TBTemplateTypes);

	TInfoWidget = new TemplateInfoWidget(this);
	MainLayout->addWidget(TInfoWidget);
	connect(TInfoWidget, SIGNAL(templateSelected(SPhotoBook::TemplateInfo)), this, SLOT(slotTemplateSelected(SPhotoBook::TemplateInfo)));
	connect(TInfoWidget, SIGNAL(manageProducts()), this, SLOT(slotManageProducts()));
	connect(TInfoWidget, SIGNAL(editInfo()), this, SLOT(slotEditInfo()));

	DInfoWidget = new DesignInfoWidget(this);
	connect(DInfoWidget, SIGNAL(templateDesignsModified()), this, SLOT(slotSelectedTemlateDesignModified()));
	connect(DInfoWidget, SIGNAL(downloadCurrentDesign()), this, SLOT(slotDownloadCurrentDesign()));
	MainLayout->addWidget(DInfoWidget);

	setupActions();
	setupToolBars();

	MPStatusWidget = new SMultiProcessStatusWidget(2, this);
	MainLayout->addWidget(MPStatusWidget);
	MPStatusWidget->setVisible(false);

}

void TemplateManagerWidget::loadTemplates()
{
	try
	{
		Templates = pManager->getTemplates();
	}
	catch (STError& _Error)
	{
		SMessageBox::critical(this, tr("Error loading templates"), _Error.description());
	}
}

void TemplateManagerWidget::setCurrentTemplateType(SPhotoBook::MetaInfo::EnTemplateType _Type)
{
	TInfoWidget->setTemplateList(Templates.subList(_Type));
}


void TemplateManagerWidget::slotFilterActionTriggered(QAction* _Action)
{
	setCurrentTemplateType(static_cast<SPhotoBook::MetaInfo::EnTemplateType>(_Action->data().toInt()));
}


void TemplateManagerWidget::slotTemplateSelected(const SPhotoBook::TemplateInfo& _TemplateInfo)
{
	DInfoWidget->selectTemplate(_TemplateInfo);
	SelectedTemplate = _TemplateInfo;
}


void TemplateManagerWidget::slotSelectedTemlateDesignModified()
{
	SPhotoBook::TemplateInfo NewTemplateInfo = DInfoWidget->templateInfo();
	Templates.updateTemplateInfo(SelectedTemplate, NewTemplateInfo);
	TInfoWidget->updateTemplateInfo(SelectedTemplate, NewTemplateInfo);
	SelectedTemplate = NewTemplateInfo;
}

void TemplateManagerWidget::slotDownloadCurrentDesign()
{
	SPhotoBook::DesignInfo CurrDesignInfo = DInfoWidget->currentDesignInfo();
	SPhotoBook::TemplateInfo CurrTemplateInfo = DInfoWidget->templateInfo();
	if (!CurrDesignInfo.isNull() && !CurrTemplateInfo.isNull()) //Defensive
	{
		if (!CurrTemplateInfo.designOnDisk(CurrDesignInfo))
		{
			try
			{
				QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
				MPStatusWidget->show();
				pManager->downloadTemplateDesign(CurrTemplateInfo, CurrDesignInfo, MPStatusWidget->procStatusWidget(0));
				MPStatusWidget->hide();
				//TODO: Refresh DInfoWidget;
				QApplication::restoreOverrideCursor();
			}
			catch (STError& _Error)
			{
				SMessageBox::critical(this, tr("Error downloading template"), _Error.description());
				MPStatusWidget->hide();
				QApplication::restoreOverrideCursor();
			}
		}
		else
			SMessageBox::information(this, tr("Downloading template"), tr("Template is already on disk."));
	}

}
