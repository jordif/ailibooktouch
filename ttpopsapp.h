/****************************************************************************
**
** Copyright (C) 2010-2011 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2011
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef TTPOPSAPP_H
#define TTPOPSAPP_H

#include "sapplication.h"
#include "templateinfolist.h"

#define pManager TtpopsApp::publisherManager()

class RemotePublisherManager;
class TtpopsApp : public SApplication
{
Q_OBJECT

	void setStyleSheet();
	static RemotePublisherManager* PubManager;
	void init(const QString& _StarlabPath);

public:
	TtpopsApp(const QString& _StarlabPath, const QString& _StarlabUser, const QString& _StarlabPassword, int & argc, char ** argv);
	TtpopsApp(int & argc, char ** argv);
	static RemotePublisherManager* publisherManager() { return PubManager; }
	static QString publisherDatabaseLocalFile() { return SApplication::userSharedPath() + "/publisher.db"; }
	static SPhotoBook::TemplateInfoList loadTemplates();
};

#endif // TtpopsAPP_H
