<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ca_ES">
<context>
    <name>AppSettings</name>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Program language.</source>
        <translation>Idioma del programa.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="869"/>
        <source>Next Order Ref</source>
        <translation>Propera referencia de comanda</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="872"/>
        <source>Image render dpis</source>
        <translation>DPIs de renderitzat d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="875"/>
        <source>Low resolution warning dpis</source>
        <translation>DPIS de l&apos;avís de baixa resolusió</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="878"/>
        <source>Default render format</source>
        <translation>Format de generació per defecte</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="881"/>
        <source>Print Password</source>
        <translation>Contrassenya d&apos;impressió</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="884"/>
        <source>Main menu effect</source>
        <translation>Efecte de menú principal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1051"/>
        <source>Sync publisher data</source>
        <translation>Sincronitzar les dades del publicant</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="907"/>
        <source>Run TOM on order commit</source>
        <translation>Executar TOM al final de les comandes</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="887"/>
        <source>Show legal disclaimer</source>
        <translation>Ensenyar avis legal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="890"/>
        <source>Order files path</source>
        <translation>Carpeta d&apos;arxius de comandes</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="893"/>
        <source>Settings access password</source>
        <translation>Contrasenya d&apos;accés a configuració</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="896"/>
        <source>Enable direct module access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="899"/>
        <source>Direct module index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="910"/>
        <source>Advanced Order Management</source>
        <translation>Gestió de comandes avançada </translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="916"/>
        <source>Excluded drives for media cards (Ex:A,C,D)</source>
        <translation>Unitats excloses de les targes multimedia (Ex: A,C,D)</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="919"/>
        <source>Drive for CD unit</source>
        <translation>Unitat per al CD</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="922"/>
        <source>Path where system mounts media</source>
        <translation>Directori on el sistema munta el suport media</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="940"/>
        <source>Path where bluetooth module puts images</source>
        <translation>Directori on el mòdul bluetooth situa les imatges</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="943"/>
        <source>Run eject at the end of orders ?</source>
        <translation>Executar eject al final de cada comanda ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="946"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="949"/>
        <source>Surname</source>
        <translation>Cognom</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="952"/>
        <source>Email</source>
        <translation>Adreça de correu electrònic</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="955"/>
        <source>Phone</source>
        <translation>Telèfon</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="958"/>
        <source>Mobile Phone</source>
        <translation>Telèfon mòbil</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="961"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="964"/>
        <source>City</source>
        <translation>Ciutat</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="967"/>
        <source>Country</source>
        <translation>Pais</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="970"/>
        <source>State</source>
        <translation>Provincia</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="973"/>
        <source>Zip</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="976"/>
        <source>Account number</source>
        <translation>Número de compte</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="979"/>
        <source>Transport Route</source>
        <translation>Ruta de transport</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="983"/>
        <source>Send to publisher</source>
        <translation>Enviar al laboratori</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="986"/>
        <source>Ask for user phone and name?</source>
        <translation>Demanar telèfon i el nom del client ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="989"/>
        <source>Image crop mode</source>
        <translation>Mode de tall d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="992"/>
        <source>Image adjustment mode</source>
        <translation>Mode d&apos;ajust d&apos;imatge</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format for photobook renders</source>
        <translation>Format d&apos;imatge per a la creació de Fotollibres</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format to use when render photobook.</source>
        <translation>Format d&apos;imatge per a crear Fotollibres.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format for photobook cover renders</source>
        <translation>Format d&apos;imatge per a crear la portada dels Fotollibres</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format to use when render photobook cover.</source>
        <translation>Format d&apos;imatge per a la creació de la portada dels Fotollibres.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Could not open publisher database</source>
        <translation>No es pot obrir la base de dades de l&apos;editor</translation>
    </message>
    <message>
        <source>Print first page at last</source>
        <translation type="obsolete">Imprimir la primera pàgina al final</translation>
    </message>
    <message>
        <source>Print first page at last, suitable to print magazines.</source>
        <translation type="obsolete">Imprimir la primera pàgina al final, adient per a imprimir revistes.</translation>
    </message>
    <message>
        <source>Print Preprocess Type</source>
        <translation type="obsolete">Tipus de porcessat d&apos;impressió</translation>
    </message>
    <message>
        <source>Type of processing before print.</source>
        <translation type="obsolete">Tipus de processat abans de la impressió.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1005"/>
        <source>Dpis</source>
        <translation>Dpis</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1008"/>
        <source>Draw frames ?</source>
        <translation>Dibuixar marcs ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1011"/>
        <source>Draw file numbers ?</source>
        <translation>Dibuixar números d&apos;arxiu ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1014"/>
        <source>Draw file names ?</source>
        <translation>Dibuixar noms d&apos;arxiu?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1017"/>
        <source>Estimated num of images per page:</source>
        <translation>Número estimat d&apos;imatges per pàgina:</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1020"/>
        <source>Background Color:</source>
        <translation>Color de fons:</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1023"/>
        <source>Margin:</source>
        <translation>Marge:</translation>
    </message>
    <message>
        <source>Template info URL</source>
        <translation type="obsolete">URL d&apos;informació de plantilles</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1026"/>
        <location filename="../appsettings.cpp" line="1027"/>
        <source>Print ticket at the end of command ?</source>
        <translation>Imprimir el tiquet al final de la comanda ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1029"/>
        <location filename="../appsettings.cpp" line="1030"/>
        <source>Print a copy of ticket at the end of command ?</source>
        <translation>Imprimir una copia del tiquet al final de la comanda ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1032"/>
        <source>Ticket header</source>
        <translation>Capçalera de la comanda</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1033"/>
        <source>Ticket footer</source>
        <translation>Peu de tiquet</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>My Name</source>
        <translation>El meu nom</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>Company name</source>
        <translation>Nom de la empresa</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>My Address</source>
        <translation>La meva adreça</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>Company address</source>
        <translation>Adrça de l&apos;empresa</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1038"/>
        <source>User Name</source>
        <translation>Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1039"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1040"/>
        <source>Key</source>
        <translation>Clau</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1054"/>
        <source>Keep local prices</source>
        <translation>Mantenir els preus locals</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1057"/>
        <source>Sync interval in minutes</source>
        <translation>Interval de sincronització en minuts</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1061"/>
        <source>Manually install templates</source>
        <translation>Instalar plantilles manualment</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Error openning publisher database</source>
        <translation>Error obrint la base de dades del publicant</translation>
    </message>
    <message>
        <source>Could not open publisher database file %1</source>
        <translation type="obsolete">No es pot obrir l&apos;arxiu de la base de dades del labroratori %1</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1426"/>
        <source>&lt;h1&gt;Enter access password&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Entra la contrassenya d&apos;accés&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1452"/>
        <source>&lt;h1&gt;Enter print password&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Introdueixi la contrassenya d&apos;impressió&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1438"/>
        <location filename="../appsettings.cpp" line="1464"/>
        <source>&lt;font color=red&gt;&lt;h1&gt; Wrong password, please try again &lt;/h1&gt;&lt;/font&gt;</source>
        <translation>&lt;font color=red&gt;&lt;h1&gt;La contrassenya es incorrecte, si us plau intenti-ho un altre cop &lt;/h1&gt;&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>AppSettingsDialog</name>
    <message>
        <location filename="../appsettings.cpp" line="799"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="803"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel·lar</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="807"/>
        <source>&amp;Exit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="794"/>
        <source>TTPops settings dialog</source>
        <translation>Diàleg de configuració de TTpops</translation>
    </message>
</context>
<context>
    <name>AppSettingsWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="747"/>
        <source>Burning</source>
        <translation>Enregistrament</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="744"/>
        <source>Ticket Printer</source>
        <translation>Impressora de tiquets</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="707"/>
        <source>Publisher</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="724"/>
        <source>Print Output</source>
        <translation>Sortida d&apos;impressió</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="728"/>
        <source>User Account</source>
        <translation>Conta d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="732"/>
        <source>Main</source>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="735"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="738"/>
        <source>PhotoBook</source>
        <translation>Fotolllibre</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="741"/>
        <source>PhotoIndex</source>
        <translation>Foto Index</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="750"/>
        <source>Database</source>
        <translation>Base de dades</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation type="obsolete">Col·lecció</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="753"/>
        <source>Templates</source>
        <translation>Plantilles</translation>
    </message>
</context>
<context>
    <name>BatchTemplateWizard</name>
    <message>
        <source>Could not find template: %1</source>
        <translation type="obsolete">No es troba la plantilla: %1</translation>
    </message>
    <message>
        <source>Loading photobook</source>
        <translation type="obsolete">Carregant Fotollibre</translation>
    </message>
    <message>
        <source>Error loading photobook template: %1</source>
        <translation type="obsolete">Errada carregant la plantilla del Fotollibre: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El mòdul %1 no defineix cap ruta per a plantilles</translation>
    </message>
    <message>
        <source>Temp directory %1 could not be created</source>
        <translation type="obsolete">No es pot crear el directori temporal %1</translation>
    </message>
    <message>
        <source>Rendering...</source>
        <translation type="obsolete">Generant...</translation>
    </message>
    <message>
        <source>Error saving file: %1</source>
        <translation type="obsolete">Errada desant arxiu: %1</translation>
    </message>
    <message>
        <source>Error Rendering images</source>
        <translation type="obsolete">Errada generant imatges</translation>
    </message>
    <message>
        <source>Invalid template</source>
        <translation type="obsolete">Plantilla invàlda</translation>
    </message>
    <message>
        <source>Invalid template selected.</source>
        <translation type="obsolete">Plantilla seleccionada no vàlida.</translation>
    </message>
    <message>
        <source>Too few images</source>
        <translation type="obsolete">Molt poques imatges</translation>
    </message>
    <message>
        <source>You have to select more images to generate this photo book. </source>
        <translation type="obsolete">Ha de seleccionar més imatges per a generar aquest Ffotollibre.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errada</translation>
    </message>
    <message>
        <source>Order cancel question</source>
        <translation type="obsolete">Qüestió per a cancel·lar la comanda</translation>
    </message>
    <message>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="obsolete">Si cancel·les perdràs tots el canvis realitzats. Cancel·lar de totes maneres ?</translation>
    </message>
</context>
<context>
    <name>BuildSettingsPage</name>
    <message>
        <source>&lt;h1&gt;Please pay attention to build settings&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt; Si us plau fixi&apos;s amb els paràmetres de construcció&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="44"/>
        <source>&lt;h2&gt;Please pay attention to build settings&lt;/h2&gt;</source>
        <translation>&lt;h2&gt; Si us plau fixi&apos;s amb els paràmetres de construcció&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="45"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="46"/>
        <source>Next</source>
        <translation>Següent</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="47"/>
        <source>Main</source>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="48"/>
        <source>Calendar</source>
        <translation>Calendari</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="49"/>
        <source>PhotoBook</source>
        <translation>Fotolllibre</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="50"/>
        <source>&lt;h2&gt;Assembly title:&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Títol del montatge&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="51"/>
        <source>Include text frames</source>
        <translation>Incloure textes</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="52"/>
        <source>Auto adjust images to frames ?</source>
        <translation>Auto ajustar les imatges als marcs ?</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="53"/>
        <source>Use images as backgrounds ?</source>
        <translation>Utilitzar imatges com a fons de pàgina ?</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="54"/>
        <source>Background images in Black and White</source>
        <translation>Imatges de fons en blanc i negre</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="55"/>
        <source>Auto Improve images</source>
        <translation>Millora d&apos;imatges automática</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="56"/>
        <source>Number of Pages:</source>
        <translation>Nombre de pàgines:</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="57"/>
        <source>From Date</source>
        <translation>Desde data</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="58"/>
        <source>To Date</source>
        <translation>Fins a data</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="92"/>
        <source>Select a design</source>
        <translation>Selecciona un disseny</translation>
    </message>
</context>
<context>
    <name>CDRecordPage</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="90"/>
        <source>Error saving file %1</source>
        <translation>Error desant archiu %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="114"/>
        <location filename="../cdrecordpage.cpp" line="370"/>
        <source>Cancel Burn process</source>
        <translation>Cancel·lar l&apos;enregistrament</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="115"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <location filename="../cdrecordpage.cpp" line="191"/>
        <source>CD Burning...</source>
        <translation>Enregistrament de CD...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <source>Getting information to start the CD burning.</source>
        <translation>Obtenint informació per a iniciar l&apos;¡enregistrament.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="182"/>
        <location filename="../cdrecordpage.cpp" line="187"/>
        <source>Getting ready to burn the CD...</source>
        <translation>Preparant per a l&apos;enregistrament...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="183"/>
        <source>Compiling the selected images.</source>
        <translation>Compilant les imatges seleccionades.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="188"/>
        <source>Creating an image of the CD.</source>
        <translation>Creant una imatge del CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="192"/>
        <source>CD Burning. Wait a moment please.</source>
        <translation>Enregistrant el CD, esperi un moment si us plau.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="198"/>
        <source>The CD is already burned!.&lt;br/&gt;Click on the screen to finish.</source>
        <translation>El CD ja s&apos;ha enregistrat!-&lt;br/&gt;Pitgi OK per Finalitzar.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="199"/>
        <source>&lt;b&gt;Thanks for your attention.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Gracies per la seva atenció.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="221"/>
        <source>Oh oh... I couldn&apos;t burn the CD!</source>
        <translation>No he pogut enregistrar el CD!</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="222"/>
        <source>There has been a problem during the burning process: </source>
        <translation>Ha sorgit un problema durant el procés d&apos;enregistrament:</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="250"/>
        <source>Preparing the images of the order...</source>
        <translation>Preparant la imatge de la comanda...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="254"/>
        <source>Could not delete temp directory %1</source>
        <translation>No es pot esborrar el directori temporal %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="256"/>
        <source>Temp directory %1 could not be created</source>
        <translation>No es pot crar el directori temporal %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="262"/>
        <source>The images from the order are ready.</source>
        <translation>Les imatges de la comanda estan preparades.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="268"/>
        <source>Preparing CD image.</source>
        <translation>Preparant la imatge del CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="285"/>
        <source>The image of the CD are correctly burned.</source>
        <translation>La imatge del CD s&apos;ha escrit correctament.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="292"/>
        <source>Ready to burn</source>
        <translation>Preparat per a enregistrar</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="299"/>
        <source>CD correctly burned!</source>
        <translation>El CD s&apos;ha enregistrat correctament!</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="312"/>
        <source>cdrecord has finished uncorrently: ExitCode = %1</source>
        <translation>Cdrecord ha finalitzat de manera incorrecta: Codi de Sortida = %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="313"/>
        <location filename="../cdrecordpage.cpp" line="324"/>
        <source>Error burning CD</source>
        <translation>Errada enregistrant el CD</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="314"/>
        <source>CD Burn process fails, would you like to try again ?</source>
        <translation>El procés d&apos;enregistrament ha fallat, vol tornar-ho a intentar ?</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="321"/>
        <source>There have been some errors creating the ISO image.</source>
        <translation>Han sorgit errades creant la imatge ISO.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="322"/>
        <source>mkisofs has finished uncorrectly: ExitCode = %1</source>
        <translation>mkisofs ha finalitzat de manera incorrecta: Codi de Sortida = %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="325"/>
        <source>mkisofs process fails, would you like to try again ?</source>
        <translation>mkisofs ha fallat, vol tornar-ho a intentar ?</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="348"/>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>CD burning.</source>
        <translation>Enregistrament de CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="349"/>
        <source>Please, insert and empty or appendale CD-R(W) num. (%1 of %2) copy (%3) into drive.</source>
        <translation>Si us plau, inseriu un CD-R(W) amb suficient espai lliure num (%1 de %2 copia (%3) a la unitat de cd.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>The images do on fit in the CD. Please change the selection.</source>
        <translation>Les imatges no hi caben al CD, si us plau canvieu la selecció.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="371"/>
        <source>Are you sure you want to cancel burn process ?</source>
        <translation>Segur que vol cancel·lar el procés d&apos;enregistrament ?</translation>
    </message>
</context>
<context>
    <name>CDRecordWizard</name>
    <message>
        <location filename="../cdrecordwizard.cpp" line="76"/>
        <source>&lt;h2&gt;Select files you want to store.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Selecciona els fitxers que vols emmagatzemar.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>Order cancel question</source>
        <translation>Qüestió per a cancel·lar la comanda</translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancel·les perdràs tots els canvis realitzats. Vols cancel·lar de totes maneres ?</translation>
    </message>
</context>
<context>
    <name>DatabaseConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="323"/>
        <source>Click to setup prices...</source>
        <translation>Clic per configurar preus...</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="330"/>
        <source>Restore default database Values</source>
        <translation>Restaurar els valors per defecte</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Setting up prices</source>
        <translation>Configurant preus</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation>Error configurant preus: &apos;%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <location filename="../appsettings.cpp" line="403"/>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Restoring defaults</source>
        <translation>Restaurant els valors predeterminats</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <source>Are you sure you want to restore database to it defaults values ?</source>
        <translation>Segur que vols restaurar els valors per defecte a la base de dades ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="403"/>
        <source>Default settings restored</source>
        <translation>Valors per defecte restaurats</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Error restoring defaults: &apos;%1&apos;.
</source>
        <translation>Error restaurant valors per defecte:&apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>DesignInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="143"/>
        <source>Uninstall</source>
        <translation>Desinstalar</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="147"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="161"/>
        <source>&lt;h2&gt;Designs&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Dissenys&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Delete design</source>
        <translation>Eliminar disseny</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Are you sure you want to delete current Design?</source>
        <translation>Segur que vol eliminar el disseny actual ?</translation>
    </message>
</context>
<context>
    <name>DigiprintWizard</name>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>Order cancel question</source>
        <translation>Qüestió per a cancel·lar la comanda</translation>
    </message>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancel·les perdràs tots el canvis realitzats. Vols cancel·lar de totes maneres ?</translation>
    </message>
</context>
<context>
    <name>FormatListPage</name>
    <message>
        <location filename="../formatlistpage.cpp" line="72"/>
        <source>Main Menu</source>
        <translation>Menú Principal</translation>
    </message>
</context>
<context>
    <name>GetMediaPage</name>
    <message>
        <location filename="../getmediapage.cpp" line="74"/>
        <source>&lt;h1&gt;Introduce the card or CD in the right socket.&lt;/h1&gt; &lt;h2&gt;Then press the selected media button.&lt;/h2&gt;</source>
        <translation>&lt;h1&gt;Introdueixi la tarja o el CD a la ranura correcte.&lt;/h&gt; &lt;h2&gt;Després pressioni el botó adient-&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="76"/>
        <source>Mem.C./Pendrive</source>
        <translation>Tar/Llapis de memoria</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="77"/>
        <source>CD or DVD</source>
        <translation>CD o DVD</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="78"/>
        <source>Facebook</source>
        <translation>Facebook</translation>
    </message>
    <message>
        <source>Pen drive</source>
        <translation type="obsolete">Llapis de memoria</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation type="obsolete">Col·lecció</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="79"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="80"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
</context>
<context>
    <name>LegalProtPage</name>
    <message>
        <location filename="../legalprotpage.cpp" line="33"/>
        <source>Do not Accept</source>
        <translation>No acceptar</translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="34"/>
        <source>Accept contitions</source>
        <translation>Acceptar les condicions</translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="80"/>
        <source>&lt;h1 align=center&gt;No legal disclaimer available for this language.&lt;/h1&gt;</source>
        <translation>&lt;h1 lign=center&gt;No hi ha cap avís legal per a aquest llenguatge &lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>MainWizard</name>
    <message>
        <source>Could not open publisher database file %1</source>
        <translation type="obsolete">No es pot obrir l&apos;arxiu de la base de dades del labroratori %1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="409"/>
        <source>Error initialising publisher</source>
        <translation>Errada iniciant l&apos;editor</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>Warning</source>
        <translation>Alerta</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>No images found in media</source>
        <translation>No hi ha imatges en el medi</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="481"/>
        <source>Error getting publisher data</source>
        <translation>Errada obtenint les dades del laboratori</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="515"/>
        <source>Could not open publisher database.</source>
        <translation>No puc obrir la base de dades de l&apos;editor.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="162"/>
        <source>Please enter name and phone</source>
        <translation>Si us plau introdueixi el seu nom i el seu telèfon</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Enter print password&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Introdueixi la contrasenya d&apos;impressió&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;font color=red&gt;&lt;h1&gt; Wrong password, please try again &lt;/h1&gt;&lt;/font&gt;</source>
        <translation type="obsolete">&lt;font color=red&gt;&lt;h1&gt;La contrasenya es incorrecte, si us plau intenti-ho un altre cop &lt;/h1&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Order commited successfull...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Ordre confirmada amb éxit...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <source>Error storing order</source>
        <translation type="obsolete">Errada emmagatzemant la comanda</translation>
    </message>
    <message>
        <source>Error printing order</source>
        <translation type="obsolete">Errada imprimint la comanda</translation>
    </message>
    <message>
        <source>(Lab)</source>
        <translation type="obsolete">(Lab)</translation>
    </message>
    <message>
        <source>No suitable products for selected template</source>
        <translation type="obsolete">No hi ha productes aptes per a la plantilla seleccionada</translation>
    </message>
    <message>
        <source>&lt;h1&gt;You must be connected to internet to see this information.&lt;/h1&gt;&lt;i&gt;If you have any problem, please contact to &lt;a href=&quot;www.starblitz-k.com&quot;&gt;www.starblitz-k.com&lt;/a&gt;&lt;/i&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Ha d&apos;estar conectat a internet per veure aquesta informació.&lt;/h1&gt;&lt;i&gt;Si té algun problema, si us plau contacti a &lt;a href=&quot;www.starblitz-k.com&quot;&gt;www.starblitz-k.com&lt;/a&gt;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="429"/>
        <source>Looking for supported files, please wait...</source>
        <translation>Cercant arxius suportats, si us plau esperi...</translation>
    </message>
    <message>
        <source>Error loading photobook.</source>
        <translation type="obsolete">Errada carregant el Fotollibre.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="517"/>
        <source>Could not open temp ticket database.</source>
        <translation>No es pot obrir la base de dades temporal per a tiquets.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="546"/>
        <source>Error printing ticket.</source>
        <translation>Errada imprimint tiquets.</translation>
    </message>
    <message>
        <source>Could not load settings file: %1</source>
        <translation type="obsolete">No es pot carregar l&apos;arxiu de configuració:%1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="99"/>
        <source>&lt;h1&gt;Order commited successfull...&lt;/h1&gt;&lt;br/&gt;&lt;h2&gt;Order number: %1&lt;/h2&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation>&lt;h1&gt;Ordre confirmada amb éxit...&lt;/h1&gt;&lt;br/&gt;&lt;h2&gt;Número d&apos;ordre: %1&lt;/h2&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="105"/>
        <source>Error commiting order</source>
        <translation>Error confirmant la comanda</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="106"/>
        <source>&lt;h1&gt;Order commited with errors...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/commitwarning.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation>&lt;h1&gt;Comanda confirmada amb errors...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/commitwarning.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <source>Unable to open temp database: %1</source>
        <translation type="obsolete">No es pot obrir la base de dades temporal: %1</translation>
    </message>
    <message>
        <source>Unable to open publisher database: %1</source>
        <translation type="obsolete">No es pot obrir la base de dades de l&apos;editor: %1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="373"/>
        <location filename="../mainwizard.cpp" line="386"/>
        <location filename="../mainwizard.cpp" line="404"/>
        <source>Error in sync process</source>
        <translation>Errada en el procés de sincronització</translation>
    </message>
</context>
<context>
    <name>MediaConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="614"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; This directory is deleted every time we get the media screen.</source>
        <translation>&lt;b&gt;Atenció:&lt;/b&gt; Aquest directori es borrarà cada cop que aparegui la pantalla de media.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="625"/>
        <source>Custom Media Paths</source>
        <translation>Directoris d&apos;imatge a mida</translation>
    </message>
</context>
<context>
    <name>ModuleListPage</name>
    <message>
        <location filename="../modulelistpage.cpp" line="261"/>
        <location filename="../modulelistpage.cpp" line="329"/>
        <source>Main Menu</source>
        <translation>Menu Principal</translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="263"/>
        <location filename="../modulelistpage.cpp" line="341"/>
        <source>Load previously stored PhotoBook</source>
        <translation>Carregar Foto llibre prèviament emmagatzemat</translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="367"/>
        <source>Direct Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="479"/>
        <source>Errors loading ttpops modules</source>
        <translation>Errades carregant els mòduls de ttpops</translation>
    </message>
</context>
<context>
    <name>OMarketPlaceConfigWidget</name>
    <message>
        <source>Click to get more templates...</source>
        <translation type="obsolete">Clic per a obtenir més plantilles...</translation>
    </message>
    <message>
        <source>Getting Marketplace Data</source>
        <translation type="obsolete">Obtingent dades del mercat</translation>
    </message>
    <message>
        <source>Error retrieving marketplace data: &apos;%1&apos;.
</source>
        <translation type="obsolete">Error obtinguent dades del mercat: &apos;%1&apos;.
</translation>
    </message>
</context>
<context>
    <name>OutputConfigWidget</name>
    <message>
        <source>Don&apos;t crop images and leave white margin.</source>
        <translation type="obsolete">No tallar les imatges i deixar marge blanc.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="530"/>
        <source>Crop Images</source>
        <translation>Tallar imatges</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="531"/>
        <source>Don&apos;t crop images and leave white margin centered.</source>
        <translation>No retallis les imatges i deixa el marge blanc centrat.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="532"/>
        <source>Don&apos;t crop images and leave white margin on the right side.</source>
        <translation>No retallis les imatges y deixa el marge blanc al costat dret.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="541"/>
        <source>Send original image files</source>
        <translation>Envia els fitxers d&apos;imatge originals</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="542"/>
        <source>Adjust image weight for selected format.</source>
        <translation>Ajusta el pes de la imatge al format seleccionat.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="513"/>
        <source>Use system printer driver</source>
        <translation>Utilitzar el controlador d&apos;impressora del sistema</translation>
    </message>
</context>
<context>
    <name>PBPagesView</name>
    <message>
        <location filename="../pbpagesview.cpp" line="35"/>
        <location filename="../pbpagesview.cpp" line="48"/>
        <source>Insert Page</source>
        <translation>Afegeixi una pàgina</translation>
    </message>
    <message>
        <location filename="../pbpagesview.cpp" line="39"/>
        <location filename="../pbpagesview.cpp" line="47"/>
        <source>Remove Page</source>
        <translation>Esborri una pàgina</translation>
    </message>
</context>
<context>
    <name>PagesListView</name>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>Adding Pages</source>
        <translation>Afegint pàgines</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>This PBDocument could not contain more than %1 pages</source>
        <translation>Aquest document no pot contindre més de %1 pàgines</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Remove current page</source>
        <translation>Elimini la pàgina actual</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Are you sure to delete the current page ?</source>
        <translation>Realment vol esborrar la pàgina actual?</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>Removing Pages</source>
        <translation>Esborrant pàgines</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation>Ho sento pero no pot esborrar la primera pàgina.</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>This PBDocument need at least %1 pages</source>
        <translation>Aquest document necessita almenys %1 pàgines</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="110"/>
        <source>Insert Page</source>
        <translation>Afegeixi una pàgina</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="114"/>
        <source>Remove Page</source>
        <translation>Esborri una pàgina</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="118"/>
        <source>Move Page left</source>
        <translation>Moure pàg esquerra</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="122"/>
        <source>Move Page right</source>
        <translation>Moure pàg dreta</translation>
    </message>
</context>
<context>
    <name>PhotoBookConfigWidget</name>
    <message>
        <source>None</source>
        <translation type="obsolete">Cap</translation>
    </message>
    <message>
        <source>Booklet</source>
        <translation type="obsolete">LLibret</translation>
    </message>
</context>
<context>
    <name>PhotoBookEditor</name>
    <message>
        <location filename="../photobookeditor.cpp" line="61"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="62"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="108"/>
        <source>Error</source>
        <translation>Errada</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="142"/>
        <source>Enter text:</source>
        <translation>Introdueixi el text:</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="182"/>
        <source>Error loading image: %1</source>
        <translation>Errada carrgant imatge: %1</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="200"/>
        <source>Error opening image</source>
        <translation>Errada obrint la imatge</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="63"/>
        <location filename="../photobookeditor.cpp" line="209"/>
        <source>More Actions</source>
        <translation>Més accions</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="64"/>
        <location filename="../photobookeditor.cpp" line="213"/>
        <source>Previous page</source>
        <translation>Pàgina anterior</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="215"/>
        <source>Goes to the previous page of current photobook.</source>
        <translation>Va a la pàgina anterior del Fotollibre actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="65"/>
        <location filename="../photobookeditor.cpp" line="218"/>
        <source>Next page</source>
        <translation>Pàgina següent</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="220"/>
        <source>Goes to the next page of current photobook.</source>
        <translation>Va ala pàgina següent del Fotollibre actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="225"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="70"/>
        <location filename="../photobookeditor.cpp" line="232"/>
        <source>Move images</source>
        <translation>Moure imatges</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="233"/>
        <source>Move the image</source>
        <translation>Mou la imatge</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="71"/>
        <location filename="../photobookeditor.cpp" line="239"/>
        <source>Move frames</source>
        <translation>Mou els marcs</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="240"/>
        <source>Move the frame</source>
        <translation>Mou el marc</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="72"/>
        <location filename="../photobookeditor.cpp" line="246"/>
        <source>New text line frame</source>
        <translation>Nova linia de text</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="73"/>
        <location filename="../photobookeditor.cpp" line="249"/>
        <source>New photo frame</source>
        <translation>Nou marc de fotografia</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="74"/>
        <location filename="../photobookeditor.cpp" line="252"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="253"/>
        <source>Removes selected items.</source>
        <translation>Eliminar els elements seleccionats.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="76"/>
        <location filename="../photobookeditor.cpp" line="257"/>
        <source>Bring To Front</source>
        <translation>Portar al davant</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="77"/>
        <location filename="../photobookeditor.cpp" line="260"/>
        <source>Send To Back</source>
        <translation>Enviar al fons</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="81"/>
        <location filename="../photobookeditor.cpp" line="263"/>
        <source>Select All</source>
        <translation>Seleccionar-ho tot</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="78"/>
        <location filename="../photobookeditor.cpp" line="267"/>
        <location filename="../photobookeditor.cpp" line="268"/>
        <source>Text Font</source>
        <translation>Font del text</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="271"/>
        <source>Text Color</source>
        <translation>Color del text</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="272"/>
        <source>Changes the text color</source>
        <translation>Canvia el color del text</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="80"/>
        <location filename="../photobookeditor.cpp" line="276"/>
        <source>Edit item</source>
        <translation>Edita element</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="278"/>
        <source>Edit current item.</source>
        <translation>Edita l&apos;element actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="89"/>
        <location filename="../photobookeditor.cpp" line="280"/>
        <source>Shadow</source>
        <translation>Ombra</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="282"/>
        <source>Toggle item shadow.</source>
        <translation>Activa o desactiva l&apos;ombra.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="90"/>
        <location filename="../photobookeditor.cpp" line="287"/>
        <source>Border</source>
        <translation>Vora</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="83"/>
        <location filename="../photobookeditor.cpp" line="290"/>
        <source>Zoom In</source>
        <translation>Zoom dins</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="292"/>
        <source>Zoom in the current image.</source>
        <translation>Zoom dins de la imatge actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="84"/>
        <location filename="../photobookeditor.cpp" line="295"/>
        <source>Zoom Out</source>
        <translation>Zoom fora</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="297"/>
        <source>Zoom out the current image.</source>
        <translation>Zoom per fora de la imatge actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="85"/>
        <location filename="../photobookeditor.cpp" line="300"/>
        <source>Rotate left</source>
        <translation>Girar a l&apos;esquerra</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="301"/>
        <source>Rotates current image 270?.</source>
        <translation>Girar la imatge actual 270 graus?.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="86"/>
        <location filename="../photobookeditor.cpp" line="305"/>
        <source>Rotate right</source>
        <translation>Girar a la dreta</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="67"/>
        <source>Undo</source>
        <translation>Desfer</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="68"/>
        <source>Redo</source>
        <translation>Refer</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="79"/>
        <source>Item Color</source>
        <translation>Color del elemento</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="87"/>
        <source>Fit to Frame</source>
        <translation>Encaixar al marc</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="306"/>
        <source>Rotates current image 90?.</source>
        <translation>Girar la imatge actual 90º?.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="309"/>
        <source>Fit To Frame</source>
        <translation>Encaixar al marc</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="310"/>
        <source>Fit image to frame size.</source>
        <translation>Encaixar la imatge a la mida del marc.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="313"/>
        <source>Copy Page Xml</source>
        <translation>Copiar xml de la pàgina</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="496"/>
        <source>New Text Frame</source>
        <translation>Nova linia de text</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Delete item(s)</source>
        <translation>Eliminar element(s)</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Do you want to delete all selected items ?</source>
        <translation>Vol esborrar tots els elements seleccionats ?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="563"/>
        <source>Please, choose the font for selected text items</source>
        <translation>Si us plau, seleccioni la font per als elements seleccionats</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="615"/>
        <source>Information</source>
        <translation>Informació</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="616"/>
        <source>Please, select a item to edit.</source>
        <translation>Si us plau, seleccioni un element per editar.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Modify page layout</source>
        <translation>Modifica la distribució de la pàgina</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Do you want to change the layout of current page ?</source>
        <translation>Vol canviar la distribució de la pàgina actual ?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="673"/>
        <source>No Reason</source>
        <translation>Sense raó</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>Not suitable template</source>
        <translation>Plantilla no vàlida</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>You can not apply this template for current page because %1</source>
        <translation>No pot aplicar aquesta plantilla a la pàgina actual perquè %1</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="720"/>
        <source>Photobook is not correct.</source>
        <translation>El Fotollibre no es correcte.</translation>
    </message>
    <message>
        <source>The Photobook contains empty frames. Please delete or fill it to continue.</source>
        <translation type="obsolete">El Fotollibre conté marcs d&apos;imatges buits. Si us plau ompli&apos;ls o elimini&apos;ls per continuar.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>Adding Pages</source>
        <translation>Afegint pàgines</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>This PhotoBook could not contain more than %1 pages</source>
        <translation>Aquest Fotollibre no pot tenir més de %1 pàgines</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Remove current page</source>
        <translation>Elimini la pàgina actual</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Are you sure to delete the current page ?</source>
        <translation>Realment vol esborrar la pàgina actual?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>Removing Pages</source>
        <translation>Esborrant pàgines</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation>Ho sento pero no pot esborrar la primera pàgina.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>This PhotoBook need at least %1 pages</source>
        <translation>Aquest Fotollibre necessita almenys %1 pàgines</translation>
    </message>
</context>
<context>
    <name>PhotoBookListViewTabWidget</name>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="43"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="120"/>
        <source>Layouts</source>
        <translation>Distribucions</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="48"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="121"/>
        <source>Covers</source>
        <translation>Portades</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="53"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="122"/>
        <source>BackCovers</source>
        <translation>Contra Portadas</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="58"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="123"/>
        <source>Clipart</source>
        <translation>Clipart</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="63"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="124"/>
        <source>Masks</source>
        <translation>Màscares</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="68"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="125"/>
        <source>Frames</source>
        <translation>Marcs</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="73"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="126"/>
        <source>Backgrounds</source>
        <translation>Imatges de Fons</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="78"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="127"/>
        <source>Pages</source>
        <translation>Pàgines</translation>
    </message>
</context>
<context>
    <name>PhotoBookOutputPage</name>
    <message>
        <location filename="../photobookoutputpage.cpp" line="203"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Aquest es el resum de la seva comanda.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="43"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="44"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="45"/>
        <source>Store</source>
        <translation>Emmagatzemar</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="76"/>
        <source>Building, please wait...</source>
        <translation>Construïnt, si us plau espereu...</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>Error building PhotoBook</source>
        <translation>Error construïnt el fotollibre</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>The following errors has occurred during photobook export:
 %1</source>
        <translation>Les següents errades s&apos;han produït durant la exportació del Fotollibre:
%1</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="200"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Processant...&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>PhotoBookWizard</name>
    <message>
        <location filename="../photobookwizard.cpp" line="123"/>
        <source>Building photobook...</source>
        <translation>Generant Fotollibre...</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="218"/>
        <source>&lt;h2&gt;Please select pictures for you photo assembly.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Si us plau, seleccioneu les fotografies per el seu muntatge.&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>Loading photobook...</source>
        <translation type="obsolete">Carregant el Fotollibre...</translation>
    </message>
    <message>
        <source>Storing photobook...</source>
        <translation type="obsolete">Emmagatzemant el Fotollibre...</translation>
    </message>
    <message>
        <source>Process information</source>
        <translation type="obsolete">Processant informació</translation>
    </message>
    <message>
        <source>Your photobook has been stored in your media.</source>
        <translation type="obsolete">El seu Fotolibre s&apos;ha desat en el seu dispositiu.</translation>
    </message>
    <message>
        <source>Error storing photobook</source>
        <translation type="obsolete">Errada en desar el Fotollibre</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="107"/>
        <source>Downloading template...</source>
        <translation>Descarregant plantilla...</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>PhotoBook creation</source>
        <translation>Creació del Fotolibre</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>Create a new PhotoBook ?</source>
        <translation>Crear un Fotollibre nou ?</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>Too few images</source>
        <translation>Molt poques imatges</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>You have to select more images to generate this photo book. </source>
        <translation>Ha de seleccionar més imatges per a generar aquest Fotollibre.</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Loading photobook</source>
        <translation>Crregant el Fotollibre</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Error loading photobook template: %1</source>
        <translation>Errada carregant la plantilla del Fotollibre: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El mòdul %1 no defineix cap ruta per plantilles</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>Order cancel question</source>
        <translation>Pregunta per cancel·lar la comanda</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancel·les perdràs tots els canvis realitzats, Cancel·lar de totes maneres ?</translation>
    </message>
    <message>
        <source>Error exporting PhotoBook</source>
        <translation type="obsolete">Errada exportant el Fotollibre</translation>
    </message>
    <message>
        <source>Downloading required files for this template, please wait...</source>
        <translation type="obsolete">Descarregant els archius necessaris per aquesta plantilla, si us plau esperi...</translation>
    </message>
    <message>
        <source>Errors downloading templates</source>
        <translation type="obsolete">Error descarregant plantilles</translation>
    </message>
    <message>
        <source>There was errors downloading templates. Some of the layouts for this model may not work. Please contact to your software provider.</source>
        <translation type="obsolete">S&apos;han produït errors descarregant les plantilles. Algunes de les distribucions per aquest model pot ser que no funcionin. Si us plau contacti amb el seu proveidor de software.</translation>
    </message>
    <message>
        <source>The following errors has occurred during photobook export:
 %1</source>
        <translation type="obsolete">Les següents errades s&apos;han produït durant la exportació del Fotollibre:
%1</translation>
    </message>
</context>
<context>
    <name>PhotoIndexConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="302"/>
        <source>Click to Select</source>
        <translation>Clica per a seleccionar</translation>
    </message>
</context>
<context>
    <name>PhotoIndexWizard</name>
    <message>
        <location filename="../photoindexwizard.cpp" line="39"/>
        <source>&lt;h2&gt;Please, select images for index and the product.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Per favor, seleccioneu les imatges per l&apos;índex i el producte.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="121"/>
        <source>Building photoindex...</source>
        <translation>Construïnt el foto índex...</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="137"/>
        <source>Error saving photoindex page %1</source>
        <translation>Errada desant la pàgina del foto índex %1</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="150"/>
        <source>Error creating photoindex</source>
        <translation>Errada creant el foto índex</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>Photo Index warning</source>
        <translation>Avís de foto índex</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>You have to select at least one image.</source>
        <translation>Has de seleccionar almenys una imatge.</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>Order cancel question</source>
        <translation>Pregunta per a cancel·lar la comanda</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancel·les la comanda perdràs tots els canvis realitzats. Cancel·lar de totes maneres ?</translation>
    </message>
</context>
<context>
    <name>ProductTypeSelectionPage</name>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="68"/>
        <source>Order Prints</source>
        <translation>Copies Digitals</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="69"/>
        <source>Print your photos in standard format sizes</source>
        <translation>Imprimeix les teves fotos en mides estandard</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="72"/>
        <source>Photo Id</source>
        <translation>Foto Carnet</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="73"/>
        <source>Make your photo ids</source>
        <translation>Fes les teves fotos de carnet</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="75"/>
        <source>Photo Index</source>
        <translation>Foto índex</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="76"/>
        <source>Build and index with your photos</source>
        <translation>Constueix un índex amb les teves fotos</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="77"/>
        <source>CD Burning</source>
        <translation>Gravar CDs</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="78"/>
        <source>Save your photos on CD</source>
        <translation>Desa les fotos en CD</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="81"/>
        <source>PhotoBooks</source>
        <translation>Fotollibres</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="82"/>
        <source>PhotoBooks, Photo Magazines, with photo quality</source>
        <translation>Fotollibres, Fotorevistes amb calitat foto</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="85"/>
        <source>Calendars</source>
        <translation>Calendaris</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="86"/>
        <source>Build a calendar with your photos</source>
        <translation>Fes calendaris amb les teves fotos</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="89"/>
        <source>Decorations</source>
        <translation>Decoracions</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="90"/>
        <source>Use your photos to wish christmas, birthdays, ... </source>
        <translation>Utilitza les teves fotos per a felicitar aniversari, festius, ...</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="93"/>
        <source>Other</source>
        <translation>Altres</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="94"/>
        <source>Other designs with your photos</source>
        <translation>Altres dissenys amb les teves fotos</translation>
    </message>
</context>
<context>
    <name>PubDatabaseCreationDialog</name>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="45"/>
        <source>Publisher default database creation</source>
        <translation>Creació de la base de dades predeterminada de l&apos;Editor</translation>
    </message>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="48"/>
        <source>&lt;h2&gt;Please, select one of the following options:&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Si us plau, selecciona una de les següents opcions&lt;/h2&gt;</translation>
    </message>
</context>
<context>
    <name>PublisherConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="187"/>
        <source>Contact</source>
        <translation>Contacte</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="200"/>
        <source>Send Address</source>
        <translation>Adreça d&apos;enviament</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="220"/>
        <source>Orher Data</source>
        <translation>Altres dades</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="230"/>
        <source>Required fields</source>
        <translation>Camps necessaris</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Could not find style.txt file</source>
        <translation type="obsolete">No puc trobar l&apos;arxiu style.txt</translation>
    </message>
    <message>
        <location filename="../ttpopsapp.cpp" line="44"/>
        <source>Could not find ttpops_style.txt file</source>
        <translation>No puc trobar ttpops_style.txt</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="80"/>
        <source>Catalan</source>
        <translation>Català</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Spanish</source>
        <translation>Espanyol</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>English</source>
        <translation>Anglès</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>Loggin in...</source>
        <translation>Auntenticant-se...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="64"/>
        <location filename="../cdrecordpage.cpp" line="77"/>
        <source>Could not create directory: %1</source>
        <translation>No puc crear el directori: %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="97"/>
        <source>Low res. image</source>
        <translation>Baixa resolusio</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>Login Failed</source>
        <translation>Errada en el registre</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>You will not have access to printers untill you login correctly</source>
        <translation>No tindrà accés a les impressores fins que no es registri correctament</translation>
    </message>
    <message>
        <source>Could not remove file %1</source>
        <translation type="obsolete">No puc borrar l&apos;arxiu %1</translation>
    </message>
    <message>
        <source>Could not open file %1 for database export.</source>
        <translation type="obsolete">No puc obrir l&apos;arxiu %1 per a ser exportat a la base de dades.</translation>
    </message>
    <message>
        <source>Could not database %1 for database export.</source>
        <translation type="obsolete">No puc obrir l&apos;arxiu %1 per a ser exportat a la base de dades.</translation>
    </message>
    <message>
        <source>Publisher Databsase Manager error</source>
        <translation type="obsolete">Error del gestor de la base de dades de l&apos;editor</translation>
    </message>
    <message>
        <source>Could not load settings file: %1</source>
        <translation type="obsolete">No es pot carregar l&apos;arxiu de configuració:%1</translation>
    </message>
    <message>
        <source>Publisher database file: %1 , does not exists!</source>
        <translation type="obsolete">L&apos;Arxiu de la base de dades de l&apos;editor %1, no existeix!</translation>
    </message>
    <message>
        <source>Error opening publisher database %1</source>
        <translation type="obsolete">Error obrint l&apos;arxiu de la base de dades de l&apos;editor %1</translation>
    </message>
    <message>
        <location filename="../iorderwizard.cpp" line="32"/>
        <source>Could not open publisher database file %1</source>
        <translation>No es pot obrir l&apos;arxiu de la base de dades del labroratori %1</translation>
    </message>
</context>
<context>
    <name>STAppModule</name>
    <message>
        <location filename="../stappmodule.cpp" line="79"/>
        <location filename="../stappmodule.cpp" line="83"/>
        <source>Unknown</source>
        <translation>Desconegut</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="86"/>
        <source>Digital Prints</source>
        <translation>Impressions digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="89"/>
        <source>Decorations</source>
        <translation>Decoracions</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="92"/>
        <location filename="../stappmodule.cpp" line="128"/>
        <source>PhotoBooks</source>
        <translation>Fotollibres</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="95"/>
        <source>CD Record</source>
        <translation>Cremat de CD</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="98"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="101"/>
        <location filename="../stappmodule.cpp" line="137"/>
        <source>Gifts</source>
        <translation>Personalitzats</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="104"/>
        <source>Photo Index</source>
        <translation>Foto índex</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="107"/>
        <source>Bash Templates</source>
        <translation>Plantilles multifoto</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="110"/>
        <source>PDF Documents</source>
        <translation>Documents PDF</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="122"/>
        <source>Digital Photo</source>
        <translation>Foto digital</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="125"/>
        <source>Photo Assembly</source>
        <translation>Muntatge fotogràfic</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="131"/>
        <source>Digital Storage</source>
        <translation>Emmagatzematge digital</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="134"/>
        <source>Digital Documents</source>
        <translation>Documents digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="149"/>
        <source>Operations with digital pictures.</source>
        <translation>Operacions amb imatges digitals.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="152"/>
        <source>Assemblies and decorations for digital pictures.</source>
        <translation>Muntatges i decoracions per fotografies digitals.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="155"/>
        <source>Multipage assemblies for digital pictures</source>
        <translation>Muntatges multipàgina per a fotografies digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="158"/>
        <source>CD/DVD Burning, Memory and pendrive storage</source>
        <translation>Grabat de CD/DVD, emmagatzemament de memories</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="161"/>
        <source>Operations with digital documents.</source>
        <translation>Operacions amb documents digitals.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="164"/>
        <source>Gifts built with your digital pictures</source>
        <translation>Personalitzats amb les seves imatges digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="176"/>
        <source>Calendars</source>
        <translation>Calendaris</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="179"/>
        <source>PhotoId</source>
        <translation>Foto carnet</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="182"/>
        <source>Cards</source>
        <translation>Targetes</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="185"/>
        <source>Batch Templates</source>
        <translation>Plantilles multifoto</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="188"/>
        <location filename="../stappmodule.cpp" line="212"/>
        <source>None</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="200"/>
        <source>Calendar assemblies for digital pictures</source>
        <translation>Muntatges amb calendaris per imatges digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="203"/>
        <source>Photo id assemblies</source>
        <translation>Muntatges de fotocarnet</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="206"/>
        <source>Card assemblies for digital pictures</source>
        <translation>Muntatges amb targetes per imatges digitals</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="209"/>
        <source>Many photos in a single template</source>
        <translation>Moltes fotos en una sola plantilla</translation>
    </message>
</context>
<context>
    <name>SinglePhotoSelPage</name>
    <message>
        <location filename="../singlephotoselpage.cpp" line="46"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="47"/>
        <source>All</source>
        <translation>Tot</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="48"/>
        <source>None</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="49"/>
        <source>Filter</source>
        <translation>Filtrar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="50"/>
        <source>NO Filter</source>
        <translation>Sense filtre</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="51"/>
        <source>Accept</source>
        <translation>Acceptar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="52"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="254"/>
        <source>Please select directory to filter</source>
        <translation>Si us plau seleccioni la carpeta a filtrar</translation>
    </message>
    <message>
        <source>&lt;b style=&quot;font-size:24pt;&quot;&gt;%1&lt;/b&gt; of %2 &lt;em&gt;images selected.&lt;/em&gt;</source>
        <translation type="obsolete">&lt;b style=&quot;font-size:24pt;&quot;&gt;%1&lt;/b&gt; de %2 &lt;em&gt;imatges seleccionades.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="298"/>
        <source>Error opening image</source>
        <translation>Errada obrint la imatge</translation>
    </message>
</context>
<context>
    <name>StorageOutputPage</name>
    <message>
        <location filename="../storageoutputpage.cpp" line="32"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Aquest es el resum de la seva comanda.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="33"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="34"/>
        <source>Store</source>
        <translation>Emmagatzemar</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
</context>
<context>
    <name>StoredTemplateSelPage</name>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="34"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="35"/>
        <source>Load</source>
        <translation>Carregar</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="36"/>
        <source>&lt;h1&gt;Please select a PhotoBook to load.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Per favor, selecciona el Foto llibre a carregar.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Photobook selection</source>
        <translation>Selecció de Fotollibre</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Sorry but there are no stored PhotoBooks in this media</source>
        <translation>Ho sento pero no hi ha cap Fotollibre en aquest dispositiu</translation>
    </message>
</context>
<context>
    <name>TPOrderSummaryPage</name>
    <message>
        <location filename="../tpordersummarypage.cpp" line="114"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Aquest es el resum de la seva comanda.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="30"/>
        <source>Photos</source>
        <translation>Fotografies</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="31"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="32"/>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="117"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Processant...&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>TPSendProgressPage</name>
    <message>
        <source>&lt;h1&gt; Sending command ... &lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Enviant la comanda ... &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="29"/>
        <source>&lt;center&gt;&lt;h1&gt; Sending command ... &lt;/h1&gt;&lt;/center&gt;</source>
        <translation>&lt;center&gt;&lt;h1&gt;Enviant la comanda ... &lt;/h1&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="43"/>
        <source>Progress...</source>
        <translation>Progrés...</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Continuar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel·lar</translation>
    </message>
    <message>
        <source>Enter text:</source>
        <translation type="obsolete">Introdueixi el text:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errada</translation>
    </message>
    <message>
        <source>Error loading image: %1</source>
        <translation type="obsolete">Errada carregant la imatge: %1</translation>
    </message>
    <message>
        <source>Error opening image</source>
        <translation type="obsolete">Errada obrint la imatge</translation>
    </message>
    <message>
        <source>Masks</source>
        <translation type="obsolete">Màscares</translation>
    </message>
    <message>
        <source>Cliparts</source>
        <translation type="obsolete">Cliparts</translation>
    </message>
    <message>
        <source>New text line frame</source>
        <translation type="obsolete">Nova linia de text</translation>
    </message>
    <message>
        <source>New photo frame</source>
        <translation type="obsolete">Nou marc de fotografia</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Color</translation>
    </message>
    <message>
        <source>Change text or background color.</source>
        <translation type="obsolete">Canviar el color del fons del text.</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Eliminar</translation>
    </message>
    <message>
        <source>Removes selected items.</source>
        <translation type="obsolete">Elimina els elements seleccionats.</translation>
    </message>
    <message>
        <source>Bring To Front</source>
        <translation type="obsolete">Portar al davant</translation>
    </message>
    <message>
        <source>Send To Back</source>
        <translation type="obsolete">Enviar al fons</translation>
    </message>
    <message>
        <source>Text Font</source>
        <translation type="obsolete">Font del text</translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation type="obsolete">Editar element</translation>
    </message>
    <message>
        <source>Edit current item.</source>
        <translation type="obsolete">Editar element actual.</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="obsolete">Zoom dins</translation>
    </message>
    <message>
        <source>Zoom in the current image.</source>
        <translation type="obsolete">Zoom per dins de la imatge actual.</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="obsolete">Zoom fora</translation>
    </message>
    <message>
        <source>Zoom out the current image.</source>
        <translation type="obsolete">Zoom fora de la imatge actual.</translation>
    </message>
    <message>
        <source>Rotate left</source>
        <translation type="obsolete">Girar a l&apos;esquerra</translation>
    </message>
    <message>
        <source>Rotates current image 270?.</source>
        <translation type="obsolete">Girar la imatge actual 270º?.</translation>
    </message>
    <message>
        <source>Shadow</source>
        <translation type="obsolete">Ombra</translation>
    </message>
    <message>
        <source>Toggle item shadow.</source>
        <translation type="obsolete">Activa o desactiva l&apos;ombra.</translation>
    </message>
    <message>
        <source>Border</source>
        <translation type="obsolete">Vora</translation>
    </message>
    <message>
        <source>Loading images</source>
        <translation type="obsolete">Carregant imatges</translation>
    </message>
    <message>
        <source>Rendering template</source>
        <translation type="obsolete">Generant la plantilla</translation>
    </message>
    <message>
        <source>Generating multi image</source>
        <translation type="obsolete">Generant multi imatge</translation>
    </message>
    <message>
        <source>New Text Frame</source>
        <translation type="obsolete">Nova linia de text</translation>
    </message>
    <message>
        <source>Delete item(s)</source>
        <translation type="obsolete">Eliminar element(s)</translation>
    </message>
    <message>
        <source>Do you want to delete all selected items ?</source>
        <translation type="obsolete">Vol eliminar els elements seleccionats ?</translation>
    </message>
    <message>
        <source>Please, choose the font for selected text items</source>
        <translation type="obsolete">Si us plau, esculli una font per als elements seleccionats</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informació</translation>
    </message>
    <message>
        <source>Please, select a item to edit.</source>
        <translation type="obsolete">Si us plau, seleccioni un element per editar.</translation>
    </message>
</context>
<context>
    <name>TemplateInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="60"/>
        <source>&lt;h2&gt;Templates&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Plantilles&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="70"/>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
</context>
<context>
    <name>TemplateManagerWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="242"/>
        <source>PhotoBook</source>
        <translation>Fotolllibre</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="247"/>
        <source>Calendar</source>
        <translation>Calendari</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="252"/>
        <source>Decorations</source>
        <translation>Decoracions</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="257"/>
        <source>Photo id</source>
        <translation>Foto carnet</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="262"/>
        <source>Other</source>
        <translation>Altres</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="315"/>
        <source>Error loading templates</source>
        <translation>Error carregant plantilles</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="365"/>
        <source>Error downloading template</source>
        <translation>Error descarregant plantilla</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Downloading template</source>
        <translation>Descarregant plantilla</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Template is already on disk.</source>
        <translation>La plantilla ja és a disc.</translation>
    </message>
</context>
<context>
    <name>TemplateSelectionPage</name>
    <message>
        <location filename="../templateselectionpage.cpp" line="63"/>
        <source>&lt;h1&gt;Getting info from internet.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Obtinguent informació d&apos;internet.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="69"/>
        <source>&lt;h1&gt;There is no information for this item.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;No hi ha informació per aquest element.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="85"/>
        <source>&lt;h2&gt;Please, select a template from the list.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Si us plau, seleccioni una plantilla de la llista.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="96"/>
        <source>Products:</source>
        <translation>Productes:</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="109"/>
        <source>&lt;h2&gt;There is no templates of this type.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;No hi ha plantilles d&apos;aquest tipus.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="121"/>
        <source>Size:</source>
        <translation>Mida:</translation>
    </message>
</context>
<context>
    <name>TemplateWizard</name>
    <message>
        <source>Could not find template: %1</source>
        <translation type="obsolete">No es pot trobar la plantilla: %1</translation>
    </message>
    <message>
        <source>Loading photobook</source>
        <translation type="obsolete">Carregant Fotollibre</translation>
    </message>
    <message>
        <source>Error loading photobook template: %1</source>
        <translation type="obsolete">Errada carregant la plantilla del Fotollibre: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El mòdul %1 no defineix cap arxiu de plantilla</translation>
    </message>
    <message>
        <source>Temp directory %1 could not be created</source>
        <translation type="obsolete">No es pot crear el directori temporal %1</translation>
    </message>
    <message>
        <source>Error saving file: %1</source>
        <translation type="obsolete">Errada desant l&apos;arxiu: %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errada</translation>
    </message>
    <message>
        <source>Order cancel question</source>
        <translation type="obsolete">Pregunta per cancel·lar la comanda</translation>
    </message>
    <message>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="obsolete">Si cancel·les perdràs tots els canvis realitzats. Cancel·lar de totes maneres ?</translation>
    </message>
</context>
<context>
    <name>TemplatesConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="427"/>
        <source>Click to setup templates...</source>
        <translation>Clic per configurar plantilles...</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Setting up prices</source>
        <translation>Configurant preus</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation>Error configurant preus: &apos;%1&apos;.
</translation>
    </message>
</context>
</TS>
