<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AppSettings</name>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Program language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="869"/>
        <source>Next Order Ref</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="872"/>
        <source>Image render dpis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="875"/>
        <source>Low resolution warning dpis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="878"/>
        <source>Default render format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="881"/>
        <source>Print Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="884"/>
        <source>Main menu effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1051"/>
        <source>Sync publisher data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="907"/>
        <source>Run TOM on order commit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="887"/>
        <source>Show legal disclaimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="890"/>
        <source>Order files path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="893"/>
        <source>Settings access password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="896"/>
        <source>Enable direct module access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="899"/>
        <source>Direct module index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="910"/>
        <source>Advanced Order Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="916"/>
        <source>Excluded drives for media cards (Ex:A,C,D)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="919"/>
        <source>Drive for CD unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="922"/>
        <source>Path where system mounts media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="940"/>
        <source>Path where bluetooth module puts images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="943"/>
        <source>Run eject at the end of orders ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="946"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="949"/>
        <source>Surname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="952"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="955"/>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="958"/>
        <source>Mobile Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="961"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="964"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="967"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="970"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="973"/>
        <source>Zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="976"/>
        <source>Account number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="979"/>
        <source>Transport Route</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="983"/>
        <source>Send to publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="986"/>
        <source>Ask for user phone and name?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="989"/>
        <source>Image crop mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="992"/>
        <source>Image adjustment mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format for photobook renders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format to use when render photobook.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format for photobook cover renders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format to use when render photobook cover.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Could not open publisher database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1005"/>
        <source>Dpis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1008"/>
        <source>Draw frames ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1011"/>
        <source>Draw file numbers ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1014"/>
        <source>Draw file names ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1017"/>
        <source>Estimated num of images per page:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1020"/>
        <source>Background Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1023"/>
        <source>Margin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1026"/>
        <location filename="../appsettings.cpp" line="1027"/>
        <source>Print ticket at the end of command ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1029"/>
        <location filename="../appsettings.cpp" line="1030"/>
        <source>Print a copy of ticket at the end of command ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1032"/>
        <source>Ticket header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1033"/>
        <source>Ticket footer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>My Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>Company name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>My Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>Company address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1038"/>
        <source>User Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1039"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1040"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1054"/>
        <source>Keep local prices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1057"/>
        <source>Sync interval in minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1061"/>
        <source>Manually install templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Error openning publisher database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1426"/>
        <source>&lt;h1&gt;Enter access password&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1452"/>
        <source>&lt;h1&gt;Enter print password&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1438"/>
        <location filename="../appsettings.cpp" line="1464"/>
        <source>&lt;font color=red&gt;&lt;h1&gt; Wrong password, please try again &lt;/h1&gt;&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettingsDialog</name>
    <message>
        <location filename="../appsettings.cpp" line="794"/>
        <source>TTPops settings dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="799"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="803"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="807"/>
        <source>&amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppSettingsWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="707"/>
        <source>Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="724"/>
        <source>Print Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="728"/>
        <source>User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="732"/>
        <source>Main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="735"/>
        <source>Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="738"/>
        <source>PhotoBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="741"/>
        <source>PhotoIndex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="744"/>
        <source>Ticket Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="747"/>
        <source>Burning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="753"/>
        <source>Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="750"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BuildSettingsPage</name>
    <message>
        <location filename="../buildsettingspage.cpp" line="44"/>
        <source>&lt;h2&gt;Please pay attention to build settings&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="45"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="46"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="47"/>
        <source>Main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="48"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="49"/>
        <source>PhotoBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="50"/>
        <source>&lt;h2&gt;Assembly title:&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="51"/>
        <source>Include text frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="52"/>
        <source>Auto adjust images to frames ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="53"/>
        <source>Use images as backgrounds ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="54"/>
        <source>Background images in Black and White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="55"/>
        <source>Auto Improve images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="56"/>
        <source>Number of Pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="57"/>
        <source>From Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="58"/>
        <source>To Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="92"/>
        <source>Select a design</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CDRecordPage</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="90"/>
        <source>Error saving file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="114"/>
        <location filename="../cdrecordpage.cpp" line="370"/>
        <source>Cancel Burn process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="115"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <location filename="../cdrecordpage.cpp" line="191"/>
        <source>CD Burning...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <source>Getting information to start the CD burning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="182"/>
        <location filename="../cdrecordpage.cpp" line="187"/>
        <source>Getting ready to burn the CD...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="183"/>
        <source>Compiling the selected images.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="188"/>
        <source>Creating an image of the CD.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="192"/>
        <source>CD Burning. Wait a moment please.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="198"/>
        <source>The CD is already burned!.&lt;br/&gt;Click on the screen to finish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="199"/>
        <source>&lt;b&gt;Thanks for your attention.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="221"/>
        <source>Oh oh... I couldn&apos;t burn the CD!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="222"/>
        <source>There has been a problem during the burning process: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="250"/>
        <source>Preparing the images of the order...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="254"/>
        <source>Could not delete temp directory %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="256"/>
        <source>Temp directory %1 could not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="262"/>
        <source>The images from the order are ready.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="268"/>
        <source>Preparing CD image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="285"/>
        <source>The image of the CD are correctly burned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="292"/>
        <source>Ready to burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="299"/>
        <source>CD correctly burned!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="312"/>
        <source>cdrecord has finished uncorrently: ExitCode = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="313"/>
        <location filename="../cdrecordpage.cpp" line="324"/>
        <source>Error burning CD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="314"/>
        <source>CD Burn process fails, would you like to try again ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="321"/>
        <source>There have been some errors creating the ISO image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="322"/>
        <source>mkisofs has finished uncorrectly: ExitCode = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="325"/>
        <source>mkisofs process fails, would you like to try again ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="348"/>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>CD burning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="349"/>
        <source>Please, insert and empty or appendale CD-R(W) num. (%1 of %2) copy (%3) into drive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>The images do on fit in the CD. Please change the selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="371"/>
        <source>Are you sure you want to cancel burn process ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CDRecordWizard</name>
    <message>
        <location filename="../cdrecordwizard.cpp" line="76"/>
        <source>&lt;h2&gt;Select files you want to store.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>Order cancel question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DatabaseConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="323"/>
        <source>Click to setup prices...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="330"/>
        <source>Restore default database Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Setting up prices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <location filename="../appsettings.cpp" line="403"/>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Restoring defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <source>Are you sure you want to restore database to it defaults values ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="403"/>
        <source>Default settings restored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Error restoring defaults: &apos;%1&apos;.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DesignInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="143"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="147"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="161"/>
        <source>&lt;h2&gt;Designs&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Delete design</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Are you sure you want to delete current Design?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DigiprintWizard</name>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>Order cancel question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormatListPage</name>
    <message>
        <location filename="../formatlistpage.cpp" line="72"/>
        <source>Main Menu</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GetMediaPage</name>
    <message>
        <location filename="../getmediapage.cpp" line="74"/>
        <source>&lt;h1&gt;Introduce the card or CD in the right socket.&lt;/h1&gt; &lt;h2&gt;Then press the selected media button.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="76"/>
        <source>Mem.C./Pendrive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="77"/>
        <source>CD or DVD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="78"/>
        <source>Facebook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="79"/>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LegalProtPage</name>
    <message>
        <location filename="../legalprotpage.cpp" line="33"/>
        <source>Do not Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="34"/>
        <source>Accept contitions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="80"/>
        <source>&lt;h1 align=center&gt;No legal disclaimer available for this language.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWizard</name>
    <message>
        <location filename="../mainwizard.cpp" line="162"/>
        <source>Please enter name and phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="373"/>
        <location filename="../mainwizard.cpp" line="386"/>
        <location filename="../mainwizard.cpp" line="404"/>
        <source>Error in sync process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="99"/>
        <source>&lt;h1&gt;Order commited successfull...&lt;/h1&gt;&lt;br/&gt;&lt;h2&gt;Order number: %1&lt;/h2&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="105"/>
        <source>Error commiting order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="106"/>
        <source>&lt;h1&gt;Order commited with errors...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/commitwarning.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="409"/>
        <source>Error initialising publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="429"/>
        <source>Looking for supported files, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>No images found in media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="481"/>
        <source>Error getting publisher data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="515"/>
        <source>Could not open publisher database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="517"/>
        <source>Could not open temp ticket database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="546"/>
        <source>Error printing ticket.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MediaConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="614"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; This directory is deleted every time we get the media screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="625"/>
        <source>Custom Media Paths</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModuleListPage</name>
    <message>
        <location filename="../modulelistpage.cpp" line="261"/>
        <location filename="../modulelistpage.cpp" line="329"/>
        <source>Main Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="263"/>
        <location filename="../modulelistpage.cpp" line="341"/>
        <source>Load previously stored PhotoBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="367"/>
        <source>Direct Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="479"/>
        <source>Errors loading ttpops modules</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="513"/>
        <source>Use system printer driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="530"/>
        <source>Crop Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="531"/>
        <source>Don&apos;t crop images and leave white margin centered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="532"/>
        <source>Don&apos;t crop images and leave white margin on the right side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="541"/>
        <source>Send original image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="542"/>
        <source>Adjust image weight for selected format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PBPagesView</name>
    <message>
        <location filename="../pbpagesview.cpp" line="35"/>
        <location filename="../pbpagesview.cpp" line="48"/>
        <source>Insert Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pbpagesview.cpp" line="39"/>
        <location filename="../pbpagesview.cpp" line="47"/>
        <source>Remove Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PagesListView</name>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>Adding Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>This PBDocument could not contain more than %1 pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Remove current page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Are you sure to delete the current page ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>Removing Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>This PBDocument need at least %1 pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="110"/>
        <source>Insert Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="114"/>
        <source>Remove Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="118"/>
        <source>Move Page left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="122"/>
        <source>Move Page right</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoBookEditor</name>
    <message>
        <location filename="../photobookeditor.cpp" line="61"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="62"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="63"/>
        <location filename="../photobookeditor.cpp" line="209"/>
        <source>More Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="64"/>
        <location filename="../photobookeditor.cpp" line="213"/>
        <source>Previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="65"/>
        <location filename="../photobookeditor.cpp" line="218"/>
        <source>Next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="67"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="68"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="70"/>
        <location filename="../photobookeditor.cpp" line="232"/>
        <source>Move images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="71"/>
        <location filename="../photobookeditor.cpp" line="239"/>
        <source>Move frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="72"/>
        <location filename="../photobookeditor.cpp" line="246"/>
        <source>New text line frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="73"/>
        <location filename="../photobookeditor.cpp" line="249"/>
        <source>New photo frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="74"/>
        <location filename="../photobookeditor.cpp" line="252"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="76"/>
        <location filename="../photobookeditor.cpp" line="257"/>
        <source>Bring To Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="77"/>
        <location filename="../photobookeditor.cpp" line="260"/>
        <source>Send To Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="78"/>
        <location filename="../photobookeditor.cpp" line="267"/>
        <location filename="../photobookeditor.cpp" line="268"/>
        <source>Text Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="271"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="80"/>
        <location filename="../photobookeditor.cpp" line="276"/>
        <source>Edit item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="79"/>
        <source>Item Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="81"/>
        <location filename="../photobookeditor.cpp" line="263"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="83"/>
        <location filename="../photobookeditor.cpp" line="290"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="84"/>
        <location filename="../photobookeditor.cpp" line="295"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="85"/>
        <location filename="../photobookeditor.cpp" line="300"/>
        <source>Rotate left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="86"/>
        <location filename="../photobookeditor.cpp" line="305"/>
        <source>Rotate right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="87"/>
        <source>Fit to Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="89"/>
        <location filename="../photobookeditor.cpp" line="280"/>
        <source>Shadow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="90"/>
        <location filename="../photobookeditor.cpp" line="287"/>
        <source>Border</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="108"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="142"/>
        <source>Enter text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="182"/>
        <source>Error loading image: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="200"/>
        <source>Error opening image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="215"/>
        <source>Goes to the previous page of current photobook.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="220"/>
        <source>Goes to the next page of current photobook.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="225"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="233"/>
        <source>Move the image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="240"/>
        <source>Move the frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="253"/>
        <source>Removes selected items.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="272"/>
        <source>Changes the text color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="278"/>
        <source>Edit current item.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="282"/>
        <source>Toggle item shadow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="292"/>
        <source>Zoom in the current image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="297"/>
        <source>Zoom out the current image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="301"/>
        <source>Rotates current image 270?.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="306"/>
        <source>Rotates current image 90?.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="309"/>
        <source>Fit To Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="310"/>
        <source>Fit image to frame size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="313"/>
        <source>Copy Page Xml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="496"/>
        <source>New Text Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Delete item(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Do you want to delete all selected items ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="563"/>
        <source>Please, choose the font for selected text items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="615"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="616"/>
        <source>Please, select a item to edit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Modify page layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Do you want to change the layout of current page ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="673"/>
        <source>No Reason</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>Not suitable template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>You can not apply this template for current page because %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="720"/>
        <source>Photobook is not correct.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>Adding Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>This PhotoBook could not contain more than %1 pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Remove current page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Are you sure to delete the current page ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>Removing Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>This PhotoBook need at least %1 pages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoBookListViewTabWidget</name>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="43"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="120"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="48"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="121"/>
        <source>Covers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="53"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="122"/>
        <source>BackCovers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="58"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="123"/>
        <source>Clipart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="63"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="124"/>
        <source>Masks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="68"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="125"/>
        <source>Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="73"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="126"/>
        <source>Backgrounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="78"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="127"/>
        <source>Pages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoBookOutputPage</name>
    <message>
        <location filename="../photobookoutputpage.cpp" line="43"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="44"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="45"/>
        <source>Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="76"/>
        <source>Building, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>Error building PhotoBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>The following errors has occurred during photobook export:
 %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="200"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="203"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoBookWizard</name>
    <message>
        <location filename="../photobookwizard.cpp" line="123"/>
        <source>Building photobook...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="218"/>
        <source>&lt;h2&gt;Please select pictures for you photo assembly.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="107"/>
        <source>Downloading template...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>PhotoBook creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>Create a new PhotoBook ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>Too few images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>You have to select more images to generate this photo book. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Loading photobook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Error loading photobook template: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>Order cancel question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoIndexConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="302"/>
        <source>Click to Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhotoIndexWizard</name>
    <message>
        <location filename="../photoindexwizard.cpp" line="39"/>
        <source>&lt;h2&gt;Please, select images for index and the product.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="121"/>
        <source>Building photoindex...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="137"/>
        <source>Error saving photoindex page %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="150"/>
        <source>Error creating photoindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>Photo Index warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>You have to select at least one image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>Order cancel question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProductTypeSelectionPage</name>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="68"/>
        <source>Order Prints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="69"/>
        <source>Print your photos in standard format sizes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="72"/>
        <source>Photo Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="73"/>
        <source>Make your photo ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="75"/>
        <source>Photo Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="76"/>
        <source>Build and index with your photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="77"/>
        <source>CD Burning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="78"/>
        <source>Save your photos on CD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="81"/>
        <source>PhotoBooks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="82"/>
        <source>PhotoBooks, Photo Magazines, with photo quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="85"/>
        <source>Calendars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="86"/>
        <source>Build a calendar with your photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="89"/>
        <source>Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="90"/>
        <source>Use your photos to wish christmas, birthdays, ... </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="93"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="94"/>
        <source>Other designs with your photos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PubDatabaseCreationDialog</name>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="45"/>
        <source>Publisher default database creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="48"/>
        <source>&lt;h2&gt;Please, select one of the following options:&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="187"/>
        <source>Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="200"/>
        <source>Send Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="220"/>
        <source>Orher Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="230"/>
        <source>Required fields</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../ttpopsapp.cpp" line="44"/>
        <source>Could not find ttpops_style.txt file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="80"/>
        <source>Catalan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>Loggin in...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="64"/>
        <location filename="../cdrecordpage.cpp" line="77"/>
        <source>Could not create directory: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="97"/>
        <source>Low res. image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>Login Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>You will not have access to printers untill you login correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../iorderwizard.cpp" line="32"/>
        <source>Could not open publisher database file %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>STAppModule</name>
    <message>
        <location filename="../stappmodule.cpp" line="79"/>
        <location filename="../stappmodule.cpp" line="83"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="86"/>
        <source>Digital Prints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="89"/>
        <source>Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="92"/>
        <location filename="../stappmodule.cpp" line="128"/>
        <source>PhotoBooks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="95"/>
        <source>CD Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="98"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="101"/>
        <location filename="../stappmodule.cpp" line="137"/>
        <source>Gifts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="104"/>
        <source>Photo Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="107"/>
        <source>Bash Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="110"/>
        <source>PDF Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="122"/>
        <source>Digital Photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="125"/>
        <source>Photo Assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="131"/>
        <source>Digital Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="134"/>
        <source>Digital Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="149"/>
        <source>Operations with digital pictures.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="152"/>
        <source>Assemblies and decorations for digital pictures.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="155"/>
        <source>Multipage assemblies for digital pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="158"/>
        <source>CD/DVD Burning, Memory and pendrive storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="161"/>
        <source>Operations with digital documents.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="164"/>
        <source>Gifts built with your digital pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="176"/>
        <source>Calendars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="179"/>
        <source>PhotoId</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="182"/>
        <source>Cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="185"/>
        <source>Batch Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="188"/>
        <location filename="../stappmodule.cpp" line="212"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="200"/>
        <source>Calendar assemblies for digital pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="203"/>
        <source>Photo id assemblies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="206"/>
        <source>Card assemblies for digital pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="209"/>
        <source>Many photos in a single template</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SinglePhotoSelPage</name>
    <message>
        <location filename="../singlephotoselpage.cpp" line="46"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="47"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="48"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="49"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="50"/>
        <source>NO Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="51"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="52"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="254"/>
        <source>Please select directory to filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="298"/>
        <source>Error opening image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StorageOutputPage</name>
    <message>
        <location filename="../storageoutputpage.cpp" line="32"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="33"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="34"/>
        <source>Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="35"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StoredTemplateSelPage</name>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="34"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="35"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="36"/>
        <source>&lt;h1&gt;Please select a PhotoBook to load.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Photobook selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Sorry but there are no stored PhotoBooks in this media</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TPOrderSummaryPage</name>
    <message>
        <location filename="../tpordersummarypage.cpp" line="30"/>
        <source>Photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="31"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="32"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="114"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="117"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TPSendProgressPage</name>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="29"/>
        <source>&lt;center&gt;&lt;h1&gt; Sending command ... &lt;/h1&gt;&lt;/center&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="43"/>
        <source>Progress...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplateInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="60"/>
        <source>&lt;h2&gt;Templates&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="70"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplateManagerWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="242"/>
        <source>PhotoBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="247"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="252"/>
        <source>Decorations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="257"/>
        <source>Photo id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="262"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="315"/>
        <source>Error loading templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="365"/>
        <source>Error downloading template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Downloading template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Template is already on disk.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplateSelectionPage</name>
    <message>
        <location filename="../templateselectionpage.cpp" line="63"/>
        <source>&lt;h1&gt;Getting info from internet.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="69"/>
        <source>&lt;h1&gt;There is no information for this item.&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="85"/>
        <source>&lt;h2&gt;Please, select a template from the list.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="96"/>
        <source>Products:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="109"/>
        <source>&lt;h2&gt;There is no templates of this type.&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="121"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TemplatesConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="427"/>
        <source>Click to setup templates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Setting up prices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
