<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es">
<context>
    <name>AppSettings</name>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="866"/>
        <source>Program language.</source>
        <translation>Idioma del programa.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="869"/>
        <source>Next Order Ref</source>
        <translation>Próxima referencia de pedido</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="872"/>
        <source>Image render dpis</source>
        <translation>DPIs de renderizado de imagen</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="875"/>
        <source>Low resolution warning dpis</source>
        <translation>DPIS del aviso de baja resolución</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="878"/>
        <source>Default render format</source>
        <translation>Formato de generación por defecto</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="881"/>
        <source>Print Password</source>
        <translation>Contraseña de impresión</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="884"/>
        <source>Main menu effect</source>
        <translation>Efecto del menu principal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1051"/>
        <source>Sync publisher data</source>
        <translation>Sincronizar los datos del editor</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="907"/>
        <source>Run TOM on order commit</source>
        <translation>Ejecutar TOM al final de los pedidos</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="887"/>
        <source>Show legal disclaimer</source>
        <translation>Enseñar aviso legal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="890"/>
        <source>Order files path</source>
        <translation>Carpeta de archivos pedidos</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="893"/>
        <source>Settings access password</source>
        <translation>Contraseña de acceso a configuración</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="896"/>
        <source>Enable direct module access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="899"/>
        <source>Direct module index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="910"/>
        <source>Advanced Order Management</source>
        <translation>Gestión de pedidos avanzada </translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="916"/>
        <source>Excluded drives for media cards (Ex:A,C,D)</source>
        <translation>Unidades excluidas de las tarjetas multimedia (Ex:A,C,D)</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="919"/>
        <source>Drive for CD unit</source>
        <translation>Unidad para el CD</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="922"/>
        <source>Path where system mounts media</source>
        <translation>Directorio donde el sistema monta el media</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="940"/>
        <source>Path where bluetooth module puts images</source>
        <translation>Directorio donde el módulo bluetooth situa las imágenes</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="943"/>
        <source>Run eject at the end of orders ?</source>
        <translation>Ejecutar eject al final de los pedidos ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="946"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="949"/>
        <source>Surname</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="952"/>
        <source>Email</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="955"/>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="958"/>
        <source>Mobile Phone</source>
        <translation>Teléfono móvil</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="961"/>
        <source>Address</source>
        <translation>Dirección</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="964"/>
        <source>City</source>
        <translation>Ciudad</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="967"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="970"/>
        <source>State</source>
        <translation>Provincia</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="973"/>
        <source>Zip</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="976"/>
        <source>Account number</source>
        <translation>Numero de cuenta</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="979"/>
        <source>Transport Route</source>
        <translation>Ruta de transporte</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="983"/>
        <source>Send to publisher</source>
        <translation>Enviar a laboratorio</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="986"/>
        <source>Ask for user phone and name?</source>
        <translation>Pedir el teléfono y el nombre al cliente ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="989"/>
        <source>Image crop mode</source>
        <translation>Modo de corte de imagen</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="992"/>
        <source>Image adjustment mode</source>
        <translation>Modo de ajuste de imagen</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format for photobook renders</source>
        <translation>Formato de imagen para generacion de foto libros</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="996"/>
        <source>Image format to use when render photobook.</source>
        <translation>Formato de imagen para generacion de foto libros.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format for photobook cover renders</source>
        <translation>Formato de imagen para generacion de la portada de los foto libros</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1000"/>
        <source>Image format to use when render photobook cover.</source>
        <translation>Formato de imagen para generacion de la portada de los foto libros.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Could not open publisher database</source>
        <translation>No se puede abrir la base de datos del editor</translation>
    </message>
    <message>
        <source>Print first page at last</source>
        <translation type="obsolete">Imprimir la primera pagina al final</translation>
    </message>
    <message>
        <source>Print first page at last, suitable to print magazines.</source>
        <translation type="obsolete">Imprimir la primera pagina al final, apropiado para imprimir revistas.</translation>
    </message>
    <message>
        <source>Print Preprocess Type</source>
        <translation type="obsolete">Tipo de preprocesado de impresión</translation>
    </message>
    <message>
        <source>Type of processing before print.</source>
        <translation type="obsolete">Tipo de procesado antes de la impresión.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1005"/>
        <source>Dpis</source>
        <translation>Dpis</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1008"/>
        <source>Draw frames ?</source>
        <translation>Dibujar marcos ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1011"/>
        <source>Draw file numbers ?</source>
        <translation>Dibujar números de archivo ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1014"/>
        <source>Draw file names ?</source>
        <translation>Dibujar nombres de archivo ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1017"/>
        <source>Estimated num of images per page:</source>
        <translation>Número estimado de imágenes por página:</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1020"/>
        <source>Background Color:</source>
        <translation>Color de fondo:</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1023"/>
        <source>Margin:</source>
        <translation>Margen:</translation>
    </message>
    <message>
        <source>Template info URL</source>
        <translation type="obsolete">URL de información de plantillas</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1026"/>
        <location filename="../appsettings.cpp" line="1027"/>
        <source>Print ticket at the end of command ?</source>
        <translation>Imprimir el tiquet al final del pedido ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1029"/>
        <location filename="../appsettings.cpp" line="1030"/>
        <source>Print a copy of ticket at the end of command ?</source>
        <translation>Imprimir una copia del tiquet al final del pedido ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1032"/>
        <source>Ticket header</source>
        <translation>Cabecera del pedido</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1033"/>
        <source>Ticket footer</source>
        <translation>Pie del tiquet</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>My Name</source>
        <translation>Mi nombre</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1034"/>
        <source>Company name</source>
        <translation>Nombre de la empresa</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>My Address</source>
        <translation>Mi dirección</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1035"/>
        <source>Company address</source>
        <translation>Dirección de la empresa</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1038"/>
        <source>User Name</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1039"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1040"/>
        <source>Key</source>
        <translation>Llave</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1054"/>
        <source>Keep local prices</source>
        <translation>Mantener los precios locales</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1057"/>
        <source>Sync interval in minutes</source>
        <translation>Intervalo de sincronización en minutos</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1061"/>
        <source>Manually install templates</source>
        <translation>Instalar plantillas manualmente</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1404"/>
        <source>Error openning publisher database</source>
        <translation>Error abriendo la base de datos del publicante</translation>
    </message>
    <message>
        <source>Could not open publisher database file %1</source>
        <translation type="obsolete">No se puede abrir el archivo de la base de datos del laboratorio %1</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1426"/>
        <source>&lt;h1&gt;Enter access password&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Entra la contraseña de acceso&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1452"/>
        <source>&lt;h1&gt;Enter print password&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Entre la contraseña de impresión&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="1438"/>
        <location filename="../appsettings.cpp" line="1464"/>
        <source>&lt;font color=red&gt;&lt;h1&gt; Wrong password, please try again &lt;/h1&gt;&lt;/font&gt;</source>
        <translation>&lt;font color=red&gt;&lt;h1&gt;La contraseña es incorrecta, por favor intetelo de nuevo &lt;/h1&gt;&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>AppSettingsDialog</name>
    <message>
        <location filename="../appsettings.cpp" line="799"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="803"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="807"/>
        <source>&amp;Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="794"/>
        <source>TTPops settings dialog</source>
        <translation>Diálogo de configuración de TTPops</translation>
    </message>
</context>
<context>
    <name>AppSettingsWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="747"/>
        <source>Burning</source>
        <translation>Grabación</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="744"/>
        <source>Ticket Printer</source>
        <translation>Impresora de tiquets</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="707"/>
        <source>Publisher</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="724"/>
        <source>Print Output</source>
        <translation>Salida de impresión</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="728"/>
        <source>User Account</source>
        <translation>Cuenta de usuario</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="732"/>
        <source>Main</source>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="735"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="738"/>
        <source>PhotoBook</source>
        <translation>Foto Libro</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="741"/>
        <source>PhotoIndex</source>
        <translation>Foto Índice</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="750"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation type="obsolete">Colección</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="753"/>
        <source>Templates</source>
        <translation>Plantillas</translation>
    </message>
</context>
<context>
    <name>BatchTemplateWizard</name>
    <message>
        <source>Could not find template: %1</source>
        <translation type="obsolete">No se encuentra la plantilla: %1</translation>
    </message>
    <message>
        <source>Loading photobook</source>
        <translation type="obsolete">Cargando fotolibro</translation>
    </message>
    <message>
        <source>Error loading photobook template: %1</source>
        <translation type="obsolete">Error cargando la plantilla del fotolibro: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El Módulo %1 no define ninguna ruta para plantillas</translation>
    </message>
    <message>
        <source>Temp directory %1 could not be created</source>
        <translation type="obsolete">No se puede crear el directorio temporal %1</translation>
    </message>
    <message>
        <source>Rendering...</source>
        <translation type="obsolete">Generando...</translation>
    </message>
    <message>
        <source>Error saving file: %1</source>
        <translation type="obsolete">Error guardando archivo: %1</translation>
    </message>
    <message>
        <source>Error Rendering images</source>
        <translation type="obsolete">Error Generando imágenes</translation>
    </message>
    <message>
        <source>Invalid template</source>
        <translation type="obsolete">Plantilla inválida</translation>
    </message>
    <message>
        <source>Invalid template selected.</source>
        <translation type="obsolete">Plantilla seleccionada no válida.</translation>
    </message>
    <message>
        <source>Too few images</source>
        <translation type="obsolete">Muy pocas imágenes</translation>
    </message>
    <message>
        <source>You have to select more images to generate this photo book. </source>
        <translation type="obsolete">Tiene que seleccionar mas imágenes para generar este foto libro.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Order cancel question</source>
        <translation type="obsolete">Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="obsolete">Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
</context>
<context>
    <name>BuildSettingsPage</name>
    <message>
        <source>&lt;h1&gt;Please pay attention to build settings&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Por favor, compruebe los parámetros de construcción&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="44"/>
        <source>&lt;h2&gt;Please pay attention to build settings&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Por favor, compruebe los parámetros de construcción&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="45"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="46"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="47"/>
        <source>Main</source>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="48"/>
        <source>Calendar</source>
        <translation>Calendario</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="49"/>
        <source>PhotoBook</source>
        <translation>Foto Libro</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="50"/>
        <source>&lt;h2&gt;Assembly title:&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Título del montaje&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="51"/>
        <source>Include text frames</source>
        <translation>Incluir textos</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="52"/>
        <source>Auto adjust images to frames ?</source>
        <translation>¿ Auto ajustar las imágenes a los marcos ?</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="53"/>
        <source>Use images as backgrounds ?</source>
        <translation>¿ Utilizar imágenes como fondo de página ?</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="54"/>
        <source>Background images in Black and White</source>
        <translation>Imágenes de fondo en blanco y negro</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="55"/>
        <source>Auto Improve images</source>
        <translation>Mejora de imágenes automática</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="56"/>
        <source>Number of Pages:</source>
        <translation>Número de páginas:</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="57"/>
        <source>From Date</source>
        <translation>Desde fecha</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="58"/>
        <source>To Date</source>
        <translation>Hasta fecha</translation>
    </message>
    <message>
        <location filename="../buildsettingspage.cpp" line="92"/>
        <source>Select a design</source>
        <translation>Selecciona un diseño</translation>
    </message>
</context>
<context>
    <name>CDRecordPage</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="90"/>
        <source>Error saving file %1</source>
        <translation>Error guardando archivo %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="114"/>
        <location filename="../cdrecordpage.cpp" line="370"/>
        <source>Cancel Burn process</source>
        <translation>Cancelar grabación</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="115"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <location filename="../cdrecordpage.cpp" line="191"/>
        <source>CD Burning...</source>
        <translation>Grabación de CD...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="122"/>
        <source>Getting information to start the CD burning.</source>
        <translation>Obteniendo información para iniciar la grabación.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="182"/>
        <location filename="../cdrecordpage.cpp" line="187"/>
        <source>Getting ready to burn the CD...</source>
        <translation>Preparando para la grabación...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="183"/>
        <source>Compiling the selected images.</source>
        <translation>Compilando las imágenes seleccionadas.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="188"/>
        <source>Creating an image of the CD.</source>
        <translation>Creando una imagen del CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="192"/>
        <source>CD Burning. Wait a moment please.</source>
        <translation>Grabando el CD, espere un momento por favor.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="198"/>
        <source>The CD is already burned!.&lt;br/&gt;Click on the screen to finish.</source>
        <translation>El CD ya está grabado!-&lt;br/&gt;Pulse OK para finalizar.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="199"/>
        <source>&lt;b&gt;Thanks for your attention.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Grácias por su atención.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="221"/>
        <source>Oh oh... I couldn&apos;t burn the CD!</source>
        <translation>No he podido grabar el CD!</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="222"/>
        <source>There has been a problem during the burning process: </source>
        <translation>Ha ocurrido un problema durante el proceso de grabación:</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="250"/>
        <source>Preparing the images of the order...</source>
        <translation>Preparando la imagen del pedido...</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="254"/>
        <source>Could not delete temp directory %1</source>
        <translation>No se puede borrar el directorio temporal %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="256"/>
        <source>Temp directory %1 could not be created</source>
        <translation>No se puede crear el directorio temporal %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="262"/>
        <source>The images from the order are ready.</source>
        <translation>Las imágenes del pedido están listas.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="268"/>
        <source>Preparing CD image.</source>
        <translation>Preparando la imagen del CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="285"/>
        <source>The image of the CD are correctly burned.</source>
        <translation>La imágen del CD se ha escrito correctamente.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="292"/>
        <source>Ready to burn</source>
        <translation>Preparado para grabar</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="299"/>
        <source>CD correctly burned!</source>
        <translation>El CD ha sido grabado correctamente!</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="312"/>
        <source>cdrecord has finished uncorrently: ExitCode = %1</source>
        <translation>cdrecord ha finalizado de forma incorrecte: Código de Salida = %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="313"/>
        <location filename="../cdrecordpage.cpp" line="324"/>
        <source>Error burning CD</source>
        <translation>Error grabando el CD</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="314"/>
        <source>CD Burn process fails, would you like to try again ?</source>
        <translation>El proceso de grabación ha fallado, quiere intentarlo de nuevo ?</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="321"/>
        <source>There have been some errors creating the ISO image.</source>
        <translation>Han ocurrido errores creando la imagen ISO.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="322"/>
        <source>mkisofs has finished uncorrectly: ExitCode = %1</source>
        <translation>mkisofs ha terminado de forma incorrecta: Codigo de Salida = %1</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="325"/>
        <source>mkisofs process fails, would you like to try again ?</source>
        <translation>mkisofs ha fallado, quiere intentarlo de  nuevo ?</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="348"/>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>CD burning.</source>
        <translation>Grabación de CD.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="349"/>
        <source>Please, insert and empty or appendale CD-R(W) num. (%1 of %2) copy (%3) into drive.</source>
        <translation>Por favor, inserte un CD-R(W) con espacio libre. num (%1 de %2) copia (%3) en la unidad de cds.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="362"/>
        <source>The images do on fit in the CD. Please change the selection.</source>
        <translation>Las imágenes no caben en el CD, por favor cambie la selección.</translation>
    </message>
    <message>
        <location filename="../cdrecordpage.cpp" line="371"/>
        <source>Are you sure you want to cancel burn process ?</source>
        <translation>Seguro que quiere cancelar el proceso de grabación ?</translation>
    </message>
</context>
<context>
    <name>CDRecordWizard</name>
    <message>
        <location filename="../cdrecordwizard.cpp" line="76"/>
        <source>&lt;h2&gt;Select files you want to store.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Seleccione los archivos que quieres almacenar.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>Order cancel question</source>
        <translation>Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <location filename="../cdrecordwizard.cpp" line="84"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
</context>
<context>
    <name>DatabaseConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="323"/>
        <source>Click to setup prices...</source>
        <translation>Clic para configurar precios...</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="330"/>
        <source>Restore default database Values</source>
        <translation>Restablecer los valores por defecto</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Setting up prices</source>
        <translation>Configurando precios</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="381"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation>Error configurando precios:.&apos;%1&apos;.
</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <location filename="../appsettings.cpp" line="403"/>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Restoring defaults</source>
        <translation>Restaurando los valores predeterminados</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="393"/>
        <source>Are you sure you want to restore database to it defaults values ?</source>
        <translation>Seguro que quieres restaurar los valores por defecto a la base de datos ?</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="403"/>
        <source>Default settings restored</source>
        <translation>Los valores por defecto se han restaurado</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="408"/>
        <source>Error restoring defaults: &apos;%1&apos;.
</source>
        <translation>Error restaurando valores por defecto:&apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>DesignInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="143"/>
        <source>Uninstall</source>
        <translation>Desinstalar</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="147"/>
        <source>Install</source>
        <translation>Instalar</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="161"/>
        <source>&lt;h2&gt;Designs&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Diseños&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Delete design</source>
        <translation>Eliminar diseño</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="223"/>
        <source>Are you sure you want to delete current Design?</source>
        <translation>Seguro que quiere eliminar el diseño actual ?</translation>
    </message>
</context>
<context>
    <name>DigiprintWizard</name>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>Order cancel question</source>
        <translation>Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <location filename="../digiprintwizard.cpp" line="74"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
</context>
<context>
    <name>FormatListPage</name>
    <message>
        <location filename="../formatlistpage.cpp" line="72"/>
        <source>Main Menu</source>
        <translation>Menú Principal</translation>
    </message>
</context>
<context>
    <name>GetMediaPage</name>
    <message>
        <location filename="../getmediapage.cpp" line="74"/>
        <source>&lt;h1&gt;Introduce the card or CD in the right socket.&lt;/h1&gt; &lt;h2&gt;Then press the selected media button.&lt;/h2&gt;</source>
        <translation>&lt;h1&gt;Introduzca la tarjeta o el CD en la ranura correcta.&lt;/h1&gt; &lt;h2&gt;Luego presione el botón adecuado.&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>Memory Card</source>
        <translation type="obsolete">Tarjeta de memoria</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="76"/>
        <source>Mem.C./Pendrive</source>
        <translation>Tar/Lapiz de memoria</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="77"/>
        <source>CD or DVD</source>
        <translation>CD o DVD</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="78"/>
        <source>Facebook</source>
        <translation>Facebook</translation>
    </message>
    <message>
        <source>Pen drive</source>
        <translation type="obsolete">Lápiz de memoria</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation type="obsolete">Colección</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="79"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../getmediapage.cpp" line="80"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>LegalProtPage</name>
    <message>
        <location filename="../legalprotpage.cpp" line="33"/>
        <source>Do not Accept</source>
        <translation>No aceptar</translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="34"/>
        <source>Accept contitions</source>
        <translation>Aceptar las condiciones</translation>
    </message>
    <message>
        <location filename="../legalprotpage.cpp" line="80"/>
        <source>&lt;h1 align=center&gt;No legal disclaimer available for this language.&lt;/h1&gt;</source>
        <translation>&lt;h1 align=center&gt;No hay ningún aviso legal para este lenguage.&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>MActionLabel</name>
    <message>
        <source>From %1 %2</source>
        <translation type="obsolete">Desde %1 %2</translation>
    </message>
</context>
<context>
    <name>MainConfigWidget</name>
    <message>
        <source>Catalan</source>
        <translation type="obsolete">Catalán</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="obsolete">Español</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Inglés</translation>
    </message>
</context>
<context>
    <name>MainWizard</name>
    <message>
        <source>Could not open publisher database file %1</source>
        <translation type="obsolete">No se puede abrir el archivo de la base de datos del laboratorio %1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="409"/>
        <source>Error initialising publisher</source>
        <translation>Error inicializando el editor</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>Warning</source>
        <translation>Alerta</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="476"/>
        <source>No images found in media</source>
        <translation>No hay imágenes en el medio</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="481"/>
        <source>Error getting publisher data</source>
        <translation>Error obteniendo los datos del laboratorio</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="515"/>
        <source>Could not open publisher database.</source>
        <translation>No puedo abrir la base de datos del editor.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="162"/>
        <source>Please enter name and phone</source>
        <translation>Por favor introduzca su nombre y su teléfono</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Enter print password&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Entre la contraseña de impresión&lt;/h1&gt;</translation>
    </message>
    <message>
        <source>&lt;font color=red&gt;&lt;h1&gt; Wrong password, please try again &lt;/h1&gt;&lt;/font&gt;</source>
        <translation type="obsolete">&lt;font color=red&gt;&lt;h1&gt;La contraseña es incorrecta, por favor intetelo de nuevo &lt;/h1&gt;&lt;/font&gt;</translation>
    </message>
    <message>
        <source>&lt;h1&gt;Order commited successfull...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Orden confirmada con éxito...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <source>Error storing order</source>
        <translation type="obsolete">Error almacenando pedido</translation>
    </message>
    <message>
        <source>Error printing order</source>
        <translation type="obsolete">Error imprimiendo pedido</translation>
    </message>
    <message>
        <source>(Lab)</source>
        <translation type="obsolete">(Lab)</translation>
    </message>
    <message>
        <source>No suitable products for selected template</source>
        <translation type="obsolete">No hay productos aptos para la plantilla seleccionada</translation>
    </message>
    <message>
        <source>&lt;h1&gt;You must be connected to internet to see this information.&lt;/h1&gt;&lt;i&gt;If you have any problem, please contact to &lt;a href=&quot;www.starblitz-k.com&quot;&gt;www.starblitz-k.com&lt;/a&gt;&lt;/i&gt;</source>
        <translation type="obsolete">&lt;h1&gt;Debe estar conectado a internet para ver esta información.&lt;/h1&gt;&lt;i&gt;Si tiene algún problema, por favor contacte en &lt;a href=&quot;www.starblitz-k.com&quot;&gt;www.starblitz-k.com&lt;/a&gt;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="429"/>
        <source>Looking for supported files, please wait...</source>
        <translation>Buscando archivos soportados, por favor espere...</translation>
    </message>
    <message>
        <source>Error loading photobook.</source>
        <translation type="obsolete">Error cargando el Fotolibro.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="517"/>
        <source>Could not open temp ticket database.</source>
        <translation>No se puede abrir la base de datos temporal para tiquets.</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="546"/>
        <source>Error printing ticket.</source>
        <translation>Error imprimiendo tiquets.</translation>
    </message>
    <message>
        <source>Could not load settings file: %1</source>
        <translation type="obsolete">No se puede cargar el archivo de configuracion: %1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="99"/>
        <source>&lt;h1&gt;Order commited successfull...&lt;/h1&gt;&lt;br/&gt;&lt;h2&gt;Order number: %1&lt;/h2&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation>&lt;h1&gt;Orden confirmada con éxito...&lt;/h1&gt;&lt;br/&gt;&lt;h2&gt;Número de orden: %1&lt;/h2&gt;&lt;center&gt;&lt;img src=&quot;:/success.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="105"/>
        <source>Error commiting order</source>
        <translation>Error confirmando el pedido</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="106"/>
        <source>&lt;h1&gt;Order commited with errors...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/commitwarning.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</source>
        <translation>&lt;h1&gt;Pedido confirmat amb errors...&lt;/h1&gt;&lt;br/&gt;&lt;center&gt;&lt;img src=&quot;:/commitwarning.png&quot;&gt;&lt;/img&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <source>Unable to open temp database: %1</source>
        <translation type="obsolete">No se puede abrir la base de datos temporal: %1</translation>
    </message>
    <message>
        <source>Unable to open publisher database: %1</source>
        <translation type="obsolete">No se puede abrir la base de datos del editor: %1</translation>
    </message>
    <message>
        <location filename="../mainwizard.cpp" line="373"/>
        <location filename="../mainwizard.cpp" line="386"/>
        <location filename="../mainwizard.cpp" line="404"/>
        <source>Error in sync process</source>
        <translation>Error en el proceso de sincronización</translation>
    </message>
</context>
<context>
    <name>MediaConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="614"/>
        <source>&lt;b&gt;Warning:&lt;/b&gt; This directory is deleted every time we get the media screen.</source>
        <translation>&lt;b&gt;Atención:&lt;/b&gt;Este directorio se borrarà cada vez que aparezca la pantalla de media.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="625"/>
        <source>Custom Media Paths</source>
        <translation>Directorios de imágen a medida</translation>
    </message>
</context>
<context>
    <name>ModuleListPage</name>
    <message>
        <location filename="../modulelistpage.cpp" line="261"/>
        <location filename="../modulelistpage.cpp" line="329"/>
        <source>Main Menu</source>
        <translation>Menú Principal</translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="263"/>
        <location filename="../modulelistpage.cpp" line="341"/>
        <source>Load previously stored PhotoBook</source>
        <translation>Cargar Foto libro previamente almacenado</translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="367"/>
        <source>Direct Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../modulelistpage.cpp" line="479"/>
        <source>Errors loading ttpops modules</source>
        <translation>Errores cargando los módulos de ttpops</translation>
    </message>
</context>
<context>
    <name>OMarketPlaceConfigWidget</name>
    <message>
        <source>Click to get more templates...</source>
        <translation type="obsolete">Clic para obtener mas plantillas...</translation>
    </message>
    <message>
        <source>Getting Marketplace Data</source>
        <translation type="obsolete">Obteniendo datos del mercado</translation>
    </message>
    <message>
        <source>Error retrieving marketplace data: &apos;%1&apos;.
</source>
        <translation type="obsolete">Error obteniendo datos del mercado: &apos;%1&apos;.
</translation>
    </message>
</context>
<context>
    <name>OutputConfigWidget</name>
    <message>
        <source>Don&apos;t crop images and leave white margin.</source>
        <translation type="obsolete">No cortar las imágenes y dejar margen blanco.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="530"/>
        <source>Crop Images</source>
        <translation>Cortar imágenes</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="531"/>
        <source>Don&apos;t crop images and leave white margin centered.</source>
        <translation>No recortes las imágenes y deja el margen blanco centrado.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="532"/>
        <source>Don&apos;t crop images and leave white margin on the right side.</source>
        <translation>No recortes las imágenes y deja el margen blanco al lado derecho.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="541"/>
        <source>Send original image files</source>
        <translation>Envia los archivos de imagen originales</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="542"/>
        <source>Adjust image weight for selected format.</source>
        <translation>Ajusta el peso de la imagen al formato seleccionado.</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="513"/>
        <source>Use system printer driver</source>
        <translation>Utilizar el controlador de impresora del sistema</translation>
    </message>
</context>
<context>
    <name>PBImageSelectWidget</name>
    <message>
        <source>Previous page</source>
        <translation type="obsolete">Página anterior</translation>
    </message>
    <message>
        <source>Goes to the previous page of current photobook.</source>
        <translation type="obsolete">Va a la pagina anterior del foto libro actual.</translation>
    </message>
    <message>
        <source>Next page</source>
        <translation type="obsolete">Siguiente página</translation>
    </message>
    <message>
        <source>Goes to the next page of current photobook.</source>
        <translation type="obsolete">Va a la siguiente página del fotolibro actual.</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="obsolete">Avanzado</translation>
    </message>
    <message>
        <source>Opens advanced actions and shows more images.</source>
        <translation type="obsolete">Abre las opciones abanzadas y muestra más imágenes.</translation>
    </message>
</context>
<context>
    <name>PBPagesView</name>
    <message>
        <location filename="../pbpagesview.cpp" line="35"/>
        <location filename="../pbpagesview.cpp" line="48"/>
        <source>Insert Page</source>
        <translation>Añada una página</translation>
    </message>
    <message>
        <location filename="../pbpagesview.cpp" line="39"/>
        <location filename="../pbpagesview.cpp" line="47"/>
        <source>Remove Page</source>
        <translation>Elimine una página</translation>
    </message>
</context>
<context>
    <name>PagesListView</name>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>Adding Pages</source>
        <translation>Añadiendo páginas</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="54"/>
        <source>This PBDocument could not contain more than %1 pages</source>
        <translation>Este documento no puede contener mas de %1 páginas</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Remove current page</source>
        <translation>Elimine la página actual</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="63"/>
        <source>Are you sure to delete the current page ?</source>
        <translation>Seguro que quiere eliminar la página actual?</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>Removing Pages</source>
        <translation>Eliminando páginas</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="74"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation>Lo siento pero no puede eliminar la primera página.</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="79"/>
        <source>This PBDocument need at least %1 pages</source>
        <translation>Este domento necesita al menos %1 páginas</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="110"/>
        <source>Insert Page</source>
        <translation>Añada una página</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="114"/>
        <source>Remove Page</source>
        <translation>Elimine una página</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="118"/>
        <source>Move Page left</source>
        <translation>Mover pág izquierda</translation>
    </message>
    <message>
        <location filename="../pageslistview.cpp" line="122"/>
        <source>Move Page right</source>
        <translation>Mover pág derecha</translation>
    </message>
</context>
<context>
    <name>PhotoBookConfigWidget</name>
    <message>
        <source>None</source>
        <translation type="obsolete">Ninguno</translation>
    </message>
    <message>
        <source>Booklet</source>
        <translation type="obsolete">Librito</translation>
    </message>
</context>
<context>
    <name>PhotoBookEditor</name>
    <message>
        <location filename="../photobookeditor.cpp" line="61"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="62"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="108"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="142"/>
        <source>Enter text:</source>
        <translation>Entre el texto:</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="182"/>
        <source>Error loading image: %1</source>
        <translation>Error cargando imagen: %1</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="200"/>
        <source>Error opening image</source>
        <translation>Error abriendo imagen</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="63"/>
        <location filename="../photobookeditor.cpp" line="209"/>
        <source>More Actions</source>
        <translation>Más acciones</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="64"/>
        <location filename="../photobookeditor.cpp" line="213"/>
        <source>Previous page</source>
        <translation>Página anterior</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="215"/>
        <source>Goes to the previous page of current photobook.</source>
        <translation>Va a la pagina anterior del foto libro actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="65"/>
        <location filename="../photobookeditor.cpp" line="218"/>
        <source>Next page</source>
        <translation>Siguiente página</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="220"/>
        <source>Goes to the next page of current photobook.</source>
        <translation>Va a la siguiente página del fotolibro actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="225"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="70"/>
        <location filename="../photobookeditor.cpp" line="232"/>
        <source>Move images</source>
        <translation>Mover imágenes</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="233"/>
        <source>Move the image</source>
        <translation>Mueve la imagen</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="71"/>
        <location filename="../photobookeditor.cpp" line="239"/>
        <source>Move frames</source>
        <translation>Mueve marcos</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="240"/>
        <source>Move the frame</source>
        <translation>Mueve el marco</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="72"/>
        <location filename="../photobookeditor.cpp" line="246"/>
        <source>New text line frame</source>
        <translation>Nueva linea de texto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="73"/>
        <location filename="../photobookeditor.cpp" line="249"/>
        <source>New photo frame</source>
        <translation>Nuevo marco de foto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="74"/>
        <location filename="../photobookeditor.cpp" line="252"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="253"/>
        <source>Removes selected items.</source>
        <translation>Eliminar los elementos seleccionados.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="76"/>
        <location filename="../photobookeditor.cpp" line="257"/>
        <source>Bring To Front</source>
        <translation>Traer al frente</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="77"/>
        <location filename="../photobookeditor.cpp" line="260"/>
        <source>Send To Back</source>
        <translation>Enviar al fondo</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="81"/>
        <location filename="../photobookeditor.cpp" line="263"/>
        <source>Select All</source>
        <translation>Seleccionar todo</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="78"/>
        <location filename="../photobookeditor.cpp" line="267"/>
        <location filename="../photobookeditor.cpp" line="268"/>
        <source>Text Font</source>
        <translation>Fuente del texto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="271"/>
        <source>Text Color</source>
        <translation>Color del texto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="272"/>
        <source>Changes the text color</source>
        <translation>Cambia el color del texto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="80"/>
        <location filename="../photobookeditor.cpp" line="276"/>
        <source>Edit item</source>
        <translation>Editar elemento</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="278"/>
        <source>Edit current item.</source>
        <translation>Edita el elemento actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="89"/>
        <location filename="../photobookeditor.cpp" line="280"/>
        <source>Shadow</source>
        <translation>Sombra</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="282"/>
        <source>Toggle item shadow.</source>
        <translation>Activa o Desactiva la sombra.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="90"/>
        <location filename="../photobookeditor.cpp" line="287"/>
        <source>Border</source>
        <translation>Borde</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="301"/>
        <source>Rotates current image 270?.</source>
        <translation>Rotar la imagen actual 270 grados?.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="306"/>
        <source>Rotates current image 90?.</source>
        <translation>Rotar la imagen actual 90º?.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="720"/>
        <source>Photobook is not correct.</source>
        <translation>El fotolibro no es correcto.</translation>
    </message>
    <message>
        <source>The Photobook contains empty frames. Please delete or fill it to continue.</source>
        <translation type="obsolete">El Fotolibro contiene marcos de imagen vacios. Por favor rellenelos o eliminelos para continuar.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>Adding Pages</source>
        <translation>Añadiendo páginas</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="738"/>
        <source>This PhotoBook could not contain more than %1 pages</source>
        <translation>Este Fotolibro no puede contener más de %1 páginas</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Remove current page</source>
        <translation>Elimine la página actual</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="747"/>
        <source>Are you sure to delete the current page ?</source>
        <translation>Seguro que quiere eliminar la página actual?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>Removing Pages</source>
        <translation>Eliminando páginas</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="758"/>
        <source>Sorry but you can&apos;t delete the first page.</source>
        <translation>Lo siento pero no puede eliminar la primera página.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="763"/>
        <source>This PhotoBook need at least %1 pages</source>
        <translation>Este Fotolibro necesita al menos %1 páginas</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="83"/>
        <location filename="../photobookeditor.cpp" line="290"/>
        <source>Zoom In</source>
        <translation>Zoom Dentro</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="292"/>
        <source>Zoom in the current image.</source>
        <translation>Zoom para dentro de la imagen actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="84"/>
        <location filename="../photobookeditor.cpp" line="295"/>
        <source>Zoom Out</source>
        <translation>Zoom Fuera</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="297"/>
        <source>Zoom out the current image.</source>
        <translation>Zoom para fuera de la imagen actual.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="85"/>
        <location filename="../photobookeditor.cpp" line="300"/>
        <source>Rotate left</source>
        <translation>Girar a la izquierda</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="67"/>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="68"/>
        <source>Redo</source>
        <translation>Rehacer</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="79"/>
        <source>Item Color</source>
        <translation>Color de l&apos;element</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="86"/>
        <location filename="../photobookeditor.cpp" line="305"/>
        <source>Rotate right</source>
        <translation>Girar a la derecha</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="87"/>
        <source>Fit to Frame</source>
        <translation>Encajar en marco</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="309"/>
        <source>Fit To Frame</source>
        <translation>Encajar en marco</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="310"/>
        <source>Fit image to frame size.</source>
        <translation>Encaja la imagen al tamaño del marco.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="313"/>
        <source>Copy Page Xml</source>
        <translation>Copiar xml de la página</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="496"/>
        <source>New Text Frame</source>
        <translation>Nueva linea de texto</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Delete item(s)</source>
        <translation>Eliminar elemento(s)</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="507"/>
        <source>Do you want to delete all selected items ?</source>
        <translation>Quiere borrar todos los elementos seleccionados ?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="563"/>
        <source>Please, choose the font for selected text items</source>
        <translation>Por favor, seleccione la fuente para los elementos seleccionados</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="615"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="616"/>
        <source>Please, select a item to edit.</source>
        <translation>Por favor, seleccione un elemento para editar.</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Modify page layout</source>
        <translation>Modifica la distribución de la página</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="671"/>
        <source>Do you want to change the layout of current page ?</source>
        <translation>Quiere cambiar la distribución de la página actual ?</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="673"/>
        <source>No Reason</source>
        <translation>Sin razón</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>Not suitable template</source>
        <translation>Plantilla no valida</translation>
    </message>
    <message>
        <location filename="../photobookeditor.cpp" line="680"/>
        <source>You can not apply this template for current page because %1</source>
        <translation>No puede aplicar esta plantilla a la página actual porque %1</translation>
    </message>
</context>
<context>
    <name>PhotoBookListViewTabWidget</name>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="43"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="120"/>
        <source>Layouts</source>
        <translation>Distribuciones</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="48"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="121"/>
        <source>Covers</source>
        <translation>Portadas</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="53"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="122"/>
        <source>BackCovers</source>
        <translation>Contraportadas</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="58"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="123"/>
        <source>Clipart</source>
        <translation>Clipart</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="63"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="124"/>
        <source>Masks</source>
        <translation>Máscaras</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="68"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="125"/>
        <source>Frames</source>
        <translation>Marcos</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="73"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="126"/>
        <source>Backgrounds</source>
        <translation>Fondos</translation>
    </message>
    <message>
        <location filename="../photobooklistviewtabwidget.cpp" line="78"/>
        <location filename="../photobooklistviewtabwidget.cpp" line="127"/>
        <source>Pages</source>
        <translation>Páginas</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
</context>
<context>
    <name>PhotoBookOutputPage</name>
    <message>
        <location filename="../photobookoutputpage.cpp" line="203"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Este es el resumen de su pedido.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="43"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="44"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="45"/>
        <source>Store</source>
        <translation>Almacenar</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="76"/>
        <source>Building, please wait...</source>
        <translation>Construyendo, por favor esperad...</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>Error building PhotoBook</source>
        <translation>Error construyendo el fotolibro</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="79"/>
        <source>The following errors has occurred during photobook export:
 %1</source>
        <translation>Los siguientes errores se han producido durante la exportación del fotolibro:
%1</translation>
    </message>
    <message>
        <location filename="../photobookoutputpage.cpp" line="200"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Procesando...&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>PhotoBookWizard</name>
    <message>
        <source>Could not find photobook: %1</source>
        <translation type="obsolete">No se encuentra el fotolibro: %1</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="123"/>
        <source>Building photobook...</source>
        <translation>Generando fotolibro...</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="218"/>
        <source>&lt;h2&gt;Please select pictures for you photo assembly.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Por favor, seleccionar las fotografias para su montaje.&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>Loading photobook...</source>
        <translation type="obsolete">Cargando el Fotolibro...</translation>
    </message>
    <message>
        <source>Storing photobook...</source>
        <translation type="obsolete">Almacenando el Fotolibro...</translation>
    </message>
    <message>
        <source>Process information</source>
        <translation type="obsolete">Procesando información</translation>
    </message>
    <message>
        <source>Your photobook has been stored in your media.</source>
        <translation type="obsolete">Su Fotolibro se ha guardado en su dispositivo.</translation>
    </message>
    <message>
        <source>Error storing photobook</source>
        <translation type="obsolete">Error guardando el Fotolibro</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="107"/>
        <source>Downloading template...</source>
        <translation>Descargando plantilla...</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>PhotoBook creation</source>
        <translation>Creación del fotolibro</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="279"/>
        <source>Create a new PhotoBook ?</source>
        <translation>Crear un nuevo fotolibro ?</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>Too few images</source>
        <translation>Muy pocas imágenes</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="290"/>
        <source>You have to select more images to generate this photo book. </source>
        <translation>Tiene que seleccionar mas imágenes para generar este foto libro.</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Loading photobook</source>
        <translation>Cargando fotolibro</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="295"/>
        <source>Error loading photobook template: %1</source>
        <translation>Error cargando la plantilla del fotolibro: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El Módulo %1 no define ninguna ruta para plantillas</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>Order cancel question</source>
        <translation>Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <location filename="../photobookwizard.cpp" line="339"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
    <message>
        <source>Error exporting PhotoBook</source>
        <translation type="obsolete">Error exportando fotolibro</translation>
    </message>
    <message>
        <source>Downloading required files for this template, please wait...</source>
        <translation type="obsolete">Descargando los archivos necesarios para esta plantilla, por favor espere...</translation>
    </message>
    <message>
        <source>Errors downloading templates</source>
        <translation type="obsolete">Error descargando plantillas</translation>
    </message>
    <message>
        <source>There was errors downloading templates. Some of the layouts for this model may not work. Please contact to your software provider.</source>
        <translation type="obsolete">Han ocurrido errores descargando las plantillas. Algunas de las distribuciones para este modelo pueden no funcionar. Por favor contacte con su proveedor de software.</translation>
    </message>
    <message>
        <source>The following errors has occurred during photobook export:
 %1</source>
        <translation type="obsolete">Los siguientes errores se han producido durante la exportación del fotolibro:
%1</translation>
    </message>
</context>
<context>
    <name>PhotoIndexConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="302"/>
        <source>Click to Select</source>
        <translation>Clica para seleccionar</translation>
    </message>
</context>
<context>
    <name>PhotoIndexWizard</name>
    <message>
        <location filename="../photoindexwizard.cpp" line="39"/>
        <source>&lt;h2&gt;Please, select images for index and the product.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Por favor, seleccionar las imágenes para el índice y el producto.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="121"/>
        <source>Building photoindex...</source>
        <translation>Construyendo foto índice...</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="137"/>
        <source>Error saving photoindex page %1</source>
        <translation>Error guardando la página del foto índice %1</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="150"/>
        <source>Error creating photoindex</source>
        <translation>Error creando el foto índice</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>Photo Index warning</source>
        <translation>Aviso de foto índice</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="155"/>
        <source>You have to select at least one image.</source>
        <translation>Debes seleccionar al menos una imagen.</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>Order cancel question</source>
        <translation>Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <location filename="../photoindexwizard.cpp" line="163"/>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation>Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
</context>
<context>
    <name>ProductTypeSelectionPage</name>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="68"/>
        <source>Order Prints</source>
        <translation>Copias Digitales</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="69"/>
        <source>Print your photos in standard format sizes</source>
        <translation>Imprime tus fotos en tamaño estandard</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="72"/>
        <source>Photo Id</source>
        <translation>Foto Carnet</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="73"/>
        <source>Make your photo ids</source>
        <translation>Haz tus fotos de carnet</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="75"/>
        <source>Photo Index</source>
        <translation>Foto índice</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="76"/>
        <source>Build and index with your photos</source>
        <translation>Construye un índice con tus fotos</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="77"/>
        <source>CD Burning</source>
        <translation>Grabar CDs</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="78"/>
        <source>Save your photos on CD</source>
        <translation>Guarda tus fotos en CD</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="81"/>
        <source>PhotoBooks</source>
        <translation>Fotolibros</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="82"/>
        <source>PhotoBooks, Photo Magazines, with photo quality</source>
        <translation>Fotolibros, Fotorevistas, con calidad foto</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="85"/>
        <source>Calendars</source>
        <translation>Calendarios</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="86"/>
        <source>Build a calendar with your photos</source>
        <translation>Construye calendarios con tus fotos</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="89"/>
        <source>Decorations</source>
        <translation>Decoraciones</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="90"/>
        <source>Use your photos to wish christmas, birthdays, ... </source>
        <translation>Usa tus fotos para felicitar cumpleaños, navidades,...</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="93"/>
        <source>Other</source>
        <translation>Otros</translation>
    </message>
    <message>
        <location filename="../producttypeselectionpage.cpp" line="94"/>
        <source>Other designs with your photos</source>
        <translation>Otros diseños usando tus fotos</translation>
    </message>
</context>
<context>
    <name>PubDatabaseCreationDialog</name>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="45"/>
        <source>Publisher default database creation</source>
        <translation>Creación de la base de datos por omisión del editor</translation>
    </message>
    <message>
        <location filename="../pubdatabasecreationdialog.cpp" line="48"/>
        <source>&lt;h2&gt;Please, select one of the following options:&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Por favor, selecciona una de las siguientes opciones&lt;/h2&gt;</translation>
    </message>
</context>
<context>
    <name>PublisherConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="187"/>
        <source>Contact</source>
        <translation>Contacto</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="200"/>
        <source>Send Address</source>
        <translation>Dirección de envio</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="220"/>
        <source>Orher Data</source>
        <translation>Otros datos</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="230"/>
        <source>Required fields</source>
        <translation>Campos requeridos</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Could not find style.txt file</source>
        <translation type="obsolete">No puedo encontrar el archivo style.txt</translation>
    </message>
    <message>
        <location filename="../ttpopsapp.cpp" line="44"/>
        <source>Could not find ttpops_style.txt file</source>
        <translation>No puedo encontrar el archivo ttpops_style.txt</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="80"/>
        <source>Catalan</source>
        <translation>Catalán</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Spanish</source>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="82"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>Loggin in...</source>
        <translation>Registrandose...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cdrecordpage.cpp" line="64"/>
        <location filename="../cdrecordpage.cpp" line="77"/>
        <source>Could not create directory: %1</source>
        <translation>No puedo crear el directorio:%1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="97"/>
        <source>Low res. image</source>
        <translation>Baja resolución</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>Login Failed</source>
        <translation>Error registrandose</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="132"/>
        <source>You will not have access to printers untill you login correctly</source>
        <translation>No tendrá acceso a las impresoras hasta que no se registre correctamente</translation>
    </message>
    <message>
        <source>Could not remove file %1</source>
        <translation type="obsolete">No puedo eliminar el archivo %1</translation>
    </message>
    <message>
        <source>Could not open file %1 for database export.</source>
        <translation type="obsolete">No puedo abrir el archivo %1 para exportarlo en la base de datos.</translation>
    </message>
    <message>
        <source>Could not database %1 for database export.</source>
        <translation type="obsolete">No puedo abrir el archivo %1 para exportarlo en la base de datos.</translation>
    </message>
    <message>
        <source>Publisher Databsase Manager error</source>
        <translation type="obsolete">Error del gestor de la base de datos del editor</translation>
    </message>
    <message>
        <source>Could not load settings file: %1</source>
        <translation type="obsolete">No se puede cargar el archivo de configuracion: %1</translation>
    </message>
    <message>
        <source>Publisher database file: %1 , does not exists!</source>
        <translation type="obsolete">El archivo de la base de datos del editor %1 no existe!</translation>
    </message>
    <message>
        <source>Error opening publisher database %1</source>
        <translation type="obsolete">Error abriendo el archivo de la base de datos del editor %1</translation>
    </message>
    <message>
        <location filename="../iorderwizard.cpp" line="32"/>
        <source>Could not open publisher database file %1</source>
        <translation>No se puede abrir el archivo de la base de datos del laboratorio %1</translation>
    </message>
</context>
<context>
    <name>STAppModule</name>
    <message>
        <location filename="../stappmodule.cpp" line="79"/>
        <location filename="../stappmodule.cpp" line="83"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="86"/>
        <source>Digital Prints</source>
        <translation>Impresiones digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="89"/>
        <source>Decorations</source>
        <translation>Decoraciones</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="92"/>
        <location filename="../stappmodule.cpp" line="128"/>
        <source>PhotoBooks</source>
        <translation>Fotolibros</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="95"/>
        <source>CD Record</source>
        <translation>Grabación de CD</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="98"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="101"/>
        <location filename="../stappmodule.cpp" line="137"/>
        <source>Gifts</source>
        <translation>Personalizados</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="104"/>
        <source>Photo Index</source>
        <translation>Foto índice</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="107"/>
        <source>Bash Templates</source>
        <translation>Plantillas multifoto</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="110"/>
        <source>PDF Documents</source>
        <translation>Documentos PDF</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="122"/>
        <source>Digital Photo</source>
        <translation>Foto digital</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="125"/>
        <source>Photo Assembly</source>
        <translation>Montaje fotográfico</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="131"/>
        <source>Digital Storage</source>
        <translation>Almacenamiento digital</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="134"/>
        <source>Digital Documents</source>
        <translation>Documentos digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="149"/>
        <source>Operations with digital pictures.</source>
        <translation>Operaciones con imágenes digitales.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="152"/>
        <source>Assemblies and decorations for digital pictures.</source>
        <translation>Montajes y decoraciones con fotografias digitales.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="155"/>
        <source>Multipage assemblies for digital pictures</source>
        <translation>Montajes multipágina para fotos digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="158"/>
        <source>CD/DVD Burning, Memory and pendrive storage</source>
        <translation>Grabado de CD/DVD y almacenaje de memorias</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="161"/>
        <source>Operations with digital documents.</source>
        <translation>Operaciones con documentos digitales.</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="164"/>
        <source>Gifts built with your digital pictures</source>
        <translation>Personalizados con sus imágenes digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="176"/>
        <source>Calendars</source>
        <translation>Calendarios</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="179"/>
        <source>PhotoId</source>
        <translation>Foto Carnet</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="182"/>
        <source>Cards</source>
        <translation>Tarjetas</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="185"/>
        <source>Batch Templates</source>
        <translation>Plantillas multifoto</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="188"/>
        <location filename="../stappmodule.cpp" line="212"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="200"/>
        <source>Calendar assemblies for digital pictures</source>
        <translation>Montajes con calendarios para imágenes digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="203"/>
        <source>Photo id assemblies</source>
        <translation>Montajes con fotocarnet</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="206"/>
        <source>Card assemblies for digital pictures</source>
        <translation>Montajes con tarjetas para imágenes digitales</translation>
    </message>
    <message>
        <location filename="../stappmodule.cpp" line="209"/>
        <source>Many photos in a single template</source>
        <translation>Muchas fotos en una sola plantilla</translation>
    </message>
</context>
<context>
    <name>SinglePhotoSelPage</name>
    <message>
        <location filename="../singlephotoselpage.cpp" line="46"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="47"/>
        <source>All</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="48"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="49"/>
        <source>Filter</source>
        <translation>Filtrar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="50"/>
        <source>NO Filter</source>
        <translation>Sin Filtro</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="51"/>
        <source>Accept</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="52"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="254"/>
        <source>Please select directory to filter</source>
        <translation>Por favor seleccione la carpeta a filtrar</translation>
    </message>
    <message>
        <source>&lt;b style=&quot;font-size:24pt;&quot;&gt;%1&lt;/b&gt; of %2 &lt;em&gt;images selected.&lt;/em&gt;</source>
        <translation type="obsolete">&lt;b style=&quot;font-size:24pt;&quot;&gt;%1&lt;/b&gt; de %2 &lt;em&gt;imágenes seleccionadas.&lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../singlephotoselpage.cpp" line="298"/>
        <source>Error opening image</source>
        <translation>Error abriendo imagen</translation>
    </message>
</context>
<context>
    <name>StorageOutputPage</name>
    <message>
        <location filename="../storageoutputpage.cpp" line="32"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Este es el resumen de su pedido.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="33"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="34"/>
        <source>Store</source>
        <translation>Almacenar</translation>
    </message>
    <message>
        <location filename="../storageoutputpage.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>StoredTemplateSelPage</name>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="34"/>
        <source>Back</source>
        <translation>Anterior</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="35"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="36"/>
        <source>&lt;h1&gt;Please select a PhotoBook to load.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Por favor, selecciona el Foto libro a cargar.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Photobook selection</source>
        <translation>Selección de Fotolibro</translation>
    </message>
    <message>
        <location filename="../storedtemplateselpage.cpp" line="103"/>
        <source>Sorry but there are no stored PhotoBooks in this media</source>
        <translation>Lo siento pero no hay ningún Fotolibro en este dispositivo</translation>
    </message>
</context>
<context>
    <name>TPOrderSummaryPage</name>
    <message>
        <location filename="../tpordersummarypage.cpp" line="114"/>
        <source>&lt;h1&gt;This is your Order Summary.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Este es el resumen de su pedido.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="30"/>
        <source>Photos</source>
        <translation>Fotos</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="31"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="32"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../tpordersummarypage.cpp" line="117"/>
        <source>&lt;h1&gt;Processing...&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Procesando...&lt;/h1&gt;</translation>
    </message>
</context>
<context>
    <name>TPSendProgressPage</name>
    <message>
        <source>&lt;h1&gt; Sending command ... &lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt; Enviando el pedido ... &lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="29"/>
        <source>&lt;center&gt;&lt;h1&gt; Sending command ... &lt;/h1&gt;&lt;/center&gt;</source>
        <translation>&lt;center&gt;&lt;h1&gt; Enviando el pedido ... &lt;/h1&gt;&lt;/center&gt;</translation>
    </message>
    <message>
        <location filename="../tpsendprogresspage.cpp" line="43"/>
        <source>Progress...</source>
        <translation>Progreso...</translation>
    </message>
</context>
<context>
    <name>TemplateEditor</name>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Continuar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>Enter text:</source>
        <translation type="obsolete">Entre el texto:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Error loading image: %1</source>
        <translation type="obsolete">Error cargando imagen: %1</translation>
    </message>
    <message>
        <source>Error opening image</source>
        <translation type="obsolete">Error abriendo imagen</translation>
    </message>
    <message>
        <source>Masks</source>
        <translation type="obsolete">Máscaras</translation>
    </message>
    <message>
        <source>Cliparts</source>
        <translation type="obsolete">Cliparts</translation>
    </message>
    <message>
        <source>New text line frame</source>
        <translation type="obsolete">Nueva linea de texto</translation>
    </message>
    <message>
        <source>New photo frame</source>
        <translation type="obsolete">Nuevo marco de foto</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Color</translation>
    </message>
    <message>
        <source>Change text or background color.</source>
        <translation type="obsolete">Cambia el color de fondo del texto.</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Eliminar</translation>
    </message>
    <message>
        <source>Removes selected items.</source>
        <translation type="obsolete">Elimina los elemetos seleccionados.</translation>
    </message>
    <message>
        <source>Bring To Front</source>
        <translation type="obsolete">Traer al Frente</translation>
    </message>
    <message>
        <source>Send To Back</source>
        <translation type="obsolete">Enviar al Fondo</translation>
    </message>
    <message>
        <source>Text Font</source>
        <translation type="obsolete">Fuente del Texto</translation>
    </message>
    <message>
        <source>Edit item</source>
        <translation type="obsolete">Editar Elemento</translation>
    </message>
    <message>
        <source>Edit current item.</source>
        <translation type="obsolete">Editar elemento actual.</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="obsolete">Zoom dentro</translation>
    </message>
    <message>
        <source>Zoom in the current image.</source>
        <translation type="obsolete">Zoom para dentro de la imagen actual.</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="obsolete">Zoom Fuera</translation>
    </message>
    <message>
        <source>Zoom out the current image.</source>
        <translation type="obsolete">Zoom para fuera de la imagen actual.</translation>
    </message>
    <message>
        <source>Rotate left</source>
        <translation type="obsolete">Girar a la izquierda</translation>
    </message>
    <message>
        <source>Rotates current image 270?.</source>
        <translation type="obsolete">Rotar la imagen actual 270 grados?.</translation>
    </message>
    <message>
        <source>Shadow</source>
        <translation type="obsolete">Sombra</translation>
    </message>
    <message>
        <source>Toggle item shadow.</source>
        <translation type="obsolete">Activa o Desactiva la sombra.</translation>
    </message>
    <message>
        <source>Border</source>
        <translation type="obsolete">Borde</translation>
    </message>
    <message>
        <source>Loading images</source>
        <translation type="obsolete">Cargando imágenes</translation>
    </message>
    <message>
        <source>Rendering template</source>
        <translation type="obsolete">Generando plantilla</translation>
    </message>
    <message>
        <source>Generating multi image</source>
        <translation type="obsolete">Generando multi imagen</translation>
    </message>
    <message>
        <source>New Text Frame</source>
        <translation type="obsolete">Nueva linea de texto</translation>
    </message>
    <message>
        <source>Delete item(s)</source>
        <translation type="obsolete">Eliminar elemento(s)</translation>
    </message>
    <message>
        <source>Do you want to delete all selected items ?</source>
        <translation type="obsolete">Quiere eliminar los elementos seleccionados ?</translation>
    </message>
    <message>
        <source>Please, choose the font for selected text items</source>
        <translation type="obsolete">Por favor, escoja una fuente para los elementos seleccionados</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Información</translation>
    </message>
    <message>
        <source>Please, select a item to edit.</source>
        <translation type="obsolete">Por favor, seleccione un elemento para editar.</translation>
    </message>
</context>
<context>
    <name>TemplateInfoWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="60"/>
        <source>&lt;h2&gt;Templates&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Plantillas&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="70"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
</context>
<context>
    <name>TemplateManagerWidget</name>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="242"/>
        <source>PhotoBook</source>
        <translation>Foto Libro</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="247"/>
        <source>Calendar</source>
        <translation>Calendario</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="252"/>
        <source>Decorations</source>
        <translation>Decoraciones</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="257"/>
        <source>Photo id</source>
        <translation>Foto carnet</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="262"/>
        <source>Other</source>
        <translation>Otros</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="315"/>
        <source>Error loading templates</source>
        <translation>Error cargando plantillas</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="365"/>
        <source>Error downloading template</source>
        <translation>Error descargando plantilla</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Downloading template</source>
        <translation>Descargando plantilla</translation>
    </message>
    <message>
        <location filename="../templatemanagerwidget.cpp" line="371"/>
        <source>Template is already on disk.</source>
        <translation>La plantilla ya está en el disco.</translation>
    </message>
</context>
<context>
    <name>TemplateSelectionPage</name>
    <message>
        <location filename="../templateselectionpage.cpp" line="63"/>
        <source>&lt;h1&gt;Getting info from internet.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Obteniendo información de internet.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="69"/>
        <source>&lt;h1&gt;There is no information for this item.&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;No hay información para este elemento.&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="85"/>
        <source>&lt;h2&gt;Please, select a template from the list.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Por favor, seleccione una plantilla de la lista.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="96"/>
        <source>Products:</source>
        <translation>Productos:</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="109"/>
        <source>&lt;h2&gt;There is no templates of this type.&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;No hay plantillas de este tipo.&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../templateselectionpage.cpp" line="121"/>
        <source>Size:</source>
        <translation>Tamaño:</translation>
    </message>
</context>
<context>
    <name>TemplateWizard</name>
    <message>
        <source>Could not find template: %1</source>
        <translation type="obsolete">No se puede encontrar la plantilla: %1</translation>
    </message>
    <message>
        <source>Loading photobook</source>
        <translation type="obsolete">Cargando fotolibro</translation>
    </message>
    <message>
        <source>Error loading photobook template: %1</source>
        <translation type="obsolete">Error cargando la plantilla del fotolibro: %1</translation>
    </message>
    <message>
        <source>Module %1 does not define any templatepath</source>
        <translation type="obsolete">El módulo %1 no define ningun archivo de plantilla</translation>
    </message>
    <message>
        <source>Temp directory %1 could not be created</source>
        <translation type="obsolete">No se puede crear el directorio temporal %1</translation>
    </message>
    <message>
        <source>Error saving file: %1</source>
        <translation type="obsolete">Error guardando archivo: %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Order cancel question</source>
        <translation type="obsolete">Pergunta para cancelar el pedido</translation>
    </message>
    <message>
        <source>You&apos;ll loose all the changes. Cancel anyway ?</source>
        <translation type="obsolete">Si cancelas perderás todos los cambios realizados. Cancelar de todos modos ?</translation>
    </message>
</context>
<context>
    <name>TemplatesConfigWidget</name>
    <message>
        <location filename="../appsettings.cpp" line="427"/>
        <source>Click to setup templates...</source>
        <translation>Clic para configurar plantillas...</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Setting up prices</source>
        <translation>Configurando precios</translation>
    </message>
    <message>
        <location filename="../appsettings.cpp" line="482"/>
        <source>Error setting up prices: &apos;%1&apos;.
</source>
        <translation>Error configurando precios:.&apos;%1&apos;.
</translation>
    </message>
</context>
</TS>
