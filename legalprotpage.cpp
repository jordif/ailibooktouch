/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "legalprotpage.h"
#include <QTextCodec>
#include <QLabel>
#include <QLayout>
#include <QToolButton>
#include <QTextEdit>
#include <QFile>

#include "sapplication.h" 


void LegalProtPage::retranslateUI()
{
	ButBack->setText(tr("Do not Accept"));
	ButContinue->setText(tr("Accept contitions"));
}


LegalProtPage::LegalProtPage(QWidget* _Parent): STIWizardPage(_Parent)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this);
	QLabel* IconLabel = new QLabel(this);
	IconLabel->setPixmap(QPixmap(":/legalprot.png"));
	IconLabel->setAlignment(Qt::AlignHCenter);
	IconLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
	MLayout->addWidget(IconLabel);
	
	LegalProtLabel = new QLabel(this);
	LegalProtLabel->setWordWrap(true);
	LegalProtLabel->setAlignment(Qt::AlignTop);
	
	MLayout->addWidget(LegalProtLabel);

	
	QHBoxLayout* ButLayout = new QHBoxLayout;
	MLayout->addLayout(ButLayout);
	
	ButBack = new QToolButton(this);
	ButBack->setObjectName("ActionButton");
	ButBack->setIcon(QIcon(":/st/tpopsl/cancel.png")); 
	ButBack->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButBack->setIconSize(QSize(64, 64)); 
	ButLayout->addWidget(ButBack);
	connect ( ButBack, SIGNAL(clicked()), this, SIGNAL(previousPage()));

	ButLayout->addItem(new QSpacerItem(10,0, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));

	
	ButContinue = new QToolButton(this);
	ButContinue->setObjectName("ActionButton");
	ButContinue->setIcon(QIcon(":/st/tpopsl/accept.png")); 
	ButContinue->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButContinue->setIconSize(QSize(64, 64)); 
	ButLayout->addWidget(ButContinue);
	connect ( ButContinue, SIGNAL(clicked()), this, SIGNAL(nextPage()));
}


void LegalProtPage::init()
{
	QString LegalProtText = tr("<h1 align=center>No legal disclaimer available for this language.</h1>");
	QString LegalProtFileName = QCoreApplication::applicationDirPath() + "/docs" + "/legalprot_" + SApplication::currentLocale() + ".html";
	QFile InfoFile(LegalProtFileName);
	if (InfoFile.open(QFile::ReadOnly))
	{
		QByteArray Data = InfoFile.readAll();
		QTextCodec *codec = Qt::codecForHtml(Data);
		if (Data.size() > 0)
			LegalProtText = codec->toUnicode(Data);
	
	}
	LegalProtLabel->setText(LegalProtText);
	retranslateUI();
}

void LegalProtPage::setFromPrevious()
{
	emit previousPage();
}



