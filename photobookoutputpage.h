/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOBOOKOUTPUTPAGE_H
#define PHOTOBOOKOUTPUTPAGE_H

#include "stiwizardpage.h"
#include <QSqlRecord> 
#include "document.h"
#include "publisherdatabase.h"

/**
	@author 
*/
namespace STDom
{
	class PrintJobModel;
}
class QLabel; 
class QToolButton;
class TPProductListView;
class QAbstractItemModel; 
class STPhotoBook; 
class SProcessStatusWidget;
class QFrame;
class PhotoBookOutputPage : public STIWizardPage
{
Q_OBJECT;

private:
	enum EnStatus
	{
		StatusRendering,
		StatusShowInfo
	};

	QLabel* HeaderLabel; 
	QLabel* SummaryLabel; 
	QToolButton* ButPrint; 
	QToolButton* ButStore; 
	QToolButton* ButPrevious; 
	QFrame* FrameProdSelect;
	STDom::PrintJobModel* Model;
	STDom::PublisherBill::PublisherShippingMethod  ShippingMethod;
	TPProductListView* LVProducts;
	SPhotoBook::Document* MPhotoBook;
	bool StoreEnabled;
	SProcessStatusWidget* StatW;

	void retranslateUi();
	QToolButton* newActionButton(const QIcon& _Icon);
	void renderAlbum();


public:
	PhotoBookOutputPage(QWidget* _Parent = 0);
	void init();
	void setModel(STDom::PrintJobModel* _Model);
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod& _Value) { ShippingMethod = _Value; }
	void setPhotoBook(SPhotoBook::Document* _PhotoBook) { MPhotoBook = _PhotoBook; }
	void calcBill();
	void setStoreEnabled(bool _Enabled);
	bool storeEnabled() const;
	void setStatus(EnStatus _Status);

private slots:
	void editAddProductToAll();
	void editDelProductToAll();

signals:
	void storePhotoBook(); 
	void printPhotoBook(); 
};

#endif
