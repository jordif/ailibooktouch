/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QFile>
#include <QTextStream>
#include <QPixmapCache>
#include <QPlastiqueStyle>
#include <QMessageBox>
#include <QPixmapCache> 
#include <QItemEditorFactory>
#include <QDebug>

#include "sapplication.h" 
#include "sterror.h"
#include "cursors.h"
#include "mainwizard.h" 
#include "appsettings.h"
#include "stlog.h" 
#include "abstractgraphicsitem.h"
#include "graphicsphotoitem.h"
#include "stimage.h"
#include "printjobmodel.h"

// -- PrintKeeper
#include "printkeeper.h"
#include "printkeeperloginmanager.h"

// -- Miscelanea
#include "smessagebox.h"
#include "pitemeditorfactory.h"

// -- Order Manager
#ifndef STANDARD_EDITION
#include "stomwatchdog.h"
#endif

// -- Database
#include "fsqldatabasesettings.h"
#include "pubdatabasemanager.h"

// -- App
#include "ttpopsapp.h"

static QByteArray mlocale_encode(const QString &f)
{
	return f.toAscii();
}

static QString mlocale_decode(const QByteArray &f)
{
	return QString::fromAscii(f);
}


int main(int argc, char *argv[])
{
	int Res = 0;

	TtpopsApp App(argc, argv);
	//App.setStyle(new QPlastiqueStyle);

	//Language
	App.addLocale("ca", QApplication::tr("Catalan"));
	App.addLocale("es", QApplication::tr("Spanish"));
	App.addLocale("en", QApplication::tr("English"));
	//App.addLocale("fr", QApplication::tr("French"));
	SApplication::setLocaleFilePrefix("ailiphotobooktouch");
	SApplication::setLocale(AppSettings::mainLocale());

	App.showSplashScreen(QPixmap(":/splash.png"));
	App.showSplashMessage(QApplication::tr("Initializing ..."), Qt::AlignHCenter | Qt::AlignBottom, Qt::white);

	//Inicialitzacions dels cursors.
	loadCursors(); // Per el codi de AS

	//Creating Settings
	AppSettings Settings;

	//Model Initializations
	STDom::PrintJobModel::setCutWarningImage(QImage(":/scissors.png"));
	STDom::PrintJobModel::setLowResWarningImage(QImage(":/warning.png"));
	STDom::PrintJobModel::setLowResWarningLimit(Settings.mainLowResWarningDpis());
	SPhotoBook::GraphicsPhotoItem::setLowResWarning(QObject::tr("Low res. image"), Settings.mainLowResWarningDpis());

	//Log system
	STLog::setIdentity(App.applicationName());

	//Database
	App.showSplashMessage(QApplication::tr("Loading resources..."), Qt::AlignHCenter | Qt::AlignBottom, Qt::white);
	FSqlDatabaseSettings::setEmbededResources(true);
	SApplication::setDbFilesPath(Settings.mainOrdersFilesPath());
	PubDatabaseManager::checkForPublisherDB();
	PubDatabaseManager::updatePublisherDBTemplateProducts();

	//Miscelanea 
	STLog::setIdentity(App.applicationName());
	SPhotoBook::GraphicsPhotoItem::setTouchInterface(true);
	STImage::setDefaultEncryptionKey("SPiscool");
	QItemEditorFactory::setDefaultFactory(new PItemEditorFactory);

	
	QPixmapCache::setCacheLimit(1024*256);
	//OPhotoCollection::start(); //To import collection images.
	SPhotoBook::AbstractGraphicsItem::setGridValue(1);

#ifdef STANDARD_EDITION
	try
	{
		// --- PrintKeeper
		App.showSplashMessage(QApplication::tr("Loggin in..."), Qt::AlignHCenter | Qt::AlignBottom, Qt::white);
		PrintKeeperLoginManager PKLogin(QUrl("http://eprintkeeper.starblitz-k.com"));
		if (PKLogin.login(Settings.accountUserName(), Settings.accountPassword(), Settings.accountKey()))
		{
			Settings.setAccountUserName(PKLogin.userName());
			Settings.setAccountPassword(PKLogin.password());
			Settings.setAccountKey(PKLogin.accessKey());
		}
		else
			SMessageBox::warning(0, QObject::tr("Login Failed"), QObject::tr("You will not have access to printers untill you login correctly"));
	}
	catch ( STError& _Error)
	{
		QMessageBox::critical(0, "Error getting account info", _Error.description());
	}
#endif

	MainWizard* MWiz = 0;
	try
	{
		QFile::setEncodingFunction(mlocale_encode);
		QFile::setDecodingFunction(mlocale_decode);
		App.showSplashMessage(QApplication::tr("Initializing GUI..."), Qt::AlignHCenter | Qt::AlignBottom, Qt::white);
		MWiz = new MainWizard;
		MWiz->init();
		App.showSplashMessage("Welcome to " + App.fullApplicationName(), Qt::AlignHCenter | Qt::AlignBottom, Qt::white);
		App.finishSplashDelayed(MWiz);

		Res = App.exec();
	}
	catch (STError _err)
	{
		QMessageBox::critical(MWiz, App.fullApplicationName(), _err.description());
	}
	catch ( ... )
	{
		QMessageBox::critical(MWiz, "Unknown error", "An uncontrolled error has occurred, please contact to your technical service.");
	}
	return Res;

} 

