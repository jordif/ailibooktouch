/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "mainwizard.h"
#include <QCoreApplication> 
#include <QSettings>
#include <QDebug>

//Pages
#include "getmediapage.h"
#include "tpordersummarypage.h" 
#include "tpsendprogresspage.h" 
#include "legalprotpage.h" 

//Wizards
#include "digiprintwizard.h" 
#include "photobookwizard.h"
#include "photoindexwizard.h" 
#include "cdrecordwizard.h" 
#include "iorderwizard.h"

#include "stimagetools.h"

#include "publisherdatabase.h"
#include "stcollectionpublishermodel.h" 
#include "smessagebox.h" 

//Commiting order
#include "tpinputdialog.h" 
#include "xmlorder.h"

#include "appsettings.h" 

//Temp SetShippingMethod
#include <QSqlQuery> 
#include <QSqlRecord> 

//Printing
#include <QPrinter>
#include <QProgressBar>
#include <QApplication>
#include "printjobprinter.h"
#include "sprintersettings.h" 
#include "stimage.h" 

//Publisher data sync.
#include "stftpordertransfer.h"
#include "fsqldatabasemanager.h"
#include "remotepublishermanager.h"

//Tiquet
#include "sticketprintersettings.h"
#include "sticketprinter.h" 
#include "ticketdatabase.h"

//Interface
#include "stbusyinfowidget.h"

//Miscelanea
#include "stutils.h"
#include "sttomwatchdog.h"

//Webinfo
#include "owebinfowidget.h"

//App
#include "ttpopsapp.h"

//Order Manager
#ifndef STANDARD_EDITION
#include "stomwatchdog.h"
#endif

void MainWizard::commitOrderOnAccess(const STDom::PrintJob& _Job)
{
	AppSettings Settings;
	if (Settings.commitAccess())
	{
		try
		{
			QString OrderRef = commitOrder(_Job);

			ProgPage->showMessageAndQuit(tr("<h1>Order commited successfull...</h1><br/>"
											"<h2>Order number: %1</h2>"
											"<center><img src=\":/success.png\"></img></center>").arg(OrderRef));
		}
		catch (STError& _Error)
		{
			SMessageBox::criticalDetailed(this, tr("Error commiting order"), _Error.description(), _Error.advancedDesc());
			ProgPage->showMessageAndQuit(tr("<h1>Order commited with errors...</h1><br/>"
											"<center><img src=\":/commitwarning.png\"></img></center>"));
		}
		if (Settings.mainRunTomOnOrderCommit())
		{
#ifndef STANDARD_EDITION
			if (Settings.mainAdvancedOM())
				STOmWatchDog::runOmIfNotAlive();
			else
#endif
			{
				if (!STTomWatchDog::isTomAlive())
					STTomWatchDog::runTom();
			}
		}
	}
}

QString MainWizard::storeOrder(const STDom::PrintJob& _Job, const QString& _SenderComment, bool _PrintTicket)
{
	AppSettings Settings;

	STDom::PrintJobPrinter Printer;
	Printer.setDpis(Settings.mainDpis());
	//TODO: Configure fit method.
	QString OrderRef = QString::number(Settings.mainNewOrderRef());
	//Find a new Order Num
	STDom::XmlOrder XmlOrder(OrderRef);

	//Setting order data
	XmlOrder.setSender(Settings.sender());
	//XmlOrder.setType(KPSqlCommand::TypeDigitalPrint);
	XmlOrder.setSentDateTime(QDateTime::currentDateTime());
	XmlOrder.setCreationDateTime(QDateTime::currentDateTime());
	XmlOrder.setSenderComment(_SenderComment);

	Printer.clearErrorStack();
	Printer.store(_Job, XmlOrder, Settings.imageAdjustmentMode() == AppSettings::ModeAdjustWeight,
				  pManager->publisherDatabaseFilePath(), ProgPage->progressBar());
	Assert(Printer.errorStack().isEmpty(), Error("Error storing order.", Printer.errorStack().errorString().join(",")));
	if (_PrintTicket)
		printTicket(_Job, ShippingMethod, OrderRef, _SenderComment);
	return OrderRef;
}


QString MainWizard::commitOrder(const STDom::PrintJob& _Job)
{
	AppSettings Settings;
	QString Res;

	if (SelectedType !=  ProductTypeSelectionPage::TypeCDRecord )
	{
		QString SenderComment = "";
		if (Settings.askForUserData())
		{
			TPInputDialog InDialog(tr("Please enter name and phone"));
			if(InDialog.exec() == QDialog::Accepted);
				SenderComment = InDialog.strResult();
		}

		if (Settings.sendToPublisherMode())
		{
			nextPage();
			QCoreApplication::processEvents();
			Res = storeOrder(_Job, SenderComment);
		}
		else //Print the order to printer.
		{
			STDom::PrintJobPrinter Printer;
			Printer.setDpis(Settings.mainDpis());
			SPrinterSettings PrnSettings;
			Printer.configure(PrnSettings);
			Printer.setAtomicPrint(SelectedType  == ProductTypeSelectionPage::TypeCard ||
								   SelectedType == ProductTypeSelectionPage::TypeCalendar ||
								   SelectedType == ProductTypeSelectionPage::TypePhotoBook );

			switch (Settings.imageCropMode())
			{
				case AppSettings::ModeCrop:
					Printer.setFitMode(STDom::PrintJobPrinter::FitModeCrop);
				break;
				case AppSettings::ModeWhiteMarginCentered:
					Printer.setFitMode(STDom::PrintJobPrinter::FitModeWhiteMarginCenter);
				break;
				case AppSettings::ModeWhiteMarginRight :
					Printer.setFitMode(STDom::PrintJobPrinter::FitModeWhiteMarginRight);
				break;
			}
			nextPage();
			QString OrderRef = QString::number(Settings.mainNewOrderRef());
			Res = OrderRef;
			Printer.clearErrorStack();
			STDom::PrintJob JobToStore = Printer.print(_Job, "TPOPS-" + OrderRef, ProgPage->progressBar());
			if (!JobToStore.isEmpty())
				storeOrder(JobToStore, SenderComment, false);
			Assert(Printer.errorStack().isEmpty(), Error("Error storing order.", Printer.errorStack().errorString().join(",")));
			printTicket(_Job, ShippingMethod, OrderRef, SenderComment);
		}
	}
	else
	{
		QString OrderRef = QString::number(Settings.mainNewOrderRef());
		printTicket(_Job, ShippingMethod, OrderRef);
	}
	return Res;
}

void MainWizard::syncPublisherData()
{
	AppSettings Settings;

	if (Settings.databaseSyncPublisherData() && Settings.isTimeToSync())
	{
		qApp->setOverrideCursor( QCursor(Qt::WaitCursor));
		try
		{
/*			QFileInfo PublXml = PubInfo.publisherXmlFile();
			//Lets sync products database:
			QString PublisherPath = PublXml.dir().absolutePath();
			STDom::STXmlPublisherSettings PXmlS;
			Assert(PXmlS.loadXml(PublXml.absoluteFilePath()), Error(QString(tr("Could not load settings file: %1")).arg(PublXml.absoluteFilePath())));
			bool KeepPrices = Settings.databaseKeepLocalPrices();

			STDom::PublisherDatabase TmpDatabase(QDir::temp().absoluteFilePath("ttpopssavedprices.db"));
			if (KeepPrices)
			{
				//Save prices and products:
				Assert(TmpDatabase.open(), Error(tr("Unable to open temp database: %1").arg(TmpDatabase.databaseName())));
				//qDebug() << TmpDatabase.databaseName();
				STDom::PublisherDatabase SourceDatabase(PubInfo.publisherDatabaseFile().absoluteFilePath());
				Assert(SourceDatabase.open(), Error(tr("Unable to open publisher database: %1").arg(SourceDatabase.databaseName())));

				FSqlDatabaseManager Manager(SourceDatabase, ":/");
				TmpDatabase.importTable(Manager, "productprices");
				TmpDatabase.importTable(Manager, "products");
				TmpDatabase.importTable(Manager, "formats");
				SourceDatabase.close();
			}

			QDateTime PrevSincModifiedDate = PubInfo.publisherDatabaseFile().lastModified();
			//Sync databases
			if (FtpTrans)
				delete FtpTrans;
			FtpTrans = new STDom::STFtpOrderTransfer(this);
			FtpTrans->syncRemoteDir(PublisherPath, PXmlS.dbHost(), PXmlS.dbPort(), PXmlS.dbUser(), PXmlS.dbPassword(),  PXmlS.dbDir(), static_cast<QFtp::TransferMode>(PXmlS.dbTransferMode()));

			//Restore saved prices
			if (KeepPrices && (PrevSincModifiedDate != PubInfo.publisherDatabaseFile().lastModified()))
			{
				//qDebug("Keeping prices.");
				STDom::PublisherDatabase PubDatabase(PubInfo.publisherDatabaseFile().absoluteFilePath());
				Assert(PubDatabase.open(), Error(tr("Unable to open publisher database: %1").arg(PubDatabase.databaseName())));
				FSqlDatabaseManager TmpDBManager(TmpDatabase, ":/");
				PubDatabase.importTable(TmpDBManager, "productprices");
				PubDatabase.importLocalFormats(TmpDBManager.database());
				PubDatabase.importLocalProducts(TmpDBManager.database());
				PubDatabase.close();
				TmpDatabase.close();
			}
*/
			pManager->pullProducts(true, Settings.databaseKeepLocalPrices());
			qApp->restoreOverrideCursor();
			Settings.updateSyncTime();
		}
		catch (...)
		{
			qApp->restoreOverrideCursor();
			throw;
		}
	}
}

SPhotoBook::MetaInfo::EnTemplateType MainWizard::selectedTypeToTemplateType(ProductTypeSelectionPage::EnProductType _Type)
{
	SPhotoBook::MetaInfo::EnTemplateType Res;
	switch(_Type)
	{
		case ProductTypeSelectionPage::TypePhotoId :
			Res = SPhotoBook::MetaInfo::TypeIdPhoto;
		break;
		case ProductTypeSelectionPage::TypePhotoBook :
			Res = SPhotoBook::MetaInfo::TypePhotoBook;
		break;
		case ProductTypeSelectionPage::TypeCalendar :
			Res = SPhotoBook::MetaInfo::TypeCalendar;
		break;
		case ProductTypeSelectionPage::TypeCard :
			Res = SPhotoBook::MetaInfo::TypeCard;
		break;
		case ProductTypeSelectionPage::TypeOther :
			Res = SPhotoBook::MetaInfo::TypeMultiPhoto;
		break;
	}
	return Res;
}

MainWizard::MainWizard(QWidget* _Parent): STIWizard(_Parent), ProdModel(0), FtpTrans(0)
{
	PTypePage = new ProductTypeSelectionPage(this);
	connect(PTypePage, SIGNAL(settingsSelected()), this, SLOT(settingsSelected()));
	connect(PTypePage, SIGNAL(typeSelected(ProductTypeSelectionPage::EnProductType)),
			this, SLOT(slotTypeSelected(ProductTypeSelectionPage::EnProductType)));
	addPage(PTypePage);

	AppSettings Settings;
	if (Settings.mainShowLegalInfo())
	{
		LegalProtPage* LProtPage = new LegalProtPage(this);
		addPage(LProtPage);
	}

	MediaPage = new GetMediaPage(this);
	connect(MediaPage, SIGNAL(mediaSelected(const QDir&)), this, SLOT(mediaSelected(const QDir&)));
	//connect(FirstPage, SIGNAL(directDigitalPrints(const QString&)), this, SLOT(directDigitalPrints(const QString&)));
	addPage(MediaPage);

	DPWizard = new DigiprintWizard(this);
	connect(DPWizard, SIGNAL(commitOrder()), this, SLOT(slotCommitOrder()));
	connect(DPWizard, SIGNAL(finished()), this, SLOT(firstPage()));
	addWizard(DPWizard);

	PBWizard = new PhotoBookWizard(this);
	connect(PBWizard, SIGNAL(commitOrder()), this, SLOT(slotCommitOrder()));
	addWizard(PBWizard);

	PIWizard = new PhotoIndexWizard(this);
	connect(PIWizard, SIGNAL(commitOrder()), this, SLOT(slotCommitOrder()));
	connect(PIWizard, SIGNAL(finished()), this, SLOT(firstPage()));
	addWizard(PIWizard);

	CDRWizard = new CDRecordWizard(this);
	connect(CDRWizard, SIGNAL(commitOrder()), this, SLOT(slotCommitOrder()));
	connect(CDRWizard, SIGNAL(finished()), this, SLOT(firstPage()));
	addWizard(CDRWizard);

	ProgPage = new TPSendProgressPage(this); 
	connect(ProgPage, SIGNAL(finish()), this, SLOT(firstPage()));
	addPage(ProgPage);


}

MainWizard::~MainWizard()
{
}

void MainWizard::init()
{
	AppSettings Settings; 
	if (!Settings.validSettings())
		settingsSelected(); 
	show();
	firstPage();
	showFullScreen(); 

	if (!Settings.templatesManuallyInstall())
	{
		try //Try to update templates from publisher.
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
			pManager->getTemplates();
			PTypePage->reloadMainOptions();
			QApplication::restoreOverrideCursor();
		}
		catch (STError& _Error)
		{
			QApplication::restoreOverrideCursor();
			SMessageBox::warning(this, tr("Error in sync process"), _Error.description());
		}
	}

	//Order Management
	if (Settings.mainAdvancedOM())
	{
		try
		{
			syncPublisherData();
		}
		catch (STError& _Error)
		{
			SMessageBox::warning(this, tr("Error in sync process"), _Error.description());
		}
#ifndef STANDARD_EDITION
		STOmWatchDog::runOmIfNotAlive();
#endif
	}
}

void MainWizard::initPublisher()
{
	try
	{
		try
		{
			syncPublisherData();
		}
		catch (STError& _Error)
		{
			SMessageBox::warning(this, tr("Error in sync process"), _Error.description());
		}
	}
	catch (STError& _Error)
	{
		SMessageBox::critical(this, tr("Error initialising publisher"), _Error.description());
	}
}


void MainWizard::slotTypeSelected(ProductTypeSelectionPage::EnProductType _Type)
{
	SelectedType = _Type;
	nextPage();
}


void MainWizard::mediaSelected(const QDir& _Dir)
{
	try
	{
		initPublisher();
		QFileInfoList Images;
		int NumMediaAccessTries = 0;
		bool FilesFound = false;
		STBusyInfoWidget::show(tr("Looking for supported files, please wait..."));
		while (NumMediaAccessTries  < 5 && !FilesFound)
		{
			QApplication::processEvents();
			Images = STImageTools::recursiveImagesEntryInfoList(_Dir, true);
			FilesFound = !Images.isEmpty();
			if (!FilesFound)
				STUtils::sleep(2000);
			NumMediaAccessTries++;
		}
		QDir RootDir = _Dir;
#ifndef Q_OS_WIN32
		QString CommonDir = STImageTools::commonDir(Images);
		if (!CommonDir.isEmpty())
			RootDir = QDir(CommonDir);
#endif


		STBusyInfoWidget::hide();
		if (FilesFound)
		{
			switch (SelectedType)
			{
				case ProductTypeSelectionPage::TypeDigitalCopies :
					DPWizard->setImages(Images, RootDir);
					showWizard(DPWizard);
				break;
				case ProductTypeSelectionPage::TypePhotoIndex:
					PIWizard->setImages(Images, RootDir);
					showWizard(PIWizard);
				break;
				case ProductTypeSelectionPage::TypePhotoId :
				case ProductTypeSelectionPage::TypeCalendar :
				case ProductTypeSelectionPage::TypeCard :
				case ProductTypeSelectionPage::TypePhotoBook :
				case ProductTypeSelectionPage::TypeOther :
					PBWizard->setImages(Images, RootDir);
					PBWizard->setTemplateType(selectedTypeToTemplateType(SelectedType));
					showWizard(PBWizard);
				break;
				case ProductTypeSelectionPage::TypeCDRecord :
					CDRWizard->setImages(Images, RootDir);
					showWizard(CDRWizard);
				break;
			}
		}
		else
			SMessageBox::warning(this, tr("Warning"), tr("No images found in media"));

	}
	catch (STError& _Error)
	{
		SMessageBox::critical(this, tr("Error getting publisher data"), _Error.description());
	}
}


void MainWizard::settingsSelected()
{
	//initPublisher();
	AppSettings Settings;
	if (Settings.settingsAccess())
	{
		Settings.execDialog();
	}
}


void MainWizard::slotCommitOrder()
{
	if (IOrderWizard* LastWizard = dynamic_cast<IOrderWizard*>(sender()))
	{
		commitOrderOnAccess(LastWizard->getPrintJob());
	}
}

void MainWizard::printTicket(const STDom::PrintJob& _Job, const STDom::PublisherBill::PublisherShippingMethod & _ShippingMethod,
							 const QString& _OrderRef, const QString& _Name)
{
	AppSettings Settings;
	STDom::PublisherDatabase PublDB = pManager->getProducts();

	try
	{
		if (Settings.ticketPrintTicket())
		{
			Assert(PublDB.open(), Error(tr("Could not open publisher database.")));
			STDom::TicketDatabase TicketDB = STDom::TicketDatabase::addDatabase();
			Assert(TicketDB.open(), Error(tr("Could not open temp ticket database.")));
			TicketDB.createTables();
			TicketDB.setOrderId(_OrderRef); 
			TicketDB.setTicketHeader(Settings.ticketHeader()); 
			TicketDB.setTicketFooter(Settings.ticketFooter());
			TicketDB.setCompanyName(Settings.ticketCompanyName());
			TicketDB.setCompanyAddress(Settings.ticketCompanyAddress());
		
			STicketPrinterSettings TPSettings; 
			STicketPrinter TicketPrinter;
			TicketPrinter.configure(TPSettings);
			
			STDom::PublisherBill Bill;
			if (SelectedType == ProductTypeSelectionPage::TypePhotoIndex )
				Bill = PublDB.calcBill(_Job, _ShippingMethod, Settings.photoIndexImagesPerPage());
			else 
				Bill = PublDB.calcBill(_Job, _ShippingMethod);
		
			Bill.setCustomerName(_Name);
			TicketDB.prepareForPrint(TicketPrinter,Bill);
			TicketPrinter.print(); 
			if (Settings.ticketPrintTicketCopy())
				TicketPrinter.print(); 
			PublDB.close();
			//TicketDB.removeDatabase(); 
		}
	}
	catch (STError& _Error)
	{
		SMessageBox::warning(this, tr("Error printing ticket."), _Error.description());
	}
}
