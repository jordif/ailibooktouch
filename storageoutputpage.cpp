/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "storageoutputpage.h"
#include <QLayout> 
#include <QLabel> 
#include <QToolButton> 
#include <QListView>  

#include "publisherdatabase.h"
#include "tpproductlistview.h" 
#include "addremovewidget.h" 

void StorageOutputPage::retranslateUi()
{
	HeaderLabel->setText(tr("<h1>This is your Order Summary.</h1>")); 
	ButPrevious->setText(tr("Back")); 
	ButStore->setText(tr("Store"));
	ButCancel->setText(tr("Cancel")); 
}

QToolButton* StorageOutputPage::newActionButton(const QIcon& _Icon)
{
	QToolButton* ButRes = new QToolButton(this); 
	ButRes->setIcon(_Icon);
	ButRes->setIconSize(QSize(128, 128));
	ButRes->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
	ButRes->setObjectName("ActionButton"); 
	return ButRes; 
}

StorageOutputPage::StorageOutputPage(QWidget* _Parent): STIWizardPage(_Parent), NCopies(0)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	HeaderLabel = new QLabel(this); 
	HeaderLabel->setObjectName("SummaryHeaderLabel");
	HeaderLabel->setAlignment(Qt::AlignHCenter);
	MLayout->addWidget(HeaderLabel);

	SummaryLabel = new QLabel(this);
	SummaryLabel->setObjectName("SummaryLabel");
	SummaryLabel->setAlignment(Qt::AlignTop | Qt::AlignHCenter); 
	SummaryLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::MinimumExpanding);
	MLayout->addWidget(SummaryLabel);
	MLayout->setAlignment(SummaryLabel, Qt::AlignCenter);

	QHBoxLayout* PWidgetLayout = new QHBoxLayout; 
	MLayout->addLayout(PWidgetLayout); 
	MLayout->setAlignment(PWidgetLayout, Qt::AlignCenter);

	LVProducts = new TPProductListView(this); 
	LVProducts->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	PWidgetLayout->addWidget(LVProducts);

	AddRemoveWidget* ARWidget = new AddRemoveWidget(this); 
	connect(ARWidget, SIGNAL(addClicked()), this, SLOT(editAddProductToAll())); 
	connect(ARWidget, SIGNAL(removeClicked()), this, SLOT(editDelProductToAll())); 
	PWidgetLayout->addWidget(ARWidget); 

	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));
	
	QGridLayout* ButLayout = new QGridLayout;
	MLayout->addLayout(ButLayout); 

	ButCancel = newActionButton(QIcon(":/st/tpopsl/cancel.png"));
	connect(ButCancel, SIGNAL(clicked( bool )), this, SIGNAL(orderCanceled())); 
	ButLayout->addWidget(ButCancel, 0, 0); 

	ButPrevious = newActionButton(QIcon(":/st/tpopsl/previous.png"));
	connect(ButPrevious, SIGNAL(clicked( bool )), this, SIGNAL(previousPage())); 
	ButLayout->addWidget(ButPrevious, 1, 0); 
	
	ButLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred), 0, 1); 

	ButStore = newActionButton(QIcon(":/usbpendrive.png"));
	connect(ButStore, SIGNAL(clicked( bool )), this, SLOT(slotStore())); 
	ButLayout->addWidget(ButStore, 1, 2); 
}

void StorageOutputPage::setStoreIcon(const QIcon& _Icon)
{
	ButStore->setIcon(_Icon); 
}

void StorageOutputPage::init()
{
	retranslateUi(); 
}

void StorageOutputPage::setFiles(const QFileInfoList& _Files)
{
	MPrintJob.clear();
	QFileInfoList::const_iterator it;
	for (it = _Files.begin(); it != _Files.end(); ++it)
	{
		MPrintJob.addCopies(LVProducts->currentProduct(), *it, 1);
	}
	NCopies = 1;
	calcBill();
}

void StorageOutputPage::setProductsModel(QAbstractItemModel* _Model)
{
	LVProducts->setModel(_Model);
}

void StorageOutputPage::calcBill()
{
	STDom::PublisherDatabase PublDB;
	QString StrBill = PublDB.billRitchText(MPrintJob, ShippingMethod);
	SummaryLabel->setText(StrBill);
}

void StorageOutputPage::editAddProductToAll()
{
	MPrintJob.addCopiesAll(LVProducts->currentProduct(), 1);
	NCopies++;
	calcBill();
}

void StorageOutputPage::editDelProductToAll()
{
	if (NCopies > 1)
	{
		MPrintJob.addCopiesAll(LVProducts->currentProduct(), -1);
		NCopies--;
		calcBill();
	}
}

void StorageOutputPage::slotStore()
{
	emit store(NCopies); 
}
