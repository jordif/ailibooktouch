/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "pageslistview.h"
#include <QAction>
#include <QToolBar>

#include "pagethumbnailmodel.h"
#include "stthumbnailview.h"
#include "smessagebox.h"
#include "documentviewwidget.h"
#include "document.h"
#include "templatescene.h"

void PagesListView::checkStatus()
{
	RemovePageAction->setEnabled(PBDocument->pages().size() > 1);
	MovePageLeftAction->setEnabled(PBDocument->pages().size() > 1);
	MovePageRightAction->setEnabled(PBDocument->pages().size() > 1);
}

void PagesListView::modifyPhotoBookAction(QAction* _Sender)
{
	if (_Sender == InsertPageAction || _Sender == RemovePageAction ||
		_Sender == MovePageLeftAction || _Sender == MovePageRightAction) //Defensive
	{
		int CurrentIndex = 0;
		if (_Sender == InsertPageAction)
		{
			if (PBDocument->pages().size() < PBDocument->metaInfo().maxPages())
			{
				CurrentIndex = PhotoBookView->currentPageIndex() + 1;
                QString ReasonNot;
                if (!PBDocument->insertRandomPage(CurrentIndex, ReasonNot))
                    SMessageBox::warning(this, tr("Adding Pages"), QString(tr("You could not add page because %1")).arg(ReasonNot));

			}
			else
			{
				SMessageBox::warning(this, tr("Adding Pages"), QString(tr("This PBDocument could not contain more than %1 pages")).arg(PBDocument->metaInfo().maxPages()));
				return;
			}
		}
		else
		if (_Sender == RemovePageAction)
		{
			if (PBDocument->pages().size() -1 >= PBDocument->metaInfo().minPages())
			{
				if (SMessageBox::question(this, tr("Remove current page"), tr("Are you sure to delete the current page ?")) == QMessageBox::Yes)
				{
					int MIndex = PhotoBookView->currentPageIndex();
					if (MIndex != 0)
					{
						PBDocument->removePage(MIndex);
						CurrentIndex = qMax(MIndex - 1, 0);
						emit currentPageRemoved();
						// UndoStack->clear(); !!!!@Review Mark !!!
					}
					else
						SMessageBox::information(this, tr("Removing Pages"), tr("Sorry but you can't delete the first page."));
				}
			}
			else
			{
				SMessageBox::warning(this, tr("Removing Pages"), QString(tr("This PBDocument need at least %1 pages")).arg(PBDocument->metaInfo().minPages()));
				return;
			}
		}
		else
		if (_Sender == MovePageRightAction)
		{
			int NewPageIndex = qMin(PhotoBookView->currentPageIndex() +1, PBDocument->pages().size() -1);
			PBDocument->movePage(PhotoBookView->currentPageIndex(), NewPageIndex);
			CurrentIndex = NewPageIndex;
		}
		else
		if (_Sender == MovePageLeftAction)
		{
			int NewPageIndex = qMax(PhotoBookView->currentPageIndex() -1, 0);
			PBDocument->movePage(PhotoBookView->currentPageIndex(), NewPageIndex);
			CurrentIndex = NewPageIndex;
		}

		Model->setPages(PBDocument->pages());
		PhotoBookView->reloadPages();
		PhotoBookView->setCurrentPage(CurrentIndex);
		checkStatus();
	}
}

PagesListView::PagesListView(SPhotoBook::DocumentViewWidget* _PhotoBookView, QWidget* _Parent) :
		SPToolbarListView(Qt::Vertical, _Parent), PhotoBookView(_PhotoBookView), Model(0), PBDocument(0)
{
	toolBar()->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	InsertPageAction = new QAction(QIcon(":/list-add.png"), tr("Insert Page"), this);
	connect(InsertPageAction, SIGNAL(triggered()), this, SLOT(slotModifyPhotoBookAction()));
	toolBar()->addAction(InsertPageAction);

	RemovePageAction = new QAction(QIcon(":/list-remove.png"), tr("Remove Page"), this);
	connect(RemovePageAction, SIGNAL(triggered()), this, SLOT(slotModifyPhotoBookAction()));
	toolBar()->addAction(RemovePageAction);

	MovePageLeftAction = new QAction(QIcon(":/go-left.png"), tr("Move Page left"), this);
	connect(MovePageLeftAction, SIGNAL(triggered()), this, SLOT(slotModifyPhotoBookAction()));
	toolBar()->addAction(MovePageLeftAction);

	MovePageRightAction = new QAction(QIcon(":/go-right.png"), tr("Move Page right"), this);
	connect(MovePageRightAction, SIGNAL(triggered()), this, SLOT(slotModifyPhotoBookAction()));
	toolBar()->addAction(MovePageRightAction);

	listView()->setViewMode(QListView::IconMode);
	listView()->setWrapping(true);
	listView()->setSelectionMode(QAbstractItemView::NoSelection);


	//Data
	Model = new SPhotoBook::PageThumbnailModel(this);
	setModel(Model);
	connect(listView(), SIGNAL(clicked( const QModelIndex& )), this, SLOT(slotPageIndexClicked(const QModelIndex&)));
	connect(PhotoBookView, SIGNAL(pageChanged(int,int)), this, SLOT(slotPageChange(int, int)));
}


void PagesListView::setDocument(SPhotoBook::Document* _PBDocument)
{
	PBDocument = _PBDocument;
	Model->setPages(_PBDocument->pages());
}

void PagesListView::addPage()
{
	modifyPhotoBookAction(InsertPageAction);
}

QSize PagesListView::sizeHint () const
{
	return QSize(200, size().height());
}


void PagesListView::slotPageIndexClicked(const QModelIndex& _Index)
{
	PhotoBookView->showPage(_Index.row());
}


void PagesListView::slotModifyPhotoBookAction()
{
	if (QAction* Sender = qobject_cast<QAction*>(sender()))
		modifyPhotoBookAction(Sender);
}


void PagesListView::slotPageChange(int _FromPage, int _ToPage)
{
	if (PBDocument) //Defensive
		Model->updateThumbnail(PBDocument->pages(), _FromPage);
	listView()->setCurrentIndex(Model->index(_ToPage, 0));
}

