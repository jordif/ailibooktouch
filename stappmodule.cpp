/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "stappmodule.h"
#include <QFile> 
#include <QDir> 
#include <QTextStream>
#include <QObject> 
#include <QTextCodec> 
#include <QApplication> 
#include "sapplication.h" 
#include "metainfo.h"
#include "systemtemplates.h"
// _________________________________________________________________________*/
//
//   Class STAppModule
// _________________________________________________________________________*/

void STAppModule::clear()
{
	Type = TypeUnknown; 
	Name.clear(); 
	Description.clear();
	Icon = QIcon(); 
	Price = 0;
	LoadTemplateFromDisk = false;
}


STAppModule::STAppModule()
{
	clear(); 
}

STAppModule::STAppModule(STAppModule::TModuleType _Type, STAppModule::TModuleGroup _Group, STAppModule::TModuleSubGroup _SubGroup)
{
	clear();
	Type = _Type;
	Group = _Group;
	SubGroup = _SubGroup;
}

void STAppModule::setTemplateInfo(const SPhotoBook::TemplateInfo& _TemplateInfo)
{
	setIcon(SPhotoBook::MetaInfo::typePixmap(_TemplateInfo.type()));
	setName(_TemplateInfo.name());
	TemplateInfo = _TemplateInfo;
}


bool STAppModule::hasSubGroups(STAppModule::TModuleGroup _Group)
{
	return _Group == GroupPhotoAssembly;
}

bool STAppModule::operator<(const STAppModule& _Other) const
{
	return type() < _Other.type();
}

QString STAppModule::typeName(STAppModule::EnModuleType _Type)
{
	QString Res = tr("Unknown"); 
	switch (_Type)
	{
		case TypeUnknown :
			Res = tr("Unknown"); 
		break; 
		case TypeDigiprints :
			Res = tr("Digital Prints"); 
		break; 
		case TypeDecorations :
			Res = tr("Decorations"); 
		break; 
		case TypePhotoBooks :
			Res = tr("PhotoBooks"); 
		break; 
		case TypeCDRecord :
			Res = tr("CD Record"); 
		break; 
		case TypeVideo :
			Res = tr("Video"); 
		break; 
		case TypeGifts :
			Res = tr("Gifts"); 
		break; 
		case TypeIndex :
			Res = tr("Photo Index"); 
		break; 
		case TypeBatchTemplate :
			Res = tr("Bash Templates"); 
		break; 
		case TypePDFDoc :
			Res = tr("PDF Documents"); 
		break; 
	};
	return Res; 
}

QString STAppModule::groupName(STAppModule::EnModuleGroup _Group)
{
	QString Res; 
	switch (_Group) 
	{
		case GroupDigitalPhoto :
			Res = tr("Digital Photo"); 
		break; 
		case GroupPhotoAssembly : 
			Res = tr("Photo Assembly"); 
		break; 
		case GroupPhotoBook : 
			Res = tr("PhotoBooks"); 
		break; 
		case GroupStorage : 
			Res = tr("Digital Storage");
		break; 
		case GroupDigitalDocs : 
			Res = tr("Digital Documents"); 
		break; 
		case GroupPhotoGift : 
			Res = tr("Gifts"); 
		break; 
	};
	return Res; 
}

QString STAppModule::groupDescription(STAppModule::EnModuleGroup _Group)
{
	QString Res; 
	switch (_Group) 
	{
		case GroupDigitalPhoto :
			Res = tr("Operations with digital pictures."); 
		break; 
		case GroupPhotoAssembly : 
			Res = tr("Assemblies and decorations for digital pictures."); 
		break; 
		case GroupPhotoBook : 
			Res = tr("Multipage assemblies for digital pictures"); 
		break; 
		case GroupStorage : 
			Res = tr("CD/DVD Burning, Memory and pendrive storage");
		break; 
		case GroupDigitalDocs : 
			Res = tr("Operations with digital documents."); 
		break; 
		case GroupPhotoGift : 
			Res = tr("Gifts built with your digital pictures"); 
		break; 
	};
	return Res; 
}

QString STAppModule::subGroupName(STAppModule::EnModuleSubGroup _SubGroup)
{
	QString Res;
	switch (_SubGroup)
	{
		case SubGroupCalendars :
			Res = tr("Calendars");
		break;
		case SubGroupPhotoId :
			Res = tr("PhotoId");
		break;
		case SubGroupCards :
			Res = tr("Cards");
		break;
		case SubGroupBatchTemplates :
			Res = tr("Batch Templates");
		break;
		case SubGroupNoSubGroup :
			Res = tr("None");
		break;
	};
	return Res;
}

QString STAppModule::subGroupDescription(STAppModule::EnModuleSubGroup _SubGroup)
{
	QString Res;
	switch (_SubGroup)
	{
		case SubGroupCalendars :
			Res = tr("Calendar assemblies for digital pictures");
		break;
		case SubGroupPhotoId :
			Res = tr("Photo id assemblies");
		break;
		case SubGroupCards :
			Res = tr("Card assemblies for digital pictures");
		break;
		case SubGroupBatchTemplates :
			Res = tr("Many photos in a single template");
		break;
		case SubGroupNoSubGroup :
			Res = tr("None");
		break;
	};
	return Res;
}


// _________________________________________________________________________*/
//
//   Class STAppModuleList
// _________________________________________________________________________*/

void STAppModuleList::addTemplates(STAppModule::EnModuleType _Type, STAppModule::EnModuleGroup _Group, STAppModule::EnModuleSubGroup _SubGroup, const SPhotoBook::TemplateInfoList& _Templates)
{
	SPhotoBook::TemplateInfoList::const_iterator it;
	for (it = _Templates.begin(); it != _Templates.end(); ++it)
	{
		STAppModule NewModule(_Type, _Group, _SubGroup);
		NewModule.setTemplateInfo(*it);
		push_back(NewModule);
	}
}

bool STAppModuleList::loadFromTemplates()
{
	SPhotoBook::TemplateInfoList Templates = SPhotoBook::SystemTemplates::load();

	addTemplates(STAppModule::TypeCalendars, STAppModule::GroupPhotoAssembly, STAppModule::SubGroupCalendars,
				 Templates.subList(SPhotoBook::MetaInfo::TypeCalendar));
	addTemplates(STAppModule::TypeDecorations, STAppModule::GroupPhotoAssembly, STAppModule::SubGroupCards,
				 Templates.subList(SPhotoBook::MetaInfo::TypeCard));
	addTemplates(STAppModule::TypePhotoId, STAppModule::GroupPhotoAssembly, STAppModule::SubGroupPhotoId,
				 Templates.subList(SPhotoBook::MetaInfo::TypeIdPhoto));
	addTemplates(STAppModule::TypeBatchTemplate, STAppModule::GroupPhotoAssembly, STAppModule::SubGroupBatchTemplates,
				 Templates.subList(SPhotoBook::MetaInfo::TypeMultiPhoto));
	addTemplates(STAppModule::TypePhotoBooks, STAppModule::GroupPhotoBook, STAppModule::SubGroupNoSubGroup,
				 Templates.subList(SPhotoBook::MetaInfo::TypePhotoBook));
}
