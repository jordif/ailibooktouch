/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "cdrecordpage.h"
#include <QLayout> 
#include <QLabel> 
#include <QProgressBar>
#include <QCoreApplication>
#include <QToolButton> 
#include <QDir> 
#include <QTimer> 
#include <QApplication>

#include "sprocessstatuswidget.h" 
#include "smessagebox.h" 

#include "stmkisofs.h" 
#include "stcdrecord.h" 
#include "stutils.h" 
#include "stburningsettings.h" 
#include "appsettings.h"

#include <math.h> //pow()

#define CD_SIZE 700

QString CDRecordPage::volumeDirName(int _NumVolume) const
{
	return QString("vol_%1").arg(_NumVolume, 3, 10, QChar('0'));
}

int CDRecordPage::storeImagesOnDisk(const QFileInfoList& _Files, const QDir& _DestDir, int _VolumeSize, QProgressBar* _ProgBar)
{
	if (!_DestDir.exists())
		_DestDir.mkpath(_DestDir.absolutePath());

	if (_ProgBar)
	{
		_ProgBar->setRange(0, _Files.size());
		_ProgBar->setValue(0);
		QApplication::processEvents();
	}

	int CurrVolume = 1;
	qint64 CurrVolumeSize = 0;
	QDir CurrVolumeDir =  _DestDir.absoluteFilePath(volumeDirName(CurrVolume));
	Assert(CurrVolumeDir.mkpath(CurrVolumeDir.absolutePath()),
		Error(QString(QObject::tr("Could not create directory: %1")).arg(CurrVolumeDir.absolutePath())));


	for (int Vfor = 0; Vfor < _Files.size(); Vfor++)
	{
		QFileInfo Img = _Files[Vfor];
		CurrVolumeSize+= QFile(Img.absoluteFilePath()).size();
		if (CurrVolumeSize > (_VolumeSize* 1024 * 1024)) //We create a new volume dir.
		{
			CurrVolume ++;
			CurrVolumeSize = QFile(Img.absoluteFilePath()).size();;
			CurrVolumeDir =  _DestDir.absoluteFilePath(volumeDirName(CurrVolume));
			Assert(CurrVolumeDir.mkpath(CurrVolumeDir.absolutePath()),
				Error(QString(QObject::tr("Could not create directory: %1")).arg(CurrVolumeDir.absolutePath())));
		}

		QString CurrFileName = Img.fileName();
		//Si el fitxer ja existeix hi afegim un prefix.
		if (CurrVolumeDir.exists(CurrFileName) )
		{
			QStringList StrlFiles = CurrVolumeDir.entryList(QStringList("*" + CurrFileName));
			CurrFileName = QString::number(StrlFiles.size()) + "_" + CurrFileName;
		}

		QString CurrAbsFilePath = CurrVolumeDir.absolutePath() + "/" + CurrFileName;
		//TODO: S'ha de informar per pantalla i dir si es vol reintentar !!!.
		StackAssert(QFile::copy(Img.absoluteFilePath(), CurrAbsFilePath), Error(tr("Error saving file %1").arg(CurrAbsFilePath)), ErrorStack);

		if (_ProgBar)
		{
			_ProgBar->setValue(_ProgBar->value() + 1);
			QCoreApplication::processEvents();
		}
	}
	return CurrVolume;
}

QToolButton* CDRecordPage::newActionButton(const QIcon& _Icon)
{
	QToolButton* ButRes = new QToolButton(this); 
	ButRes->setIcon(_Icon);
	ButRes->setIconSize(QSize(128, 128));
	ButRes->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
	ButRes->setObjectName("ActionButton"); 
	return ButRes; 
}


void CDRecordPage::retranslateUi()
{
	BackButton->setText(tr("Cancel Burn process"));
	OKButton->setText(tr("Ok")); 
}

CDRecordPage::CDRecordPage(QWidget* _Parent): STIWizardPage(_Parent), BurnOk(false)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this);

	MainLabel = new QLabel(QString("<h4>") + tr("CD Burning...") + "</h4>" + tr("Getting information to start the CD burning."), this);
	MainLabel->setTextFormat(Qt::RichText);
	MainLabel->setWordWrap(true);
 	MLayout->addWidget(MainLabel);
	//MainLabel->setFixedWidth(500);

	PrepCommandStatus = new SProcessStatusWidget(this);
	MLayout->addWidget(PrepCommandStatus);

	MKisoFsStatus = new SProcessStatusWidget(this);
	MLayout->addWidget(MKisoFsStatus);

	CdRecordStatus = new SProcessStatusWidget(this);
	MLayout->addWidget(CdRecordStatus);
	
	PIsoFs = new STMkIsoFs(this);
	//connect(PIsoFs, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(mkisofsFinished(int, QProcess::ExitStatus)));
	connect(PIsoFs, SIGNAL(statusChanged(const QString&, int )), MKisoFsStatus, SLOT(statusChanged(const QString&, int )));

	PCdRecord = new STCdRecord(this);
	//connect(PCdRecord, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(cdrecordFinished(int, QProcess::ExitStatus)));
	connect(PCdRecord, SIGNAL(statusChanged(const QString&, int )), CdRecordStatus, SLOT(statusChanged(const QString&, int )));

	MLayout->addItem(new QSpacerItem(0,100, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));	


	QHBoxLayout* BottomLayout = new QHBoxLayout; 
	MLayout->addLayout(BottomLayout); 

	BackButton = newActionButton(QIcon(":/st/tpopsl/cancel.png"));
	connect (BackButton, SIGNAL(clicked()), this, SLOT(slotBack()));
	BottomLayout->addWidget(BackButton);

	BottomLayout->addItem(new QSpacerItem(100,0, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred));	

	OKButton = newActionButton(QIcon(":/st/tpopsl/accept.png"));
	connect (OKButton, SIGNAL(clicked()), this, SLOT(slotBurnOK()));
	BottomLayout->addWidget(OKButton);
	OKButton->setVisible(false); 


// 	QLabel* BannerLabel = new QLabel(this);
// 	BannerLabel->setPixmap(kpsTheme.pixmap(":/cdburningbanner.png"));
// 	MLayout->addWidget(BannerLabel, MLayout->rowCount() -1, 0, 2, 2);
	
	//MLayout->addItem(new QSpacerItem(100,180, QSizePolicy::Preferred, QSizePolicy::Fixed), MLayout->rowCount(), 0);	
	//MLayout->addItem(new QSpacerItem(100,100, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred), 1, 0);	
}

void CDRecordPage::setState(EnState _State)
{
	QColor LabTextColor = palette().color(QPalette::WindowText);
	BackButton->setVisible(_State != StateBurnedOk);
	OKButton->setVisible(_State == StateBurnedOk); 
	CdRecordStatus->setVisible(_State == StateBurning);
	switch(_State)
	{
		case StateInit:
			PrepCommandStatus->show();
			MKisoFsStatus->hide();
			MainLabel->setText(QString("<h4>") +  tr("Getting ready to burn the CD...") + "</h4>" + 
				tr("Compiling the selected images."));
		break;
		case StateMakeIso:
			MKisoFsStatus->show();
			MainLabel->setText(QString("<h4>") +  tr("Getting ready to burn the CD...") + "</h4>" + 
				tr("Creating an image of the CD."));
		break;
		case StateBurning:
			MainLabel->setText(QString("<h4>") +  tr("CD Burning...") + "</h4>" + 
				tr("CD Burning. Wait a moment please."));
		break;
		case StateBurnedOk:
			MKisoFsStatus->hide();
			PrepCommandStatus->hide();
			LabTextColor = Qt::green;
			MainLabel->setText(QString("<h1>") +  tr("The CD is already burned!.<br/>Click on the screen to finish.") + "</h1>" + 
				tr("<b>Thanks for your attention.</b>"));

			QTimer::singleShot(10000, this, SLOT(slotBurnOK()));
		break;
// 		case BurnScheduled:
// 			PrepCommandStatus->hide();
// 			LabTextColor = Qt::green;
// 			MainLabel->setText(QString("<h4>") +  tr("Your CD/DVD order has been succesfully sent!.<br/>Click on the screen to finish.") + "</h4>" + 
// 				tr("Please, dont forget to make the payment on the counter to validate your order.<br/><b>Thanks for your attention.</b>"));
// 		break;
		case StateError:
			LabTextColor = Qt::red;
		break;
	}
	QPalette LabPal = MainLabel->palette(); 
	LabPal.setColor(QPalette::Text, LabTextColor);
	MainLabel->setPalette(LabPal);
}

void CDRecordPage::setError(const QString& _Error)
{
	setState(StateError);
	MainLabel->setText(QString("<h4>") +  tr("Oh oh... I couldn't burn the CD!") + "</h4>" + 
		tr("There has been a problem during the burning process: ") + _Error);
}

//! Finish and kills running processes.
void CDRecordPage::finish()
{
	if (PIsoFs->state() != QProcess::NotRunning)
		PIsoFs->kill();
	if (PCdRecord->state() != QProcess::NotRunning)
		PCdRecord->kill();
}

void CDRecordPage::init()
{
	retranslateUi();
	STBurningSettings Settings; 
	PIsoFs->configure(Settings); 
	PCdRecord->configure(Settings); 
}


void CDRecordPage::burn(const QFileInfoList& _Files, int _NCopies)
{
	try
	{
		BurnOk = false;
		setState(StateInit);
		//Preparem la comanda. 
		PrepCommandStatus->showProgressBar(tr("Preparing the images of the order..."), 0);
		
		QDir TmpDir(QDir::tempPath() + "/ttpopscdrecordtemp"); 
		Assert(STUtils::rmDir(TmpDir.absolutePath()),
			Error(QString(tr("Could not delete temp directory %1")).arg(TmpDir.absolutePath())));
		Assert(TmpDir.mkpath(TmpDir.absolutePath()), 
			Error(QString(tr("Temp directory %1 could not be created")).arg(TmpDir.absolutePath())));

		ErrorStack.clear();
		int NumVolumes = storeImagesOnDisk(_Files, TmpDir, CD_SIZE, PrepCommandStatus->progressBar());
		Assert(ErrorStack.isEmpty(), Error(ErrorStack.errorString().join(" \n")));
		 
		PrepCommandStatus->updateProgress(PrepCommandStatus->progressBar()->value(), tr("The images from the order are ready."));
		Cancelled = false;
		int BurnedVolumes = 0;
		while  (!Cancelled && BurnedVolumes < NumVolumes)
		{
			setState(StateMakeIso);
			MKisoFsStatus->showProgressBar(tr("Preparing CD image."), 100);
			getCD(BurnedVolumes + 1, NumVolumes);
			QCoreApplication::processEvents();
			QString MSInfo = "";
			STBurningSettings Settings;
			if (Settings.multiSession())
				MSInfo = PCdRecord->getMsInfo();
			
			//PIsoFs->start(CurrCommand.commandCDVolumeDir(BurnedVolumes + 1).absolutePath(), MSInfo);
			PIsoFs->start(TmpDir.absoluteFilePath(volumeDirName(BurnedVolumes + 1)), MSInfo);
			while (PIsoFs->state() != QProcess::NotRunning)
			{
				QCoreApplication::processEvents();
			}
			
			if (PIsoFs->exitCode() == 0)
			{
				MKisoFsStatus->updateProgress(100, tr("The image of the CD are correctly burned."));
				setState(StateBurning);

				int NumCopies = 1; 
				while (NumCopies <= _NCopies && !Cancelled)
				{					
					PCdRecord->start();	
					CdRecordStatus->showProgressBar(tr("Ready to burn"), 100);
					while (PCdRecord->state() != QProcess::NotRunning)
					{
						QCoreApplication::processEvents();
					}
					if (PCdRecord->exitStatus() == QProcess::NormalExit && PCdRecord->exitCode() == 0)
					{
						CdRecordStatus->updateProgress(100, tr("CD correctly burned!"));
						if (NumCopies < _NCopies)
							getCD(BurnedVolumes + 1, NumVolumes, NumCopies);
						else 
						if (NumCopies == _NCopies)
							BurnedVolumes++;

						NumCopies ++;
					}
					else 
					{	
						if (!Cancelled) //Some slots may change Cancelled
						{
							setError(QString(tr("cdrecord has finished uncorrently: ExitCode = %1")).arg(PCdRecord->exitCode()));
							Cancelled = SMessageBox::question(this, tr("Error burning CD"), 
								tr("CD Burn process fails, would you like to try again ?") ) == QMessageBox::No;
						}
					}
				}
			}
			else 
			{
				MKisoFsStatus->updateProgress(100, tr("There have been some errors creating the ISO image."));
				setError(QString(tr("mkisofs has finished uncorrectly: ExitCode = %1")).arg(PIsoFs->exitCode()));

				Cancelled = SMessageBox::question(this, tr("Error burning CD"), 
					tr("mkisofs process fails, would you like to try again ?") ) == QMessageBox::No;

			}
		}
		if (!Cancelled)
		{
			setState(StateBurnedOk);
		}
		else 
			emit aborted();
	}
	catch(STError& _Error)
	{
		//Reportem l'error !
		setError(_Error.description());
	}
	finish();
}


void CDRecordPage::getCD(int _NumVolumes, int _CurrentVolume, int _NCopy)
{
	system("/usr/bin/eject");
	SMessageBox::information(this, tr("CD burning."), 
										 QString(tr("Please, insert and empty or appendale CD-R(W) num. (%1 of %2) copy (%3) into drive.")
										).arg(_NumVolumes).arg(_CurrentVolume).arg(_NCopy));
}

//! \return true if data fits on CD.
bool CDRecordPage::dataFitOnCD()
{
	bool Res = true;
	//TODO: Use k3bdevice to retrieve device info.
	qint64 CDSize = CD_SIZE * static_cast<qint64>(pow(2, 20)); 
	if ( PIsoFs->isoFileSize() > CDSize )
	{
		Res = false;
		SMessageBox::information(this, tr("CD burning."), tr("The images do on fit in the CD. Please change the selection."));
	}
	return Res;
}


void CDRecordPage::slotBack()
{
	if (SMessageBox::question(this, tr("Cancel Burn process"), 
		tr("Are you sure you want to cancel burn process ?") ) == QMessageBox::Yes)
	{
		finish();
		emit aborted();	

		Cancelled = true;
		system("/usr/bin/killall cdrecord");
		system("/usr/bin/eject");
	}
}

void CDRecordPage::slotBurnOK()
{
	if (!BurnOk)
	{
		BurnOk = true; 
		emit burned(); 
	}
}
