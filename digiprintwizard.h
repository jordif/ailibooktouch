/****************************************************************************
**
** Copyright (C) 2006-2008 Softtopia. All rights reserved.
**
** This file is part of Softtopia Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Softtopia reserves all rights not expressly granted herein.
** 
** Softtopia (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef DIGIPRINTWIZARD_H
#define DIGIPRINTWIZARD_H

#include "stiwizard.h"
#include "iorderwizard.h"
#include "publisherdatabase.h"

/**
Wizard for digital prints.

	@author 
*/
class PhotoSelPage; 
class STProductPrintsProxyModel; 
class QAbstractItemModel; 
class QSqlRecord; 
class TPOrderSummaryPage; 
namespace STDom
{
	class PrintJobModel;
};
class DigiprintWizard : public STIWizard, public IOrderWizard
{
Q_OBJECT

	PhotoSelPage* PSPage;
	TPOrderSummaryPage* SummPage;
	STDom::PrintJobModel* Model;

public:
	DigiprintWizard(QWidget* _Parent);
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value);
	virtual void init(); 
	virtual STDom::PrintJob getPrintJob() const;
	virtual void setImages(const QFileInfoList& _Images, const QDir& _RootDir);


private slots: 
	void wantsToFinish();
	void slotShowSummary();

signals:
	void commitOrder();
};

#endif
