/****************************************************************************
**
** Copyright (C) 2010-2011 Softtopia. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Softtopia reserves all rights not expressly granted herein.
**
** Softtopia (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef TEMPLATEMANAGERWIDGET_H
#define TEMPLATEMANAGERWIDGET_H

#include <QWidget>
#include "templateinfolist.h"
#include "templateinfomodel.h"
#include "sterror.h"

class QToolBar;
class QActionGroup;
class QWebView;
class QComboBox;
class QListView;
class SMultiProcessStatusWidget;
class TemplateInfoWidget : public QWidget
{
	Q_OBJECT
	QActionGroup* AGTemplateActions;
	SPhotoBook::TemplateInfoModel* Model;
	QWebView* WVInfoTemplates;
	QListView* LVTemplates;
	QComboBox* CBSize;
	SPhotoBook::TemplateInfoList TemplateList;


public:
	explicit TemplateInfoWidget(QWidget *parent = 0);
	void setTemplateList(const SPhotoBook::TemplateInfoList& _Templates);
	void updateTemplateInfo(const SPhotoBook::TemplateInfo& _Source, const SPhotoBook::TemplateInfo& _Dest);

signals:
	void templateSelected(const SPhotoBook::TemplateInfo& _TemplateInfo);

private slots:
	void slotTemplateIndexClicked(const QModelIndex& _Index);
	void slotTemplateSizeSelected(int _Index);

};

namespace SPhotoBook
{
	class DesignInfoTableModel;
}
class QTableView;
class DesignInfoWidget : public QWidget
{
	Q_OBJECT

	QActionGroup* AGDesignActions;
	QToolBar* TBDesignActions;
	SPhotoBook::DesignInfoTableModel* Model;
	QTableView* TVDesigns;

	void setupActions();
	void setupToolBars();

public:
	explicit DesignInfoWidget(QWidget *parent = 0);
	bool hasChanges() const;
	SPhotoBook::TemplateInfo templateInfo() const;
	void selectTemplate(const SPhotoBook::TemplateInfo& _Template );
	SPhotoBook::DesignInfo currentDesignInfo();
	void setCurrentDesignInfo(const SPhotoBook::DesignInfo& _Info);

signals:
	void templateDesignsModified();
	void downloadCurrentDesign();

public slots:
	void slotDeleteCurrentDesign();
};


class QLayout;
class TemplateManagerWidget : public QWidget
{
    Q_OBJECT
public:
	ST_DECLARE_ERRORCLASS();

private:
	QToolBar* TBTemplateTypes;
	QActionGroup* TypeAG;
	TemplateInfoWidget* TInfoWidget;
	DesignInfoWidget* DInfoWidget;
	SPhotoBook::TemplateInfoList Templates;
	SPhotoBook::TemplateInfo SelectedTemplate;
	SMultiProcessStatusWidget* MPStatusWidget;

	void setupActions();
	void setupToolBars();
	void setupButtons(QLayout* _Layout);
	void manageProducts();

public:
    explicit TemplateManagerWidget(QWidget *parent = 0);
	void loadTemplates();
	void setCurrentTemplateType(SPhotoBook::MetaInfo::EnTemplateType _Type);

signals:
	void closeClicked();

public slots:
	void slotFilterActionTriggered(QAction* _Action);
	void slotTemplateSelected(const SPhotoBook::TemplateInfo& _TemplateInfo);
	void slotSelectedTemlateDesignModified();
	void slotDownloadCurrentDesign();
};

#endif // TEMPLATEMANAGERWIDGET_H
