/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOBOOKLISTVIEWTABWIDGET_H
#define PHOTOBOOKLISTVIEWTABWIDGET_H

#include <QTabWidget>

/**
Tabwidget for photobook list views like cliparts, masks, etc...

	@author
*/

namespace SPhotoBook
{
	class Document;
	class TemplateScene;
	class Resource;
	class DocumentViewWidget;
}
class ResourceListView;
class QModelIndex; 
class PagesListView;
class TemplateListViewWidget;
class PhotoBookListViewTabWidget : public QTabWidget
{
	Q_OBJECT

public:
	enum EnTabs
	{
		TabLayoutView = 0,
		TabCoverView,
		TabBackCoverView,
		TabClipartView,
		TabMaskView,
		TabFrameView,
		TabBackgroundView,
		TabPhotoBookView
	};

private:
	SPhotoBook::Document* MPhotoBook;
	TemplateListViewWidget* TemplatesView;
	TemplateListViewWidget* CoversView;
	TemplateListViewWidget* BackCoversView;
	ResourceListView* ClipartListView;
	ResourceListView* MaskListView;
	ResourceListView* BackgroundListView;
	ResourceListView* FrameListView;
	PagesListView* PhotoBookView;


public:
	PhotoBookListViewTabWidget(SPhotoBook::DocumentViewWidget* _AlbumWidget, QWidget *parent = 0 );
	~PhotoBookListViewTabWidget();
	void setDocument(SPhotoBook::Document* _PBDoc);
	void retranslateUi();
	void setCurrentTab(EnTabs _Tab);

private slots:
	void pageIndexClicked(const QModelIndex& _Index);
	void updatePhotoBookPreview();
	void updatePhotoBookPreview(int _Page);

signals:
	void pageClicked(int _Page);
	void addPage();
	void removePage();
	void applyTemplate(SPhotoBook::TemplateScene* _Template);
	void resourceSelected(const SPhotoBook::Resource& _Resource);
};

#endif
