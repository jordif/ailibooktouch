/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "resourcelistview.h"
#include <QToolBar>
#include <QComboBox>
#include <QAction>
#include <QLabel>
#include <QFileDialog>
#include <QDebug>

#include "resourcemodel.h"

//GUI
#include "smessagebox.h"

#include "stthumbnailview.h"


ResourceListView::ResourceListView(QWidget* _Parent) : SPToolbarListView(Qt::Horizontal, _Parent), ResListChanged(false)
{
	//Data
	ResModel = new SPhotoBook::ResourceModel(this);
	SPToolbarListView::setModel(ResModel);

	connect(listView(), SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(slotApplyItem(const QModelIndex&)));
}

void ResourceListView::setResourceList(const SPhotoBook::ResourceList& _List, SPhotoBook::Resource::EnResourceType _Type)
{
	ResType = _Type;
	ResListChanged = true;
	MyResList = _List.subList(_Type);
	if (isVisible())
		updateResList();
}

void ResourceListView::setNullItem(const SPhotoBook::Resource& _Resource)
{
	ResModel->setHasNullItem(QImage(":/remove-resource.png"), _Resource);
}

void ResourceListView::updateResList()
{
	if (ResListChanged)
		ResModel->setResourceList(MyResList);
	ResListChanged = false;
}

void ResourceListView::showEvent(QShowEvent* _Event)
{
	updateResList();
}

void ResourceListView::slotApplyItem(const QModelIndex& _Index)
{
	emit resourceSelected(ResModel->resource(_Index));
	emit indexSelected(_Index);
}

