/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photobookeditor.h"
#include <QLayout> 
#include <QToolBar> 
#include <QAction> 
#include <QMenu> 
#include <QFontDialog>
#include <QToolButton>
#include <QGraphicsItem> 
#include <QUndoStack>
#include <QColorDialog> 
#include <QApplication> 
#include <QDebug>
#include <QClipboard>

#include "imageslistviewbase.h"
#include "documentviewwidget.h"
#include "photobooklistviewtabwidget.h"
#include "siconfactory.h"
#include "tsinputdialog.h" 
#include "smessagebox.h" 
#include "tpphotoeditor.h" 

#include "graphicsitemoperation.h"
#include "graphicstextitem.h"
#include "templatescene.h"
#include "graphicspageitem.h"
#include "document.h"
#include "ddocfactory.h"
#include "documentviewwidget.h"
#include "stthumbnailview.h"
#include "resource.h"

#ifdef TTPOPS_KINETICSCROLL
#include "qscrollareakineticscroller.h"
#endif

#include "doccheckedproxymodel.h"


void PhotoBookEditor::retranslateUi()
{
	ButContinue->setText(tr("Continue")); 
	ButBack->setText(tr("Back")); 
	MoreActionsAct->setText(tr("More Actions"));
	PreviousPageAct->setText(tr("Previous page"));
	NextPageAct->setText(tr("Next page"));

	UndoAct->setText(tr("Undo"));
	RedoAct->setText(tr("Redo"));

	ImageMoveAct->setText(tr("Move images"));
	FrameMoveAct->setText(tr("Move frames"));
	NewTextLineFrameAct->setText(tr("New text line frame"));
	NewPhotoFrameAct->setText(tr("New photo frame"));
	DeleteItemAct->setText(tr("Remove"));

	BringToFrontFrameAct->setText(tr("Bring To Front"));
	SendToBackFrameAct->setText(tr("Send To Back"));
	TextFontAct->setText(tr("Text Font"));
	TextColorAct->setText(tr("Item Color"));
	ItemEditAct->setText(tr("Edit item"));
	SelectAllAct->setText(tr("Select All"));

	ImageZoomInAct->setText(tr("Zoom In"));
	ImageZoomOutAct->setText(tr("Zoom Out"));
	ImageRotateLeftAct->setText(tr("Rotate left"));
	ImageRotateRightAct->setText(tr("Rotate right"));
	ImageFitToFrameAct->setText(tr("Fit to Frame"));

	ShadowAct->setText(tr("Shadow"));
	BorderAct->setText(tr("Border"));

	PBTabWidget->retranslateUi();

}

void PhotoBookEditor::editItem(QGraphicsItem* _Item)
{
	try
	{
		SPhotoBook::Document::EnItemType CItemType = SPhotoBook::Document::itemType(_Item);
		if (CItemType == SPhotoBook::Document::TextItemType || CItemType == SPhotoBook::Document::RitchTextItemType)
			editTextItem(_Item); 
		else 
			editCurrentItemImage();
	}
	catch(STError& _Error)
	{
		SMessageBox::critical(this, tr("Error"), _Error.description()); 
	}
}

void PhotoBookEditor::performOpToCurrentSceneSelectedItems(SPhotoBook::GraphicsItemOperation* _Operation)
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
	{
		QList<QGraphicsItem*> SelItems = CScene->selectedItems(); 
		QList<QGraphicsItem*>::iterator it; 
		for (it = SelItems.begin(); it != SelItems.end(); ++it)
		{
			SPhotoBook::GraphicsItemOperation* NewOp = 0;
			if ((*it) != _Operation->item())
				NewOp = _Operation->clone(*it);
			else 
				NewOp = _Operation;
			 
			if (NewOp)
				UndoStack->push(NewOp); 
		}
	}
}


/*!
	\return true if the edition was accepted.
*/
bool PhotoBookEditor::editTextItem(QGraphicsItem* _Item)
{
	bool Res = false; 
	if (_Item) //Defensive 
	{
		if (!TSIDialog)
			TSIDialog = new TSInputDialog(tr("Enter text:"), this);

		if (_Item->type() == SPhotoBook::GraphicsTextItem::Type)
		{
			SPhotoBook::GraphicsTextItem* CItem = qgraphicsitem_cast<SPhotoBook::GraphicsTextItem*>(_Item);
			QString CurrentPlainText = CItem->toPlainText();
			TSIDialog->setText(CurrentPlainText);
			if (TSIDialog->exec() == QDialog::Accepted)
			{
				QString OldText = CItem->toHtml();
				QString NewText;
				if (!CurrentPlainText.isEmpty())
					NewText = OldText.replace(CItem->toPlainText(), TSIDialog->strResult());
				else
					NewText = TSIDialog->strResult();
				UndoStack->push(new SPhotoBook::STChangeTextGIO(CItem, NewText));
				Res = true; 
				//CItem->setManuallyEdited(CItem->toHtml() != OldText);
			}
		}
	}
	return Res; 
}

void PhotoBookEditor::editCurrentItemImage()
{
	if (!PEditor)
	{
		PEditor = new TPPhotoEditor(this); 
		PEditor->setTempPath(tempPath());
	}

	SPhotoBook::GraphicsPhotoItem* CPhotoItem = MainPBView->currentPhotoItem();
	if (CPhotoItem)
	{
		QApplication::setOverrideCursor( QCursor(Qt::WaitCursor));
		try
		{
			QString CurrImageFile = CPhotoItem->imageResource().fileInfo().absoluteFilePath();
			STImage Img;
			Assert(Img.load(CurrImageFile), Error(tr("Error loading image: %1").arg(CurrImageFile))); 
			{
				PEditor->setImage(Img); 
				PEditor->setAspectRatio(CPhotoItem->rect().size());
				QApplication::restoreOverrideCursor();
				if (PEditor->exec() == QDialog::Accepted)
				{
					if (Model) //Defensive
					{
						CPhotoItem->setImage(PEditor->currentImage(), PEditor->savedImage().absoluteFilePath());
						Model->insertDoc(ISelWidg->listView()->currentIndex(), STDom::DDocFactory::newDoc(PEditor->savedImage()));
					}
				}
			}
		}
		catch(const STError& _Error)
		{
			QApplication::restoreOverrideCursor();
			SMessageBox::critical(this, tr("Error opening image"), _Error.description());
		}
	}
}


void PhotoBookEditor::setupActions()
{
	// Navigator actions
	MoreActionsAct = new QAction(QIcon(":/moreactions.png"), tr("More Actions"), this);
	connect(MoreActionsAct, SIGNAL(triggered()), this, SLOT(slotMoreActions()));

	// Navigator actions
	PreviousPageAct = new QAction(QIcon(":/prevpage.png"), tr("Previous page"), this);
	PreviousPageAct->setShortcut(Qt::Key_PageUp);
	PreviousPageAct->setStatusTip(tr("Goes to the previous page of current photobook."));
	connect(PreviousPageAct, SIGNAL(triggered()), MainPBView, SLOT(previousPage()));

	NextPageAct = new QAction(QIcon(":/nextpage.png"), tr("Next page"), this);
	NextPageAct->setShortcut(Qt::Key_PageDown);
	NextPageAct->setStatusTip(tr("Goes to the next page of current photobook."));
	connect(NextPageAct, SIGNAL(triggered()), MainPBView, SLOT(nextPage()));

	// Edit actions 
	UndoAct = UndoStack->createUndoAction(this);//new QAction(QIcon(":/undo.png"), tr("Undo"), this);
	UndoAct->setShortcut(tr("Ctrl+Z"));
	UndoAct->setIcon(QIcon(":/edit-undo.png")); 

	RedoAct = UndoStack->createRedoAction(this);
	RedoAct->setIcon(QIcon(":/edit-redo.png")); 

	QActionGroup* FrameActionGroup = new QActionGroup(this); 
	ImageMoveAct = new QAction(QIcon(":/imagemove.png"), tr("Move images"), this);
	ImageMoveAct->setStatusTip(tr("Move the image"));
	ImageMoveAct->setCheckable(true); 
	ImageMoveAct->setChecked(true);
	FrameActionGroup->addAction(ImageMoveAct); 
	ImageMoveAct->setVisible(false);
	
	FrameMoveAct = new QAction(QIcon(":/framemove.png"), tr("Move frames"), this);
	FrameMoveAct->setStatusTip(tr("Move the frame"));
	FrameMoveAct->setCheckable(true); 
	FrameActionGroup->addAction(FrameMoveAct); 
	//connect(FrameActionGroup, SIGNAL(triggered(QAction* )), this, SLOT(updateItemsMovableState()));
	FrameMoveAct->setVisible(false);
	
	NewTextLineFrameAct = new QAction(QIcon(":/newtextframe.png"), tr("New text line frame"), this); 
	connect(NewTextLineFrameAct, SIGNAL(triggered(bool )), this, SLOT(addTextFrame()));
	
	NewPhotoFrameAct = new QAction(QIcon(":/newphotoframe.png"), tr("New photo frame"), this); 
	connect(NewPhotoFrameAct, SIGNAL(triggered()), this, SLOT(newPhotoFrameAction()));

	DeleteItemAct = new QAction(stIcon(SIconFactory::EditDelete), tr("Remove"), this);
	DeleteItemAct->setToolTip(tr("Removes selected items."));
	DeleteItemAct->setShortcut(Qt::Key_Delete);
	connect(DeleteItemAct, SIGNAL(triggered()), this, SLOT(deleteItemAction()));	

	BringToFrontFrameAct = new QAction(QIcon(":/bringtofront.png"), tr("Bring To Front"), this); 
	connect(BringToFrontFrameAct, SIGNAL(triggered()), this, SLOT(bringToFrontFrameAct()));
	
	SendToBackFrameAct = new QAction(QIcon(":/sendtoback.png"), tr("Send To Back"), this); 
	connect(SendToBackFrameAct, SIGNAL(triggered()), this, SLOT(sendToBackFrameAct()));

	SelectAllAct = new QAction(QIcon(":/selectall.png"), tr("Select All"), this);
	connect(SelectAllAct, SIGNAL(triggered()), this, SLOT(slotSelectAllFrames()));

	// Text actions 
	TextFontAct = new QAction(QIcon(":/text_font.png"), tr("Text Font"), this);
	TextFontAct->setStatusTip(tr("Text Font"));
	connect(TextFontAct, SIGNAL(triggered()), this, SLOT(changeFontToSelectedItems()));

	TextColorAct = new QAction(QIcon(":/colorize.png"), tr("Text Color"), this);
	TextColorAct->setStatusTip(tr("Changes the text color"));
	connect(TextColorAct, SIGNAL(triggered()), this, SLOT(slotChangeItemColor()));

	// Frame actions
	ItemEditAct = new QAction(QIcon(":/frameedit.png"), tr("Edit item"), this);
	connect(ItemEditAct, SIGNAL(triggered()), this, SLOT(slotEditItem())); 
	ItemEditAct->setStatusTip(tr("Edit current item."));
		
	ShadowAct = new QAction(QIcon(":/shadow.png"), tr("Shadow"), this);
	connect(ShadowAct, SIGNAL(triggered()), this, SLOT(slotToggleItemShadow())); 
	ShadowAct->setStatusTip(tr("Toggle item shadow."));
#if QT_VERSION < 0x040600
	ShadowAct->setVisible(false);
#endif

	BorderAct = new QAction(QIcon(":/redframe.png"), tr("Border"), this);
	connect(BorderAct, SIGNAL(triggered()), this, SLOT(toggleItemBorder())); 

	ImageZoomInAct = new QAction(QIcon(":/framemag.png"), tr("Zoom In"), this);
	ImageZoomInAct->setAutoRepeat(true);
	ImageZoomInAct->setStatusTip(tr("Zoom in the current image."));
	connect(ImageZoomInAct, SIGNAL(triggered(bool )), this, SLOT(itemActionToggled()));

	ImageZoomOutAct = new QAction(QIcon(":/framemin.png"), tr("Zoom Out"), this);
	ImageZoomOutAct->setAutoRepeat(true);
	ImageZoomOutAct->setStatusTip(tr("Zoom out the current image."));
	connect(ImageZoomOutAct, SIGNAL(triggered(bool )), this, SLOT(itemActionToggled()));

	ImageRotateLeftAct = new QAction(QIcon(":/rotateleft.png"), tr("Rotate left"), this);
	ImageRotateLeftAct->setStatusTip(tr("Rotates current image 270?."));
	connect(ImageRotateLeftAct, SIGNAL(triggered(bool )), this, SLOT(itemActionToggled()));
	ImageRotateLeftAct->setVisible(false);

	ImageRotateRightAct = new QAction(QIcon(":/rotateright.png"), tr("Rotate right"), this);
	ImageRotateRightAct->setStatusTip(tr("Rotates current image 90?."));
	connect(ImageRotateRightAct, SIGNAL(triggered(bool )), this, SLOT(itemActionToggled()));

	ImageFitToFrameAct = new QAction(QIcon(":/fittoframe.png"), tr("Fit To Frame"), this);
	ImageFitToFrameAct->setStatusTip(tr("Fit image to frame size."));
	connect(ImageFitToFrameAct, SIGNAL(triggered(bool )), this, SLOT(itemActionToggled()));

	CopyXmlAct = new QAction(stIcon(SIconFactory::EditCopy), tr("Copy Page Xml"), this);
	connect(CopyXmlAct, SIGNAL(triggered(bool )), this, SLOT(slotCopyTemplateXmlToClipBoard()));
	CopyXmlAct->setVisible(false);
}

QToolBar* PhotoBookEditor::newToolBar(QHBoxLayout* _TopLayout)
{
	QToolBar* Res = new QToolBar(this);
	Res->setObjectName("PBEditorToolBar");
	Res->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	_TopLayout->addWidget(Res);
	_TopLayout->setAlignment(Res, Qt::AlignHCenter);
	Res->setIconSize(QSize(32,32));
	return Res;
}

void PhotoBookEditor::setupToolBar()
{
	MToolBar->addAction(PreviousPageAct);
	MToolBar->addAction(NextPageAct); 

	MToolBar->addSeparator(); 
	

	MToolBar->addAction(ItemEditAct);
	MToolBar->addAction(NewTextLineFrameAct);
	MToolBar->addAction(DeleteItemAct); 

	MToolBar->addAction(ImageZoomInAct);
	MToolBar->addAction(ImageZoomOutAct);
	MToolBar->addAction(ImageRotateRightAct);
	MToolBar->addAction(MoreActionsAct);

	MToolBar2->addAction(SelectAllAct);
	MToolBar2->addAction(BringToFrontFrameAct);
	MToolBar2->addAction(SendToBackFrameAct);
	MToolBar2->addSeparator();

	MToolBar2->addAction(NewPhotoFrameAct);
	MToolBar2->addAction(TextFontAct);
	MToolBar2->addAction(TextColorAct);
	MToolBar2->addSeparator();

	MToolBar2->addAction(ShadowAct);
	MToolBar2->addAction(BorderAct);
	MToolBar2->addAction(MoreActionsAct);

	MToolBar3->addAction(ImageRotateLeftAct);
	MToolBar3->addAction(ImageFitToFrameAct);
	MToolBar3->addAction(CopyXmlAct);
	MToolBar3->addAction(ImageMoveAct);
	MToolBar3->addAction(FrameMoveAct);
	MToolBar3->addAction(UndoAct);
	MToolBar3->addAction(RedoAct);
	MToolBar3->addAction(MoreActionsAct);


}

PhotoBookEditor::PhotoBookEditor(QWidget* _Parent): STIWizardPage(_Parent), TSIDialog(0), Model(0), PEditor(0), PhotoBook(0)
{
	QGridLayout* MLayout = new QGridLayout(this); 
	MLayout->setMargin(1); 
	MLayout->setSpacing(1); 

	QHBoxLayout* TopLayout = new QHBoxLayout; 
	MLayout->addLayout(TopLayout, 0, 0, 1, 2); 

	ButBack = new QToolButton(this);
	ButBack->setIcon(QIcon(":/st/tpopsl/previous.png")); 
	ButBack->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButBack->setIconSize(QSize(32, 32)); 
	TopLayout->addWidget(ButBack);
	connect ( ButBack, SIGNAL(clicked()), this, SIGNAL(previousPage()));

	MLayout->setMargin(1); 
	MLayout->setSpacing(1); 

	MToolBar = newToolBar(TopLayout);

	MToolBar2 = newToolBar(TopLayout);
	MToolBar2->setVisible(false);

	MToolBar3 = newToolBar(TopLayout);
	MToolBar3->setVisible(false);

	//TopLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));
	ButContinue = new QToolButton(this);
	ButContinue->setIcon(QIcon(":/st/tpopsl/accept.png")); 
	ButContinue->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	ButContinue->setIconSize(QSize(32, 32)); 
	TopLayout->addWidget(ButContinue);
	connect ( ButContinue, SIGNAL(clicked()), this, SLOT(slotContinue()));

	ISelWidg = new SPhotoBook::ImagesListViewBase(this, true, Qt::Vertical, false);
	//ISelWidg->listView()->setViewMode(QListView::IconMode);
	ISelWidg->listView()->setAcceptDrops(false);
	ISelWidg->toolBar()->setVisible(false);
	MLayout->addWidget(ISelWidg, 1, 0, 2, 1); 
#ifdef TTPOPS_KINETICSCROLL
	//QScrollAreaKineticScroller* NewScroller = new QScrollAreaKineticScroller();
	//NewScroller->setWidget(ISelWidg->listView());
#endif

	
	MainPBView = new SPhotoBook::DocumentViewWidget(this);
	MLayout->addWidget(MainPBView, 1, 1); 

	PBTabWidget = new PhotoBookListViewTabWidget(MainPBView);
	connect(PBTabWidget, SIGNAL(resourceSelected(SPhotoBook::Resource)), this, SLOT(slotResourceSelected(SPhotoBook::Resource)));
	connect(PBTabWidget, SIGNAL(applyTemplate(SPhotoBook::TemplateScene*)),
			  this, SLOT(slotApplyTemplate(SPhotoBook::TemplateScene*)));
	connect(PBTabWidget, SIGNAL(pageClicked(int)), MainPBView, SLOT(showPage(int)));
	connect(PBTabWidget, SIGNAL(addPage()), this, SLOT(slotAddPage()));
	connect(PBTabWidget, SIGNAL(removePage()), this, SLOT(slotRemovePage()));

	connect(MainPBView, SIGNAL(pageChanged(int, int)), PBTabWidget, SLOT(updatePhotoBookPreview(int))); 

	MLayout->addWidget(PBTabWidget, 2, 1);
	MLayout->setRowStretch(1, 3);
	MLayout->setRowStretch(2, 1);
	MLayout->setColumnStretch(0, 1);
	MLayout->setColumnStretch(1, 4);
 

	UndoStack = new QUndoStack(this); 
	setupActions(); 
	setupToolBar(); 

}

QDir PhotoBookEditor::tempPath()
{
	return QDir(QDir::tempPath() + "/tpopslphotobookedit_images");
}

void PhotoBookEditor::init()
{
	retranslateUi(); 
	MainPBView->firstPage(); 
	ISelWidg->updateCheckedFiles(); 
	PBTabWidget->setCurrentTab(PhotoBookListViewTabWidget::TabPhotoBookView);
	if (PhotoBook) //Defensive
	{
		if (PhotoBook->metaInfo().preferMinPages())
			PBTabWidget->setCurrentTab(PhotoBookListViewTabWidget::TabLayoutView);
		if (PhotoBook->metaInfo().multiPhoto())
			slotSelectAllFrames();
	}
	MToolBar->setVisible(true);
	MToolBar2->setVisible(false);
	MToolBar3->setVisible(false);
}

void PhotoBookEditor::setModel(STDom::DDocModel* _Model)
{
	Model = _Model;
	ISelWidg->model()->setSourceModel(_Model);
}

void PhotoBookEditor::setPhotoBook(SPhotoBook::Document* _PhotoBook)
{
	MainPBView->setPBDocument(_PhotoBook);
	PBTabWidget->setDocument(_PhotoBook);
	ISelWidg->setDocument(_PhotoBook);
	if (PhotoBook)
	{
		disconnect(PhotoBook, SIGNAL(sceneDoubleClicked(QGraphicsScene*)), this, SLOT(slotSceneDoubleClicked()));
		disconnect(PhotoBook, SIGNAL(templateDropped(SPhotoBook::TemplateScene*,SPhotoBook::TemplateScene*)),
				this, SLOT(slotApplyTemplate(SPhotoBook::TemplateScene*,SPhotoBook::TemplateScene*)));
	}
	PhotoBook = _PhotoBook;
	connect(PhotoBook, SIGNAL(sceneDoubleClicked(QGraphicsScene*)), this, SLOT(slotSceneDoubleClicked()));
	connect(PhotoBook, SIGNAL(templateDropped(SPhotoBook::TemplateScene*,SPhotoBook::TemplateScene*)),
			this, SLOT(slotApplyTemplate(SPhotoBook::TemplateScene*,SPhotoBook::TemplateScene*)));

	PBTabWidget->setCurrentTab(PhotoBookListViewTabWidget::TabPhotoBookView);
}

void PhotoBookEditor::addTextFrame()
{
	if (SPhotoBook::TemplateScene* CurrScene = MainPBView->currentScene())
	{
		QGraphicsItem* NTFrame = CurrScene->addTextFrame(tr("New Text Frame"));
		if (!editTextItem(NTFrame))
		{
			CurrScene->removeItem(NTFrame);
			delete NTFrame; 
		}
	}
}

void PhotoBookEditor::deleteItemAction()
{
	if (SMessageBox::question(this, tr("Delete item(s)"), tr("Do you want to delete all selected items ?")) == QMessageBox::Yes)
	{
		if (SPhotoBook::GraphicsPageItem* CPageItem = MainPBView->currentPageItem())
			CPageItem->setSelected(false); 
		
		if (QGraphicsItem* CItem = MainPBView->currentItem())
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STDeleteItemGIO(CItem));

		ISelWidg->updateCheckedFiles(); 
	}
}

void PhotoBookEditor::newPhotoFrameAction()
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
		CScene->addRandomPhotoFrame();
}

void PhotoBookEditor::bringToFrontFrameAct()
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
	{
		qreal TopZValue = CScene->topZValue(); 	
		if (QGraphicsItem* CItem = MainPBView->currentItem())
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STSetZValueGIO(CItem, TopZValue));
	}
}

void PhotoBookEditor::sendToBackFrameAct()
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
	{
		qreal BottomZValue = CScene->bottomZValue(); 	
		if (QGraphicsItem* CItem = MainPBView->currentItem())
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STSetZValueGIO(CItem, BottomZValue));
	}
}

void PhotoBookEditor::slotSelectAllFrames()
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
		CScene->selectAllByType(SPhotoBook::GraphicsPhotoItem::Type);
}

void PhotoBookEditor::changeFontToSelectedItems()
{
	qDebug() << "PhotoBookEditor::changeFontToSelectedItems()";
	if (QGraphicsItem* CItem = MainPBView->currentItem())
	{
		qDebug() << "CItem";
		if (SPhotoBook::Document::itemType(CItem) == SPhotoBook::Document::RitchTextItemType)
		{
			qDebug() << "Text Type";
			if (SPhotoBook::GraphicsTextItem* TItem = qgraphicsitem_cast<SPhotoBook::GraphicsTextItem*>(CItem))
			{
				bool Ok;
				QFont NFont = QFontDialog::getFont(&Ok, TItem->charFormat().font() , this, tr("Please, choose the font for selected text items"));
				if (Ok)
				{
					QTextCharFormat Format;
					Format.setFont(NFont);
					TItem->mergeCharFormat(Format);
				}
			}
		}
	}
}

void PhotoBookEditor::slotChangeItemColor()
{
	if (QGraphicsItem* Item = MainPBView->currentItem())
	{
		if (Item->type() == SPhotoBook::GraphicsTextItem::Type)
		{
			SPhotoBook::GraphicsTextItem* CItem = qgraphicsitem_cast<SPhotoBook::GraphicsTextItem*>(Item);
			QColor NewColor = QColorDialog::getColor(CItem->charFormat().foreground().color(), this);
			if (NewColor.isValid())
			{
				QTextCharFormat Format;
				Format.setForeground(NewColor);
				CItem->mergeCharFormat(Format);
			}
		}
		if (Item->type() == SPhotoBook::GraphicsPageItem::Type)
		{
			if (SPhotoBook::TemplateScene* CurrScene = MainPBView->currentScene())
			{
				QBrush CBrush = CurrScene->bgBrush(); 
				QColor NewColor = QColorDialog::getColor(CBrush.color(), this);
				if (NewColor.isValid())
				{
					CurrScene->pageItem()->clearImage(); 
					CurrScene->setBgBrush(QBrush(NewColor));
				}
			}
		}
	}
}



void PhotoBookEditor::slotEditItem()
{
	if (QGraphicsItem* CItem = MainPBView->currentItem())
	{
		editItem(CItem);
	}
	else 
		SMessageBox::information(this, tr("Information"), 
										tr("Please, select a item to edit."));
}

void PhotoBookEditor::slotToggleItemShadow()
{
#if QT_VERSION >= 0x040600
	if (QGraphicsItem* CItem = MainPBView->currentItem())
	{
		QGraphicsDropShadowEffect* NewEffect = 0;
		bool Enabled = true;
		if (! (NewEffect = qobject_cast<QGraphicsDropShadowEffect*>(CItem->graphicsEffect())))
			NewEffect = new QGraphicsDropShadowEffect(this);
		else
			Enabled = !NewEffect->isEnabled();
		NewEffect->setColor(Qt::black);
		NewEffect->setXOffset(5);
		NewEffect->setYOffset(5);
		NewEffect->setBlurRadius(10);
		NewEffect->setEnabled(Enabled);
		performOpToCurrentSceneSelectedItems(new SPhotoBook::STSetItemGraphicsEffectGIO(CItem, NewEffect));
	}
#endif
}


void PhotoBookEditor::itemActionToggled()
{
	if (SPhotoBook::GraphicsPhotoItem* CItem = MainPBView->currentPhotoItem())
	{
		if (sender() == ImageZoomInAct)
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STScaleImageGIO(CItem, 1.02));
		else
		if (sender() == ImageZoomOutAct)
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STScaleImageGIO(CItem, 1 / 1.02));
		else
		if (sender() == ImageRotateLeftAct)
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STRotateImageGIO(CItem, -90));
		else
		if (sender() == ImageRotateRightAct)
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STRotateImageGIO(CItem, 90));
		else
		if (sender() == ImageFitToFrameAct)
			performOpToCurrentSceneSelectedItems(new SPhotoBook::STFitInImageGIO(CItem));
	}
}


void PhotoBookEditor::slotApplyTemplate(SPhotoBook::TemplateScene* _TScene)
{
	slotApplyTemplate(MainPBView->currentScene(), _TScene);
}

//! Provided for convenience.
void PhotoBookEditor::slotApplyTemplate(SPhotoBook::TemplateScene* _Scene, SPhotoBook::TemplateScene* _TScene)
{
	if (SMessageBox::question(this, tr("Modify page layout"), tr("Do you want to change the layout of current page ?")) == QMessageBox::Yes)
	{
		QString Reason = tr("No Reason");
		if (PhotoBook->suitableTemplate(MainPBView->currentPageIndex(), _TScene, Reason))
		{
			_Scene->replaceTemplate(_TScene);
			ISelWidg->updateCheckedFiles();
		}
		else
			SMessageBox::information(this, tr("Not suitable template"), tr("You can not apply this template for current page because %1").arg(Reason));
	}
}


void PhotoBookEditor::slotResourceSelected(const SPhotoBook::Resource& _Resource)
{
	if (SPhotoBook::TemplateScene* CScene = MainPBView->currentScene())
		CScene->applyResource(_Resource);
}

void PhotoBookEditor::toggleItemBorder()
{
	SPhotoBook::GraphicsPhotoItem* CPhotoItem = MainPBView->currentPhotoItem();
	if (CPhotoItem)
	{
		if (CPhotoItem)
		{
			if ( CPhotoItem->pen().style() != Qt::NoPen )
				UndoStack->push(new SPhotoBook::STSetItemBorderSizeGIO(CPhotoItem, 0));
			else 
			{
				UndoStack->push(new SPhotoBook::STSetItemBorderSizeGIO(CPhotoItem, 2));
				QColor NewColor = QColorDialog::getColor(CPhotoItem->brush().color(), this);
				if (NewColor.isValid())
					UndoStack->push(new SPhotoBook::STSetItemBorderColorGIO(CPhotoItem, NewColor));
			}
		}
	}
}


void PhotoBookEditor::slotContinue()
{
	bool Correct = true;
	if (PhotoBook)
	{
		QString ErrMessage;
		Correct = PhotoBook->isPhotoBookCorrect(ErrMessage);
		if (!Correct)
			SMessageBox::warning(this, tr("Photobook is not correct."), ErrMessage);
	}
	if (Correct)
		nextPage();
}

void PhotoBookEditor::slotAddPage()
{
	int CurrentIndex = 0;
	if (PhotoBook->pages().size() < PhotoBook->metaInfo().maxPages())
	{
		CurrentIndex = MainPBView->currentPageIndex() + 1;
        QString Reason;
        if (!PhotoBook->insertRandomPage(CurrentIndex, Reason))
            SMessageBox::warning(this, tr("Adding Pages"), QString(tr("Could not add page because: %1").arg(Reason)));

		setPhotoBook(PhotoBook);
		MainPBView->setCurrentPage(CurrentIndex);
	}
	else
	{
		SMessageBox::warning(this, tr("Adding Pages"), QString(tr("This PhotoBook could not contain more than %1 pages")).arg(PhotoBook->metaInfo().maxPages()));
		return;
	}
}

void PhotoBookEditor::slotRemovePage()
{
	if (PhotoBook->pages().size() -1 >= PhotoBook->metaInfo().minPages())
	{
		if (SMessageBox::question(this, tr("Remove current page"), tr("Are you sure to delete the current page ?")) == QMessageBox::Yes)
		{
			int MIndex = MainPBView->currentPageIndex();
			if (MIndex != 0)
			{
				PhotoBook->removePage(MIndex);
				int CurrentIndex = qMax(MIndex - 1, 0);
				setPhotoBook(PhotoBook);
				MainPBView->setCurrentPage(CurrentIndex);
			}
			else
				SMessageBox::information(this, tr("Removing Pages"), tr("Sorry but you can't delete the first page."));
		}
	}
	else
	{
		SMessageBox::warning(this, tr("Removing Pages"), QString(tr("This PhotoBook need at least %1 pages")).arg(PhotoBook->metaInfo().minPages()));
		return;
	}
}

void PhotoBookEditor::slotMoreActions()
{
	if (MToolBar->isVisible())
	{
		MToolBar->setVisible(false);
		MToolBar2->setVisible(true);
	}
	else
	if (MToolBar2->isVisible())
	{
		MToolBar2->setVisible(false);
		MToolBar3->setVisible(true);
	}
	else
	{
		MToolBar3->setVisible(false);
		MToolBar->setVisible(true);
	}
}

void PhotoBookEditor::slotSceneDoubleClicked()
{
	if (QGraphicsItem* CItem = MainPBView->currentItem())
	{
		if (CItem->type() == SPhotoBook::GraphicsTextItem::Type)
		{
			SPhotoBook::GraphicsTextItem* TextItem = qgraphicsitem_cast<SPhotoBook::GraphicsTextItem*>(CItem);
			//if (!TextItem->manuallyEdited())
			{
				editTextItem(CItem);
			}
		}
	}
}

