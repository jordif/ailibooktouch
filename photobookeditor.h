/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef PHOTOBOOKEDITOR_H
#define PHOTOBOOKEDITOR_H

#include <QDir> 
#include "stiwizardpage.h"
#include "sterror.h" 
#include "ddocmodel.h"

/**
	@author
*/

namespace SPhotoBook
{
	class ImagesListViewBase;
	class Document;
	class DocumentViewWidget;
	class GraphicsItemOperation;
	class TemplateScene;
	class Resource;
}

class QAction; 
class QToolBar; 
class PBImageSelectWidget; 
class OPhotoCollectionImageModel; 
class PhotoBookListViewTabWidget;
class QToolButton; 
class QGraphicsItem; 
class TSInputDialog;
class QUndoStack; 
class TPPhotoEditor; 
class STPhotoLayoutTemplate;
class QHBoxLayout;
class PhotoBookEditor : public STIWizardPage
{
	Q_OBJECT 

public:
	ST_DECLARE_ERRORCLASS();

private:
	//Actions 
	QAction* MoreActionsAct;
	QAction* PreviousPageAct;
	QAction* NextPageAct;
	
	QAction* UndoAct;
	QAction* RedoAct;
	
	QAction* ImageMoveAct;
	QAction* FrameMoveAct;
	QAction* NewTextLineFrameAct;
	QAction* NewPhotoFrameAct;
	QAction* DeleteItemAct; 

	QAction* BringToFrontFrameAct;
	QAction* SendToBackFrameAct;
	QAction* TextFontAct;
	QAction* TextColorAct;
	QAction* ItemEditAct; 
	QAction* SelectAllAct;

	QAction* ImageZoomInAct;
	QAction* ImageZoomOutAct;
	QAction* ImageRotateLeftAct;
	QAction* ImageRotateRightAct;
	QAction* ImageFitToFrameAct;
	QAction* CopyXmlAct;

	QAction* ShadowAct;
	QAction* BorderAct;
	
	QToolButton* ButBack; 
	QToolButton* ButContinue;

	QToolBar* MToolBar;
	QToolBar* MToolBar2;
	QToolBar* MToolBar3;
	SPhotoBook::DocumentViewWidget* MainPBView;
	SPhotoBook::ImagesListViewBase* ISelWidg;
	PhotoBookListViewTabWidget* PBTabWidget;
	TSInputDialog* TSIDialog; 
	QUndoStack* UndoStack; 
	TPPhotoEditor* PEditor; 
	SPhotoBook::Document* PhotoBook;
	STDom::DDocModel* Model;

	void retranslateUi();
	void editItem(QGraphicsItem* _Item);
	void performOpToCurrentSceneSelectedItems(SPhotoBook::GraphicsItemOperation* _Operation);
	bool editTextItem(QGraphicsItem* _Item);
	void editCurrentItemImage();
	void setupActions();
	QToolBar* newToolBar(QHBoxLayout* _TopLayout);
	void setupToolBar();
	
public:
	PhotoBookEditor ( QWidget* _Parent );
	QDir tempPath();
	virtual void init();
	void setModel(STDom::DDocModel* _Model);
	void setPhotoBook(SPhotoBook::Document* _PhotoBook);

protected slots: 
	void addTextFrame();
	void deleteItemAction();
	void newPhotoFrameAction();
	void bringToFrontFrameAct();
	void sendToBackFrameAct();
	void changeFontToSelectedItems();
	void slotSelectAllFrames();
	void slotChangeItemColor();
	void slotEditItem();
	void slotToggleItemShadow();
	void itemActionToggled();
	void slotApplyTemplate(SPhotoBook::TemplateScene* _TScene);
	void slotApplyTemplate(SPhotoBook::TemplateScene* _Scene, SPhotoBook::TemplateScene* _TScene);
	void slotResourceSelected(const SPhotoBook::Resource& _Resource);
	void toggleItemBorder();
	void slotContinue();
	void slotAddPage();
	void slotRemovePage();
	void slotMoreActions();
	void slotSceneDoubleClicked();
};

#endif
