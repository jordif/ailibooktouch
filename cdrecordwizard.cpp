/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "cdrecordwizard.h"
#include "singlephotoselpage.h"
#include "storageoutputpage.h" 
#include "cdrecordpage.h"
#include "smessagebox.h"
#include "appsettings.h"

CDRecordWizard::CDRecordWizard(QWidget* _Parent) : STIWizard(_Parent)
{
	SPPage = new SinglePhotoSelPage(this);
	addPage(SPPage);
	connect(SPPage, SIGNAL(previousPage()), this, SLOT(wantsToFinish()));
	connect(SPPage, SIGNAL(accepted()), this, SLOT(setSelImages()));

	OPage = new StorageOutputPage(this); 
	OPage->setStoreIcon(QIcon(":/burn.png"));
	connect(OPage, SIGNAL(store(int )), this, SLOT(burnCD(int ))); 
	connect(OPage, SIGNAL(orderCanceled()), this, SIGNAL(finished())); 
	addPage(OPage); 
	
	CDRPage = new CDRecordPage(this); 
	connect(CDRPage, SIGNAL(aborted()), this, SIGNAL(previousWizard())); 
	connect(CDRPage, SIGNAL(burned()), this, SIGNAL(commitOrder())); 
	addPage(CDRPage);
}


CDRecordWizard::~CDRecordWizard()
{
}

void CDRecordWizard::setProductsModel(QAbstractItemModel* _Model)
{
	OPage->setProductsModel(_Model); 
}

void CDRecordWizard::setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value)
{
	OPage->setShippingMethod(_Value);
}

void CDRecordWizard::setImages(const QFileInfoList& _Images, const QDir& _RootDir)
{
	SPPage->setImages(_Images);
	SPPage->setRootDir(_RootDir);
}

STDom::PrintJob CDRecordWizard::getPrintJob() const
{
	return STDom::PrintJob();
}

void CDRecordWizard::init()
{
	firstPage();
	setProductsModel(newProductsModel(this, STDom::PublisherDatabase::CDRecordProduct));
	SPPage->setHeaderText(tr("<h2>Select files you want to store.</h2>"));
}

void CDRecordWizard::wantsToFinish()
{
	bool Finish = true;
	//if (!MPhotoBook->isEmpty())
	if (SPPage->hasChanges())
		Finish = SMessageBox::question(this, tr("Order cancel question"), tr("You'll loose all the changes. Cancel anyway ?")) == QMessageBox::Yes;

	if (Finish)
		emit previousWizard();
}

void CDRecordWizard::setSelImages()
{
	OPage->setFiles(SPPage->selectedImages());
	nextPage(); 
}

void CDRecordWizard::burnCD(int _Copies)
{
	AppSettings Settings;

	if (Settings.commitAccess())
	{
		nextPage();
		CDRPage->burn(SPPage->selectedImages(), _Copies);
	}
}
