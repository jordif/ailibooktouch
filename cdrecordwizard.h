/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef CDRECORDWIZARD_H
#define CDRECORDWIZARD_H

#include "stiwizard.h"
#include <QFileInfoList>
#include "publisherdatabase.h"
#include "iorderwizard.h"
#include "printjob.h"

/**
CDRom record wizard.

	@author
*/
class SinglePhotoSelPage;
class OPhotoCollectionImageModel;
class QAbstractItemModel;
class StorageOutputPage;
class STProductPrintsProxyModel;
class OPhotoCollectionImageModel; 
class CDRecordPage;
class QSqlRecord;
class CDRecordWizard : public STIWizard, IOrderWizard
{
	Q_OBJECT

	SinglePhotoSelPage* SPPage;
	StorageOutputPage* OPage; 
	CDRecordPage* CDRPage; 

public:
	CDRecordWizard(QWidget* _Parent = 0);
	~CDRecordWizard();

public:
	void setProductsModel(QAbstractItemModel* _Model);
	void setShippingMethod(const STDom::PublisherBill::PublisherShippingMethod & _Value);
	void setImages(const QFileInfoList& _Images, const QDir& _RootDir);
	STDom::PrintJob getPrintJob() const;

protected:
	virtual void init();

private slots:
	void wantsToFinish();
	void setSelImages();
	void burnCD(int _Copies); 

signals: 
	void commitOrder(); 
};

#endif
