/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "photoselpage.h"
#include <QLayout> 

#include "tpphotoselwidget.h"
#include "appsettings.h" 

PhotoSelPage::PhotoSelPage(QWidget *parent)
 : STIWizardPage(parent)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	MLayout->setMargin(0); 
	MLayout->setSpacing(0); 
	PSWidget = new TPPhotoSelWidget(this); 
	PSWidget->setHasScrollBar(true);
	MLayout->addWidget(PSWidget); 

	connect(PSWidget, SIGNAL(accepted()), this, SIGNAL(accepted())); 
	connect(PSWidget, SIGNAL(rejected()), this, SIGNAL(previousPage())); 
}

PhotoSelPage::~PhotoSelPage()
{
}

void PhotoSelPage::init()
{
	PSWidget->init(); 
	AppSettings Settings; 
	PSWidget->setDpis(Settings.mainDpis()); 
	PSWidget->setMinDpis(Settings.mainLowResWarningDpis());
}

void PhotoSelPage::setModel(STDom::PrintJobModel* _Model)
{
	PSWidget->setModel(_Model); 
}

void PhotoSelPage::setRootDir(const QDir& _RootDir)
{
	PSWidget->setRootDir(_RootDir);
}

void PhotoSelPage::setProductsModel(QAbstractItemModel* _Model)
{
	PSWidget->setProductsModel(_Model); 
}

void PhotoSelPage::setImagesPerSheet(int _Value)
{
	PSWidget->setImagesPerSheet(_Value); 
}

bool PhotoSelPage::hasChanges() const 
{
	return PSWidget->hasChanges(); 
}

void PhotoSelPage::setFilterEnabled(bool _Value)
{
	PSWidget->setFilterEnabled(_Value); 
}
