/****************************************************************************
**
** Copyright (C) 2006-2010 SoftTopia. All rights reserved.
**
** This file is part of SoftTopia Software.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** SoftTopia reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "pubdatabasemanager.h"
#include <QApplication>

#include "ttpopsapp.h"
#include "remotepublishermanager.h"
#include "defaultdbselectiondialog.h"
#include "systemtemplates.h"

void PubDatabaseManager::createNewPublisherDB()
{
	DefaultDbSelectionDialog Dialog;
	if (Dialog.exec() == QDialog::Accepted)
	{
		DefaultSindarDatabase DefaultSindarDb = Dialog.selectedDatabase();
		try
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
			pManager->addDefaultProducts(DefaultSindarDb);
			QApplication::restoreOverrideCursor();
		}
		catch(...)
		{
			QApplication::restoreOverrideCursor();
			throw;
		}
	}
}

void PubDatabaseManager::checkForPublisherDB()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	pManager->pullProducts();
	QApplication::restoreOverrideCursor();
	//If there is no products db ask to create some default db ?
	if (!pManager->productsDBExist())
	{
		createNewPublisherDB();
	}
}

void PubDatabaseManager::updatePublisherDBTemplateProducts()
{
	SPhotoBook::TemplateInfoList Templates = TtpopsApp::loadTemplates();
	STDom::PublisherDatabase ProductsDB = pManager->getProducts();
	pManager->updateTemplateProducts(ProductsDB, Templates);
}
