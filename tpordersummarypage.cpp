/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "tpordersummarypage.h"
#include <QLayout> 
#include <QLabel> 
#include <QToolButton> 

#include "publisherdatabase.h"
#include "sprocessstatuswidget.h"

void TPOrderSummaryPage::retranslateUi()
{
	ButPrevious->setText(tr("Photos")); 
	ButAccept->setText(tr("Ok"));
	ButCancel->setText(tr("Cancel")); 
}

QToolButton* TPOrderSummaryPage::newActionButton(const QIcon& _Icon)
{
	QToolButton* ButRes = new QToolButton(this); 
	ButRes->setIcon(_Icon);
	ButRes->setIconSize(QSize(128, 128));
	ButRes->setToolButtonStyle(Qt::ToolButtonTextUnderIcon); 
	ButRes->setObjectName("ActionButton"); 
	return ButRes; 
}

TPOrderSummaryPage::TPOrderSummaryPage(QWidget* _Parent): STIWizardPage(_Parent)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	HeaderLabel = new QLabel(this); 
	HeaderLabel->setObjectName("SummaryHeaderLabel");
	HeaderLabel->setAlignment(Qt::AlignHCenter); 
	MLayout->addWidget(HeaderLabel);

	SummaryLabel = new QLabel(this);
	SummaryLabel->setObjectName("SummaryLabel");
	SummaryLabel->setAlignment(Qt::AlignTop | Qt::AlignHCenter); 
	SummaryLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::MinimumExpanding);
	MLayout->addWidget(SummaryLabel); 
	MLayout->setAlignment(SummaryLabel, Qt::AlignHCenter);

	StatW = new SProcessStatusWidget(this);
	StatW->setFixedWidth(600);
	MLayout->addWidget(StatW);
	MLayout->setAlignment(StatW, Qt::AlignCenter);
	
	MLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));

	QGridLayout* ButLayout = new QGridLayout;
	MLayout->addLayout(ButLayout); 

	ButPrevious = newActionButton(QIcon(":/st/tpopsl/previous.png"));
	connect(ButPrevious, SIGNAL(clicked( bool )), this, SIGNAL(previousPage())); 
	ButLayout->addWidget(ButPrevious, 0, 0); 

	ButCancel = newActionButton(QIcon(":/st/tpopsl/cancel.png"));
	connect(ButCancel, SIGNAL(clicked( bool )), this, SIGNAL(orderCanceled())); 
	ButLayout->addWidget(ButCancel, 1, 0); 

	
	ButLayout->addItem(new QSpacerItem(10, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Preferred), 0, 1); 

	ButAccept = newActionButton(QIcon(":/st/tpopsl/accept.png"));
	connect(ButAccept, SIGNAL(clicked( bool )), this, SIGNAL(orderAccepted())); 
	ButLayout->addWidget(ButAccept, 1, 2); 
}

void TPOrderSummaryPage::init()
{
	retranslateUi(); 
	setStatus(StatusShowSummary);
	calcBill();
}

void TPOrderSummaryPage::setPrintJob(const STDom::PrintJob& _PrintJob)
{
	MPrintJob = _PrintJob;
	calcBill();
}


void TPOrderSummaryPage::calcBill()
{
	STDom::PublisherDatabase PublDB;
	QString StrBill = PublDB.billRitchText(MPrintJob, ShippingMethod);
	SummaryLabel->setText(StrBill);
}

void TPOrderSummaryPage::setStatus(EnStatus _Status)
{
	StatW->setVisible(_Status == StatusShowProgress);
	SummaryLabel->setVisible(_Status == StatusShowSummary);
	switch (_Status)
	{
		case StatusShowSummary:
			HeaderLabel->setText(tr("<h1>This is your Order Summary.</h1>"));
		break;
		case StatusShowProgress:
			HeaderLabel->setText(tr("<h1>Processing...</h1>"));
		break;
	}
}
