/****************************************************************************
**
** Copyright (C) 2006-2010 SoftTopia. All rights reserved.
**
** This file is part of SoftTopia Software.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** SoftTopia reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef IORDERWIZARD_H
#define IORDERWIZARD_H
#include "printjob.h"
#include "publisherdatabase.h"
#include "sterror.h"

class IOrderWizard
{
public:
	ST_DECLARE_ERRORCLASS();

private:
	QAbstractItemModel* ProductsModel;

public:
	IOrderWizard();
	virtual STDom::PrintJob getPrintJob() const = 0;
	virtual void setImages(const QFileInfoList& _Images, const QDir& _RootDir) = 0;
	QAbstractItemModel* newProductsModel(QObject* _Parent, STDom::PublisherDatabase::EnProductType _ProductType, const QString& _TemplateName = "" );
};

#endif // IORDERWIZARD_H
