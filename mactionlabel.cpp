/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "mactionlabel.h"
#include <QLayout>
#include <QDesktopWidget>
#include <QAction>
#include <QDebug>
#include "stclickablelabel.h"

// _________________________________________________________________________*/
//
//   Class ClickableWebView
// _________________________________________________________________________*/

ClickableWebView::ClickableWebView(QWidget* _Parent) : QWebView(_Parent)
{
}

void ClickableWebView::mouseReleaseEvent(QMouseEvent* )
{
	emit clicked();
}

// _________________________________________________________________________*/
//
//   Class MActionLabel
// _________________________________________________________________________*/

bool MActionLabel::showingInfo() const
{
	bool Res = true;
	if (WebView)
		Res = WebView->isVisible();
	return Res;
}

MActionLabel::MActionLabel ( QAction* _Action, const QString& _Description, const QUrl& _RemoteInfo, QWidget* _Parent, bool _Wide ) : QFrame ( _Parent ),
Action ( _Action ), RemoteInfo(_RemoteInfo), WebView(0)
{
	setObjectName ( "ModuleLabel" );

	QVBoxLayout* MLayout = new QVBoxLayout(this);

	QHBoxLayout* LabelLayout = new QHBoxLayout;
	MLayout->addLayout(LabelLayout);
	STClickableLabel* PixLabel = new STClickableLabel ( this );
	PixLabel->setPixmap ( _Action->icon().pixmap ( QSize ( 128, 128 ) ) );
	LabelLayout->addWidget ( PixLabel );
	connect ( PixLabel, SIGNAL ( clicked() ), this, SLOT ( slotLabelClicked() ) );

	DescLabel = new STClickableLabel ( this );
	if ( _Description.isEmpty() )
		DescLabel->setText ( QString ( "<h1>%1</h1>" ).arg ( _Action->text() ) );
	else
		DescLabel->setText ( QString ( "<h1>%1</h1><h4>%2</h4>" ).arg ( _Action->text() ).arg ( _Description ) );

	DescLabel->setSizePolicy ( QSizePolicy::MinimumExpanding, QSizePolicy::Preferred );
	LabelLayout->addWidget ( DescLabel );
	connect ( DescLabel, SIGNAL ( clicked() ), this, SLOT ( slotLabelClicked() ) );

	if ( !_RemoteInfo.isEmpty() && _RemoteInfo.isValid() )
	{
		STClickableLabel* GoForwardLabel = new STClickableLabel ( this );
		GoForwardLabel->setPixmap(QPixmap(":/open_moreimages.png"));
		//PriceLabel->setText ( tr ( "From %1 %2" ).arg ( 0).arg ( trUtf8 ( "\xe2\x82\xac" ) ) );
		GoForwardLabel->setSizePolicy ( QSizePolicy::Maximum, QSizePolicy::Preferred );
		LabelLayout->addWidget ( GoForwardLabel );
		connect ( GoForwardLabel, SIGNAL ( clicked() ), this, SLOT ( slotDoAction() ) );
		WebView = new ClickableWebView(this);
		WebView->setMinimumWidth(460);
		WebView->setMinimumHeight(500);
		MLayout->addWidget(WebView);
		connect(WebView, SIGNAL(clicked()), this, SLOT(slotDoAction()));
		WebView->setVisible(false);
	}

	QDesktopWidget Desktop;
	if (_Wide)
		setFixedWidth ( Desktop.screenGeometry ( Desktop.screenNumber ( this ) ).width() - 70 );
}

void MActionLabel::showUrlInfo(const QUrl& _UrlInfo)
{
	WebView->setVisible(true);
	WebView->stop();
	//! TODO: Correct the following scrappy code
	if (_UrlInfo.toString().toLower().contains("file:"))
	{
		if (_UrlInfo.toString().toLower().contains(".html"))
			WebView->load(_UrlInfo);
		else
			WebView->load(_UrlInfo + ".html");
	}
	else
		WebView->load(_UrlInfo); //Use file:/// for local files.
	//WebView->load(QUrl("file:///Users/jordifernandez/temp/webtemplates/sites.google.com/site/orometemplates/km_magazinea4/index.html"));
}

void MActionLabel::hideInfo()
{
	if (WebView)
		WebView->setVisible(false);
}

void MActionLabel::setTextAlignment ( Qt::Alignment _Alignment )
{
	DescLabel->setAlignment ( _Alignment );
}

void MActionLabel::setText(const QString& _Value)
{
	DescLabel->setText(_Value);
}

void MActionLabel::slotLabelClicked()
{
	if ( !RemoteInfo.isEmpty() && RemoteInfo.isValid() )
	{
		if (showingInfo())
			slotDoAction();
		else
		{
			emit showUrlInfoClicked(RemoteInfo);
			//showUrlInfo(RemoteInfo);
		}
	}
	else
		slotDoAction();
}

void MActionLabel::slotDoAction()
{
	Action->trigger();
}

