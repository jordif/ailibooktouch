/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QDialog>
#include <QFileInfo>
#include "sappsettings.h"
#include "sconfigwidget.h"
#include "smappedwidget.h"
#include "xmlorder.h"
#include "publisherdatabase.h"
#include "stcollectionpublishermodel.h"
#include "stappmodule.h"


/**
Widget class for account app settings.

@author Shadow
*/

class AccountConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	AccountConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};


/**
Widget class for main app settings.
 
@author Shadow
*/
class SMappableComboBox;
class MainConfigWidget : public SMappedWidget
{
Q_OBJECT
	SAppSettingsModel* Model;

public:
	MainConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};


/**
Widget class for main app settings.
 
@author Shadow
*/

class PublisherConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	PublisherConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};

/**
Widget class for main app settings.
 
@author Shadow
*/

class MediaConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	MediaConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);

};

/**
Widget class for photobook settings.
 
@author Shadow
*/

class PhotoBookConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	PhotoBookConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};

/**
Widget class for photoindex settings.
 
@author Shadow
*/

class PhotoIndexConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	PhotoIndexConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};


/**
Database config widget.

@author Shadow
*/
class WProductManager;
class FTSDialog;
class DatabaseConfigWidget : public SMappedWidget
{
Q_OBJECT
	STDom::PublisherDatabase PubDb;
	WProductManager* ManProducts;
	FTSDialog* MDialog;
	SAppSettingsModel* Model;

public:
	DatabaseConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
	void showProductsManager(QWidget* _Parent = 0);
	void setPublisherDb(const STDom::PublisherDatabase& _PubDb) { PubDb = _PubDb; }

private slots:
	void slotSetupPrices();
	void slotRestoreDefaults();
};

/**
Database config widget.

@author Shadow
*/
class TemplatesConfigWidget : public SMappedWidget
{
Q_OBJECT
	SAppSettingsModel* Model;

public:
	TemplatesConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
	void showTemplateManager(QWidget* _Parent = 0);

private slots:
	void slotSetupTemplates();
};



/**
Widget class for output app settings.
 
@author Shadow
*/
class QRadioButton;
class SPrinterSettings; 
class SPrinterSettingsWidget; 
class SMappableComboBox;
class OutputConfigWidget : public SMappedWidget
{
Q_OBJECT
	QRadioButton* RBSysDriver;
	QRadioButton* RBPrinterMode;
	SAppSettingsModel* Model; 
	SMappableComboBox* ComboCropMode;
	QLabel* CropLabel;

public:
	OutputConfigWidget(SPrinterSettings& _Settings, SAppSettingsModel* _Model, SPrinterSettingsWidget* _PrinterSettingsWidget, QDataWidgetMapper* _Mapper, QWidget* _Parent);

private slots: 
	void mapperIndexChanged();
	void printModeToggled(bool _Value);
};

/**
Application settings widget.
 
@author Shadow
*/
class SAppSettingsModel;
class AppSettingsWidget : public SConfigWidget
{
Q_OBJECT
	QDataWidgetMapper* Mapper;
	SAppSettingsModel* Model;
	SPrinterSettingsWidget* SysSettingsWidget;
	DatabaseConfigWidget* DBConfWidget;
	MainConfigWidget* MainConfig;

public:
	AppSettingsWidget(SAppSettingsModel* _Model, QAbstractItemModel* _ProductModel, QAbstractItemModel* _FormatModel, QWidget* _Parent);
	void submit();
	void revert();
	void setPublisherDb(const STDom::PublisherDatabase& _PubDb);


protected:
	QSize sizeHint() const;
};


/**
Application settings dialog.
 
@author Shadow
*/
class QAbstractItemModel; 
class AppSettingsDialog : public QDialog
{
Q_OBJECT
		
	AppSettingsWidget* MainWidget;
	SAppSettings* Settings;
	QAction* AcceptAct; 

public:
	AppSettingsDialog(SAppSettings* _Settings, QAbstractItemModel* _ProductModel, QAbstractItemModel* _FormatModel, QWidget* _Parent = 0);
	void setPublisherDb(const STDom::PublisherDatabase& _Db);

protected slots: 
	virtual void accept();
	virtual void reject();
	void exitApp(); 
	void checkValidSettings(); 
};

/**
Ticket printers config widget.
 
@author Shadow
*/
class STicketPrinterSettings;
class TicketConfigWidget : public SMappedWidget
{
Q_OBJECT
public:
	TicketConfigWidget(STicketPrinterSettings& _Settings, SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent);
};

/**
Settings widgets, model and QSettings inherited class.

	@author
*/
class AppSettings : public SAppSettings
{
public:
	typedef enum EnPrintPreProcessType
	{
		TypeNone,
		TypeBooklet
	};

	enum EnImageCropMode
	{
		ModeCrop,
		ModeWhiteMarginCentered,
		ModeWhiteMarginRight
	};

	enum EnImageAdjustmentMode
	{
		ModeRaw,
		ModeAdjustWeight
	};

private:
Q_OBJECT
public:
	static QString DefaultLocale;
	static int NumCustomPaths;

public:
	AppSettings();
	int mainNewOrderRef();
	static QString mainLocale();
	int mainDpis() const;
	int mainLowResWarningDpis() const;
	QString mainRenderFormat() const;
	QString mainPrintPassword() const;
	QString mainSettingsPassword() const;
	bool mainDirectModuleEnabled() const;
	int mainDirectModuleIndex() const;
	QString mainOrdersFilesPath() const;
	bool mainShowLegalInfo() const;
	bool mainMainMenuEffect() const;
	bool mainRunTomOnOrderCommit() const;
	bool mainAdvancedOM() const;

	#ifdef Q_OS_WIN32
	QFileInfoList mediaExcludedCardDrives() const;
	QFileInfo mediaCdDrive() const;
	#else 
	QFileInfo mediaPath() const;
	#endif 
	QFileInfo mediaBluetoothInputPath() const;
	bool mediaRunEjectEO() const;
	bool mediaCustomPathEnabled(int _Index) const;
	QString mediaCustomPathLabel(int _Index) const;
	QString mediaCustomPath(int _Index) const ;

	QString photoBookRenderFormat() const;
	QString photoBookCoverRenderFormat() const;
	AppSettings::EnImageCropMode imageCropMode() const;
	AppSettings::EnImageAdjustmentMode imageAdjustmentMode() const;
	STDom::XmlOrderDealer sender() const;
	int photoIndexDpis() const;
	bool photoIndexDrawFrames() const;
	bool photoIndexDrawFileNumbers() const;
	bool photoIndexDrawFileNames() const;
	int photoIndexImagesPerPage() const;
	QString photoIndexBackgroundColor() const;
	int photoIndexMargin() const;

	QString templatesBaseInfoUrl() const;

	bool ticketPrintTicket() const;
	bool ticketPrintTicketCopy() const;
	QString ticketHeader() const;
	QString ticketFooter() const;
	QString ticketCompanyName() const;
	QString ticketCompanyAddress() const;

	void setAccountUserName(const QString& _UserName);
	QString accountUserName() const;
	void setAccountPassword(const QString& _Value);
	QString accountPassword() const;
	void setAccountKey(const QString& _Value);
	QString accountKey() const;

	bool askForUserData() const;

	bool databaseSyncPublisherData() const;
	bool databaseKeepLocalPrices() const;
	bool templatesManuallyInstall() const;
	int databaseSyncInterval() const;
	bool isTimeToSync() const;
	void updateSyncTime();

	bool validPublisherData() const;
	bool validSettings() const;
	bool sendToPublisherMode() const;
	int execDialog();
	bool settingsAccess() const;
	bool commitAccess() const;
};

#endif
