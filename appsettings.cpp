/****************************************************************************
**
** Copyright (C) 2006-2008 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.
**
** Starblitz reserves all rights not expressly granted herein.
**
** Strablitz (c) 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "appsettings.h"
#include <QApplication> 
#include <QDataWidgetMapper> 
#include <QLayout> 
#include <QLabel>
#include <QAction>
#include <QTreeView>
#include <QGroupBox>  
#include <QRadioButton>
#include <QButtonGroup>
#include <QScrollArea> 
#include <QToolButton>
#include <QCheckBox>
#include <QLineEdit>

#include "siconfactory.h"
#include "sactionbuttonsframe.h"
#include "fileselectlineedit.h"
#include "smappablecombobox.h"
#include "sprintersettingswidget.h"
#include "stcolorbutton.h" 
#include "sticketprintersettings.h"
#include "sticketprintersettingswidget.h"
#include "stburningsettings.h" 
#include "stburningsettingswidget.h" 
#include "sapplication.h"
#include "tpinputdialog.h"
#include "qxtgroupbox.h"

//GUI
#include "smessagebox.h"
#include "templatemanagerwidget.h"

//Prices
#include "fsqldatabasemanager.h"
#include "wproductmanager.h"
#include "ftsdialog.h"
#include "pubdatabasemanager.h"
#include "publisherinfo.h"
#include "remotepublishermanager.h"
#include "ttpopsapp.h"

//__________________________________________________________________________
//
// Class AccountConfigWidget
//__________________________________________________________________________

AccountConfigWidget::AccountConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent) :
		SMappedWidget(_Model, _Mapper, _Parent)
{
	QGridLayout* MLayout = new QGridLayout(this);

	MLayout->addWidget(createHeaderLabel(_Model->index("account/username")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("account/username")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("account/password")), MLayout->rowCount() , 0);
	QLineEdit* LEPassword = new QLineEdit(this);
	LEPassword->setEchoMode(QLineEdit::PasswordEchoOnEdit);
	_Mapper->addMapping(LEPassword, _Model->index("account/password").column());

	MLayout->addWidget(LEPassword, MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("account/key")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("account/key")), MLayout->rowCount() -1 , 1);

	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
}

//__________________________________________________________________________
//
// Class MainConfigWidget
//__________________________________________________________________________

MainConfigWidget::MainConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent) :
		SMappedWidget(_Model, _Mapper, _Parent), Model(_Model)
{
	QGridLayout* MLayout = new QGridLayout(this);
	
	/*MLayout->addWidget(createHeaderLabel(_Model->index("main/templatepath")), MLayout->rowCount() , 0);
	FileSelectLineEdit* LEPath = new FileSelectLineEdit(FileSelectLineEdit::GetExistingDirectory, this);
	_Mapper->addMapping(LEPath, _Model->index("main/templatepath").column());
	MLayout->addWidget(LEPath, MLayout->rowCount(), 0);*/

	MLayout->addWidget(createHeaderLabel(_Model->index("main/orderref")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/orderref")), MLayout->rowCount() -1 , 1);

	
	MLayout->addWidget(createHeaderLabel(_Model->index("main/locale")), MLayout->rowCount() , 0);
	SMappableComboBox* CBLocale = new SMappableComboBox(this);
	SApplication::TAppLocaleList LocaleList = SApplication::locales(); 
	SApplication::TAppLocaleList::iterator it; 
	for (it = LocaleList.begin(); it != LocaleList.end(); ++it)
	{
		CBLocale->addItem(it->Description, it->Id);
	}
	
// 	CBLocale->addItem(tr("Spanish"), "es");
// 	CBLocale->addItem(tr("English"), "en");
	
	_Mapper->addMapping(CBLocale, _Model->index("main/locale").column());
	MLayout->addWidget(CBLocale, MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/dpis")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/dpis")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/lowreswarningdpis")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/lowreswarningdpis")), MLayout->rowCount() -1 , 1);


	MLayout->addWidget(createHeaderLabel(_Model->index("main/renderformat")), MLayout->rowCount() , 0);
	SMappableComboBox* ComboFormat = new SMappableComboBox(this); 
	ComboFormat->addItem("JPG, Joint Photographic Experts Group", "JPG");
	ComboFormat->addItem("PNG, Portable Network Graphics", "PNG");
	_Mapper->addMapping(ComboFormat, _Model->index("main/renderformat").column());
	MLayout->addWidget(ComboFormat, MLayout->rowCount() -1, 1); 

	MLayout->addWidget(createHeaderLabel(_Model->index("main/printpassword")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/printpassword")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/showlegalinfo")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/showlegalinfo")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/runtomonordercommit")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/runtomonordercommit")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/advancedom")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/advancedom")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/orderfilespath")), MLayout->rowCount(), 0);
	FileSelectLineEdit* LEPath = new FileSelectLineEdit(FileSelectLineEdit::GetExistingDirectory, this);
	_Mapper->addMapping(LEPath, _Model->index("main/orderfilespath").column());
	MLayout->addWidget(LEPath, MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("main/settingspassword")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("main/settingspassword")), MLayout->rowCount() -1 , 1);

	//MLayout->addWidget(createEditor(Model->index("cdrecord/speed")), MLayout->rowCount() - 1, 0);
	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
}



//__________________________________________________________________________
//
// Class PublisherConfigWidget
//__________________________________________________________________________

PublisherConfigWidget::PublisherConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent)
{
	QGridLayout* MLayout = new QGridLayout(this);
	

	MLayout->addWidget(createHeaderLabel(_Model->index("publisher/name"), this, "RequiredSetting"), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("publisher/name"), "", this, "RequiredSetting" ), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("publisher/surname")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("publisher/surname")), MLayout->rowCount() -1 , 1);
	
	QGroupBox* GBContact = new QGroupBox(tr("Contact"), this); 
	MLayout->addWidget(GBContact, MLayout->rowCount() , 0, 1, 2);
	QGridLayout* GBCLayout = new QGridLayout(GBContact);

	GBCLayout->addWidget(createHeaderLabel(_Model->index("publisher/email")), GBCLayout->rowCount() , 0);
	GBCLayout->addWidget(createEditor(_Model->index("publisher/email")), GBCLayout->rowCount() -1 , 1);

	GBCLayout->addWidget(createHeaderLabel(_Model->index("publisher/phone")), GBCLayout->rowCount() , 0);
	GBCLayout->addWidget(createEditor(_Model->index("publisher/phone")), GBCLayout->rowCount() -1 , 1);

	GBCLayout->addWidget(createHeaderLabel(_Model->index("publisher/mobilephone")), GBCLayout->rowCount() -1 , 2);
	GBCLayout->addWidget(createEditor(_Model->index("publisher/mobilephone")), GBCLayout->rowCount() -1 , 3);

	QGroupBox* GBAddr = new QGroupBox(tr("Send Address"), this); 
	MLayout->addWidget(GBAddr, MLayout->rowCount() , 0, 1, 2);
	QGridLayout* GBALayout = new QGridLayout(GBAddr);

	GBALayout->addWidget(createHeaderLabel(_Model->index("publisher/address")), GBALayout->rowCount() , 0);
	GBALayout->addWidget(createEditor(_Model->index("publisher/address")), GBALayout->rowCount() -1 , 1);

	GBALayout->addWidget(createHeaderLabel(_Model->index("publisher/zip")), GBALayout->rowCount() , 0);
	GBALayout->addWidget(createEditor(_Model->index("publisher/zip")), GBALayout->rowCount() -1 , 1);

	GBALayout->addWidget(createHeaderLabel(_Model->index("publisher/city")), GBALayout->rowCount() -1 , 2);
	GBALayout->addWidget(createEditor(_Model->index("publisher/city")), GBALayout->rowCount() -1 , 3);

	GBALayout->addWidget(createHeaderLabel(_Model->index("publisher/country")), GBALayout->rowCount() , 0);
	GBALayout->addWidget(createEditor(_Model->index("publisher/country")), GBALayout->rowCount() -1 , 1);

	GBALayout->addWidget(createHeaderLabel(_Model->index("publisher/state")), GBALayout->rowCount() -1 , 2);
	GBALayout->addWidget(createEditor(_Model->index("publisher/state")), GBALayout->rowCount() -1 , 3);


	QGroupBox* GBOther = new QGroupBox(tr("Orher Data"), this); 
	MLayout->addWidget(GBOther, MLayout->rowCount() , 0, 1, 2);
	QGridLayout* GBOLayout = new QGridLayout(GBOther);

	GBOLayout->addWidget(createHeaderLabel(_Model->index("publisher/accountnumber"), this, "RequiredSetting"), GBOLayout->rowCount() , 0);
	GBOLayout->addWidget(createEditor(_Model->index("publisher/accountnumber"), "", this, "RequiredSetting"), GBOLayout->rowCount() -1 , 1);

	GBOLayout->addWidget(createHeaderLabel(_Model->index("publisher/transportroute")), GBOLayout->rowCount() , 0);
	GBOLayout->addWidget(createEditor(_Model->index("publisher/transportroute")), GBOLayout->rowCount() -1 , 1);

	QLabel* ReqLabel = new QLabel(tr("Required fields")); 
	ReqLabel->setObjectName("RequiredSetting");
	MLayout->addWidget(ReqLabel, MLayout->rowCount(), 0); 
	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
	
}

//__________________________________________________________________________
//
// Class PhotoBookConfigWidget
//__________________________________________________________________________

PhotoBookConfigWidget::PhotoBookConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent)
{
	QVBoxLayout* MLayout = new QVBoxLayout(this);
	
	MLayout->addWidget(createHeaderLabel(_Model->index("photobook/renderformat")));
	SMappableComboBox* ComboFormat = new SMappableComboBox(this); 
	ComboFormat->addItem("JPG, Joint Photographic Experts Group", "JPG");
	ComboFormat->addItem("PNG, Portable Network Graphics", "PNG");
	ComboFormat->addItem("PBM, Portable Bitmap", "PBM");
	ComboFormat->addItem("BMP, Windows Bitmap", "BMP");
	ComboFormat->addItem(QIcon(":/pdf.png"), "PDF, Portable Document Format", "PDF");
	_Mapper->addMapping(ComboFormat, _Model->index("photobook/renderformat").column());
	MLayout->addWidget(ComboFormat);


	MLayout->addWidget(createHeaderLabel(_Model->index("photobook/coverrenderformat")));
	ComboFormat = new SMappableComboBox(this); 
	ComboFormat->addItem("JPG, Joint Photographic Experts Group", "JPG");
	ComboFormat->addItem("PNG, Portable Network Graphics", "PNG");
	ComboFormat->addItem("PBM, Portable Bitmap", "PBM");
	ComboFormat->addItem("BMP, Windows Bitmap", "BMP");
	ComboFormat->addItem(QIcon(":/pdf.png"), "PDF, Portable Document Format", "PDF");
	_Mapper->addMapping(ComboFormat, _Model->index("photobook/coverrenderformat").column());
	MLayout->addWidget(ComboFormat);
	
	
	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding));
}

//__________________________________________________________________________
//
// Class PhotoIndex PhotoIndexConfigWidget
//__________________________________________________________________________

PhotoIndexConfigWidget::PhotoIndexConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent)
{
	QGridLayout* MLayout = new QGridLayout(this);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/dpis")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/dpis")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/drawframes")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/drawframes")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/drawfilenumbers")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/drawfilenumbers")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/drawfilenames")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/drawfilenames")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/imagesperpage")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/imagesperpage")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/margin")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("photoindex/margin")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("photoindex/backgroundcolor")), MLayout->rowCount() , 0);
	STColorButton* ColButton = new STColorButton(this); 
	ColButton->setText(tr("Click to Select")); 
	_Mapper->addMapping(ColButton, _Model->index("photoindex/backgroundcolor").column());
	MLayout->addWidget(ColButton, MLayout->rowCount(), 0); 

	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
}



//__________________________________________________________________________
//
// Class DatabaseConfigWidget
//__________________________________________________________________________

DatabaseConfigWidget::DatabaseConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent), ManProducts(0), Model(_Model)
{
	QGridLayout* MLayout = new QGridLayout(this);

	QToolButton* MPButton = new QToolButton(this);
	connect(MPButton, SIGNAL(clicked()), this, SLOT(slotSetupPrices()));
	MPButton->setText(tr("Click to setup prices..."));
	MPButton->setIcon(QIcon(":/setupprices.png"));
	MPButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	MPButton->setIconSize(QSize(64,64));

	QToolButton* RestoreDefaultsButton = new QToolButton(this);
	connect(RestoreDefaultsButton, SIGNAL(clicked()), this, SLOT(slotRestoreDefaults()));
	RestoreDefaultsButton->setText(tr("Restore default database Values"));
	RestoreDefaultsButton->setIcon(QIcon(":/restoredbdefaults.png"));
	RestoreDefaultsButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	RestoreDefaultsButton->setIconSize(QSize(64,64));

	QxtGroupBox* GBSyncPubData = new QxtGroupBox(headerLabelText(Model->index("database/syncpublisherdata")), this);
	_Mapper->addMapping(GBSyncPubData, Model->index("database/syncpublisherdata").column());
	MLayout->addWidget(GBSyncPubData, MLayout->rowCount() , 0, 1, 2);
	QHBoxLayout* GBSyncPubDataLayout = new QHBoxLayout(GBSyncPubData);

	GBSyncPubDataLayout->addWidget(createHeaderLabel(_Model->index("database/syncinterval")));
	GBSyncPubDataLayout->addWidget(createEditor(_Model->index("database/syncinterval")));


	MLayout->addWidget(createHeaderLabel(_Model->index("database/keeplocalprices")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("database/keeplocalprices")), MLayout->rowCount() -1 , 1);

	MLayout->addWidget(MPButton, MLayout->rowCount() , 0, 1, 2);
	MLayout->setAlignment(MPButton, Qt::AlignCenter);

	MLayout->addWidget(RestoreDefaultsButton, MLayout->rowCount() , 0, 1, 2);
	MLayout->setAlignment(RestoreDefaultsButton, Qt::AlignCenter);

	MDialog = new FTSDialog(this);
	MDialog->addAction(new FAcceptAction(this));
	//MDialog->addAction(new FCancelAction(this));

	connect(_Model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(slotDataChanged(QModelIndex,QModelIndex)));

	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
}

void DatabaseConfigWidget::showProductsManager(QWidget* _Parent)
{
	try
	{
		FSqlDatabaseManager Manager(PubDb, ":/");
		FSqlDatabaseManager::addManager(Manager);

		if (ManProducts)
			delete ManProducts;
		ManProducts = new WProductManager(this, PubDb);
		ManProducts->setLocalAdded(true);
		MDialog->setMainWidget(ManProducts);
		MDialog->showMaximized();
		MDialog->exec();
		//OMPDialog.sync();

	}
	catch(STError& _Error)
	{
		SMessageBox::critical(_Parent, tr("Setting up prices"),tr("Error setting up prices: '%1'.\n").arg(_Error.description()));
	}
}


void DatabaseConfigWidget::slotSetupPrices()
{
	showProductsManager(this);
}

void DatabaseConfigWidget::slotRestoreDefaults()
{
	if (SMessageBox::question(this, tr("Restoring defaults"), tr("Are you sure you want to restore database to it defaults values ?")) == QMessageBox::Yes)
	{
		try
		{
			if (PubDb.driverName() == "QSQLITE") //Defensive
			{
				PubDb.close();
				PubDatabaseManager::createNewPublisherDB();
				PubDatabaseManager::updatePublisherDBTemplateProducts();
				PubDb.open();
				SMessageBox::information(this, tr("Restoring defaults"), tr("Default settings restored"));
			}
		}
		catch(STError& _Error)
		{
			SMessageBox::critical(this, tr("Restoring defaults"),tr("Error restoring defaults: '%1'.\n").arg(_Error.description()));
		}
	}
}


//__________________________________________________________________________
//
// Class TemplatesConfigWidget
//__________________________________________________________________________


TemplatesConfigWidget::TemplatesConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent), Model(_Model)
{
	QGridLayout* MLayout = new QGridLayout(this);

	QToolButton* MPButton = new QToolButton(this);
	connect(MPButton, SIGNAL(clicked()), this, SLOT(slotSetupTemplates()));
	MPButton->setText(tr("Click to setup templates..."));
	MPButton->setIcon(QIcon(":/setuptemplates.png"));
	MPButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	MPButton->setIconSize(QSize(64,64));

	QxtGroupBox* GBSyncPubData = new QxtGroupBox(headerLabelText(Model->index("templates/manualinstall")), this);
	_Mapper->addMapping(GBSyncPubData, Model->index("templates/manualinstall").column());
	MLayout->addWidget(GBSyncPubData, MLayout->rowCount() , 0, 1, 2);
	QHBoxLayout* GBSyncPubDataLayout = new QHBoxLayout(GBSyncPubData);

	GBSyncPubDataLayout->addWidget(MPButton);


	//MDialog->addAction(new FCancelAction(this));

	connect(_Model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(slotDataChanged(QModelIndex,QModelIndex)));

	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);
}

void TemplatesConfigWidget::showTemplateManager(QWidget* _Parent)
{
	try
	{

		FTSDialog* MDialog = new FTSDialog(this);
		MDialog->addAction(new FAcceptAction(this));
		TemplateManagerWidget* TemplateManag = new TemplateManagerWidget(MDialog);
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		TemplateManag->loadTemplates();
		QApplication::restoreOverrideCursor();
		MDialog->setMainWidget(TemplateManag);
		MDialog->showMaximized();
		MDialog->exec();
		PubDatabaseManager::updatePublisherDBTemplateProducts();
		delete TemplateManag;
		delete MDialog;

		/*FSqlDatabaseManager Manager(PubDb, ":/");
		FSqlDatabaseManager::addManager(Manager);

		if (ManProducts)
			delete ManProducts;
		ManProducts = new WProductManager(this, PubDb);
		ManProducts->setLocalAdded(true);


		MDialog->setMainWidget(ManProducts);
		MDialog->showMaximized();
		MDialog->exec();
		//OMPDialog.sync();*/

	}
	catch(STError& _Error)
	{
		SMessageBox::critical(_Parent, tr("Setting up prices"),tr("Error setting up prices: '%1'.\n").arg(_Error.description()));
	}
}

void TemplatesConfigWidget::slotSetupTemplates()
{
	showTemplateManager(this);
}


//__________________________________________________________________________
//
// Class OutputConfigWidget
//__________________________________________________________________________

OutputConfigWidget::OutputConfigWidget(SPrinterSettings& _PrinterSettings, SAppSettingsModel* _Model, SPrinterSettingsWidget* _PrinterSettingsWidget, QDataWidgetMapper* _Mapper, QWidget* _Parent) :
		SMappedWidget(_Model, _Mapper, _Parent), Model(_Model)
{
	connect(_Mapper, SIGNAL(currentIndexChanged( int )), this, SLOT(mapperIndexChanged())); 	
	QVBoxLayout* MLayout = new QVBoxLayout(this);
	QHBoxLayout* HLayout = new QHBoxLayout; 
	MLayout->addLayout(HLayout); 

	QButtonGroup* MButtonGroup = new QButtonGroup(this);
#ifndef STANDARD_EDITION
	RBPrinterMode = new QRadioButton(headerLabelText(_Model->index("printing/sendtopublishermode")), this);
	HLayout->addWidget(RBPrinterMode);
	MButtonGroup->addButton(RBPrinterMode); 
	_Mapper->addMapping(RBPrinterMode, _Model->index("printing/sendtopublishermode").column());
#endif
#ifndef INETKIT_EDITION
	RBSysDriver = new QRadioButton(tr("Use system printer driver"), this); 
	connect(RBSysDriver, SIGNAL(toggled( bool )), this, SLOT(printModeToggled(bool ))); 
	HLayout->addWidget(RBSysDriver); 
	MButtonGroup->addButton(RBSysDriver);
#endif

	QHBoxLayout* MidLayout = new QHBoxLayout;
	MLayout->addLayout(MidLayout);
	MidLayout->addWidget(createHeaderLabel(_Model->index("printing/askforuserdata")));
	MidLayout->addWidget(createEditor(_Model->index("printing/askforuserdata")));
	//connect(RBSysDriver, SIGNAL(toggled( bool )), EditorAsk, SLOT(setHidden(bool )));

	CropLabel = createHeaderLabel(_Model->index("printing/imagecropmode"));
	CropLabel->setVisible(false);
	MLayout->addWidget(CropLabel);

	ComboCropMode = new SMappableComboBox(this);
	ComboCropMode->addItem(tr("Crop Images"), AppSettings::ModeCrop);
	ComboCropMode->addItem(tr("Don't crop images and leave white margin centered."), AppSettings::ModeWhiteMarginCentered);
	ComboCropMode->addItem(tr("Don't crop images and leave white margin on the right side."), AppSettings::ModeWhiteMarginCentered);
	ComboCropMode->setVisible(false);
	_Mapper->addMapping(ComboCropMode, _Model->index("printing/imagecropmode").column());
	MLayout->addWidget(ComboCropMode);


	MLayout->addWidget(createHeaderLabel(_Model->index("printing/imageadjustmode")));

	SMappableComboBox* ComboImageAdjustMode = new SMappableComboBox(this);
	ComboImageAdjustMode->addItem(tr("Send original image files"), AppSettings::ModeRaw);
	ComboImageAdjustMode->addItem(tr("Adjust image weight for selected format."), AppSettings::ModeAdjustWeight);
	_Mapper->addMapping(ComboImageAdjustMode, _Model->index("printing/imageadjustmode").column());
	MLayout->addWidget(ComboImageAdjustMode);

	//connect(RBSysDriver, SIGNAL(toggled( bool )), ComboCropMode, SLOT(setVisible(bool )));
	//connect(RBSysDriver, SIGNAL(toggled( bool )), CropLabel, SLOT(setVisible(bool )));


	QScrollArea* ScrAPrinterSettingsW = new QScrollArea(this); 
	ScrAPrinterSettingsW->setWidgetResizable(true); 
	ScrAPrinterSettingsW->setWidget(_PrinterSettingsWidget); 
	//_PrinterSettingsWidget->layout()->setSizeConstraint(QLayout::SetMinAndMaxSize);
 
	MLayout->addWidget(ScrAPrinterSettingsW);
#ifndef INETKIT_EDITION
	connect(RBSysDriver, SIGNAL(toggled( bool )), ScrAPrinterSettingsW, SLOT(setVisible(bool ))); 
#endif
	
	MLayout->addItem(new QSpacerItem(0, 10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding)); 
	ScrAPrinterSettingsW->setVisible(false);

#ifdef STANDARD_EDITION
	printModeToggled(true);
	RBSysDriver->setChecked(true);
#endif
#ifdef INETKIT_EDITION
	printModeToggled(false);
	RBPrinterMode->setChecked(true);
#endif

}

void OutputConfigWidget::mapperIndexChanged()
{
#ifdef PREMIUM_EDITION
	RBSysDriver->setChecked(!RBPrinterMode->isChecked());
#endif
}

void OutputConfigWidget::printModeToggled(bool _Value)
{
	Model->setData(Model->index("printing/sendtopublishermode"), !_Value); 
	ComboCropMode->setVisible(_Value);
	CropLabel->setVisible(_Value);
}


//__________________________________________________________________________
//
// Class MediaConfigWidget
//__________________________________________________________________________

MediaConfigWidget::MediaConfigWidget(SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent):
		SMappedWidget(_Model, _Mapper, _Parent)
{
	QGridLayout* MLayout = new QGridLayout(this);
	
	#ifdef Q_OS_WIN32
		MLayout->addWidget(createHeaderLabel(_Model->index("media/excludedcarddrives")), MLayout->rowCount() , 0);
		MLayout->addWidget(createEditor(_Model->index("media/excludedcarddrives")), MLayout->rowCount() -1 , 1);
	
		MLayout->addWidget(createHeaderLabel(_Model->index("media/cddrive")), MLayout->rowCount() , 0);
		MLayout->addWidget(createEditor(_Model->index("media/cddrive")), MLayout->rowCount() -1 , 1);
	#else 

		MLayout->addWidget(createHeaderLabel(_Model->index("media/mediapath")), 0, 0);
		FileSelectLineEdit* LEPath = new FileSelectLineEdit(FileSelectLineEdit::GetExistingDirectory, this);
		_Mapper->addMapping(LEPath, _Model->index("media/mediapath").column());
		MLayout->addWidget(LEPath, MLayout->rowCount(), 0);
	#endif 

	MLayout->addWidget(createHeaderLabel(_Model->index("media/bluetoothinputpath")), MLayout->rowCount(), 0);
	QLabel* WarningLabel = new QLabel(tr("<b>Warning:</b> This directory is deleted every time we get the media screen."));
	WarningLabel->setObjectName("WarningLabel");
	MLayout->addWidget(WarningLabel, MLayout->rowCount(), 0);

	FileSelectLineEdit* LEBTPath = new FileSelectLineEdit(FileSelectLineEdit::GetExistingDirectory, this);
	_Mapper->addMapping(LEBTPath, _Model->index("media/bluetoothinputpath").column());
	MLayout->addWidget(LEBTPath, MLayout->rowCount(), 0);

	MLayout->addWidget(createHeaderLabel(_Model->index("media/runejectateo")), MLayout->rowCount() , 0);
	MLayout->addWidget(createEditor(_Model->index("media/runejectateo")), MLayout->rowCount() -1 , 1);

	QLabel* CustomSourcesLabel = new QLabel(tr("Custom Media Paths"), this);
	CustomSourcesLabel->setAlignment(Qt::AlignCenter);
	CustomSourcesLabel->setObjectName("ConfigPageLabel");
	MLayout->addWidget(CustomSourcesLabel, MLayout->rowCount(), 0, 1, 2);

	for (int Vfor = 0; Vfor < AppSettings::NumCustomPaths; Vfor++)
	{
		QHBoxLayout* CPathLayout = new QHBoxLayout;
		QCheckBox* CheckEnabled = new QCheckBox(this);
		_Mapper->addMapping(CheckEnabled, _Model->index(QString("media/custompath%1enabled").arg(Vfor)).column());
		CPathLayout->addWidget(CheckEnabled);

		CPathLayout->addWidget(createHeaderLabel(_Model->index(QString("media/custompath%1").arg(Vfor))));
		QWidget* LabelEditor = createEditor(_Model->index(QString("media/custompath%1label").arg(Vfor)));
		CPathLayout->addWidget(LabelEditor);
		FileSelectLineEdit* NewLEPath = new FileSelectLineEdit(FileSelectLineEdit::GetExistingDirectory, this);
		_Mapper->addMapping(NewLEPath, _Model->index(QString("media/custompath%1").arg(Vfor)).column());
		CPathLayout->addWidget(NewLEPath, MLayout->rowCount(), 0);
		NewLEPath->setEnabled(false);
		LabelEditor->setEnabled(false);
		connect(CheckEnabled, SIGNAL(toggled(bool)), NewLEPath, SLOT(setEnabled(bool)));
		connect(CheckEnabled, SIGNAL(toggled(bool)), LabelEditor, SLOT(setEnabled(bool)));

		MLayout->addLayout(CPathLayout, MLayout->rowCount(), 0, 1, 2);
	}

	MLayout->addItem(new QSpacerItem(0,10, QSizePolicy::Preferred, QSizePolicy::MinimumExpanding), MLayout->rowCount(), 0);	
}



// _________________________________________________________________________*/
//
//	Class TicketConfigWidget
// _________________________________________________________________________*/

TicketConfigWidget::TicketConfigWidget(STicketPrinterSettings& _Settings, SAppSettingsModel* _Model, QDataWidgetMapper* _Mapper, QWidget* _Parent) :	SMappedWidget(_Model, _Mapper, _Parent)
{
	//TODO: Posar-hi la descripció !
	QGridLayout* MLayout = new QGridLayout(this);

	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/printticket")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/printticket")), MLayout->rowCount() -1, 1);
	
	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/printticketcopy")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/printticketcopy")), MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/header")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/header")), MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/footer")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/footer")), MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/companyname")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/companyname")), MLayout->rowCount() -1, 1);

	MLayout->addWidget(createHeaderLabel(_Model->index("ticket/companyaddress")), MLayout->rowCount(), 0);
	MLayout->addWidget(createEditor(_Model->index("ticket/companyaddress")), MLayout->rowCount() -1, 1);

	
	//--- Tiquet printer configuration --- 
	STicketPrinterSettingsWidget* PrintConfig = new STicketPrinterSettingsWidget(_Settings, _Mapper, this);
	MLayout->addWidget(PrintConfig, MLayout->rowCount(), 0, 1, 2);
}

//__________________________________________________________________________
//
// Class AppSettingsWidget
//__________________________________________________________________________

AppSettingsWidget::AppSettingsWidget(SAppSettingsModel* _Model, QAbstractItemModel* _ProductModel, QAbstractItemModel* _FormatModel, QWidget* _Parent)
	: SConfigWidget(_Parent), Model(_Model)
{
	SPrinterSettings PrnSettings; 
	STicketPrinterSettings TPSettings; 
	STBurningSettings BurnSettings; 

	_Model->fetch();
	Mapper = new QDataWidgetMapper(this); 
	Mapper->setModel(_Model);
	
	PublisherConfigWidget* PubConfig = new PublisherConfigWidget(_Model, Mapper, this);
	addWidget(PubConfig, QIcon(":/publisher.png"), tr("Publisher"));

#ifndef STANDARD_EDITION
	SysSettingsWidget = new SPrinterSettingsWidget(PrnSettings, Mapper, _Parent, true);
#else
	SysSettingsWidget = new SPrinterSettingsWidget(PrnSettings, Mapper, _Parent, false);
#endif
	SysSettingsWidget->setProductModel(_ProductModel, STDom::PublisherDatabase::digiprintProdDisplayColumn(),
							 STDom::PublisherDatabase::digiprintProdKeyColumn());
	SysSettingsWidget->loadProductPrinters(); 

	SysSettingsWidget->setFormatsModel(_FormatModel, STDom::PublisherDatabase::digiprintFormatDisplayColumn(),
							 STDom::PublisherDatabase::digiprintFormatKeyColumn());
	SysSettingsWidget->loadFormatPrinters();


	OutputConfigWidget* OutConfig = new OutputConfigWidget(PrnSettings, _Model, SysSettingsWidget, Mapper, this);
	addWidget(OutConfig, QIcon(":/printoutput.png"), tr("Print Output"));

#ifndef INETKIT_EDITION
	AccountConfigWidget* AccountConfig = new AccountConfigWidget(_Model, Mapper, this);
	addWidget(AccountConfig, QIcon(":/userconfig.png"), tr("User Account"));
#endif

	MainConfig = new MainConfigWidget(_Model, Mapper, this);
	addWidget(MainConfig, QIcon(":/mainconfig.png"), tr("Main"));

	MediaConfigWidget* MediaConfig = new MediaConfigWidget(_Model, Mapper, this);
	addWidget(MediaConfig, QIcon(":/sd_mmc.png"), tr("Media"));

	PhotoBookConfigWidget* PhotoBookConfig = new PhotoBookConfigWidget(_Model, Mapper, this);
	addWidget(PhotoBookConfig , QIcon(":/photobook.png"), tr("PhotoBook"));

	PhotoIndexConfigWidget* PhotoIndexConfig = new PhotoIndexConfigWidget(_Model, Mapper, this);
	addWidget(PhotoIndexConfig, QIcon(":/photoindex.png"), tr("PhotoIndex"));

	TicketConfigWidget* PrintCWidget = new TicketConfigWidget(TPSettings, _Model, Mapper, this);
	addWidget(PrintCWidget, QIcon(":/ticketprinter.png"), tr("Ticket Printer"));

	STBurningSettingsWidget* BurnWidget = new STBurningSettingsWidget(BurnSettings, Mapper, this);
	addWidget(BurnWidget, QIcon(":/cdburn.png"), tr("Burning"));

	DBConfWidget = new DatabaseConfigWidget(_Model, Mapper, this);
	addWidget(DBConfWidget, QIcon(":/databaseconfig.png"), tr("Database"));

	TemplatesConfigWidget* TConfigWidget = new TemplatesConfigWidget(_Model, Mapper, this);
	addWidget(TConfigWidget, QIcon(":/templatemanager.png"), tr("Templates"));
//#include "fdbconfigwidget.h"
// 	FDBConfigWidget* DBConfWidget = new FDBConfigWidget(Mapper, this); 
// 	addWidget(DBConfWidget, QIcon(":databaseconfig.png"), tr("Database"));
		
	Mapper->toFirst();	
}

void AppSettingsWidget::submit()
{
	Mapper->submit();
	Model->submit();
	SysSettingsWidget->saveProductPrinters(); 
	SysSettingsWidget->saveFormatPrinters();
}

void AppSettingsWidget::revert()
{
	Mapper->revert();
	SysSettingsWidget->loadProductPrinters(); 
	SysSettingsWidget->loadFormatPrinters();
}

QSize AppSettingsWidget::sizeHint() const
{
	return QSize(900, 500);
}

void AppSettingsWidget::setPublisherDb(const STDom::PublisherDatabase& _Db)
{
	DBConfWidget->setPublisherDb(_Db);
}

//__________________________________________________________________________
//
// Class AppSettingsDialog
//__________________________________________________________________________

AppSettingsDialog::AppSettingsDialog(SAppSettings* _Settings, QAbstractItemModel* _ProductModel, QAbstractItemModel* _FormatModel, QWidget* _Parent) : QDialog(_Parent), Settings(_Settings)
{
	SAppSettingsModel* Model = _Settings->model(); 
	setWindowTitle(tr("TTPops settings dialog"));
	QVBoxLayout* MLayout = new QVBoxLayout(this); 
	MainWidget = new AppSettingsWidget(Model, _ProductModel, _FormatModel, this);
	MLayout->addWidget(MainWidget);
	
	AcceptAct = new QAction(SIconFactory::defaultFactory()->icon(SIconFactory::Accept), tr("&Ok"), this);
	AcceptAct->setShortcut(Qt::Key_Return);
	connect(AcceptAct, SIGNAL(triggered()), this, SLOT(accept()));
	
	QAction* CancelAct = new QAction(SIconFactory::defaultFactory()->icon(SIconFactory::Cancel), tr("&Cancel"), this);
	CancelAct->setShortcut(Qt::Key_Escape);
	connect(CancelAct, SIGNAL(triggered()), this, SLOT(reject()));

	QAction* ExitAct = new QAction(SIconFactory::defaultFactory()->icon(SIconFactory::Exit), tr("&Exit"), this);
	connect(ExitAct, SIGNAL(triggered()), this, SLOT(exitApp()));

	
	SActionButtonsFrame* ActButtons = new SActionButtonsFrame(this); 
	ActButtons->addAction(AcceptAct);
	ActButtons->addAction(CancelAct);
	ActButtons->addAction(ExitAct);
	MLayout->addWidget(ActButtons);

	connect(Model, SIGNAL(dataChanged( const QModelIndex&, const QModelIndex& )), this, SLOT(checkValidSettings())); 
	checkValidSettings();
}

void AppSettingsDialog::setPublisherDb(const STDom::PublisherDatabase& _Db)
{
	MainWidget->setPublisherDb(_Db);
}



void AppSettingsDialog::accept()
{
	MainWidget->submit();
	QDialog::accept();
}

void AppSettingsDialog::reject()
{
	MainWidget->revert();
	QDialog::reject();
}

void AppSettingsDialog::exitApp()
{
	QCoreApplication::exit(0); 
}

void AppSettingsDialog::checkValidSettings()
{
	AcceptAct->setEnabled(Settings->validSettings()); 
}




//__________________________________________________________________________
//
// Class AppSettings
//__________________________________________________________________________

QString AppSettings::DefaultLocale = "es";
int AppSettings::NumCustomPaths = 4;

AppSettings::AppSettings()
{
	if (!keySaved("main/locale"))
	{
		addKey("main/locale", "es", QVariant::String,
			tr("Language"), tr("Program language."));

		addKey("main/orderref", 0, QVariant::Int,
			tr("Next Order Ref"), tr("Next Order Ref"));

		addKey("main/dpis", 300, QVariant::Int,
			tr("Image render dpis"), tr("Image render dpis"));

		addKey("main/lowreswarningdpis", 100, QVariant::Int,
			tr("Low resolution warning dpis"), "");

		addKey("main/renderformat", "JPG", QVariant::String,
			tr("Default render format"), "");

		addKey("main/printpassword", "", QVariant::String,
			tr("Print Password"), "");

		addKey("main/showlegalinfo", true, QVariant::Bool,
			tr("Show legal disclaimer"), "");

		addKey("main/orderfilespath", SApplication::defaultDbFilesPath(), QVariant::String,
			tr("Order files path"), "");

		addKey("main/settingspassword", "", QVariant::String,
			tr("Settings access password"), "");

#ifndef INETKIT_EDITION
		bool RunTom = false;
#else
		bool RunTom = true;
#endif
		addKey("main/runtomonordercommit", RunTom, QVariant::Bool,
			tr("Run TOM on order commit"), "");

		addKey("main/advancedom", false, QVariant::Bool,
			tr("Advanced Order Management"), "");


		// --- Media ---
#ifdef Q_OS_WIN32
		addKey("media/excludedcarddrives", "A,C,D,X,Y,Z", QVariant::String, 
				tr("Excluded drives for media cards (Ex:A,C,D)"), "");

		addKey("media/cddrive", "D", QVariant::String, 
				tr("Drive for CD unit"), "");
#else 
		addKey("media/mediapath", "/media", QVariant::String, 
				tr("Path where system mounts media"), "");
#endif 		
		for (int Vfor = 0; Vfor < NumCustomPaths; Vfor++)
		{
			addKey(QString("media/custompath%1enabled").arg(Vfor), false, QVariant::Bool, "", "");
			addKey(QString("media/custompath%1").arg(Vfor), "", QVariant::String,
					QString("%1.-").arg(Vfor + 1), "");
			addKey(QString("media/custompath%1label").arg(Vfor), "", QVariant::String, "", "");
		}

		QString BlueToothDefPath; 
#ifdef Q_OS_WIN32
		BlueToothDefPath = "c:/bluetooth";
#else 
		BlueToothDefPath = QDir::home().absolutePath() + "/.kbluetooth4";
#endif 		

		addKey("media/bluetoothinputpath", BlueToothDefPath, QVariant::String, 
				tr("Path where bluetooth module puts images"), "");

		addKey("media/runejectateo", false, QVariant::Bool, 
				tr("Run eject at the end of orders ?"), "");

		addKey("publisher/name", "", QVariant::String,
			tr("Name"), tr("Name"));

		addKey("publisher/surname", "", QVariant::String,
			tr("Surname"), tr("Surname"));

		addKey("publisher/email", "", QVariant::String,
			tr("Email"), tr("Email"));

		addKey("publisher/phone", "", QVariant::String,
			tr("Phone"), tr("Phone"));
	
		addKey("publisher/mobilephone", "", QVariant::String,
			tr("Mobile Phone"), tr("Mobile Phone"));

		addKey("publisher/address", "", QVariant::String,
			tr("Address"), tr("Address"));

		addKey("publisher/city", "", QVariant::String,
			tr("City"), tr("City"));

		addKey("publisher/country", "", QVariant::String,
			tr("Country"), tr("Country"));

		addKey("publisher/state", "", QVariant::String,
			tr("State"), tr("State"));

		addKey("publisher/zip", "", QVariant::String,
			tr("Zip"), tr("Zip"));

		addKey("publisher/accountnumber", "", QVariant::String,
			tr("Account number"), tr("Account number"));

		addKey("publisher/transportroute", "", QVariant::String,
			tr("Transport Route"), tr("Transport Route"));

		// Printing
		addKey("printing/sendtopublishermode", false, QVariant::Bool,
			tr("Send to publisher"), tr("Send to publisher"));

		addKey("printing/askforuserdata", true, QVariant::Bool,
			tr("Ask for user phone and name?"), "");

		addKey("printing/imagecropmode", ModeWhiteMarginCentered, QVariant::Int,
			tr("Image crop mode"), "");

		addKey("printing/imageadjustmode", ModeAdjustWeight, QVariant::Int,
			tr("Image adjustment mode"), "");

		// PhotoBOok
		addKey("photobook/renderformat", "JPG", QVariant::String,
			tr("Image format for photobook renders"), tr("Image format to use when render photobook."));

		QString DefaultCoverFormat = "JPG"; 
		addKey("photobook/coverrenderformat", DefaultCoverFormat, QVariant::String,
			tr("Image format for photobook cover renders"), tr("Image format to use when render photobook cover."));


		// PhotoIndex 
		addKey("photoindex/dpis", 150, QVariant::Int,
			tr("Dpis"), tr("Dpis"));
		
		addKey("photoindex/drawframes", true, QVariant::Bool,
			tr("Draw frames ?"), tr("Draw frames ?"));

		addKey("photoindex/drawfilenumbers", true, QVariant::Bool,
			tr("Draw file numbers ?"), tr("Draw file numbers ?"));

		addKey("photoindex/drawfilenames", true, QVariant::Bool,
			tr("Draw file names ?"), tr("Draw file names ?"));

		addKey("photoindex/imagesperpage", 40, QVariant::Int,
			tr("Estimated num of images per page:"), tr("Estimated num of images per page:"));

		addKey("photoindex/backgroundcolor", "#FFFFFF", QVariant::String,
			tr("Background Color:"), tr("Background Color:"));

		addKey("photoindex/margin", 8, QVariant::Int,
			tr("Margin:"), tr("Margin:"));

		// ---- Ticket Printer -----
		addKey("ticket/printticket", false, QVariant::Bool, tr("Print ticket at the end of command ?"), 
			tr("Print ticket at the end of command ?"));
		
		addKey("ticket/printticketcopy", false, QVariant::Bool, tr("Print a copy of ticket at the end of command ?"), 
			tr("Print a copy of ticket at the end of command ?"));

		addKey("ticket/header", "", QVariant::String, tr("Ticket header"), ""); 
		addKey("ticket/footer", "", QVariant::String, tr("Ticket footer"), ""); 
		addKey("ticket/companyname", tr("My Name"), QVariant::String, tr("Company name"), ""); 
		addKey("ticket/companyaddress", tr("My Address"), QVariant::String, tr("Company address"), ""); 

		// ---- User Account ----
		addKey("account/username", "", QVariant::String, tr("User Name"), "");
		addKey("account/password", "", QVariant::String, tr("Password"), "");
		addKey("account/key", "", QVariant::String, tr("Key"), "");

		// --- Database  ---
#ifndef INETKIT_EDITION
		bool SyncData = false;
		bool KeepPrices = false;
#else
		bool SyncData = true;
		bool KeepPrices = true;
#endif
		addKey("database/syncpublisherdata", SyncData, QVariant::Bool,
			tr("Sync publisher data"), "");

		addKey("database/keeplocalprices", KeepPrices, QVariant::Bool,
			tr("Keep local prices"), "");

		addKey("database/syncinterval", 60, QVariant::Int,
			tr("Sync interval in minutes"), "");

		// --- Templates ---
		addKey("templates/manualinstall", false, QVariant::Bool,
			   tr("Manually install templates"), "");


		model()->fetch();
	}
}

int AppSettings::mainNewOrderRef() 
{
	int Res = value("main/orderref").toInt();
	setValue("main/orderref", Res + 1); 
	return Res; 
}


QString AppSettings::mainLocale() 
{
	QSettings Settings;
	return Settings.value("main/locale", DefaultLocale).toString();
}

int AppSettings::mainDpis() const
{
	return value("main/dpis").toInt(); 
}

int AppSettings::mainLowResWarningDpis() const
{
	return value("main/lowreswarningdpis").toInt();
}

QString AppSettings::mainRenderFormat() const 
{
	return value("main/renderformat").toString(); 
}

QString AppSettings::mainPrintPassword() const
{
	return value("main/printpassword").toString();
}

QString AppSettings::mainSettingsPassword() const
{
	return value("main/settingspassword").toString();
}

bool AppSettings::mainShowLegalInfo() const
{
	return value("main/showlegalinfo").toBool();
}

QString AppSettings::mainOrdersFilesPath() const
{
	return value("main/orderfilespath").toString();
}


bool AppSettings::mainRunTomOnOrderCommit() const
{
	return value("main/runtomonordercommit").toBool();
}

bool AppSettings::mainAdvancedOM() const
{
	return value("main/advancedom").toBool();
}


#ifdef Q_OS_WIN32
QFileInfoList AppSettings::mediaExcludedCardDrives() const
{
	QFileInfoList Res;
	QStringList CardDriveList = value("media/excludedcarddrives").toString().split(",");
	QStringList::iterator it; 
	for (it = CardDriveList.begin(); it != CardDriveList.end(); ++it)
	{
		Res.push_back(QFileInfo((*it).toUpper() + ":/"));
	}
	return Res; 
}

QFileInfo AppSettings::mediaCdDrive() const
{
	return QFileInfo(value("media/cddrive").toString() + ":/");
}
#else 

QFileInfo AppSettings::mediaPath() const
{
	return QFileInfo(value("media/mediapath").toString());
}
#endif 


QFileInfo AppSettings::mediaBluetoothInputPath() const
{
	return QFileInfo(value("media/bluetoothinputpath").toString()); 
}

bool AppSettings::mediaRunEjectEO() const
{
	return value("media/runejectateo").toBool(); 
}

bool AppSettings::mediaCustomPathEnabled(int _Index) const
{
	return value(QString("media/custompath%1enabled").arg(_Index)).toBool();
}

QString AppSettings::mediaCustomPathLabel(int _Index) const
{
	return value(QString("media/custompath%1label").arg(_Index)).toString();
}

QString AppSettings::mediaCustomPath(int _Index) const
{
	return value(QString("media/custompath%1").arg(_Index)).toString();
}

QString AppSettings::photoBookRenderFormat() const
{
	return value("photobook/renderformat").toString();
}

QString AppSettings::photoBookCoverRenderFormat() const
{
	return value("photobook/coverrenderformat").toString();
}

void AppSettings::setAccountUserName(const QString& _UserName)
{
	setValue("account/username", _UserName);
}

QString AppSettings::accountUserName() const
{
	return value("account/username").toString();
}

void AppSettings::setAccountPassword(const QString& _Value)
{
	setValue("account/password", _Value);
}

QString AppSettings::accountPassword() const
{
	return value("account/password").toString();
}

void AppSettings::setAccountKey(const QString& _Value)
{
	setValue("account/key", _Value);
}

QString AppSettings::accountKey() const
{
	return value("account/key").toString();
}

AppSettings::EnImageCropMode AppSettings::imageCropMode() const
{
	return static_cast<AppSettings::EnImageCropMode>(value("printing/imagecropmode").toInt());
}

AppSettings::EnImageAdjustmentMode AppSettings::imageAdjustmentMode() const
{
	return static_cast<AppSettings::EnImageAdjustmentMode>(value("printing/imageadjustmode").toInt());
}

STDom::XmlOrderDealer AppSettings::sender() const
{
	STDom::XmlOrderDealer Res;
	Res.setName(value("publisher/name").toString() + " " + value("publisher/surname").toString()); 
	Res.setAddress(value("publisher/address").toString()); 
	Res.setEmail(value("publisher/email").toString()); 
	Res.setPhone(value("publisher/phone").toString()); 
	Res.setPostalCode(value("publisher/zip").toString()); 
	Res.setCity(value("publisher/city").toString()); 
	Res.setCountry(value("publisher/country").toString()); 
	Res.setState(value("publisher/state").toString()); 
	Res.setId(value("publisher/accountnumber").toString()); 
	Res.setTransportRoute(value("publisher/transportroute").toString()); 
	return Res; 
}

int AppSettings::photoIndexDpis() const
{
	return value("photoindex/dpis").toInt(); 
}

bool AppSettings::photoIndexDrawFrames() const
{
	return value("photoindex/drawframes").toBool(); 
}

bool AppSettings::photoIndexDrawFileNumbers() const
{
	return value("photoindex/drawfilenumbers").toBool(); 
}

bool AppSettings::photoIndexDrawFileNames() const
{
	return value("photoindex/drawfilenames").toBool(); 
}

int AppSettings::photoIndexImagesPerPage() const
{
	return value("photoindex/imagesperpage").toInt(); 
}

QString AppSettings::photoIndexBackgroundColor() const
{
	return value("photoindex/backgroundcolor").toString(); 
}

int AppSettings::photoIndexMargin() const
{
	return value("photoindex/margin").toInt();
}

bool AppSettings::ticketPrintTicket() const
{
	return value("ticket/printticket").toBool();
}

bool AppSettings::ticketPrintTicketCopy() const
{
	return value("ticket/printticketcopy").toBool();
}

QString AppSettings::ticketHeader() const
{
	return value("ticket/header").toString();
}

QString AppSettings::ticketFooter() const
{
	return value("ticket/footer").toString();
}

QString AppSettings::ticketCompanyName() const
{
	return value("ticket/companyname").toString();
}

QString AppSettings::ticketCompanyAddress() const
{
	return value("ticket/companyaddress").toString();
}

bool AppSettings::askForUserData() const 
{
	return value("printing/askforuserdata").toBool(); 
}

bool AppSettings::databaseSyncPublisherData() const
{
	return value("database/syncpublisherdata").toBool();
}

bool AppSettings::databaseKeepLocalPrices() const
{
	return value("database/keeplocalprices").toBool();
}

int AppSettings::databaseSyncInterval() const
{
	return value("database/syncinterval").toInt();
}

bool AppSettings::templatesManuallyInstall() const
{
	return value("templates/manualinstall").toBool();
}

bool AppSettings::isTimeToSync() const
{
	QDateTime LastSyncTime = QSettings::value("lastsynctime", QDateTime(QDate(1999,1,1))).toDateTime();
	return LastSyncTime.secsTo(QDateTime::currentDateTime()) > (databaseSyncInterval() * 60);
}

void AppSettings::updateSyncTime()
{
	QSettings::setValue("lastsynctime", QDateTime::currentDateTime());
}


bool AppSettings::validPublisherData() const
{
	return !model()->data(model()->index("publisher/name")).toString().isEmpty() &&
			!model()->data(model()->index("publisher/accountnumber")).toString().isEmpty();
}

bool AppSettings::validSettings() const 
{
	bool Res = true; 
	if (model()->data(model()->index("printing/sendtopublishermode")).toBool())
	//if (sendToPublisherMode())
		Res = validPublisherData(); 
	return Res; 
}

bool AppSettings::sendToPublisherMode() const
{
#ifdef STANDARD_EDITION
		return false;
#else
	#ifdef INETKIT_EDITION
			return true;
	#else
		return value("printing/sendtopublishermode").toBool();
	#endif
#endif
}

int AppSettings::execDialog()
{
	//Model with no filters. With local printers there's only one publisher.
	STDom::PublisherDatabase PubDatabase = pManager->getProducts();

	QAbstractItemModel* ProductsModel = 0;
	QAbstractItemModel* FormatsModel = 0;
	if (PubDatabase.open())
	{
		 ProductsModel = PubDatabase.newProductsModel(this);
		 FormatsModel = PubDatabase.newFormatsModel(this);
	}
	else
		SMessageBox::warning(0, tr("Error openning publisher database"), tr("Could not open publisher database"));

	qApp->setOverrideCursor( QCursor(Qt::WaitCursor));
	AppSettingsDialog Dialog(this, ProductsModel, FormatsModel);
	qApp->restoreOverrideCursor();
	Dialog.setObjectName("AppSettingsDialog"); 
	Dialog.setPublisherDb(PubDatabase);
	int Res = Dialog.exec();

	if (ProductsModel)
		delete ProductsModel;

	return Res;

}

bool AppSettings::settingsAccess() const
{
	bool Res = true;
	if (mainSettingsPassword() != "")
	{
		Res = false;
		TPInputDialog InDialog(tr("<h1>Enter access password</h1>"));
		InDialog.setEchoMode(QLineEdit::Password);
		InDialog.setCancelEnabled(true);
		bool Accepted = InDialog.exec() == QDialog::Accepted;
		while (Accepted && !Res)
		{
			if ( InDialog.strResult() == mainSettingsPassword())
			{
				Res = true;
			}
			else
			{
				InDialog.setTitle(tr("<font color=red><h1> Wrong password, please try again </h1></font>"));
				Accepted = InDialog.exec();
			}
		}
	}
	return Res;
}

bool AppSettings::commitAccess() const
{
	bool Res = true;
	if (mainPrintPassword() != "")
	{
		Res = false;
		TPInputDialog InDialog(tr("<h1>Enter print password</h1>"));
		InDialog.setEchoMode(QLineEdit::Password);
		InDialog.setCancelEnabled(true);
		bool Accepted = InDialog.exec() == QDialog::Accepted;
		while (Accepted && !Res)
		{
			if ( InDialog.strResult() == mainPrintPassword())
			{
				Res = true;
			}
			else
			{
				InDialog.setTitle(tr("<font color=red><h1> Wrong password, please try again </h1></font>"));
				Accepted = InDialog.exec();
			}
		}
	}
	return Res;
}


