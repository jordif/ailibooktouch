/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "templateloaderwizard.h"
#include <QDir>

//Pages
#include "getmediapage.h"
#include "storedtemplateselpage.h"

TemplateLoaderWizard::TemplateLoaderWizard(QWidget* _Parent) : STIWizard(_Parent)
{
	GetMediaPage* MediaPage = new GetMediaPage(this);
	MediaPage->setCDSourceEnabled(false);
	MediaPage->setBluetoothSourceEnabled(false);
	connect(MediaPage, SIGNAL(mediaSelected(const QDir&)), this, SLOT(slotMediaSelected(const QDir&)));
	connect(MediaPage, SIGNAL(previousPage()), this, SIGNAL(finished()));
	addPage(MediaPage);

	TSelPage = new StoredTemplateSelPage(this);
	connect(TSelPage, SIGNAL(nextPage()), this, SLOT(slotPhotoBookSelected()));
	addPage(TSelPage);
}

void TemplateLoaderWizard::init()
{
	firstPage();
}

void TemplateLoaderWizard::slotMediaSelected(const QDir& _Dir)
{
	MediaRootDir = _Dir;
	TSelPage->setMediaRootDir(_Dir);
	nextPage();
}

void TemplateLoaderWizard::slotPhotoBookSelected()
{
	emit photoBookSelected(TSelPage->currentPhotoBookPath(), MediaRootDir);
}
