/****************************************************************************
**
** Copyright (C) 2006-2010 Starblitz. All rights reserved.
**
** This file is part of Starblitz Foto Suite.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file COPYING included in the packaging of
** this file.  
**
** Starblitz reserves all rights not expressly granted herein.
** 
** Strablitz (c) 2010
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef STOREDTEMPLATESELPAGE_H
#define STOREDTEMPLATESELPAGE_H
#include "stiwizardpage.h"
#include <QWidget>

class STPhotoBookCollectionModel;
class QToolButton;
class QDir;
class STThumbnailView;
class QLabel;
class StoredTemplateSelPage : public STIWizardPage
{
	Q_OBJECT

	STPhotoBookCollectionModel* Model;
	QToolButton* newActionButton(const QString& _Icon);
	STThumbnailView* LVImages;
	QToolButton* BackBut;
	QToolButton* OkBut;
	QLabel* LabHeader;
	void retranslateUi();

public:
	StoredTemplateSelPage(QWidget *parent = 0);
	void init();
	void setMediaRootDir(const QDir& _Dir);
	QString currentPhotoBookPath() const;
};

#endif // STOREDTEMPLATESELPAGE_H
